package router

import (
	"arjuna-api/controller"
	"arjuna-api/docs"
	"arjuna-api/helper"
	"arjuna-api/middleware"
	"fmt"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/recover"
	fiberSwagger "github.com/swaggo/fiber-swagger"
)

func Router() *fiber.App {
	app := fiber.New()

	app.Use(helper.HandleLogger(), recover.New(), helper.HandleCors())

	app.Get("/", func(c *fiber.Ctx) error {
		msg := fmt.Sprintf("API ARJUNA")
		return c.SendString(msg)
	})

	// app.Get("/Update/imageProfile/old", func(c *fiber.Ctx) error {
	// 	var errorID []int64
	// 	db := config.Con()
	// 	defer config.CloseCon(db)
	// 	var personal []entity.Personal
	// 	db.Table("personal").Select("id_personal, nama").Find(&personal)
	// 	for _, v := range personal {
	// 		var file string = "./files/photos/" + strconv.Itoa(int(v.IDPersonal)) + ".jpg"
	// 		fileName := strconv.Itoa(int(v.IDPersonal)) + ".jpg"
	// 		exist := fileExists(file)
	// 		if exist {
	// 			update := db.Table("personal").Where("id_personal = ?", v.IDPersonal).Update("image", fileName)
	// 			if update.Error != nil {
	// 				errorID = append(errorID, v.IDPersonal)
	// 			}
	// 		}
	// 	}
	// Logo Jurnal
	// 	var iden_jurnal []entity.IdentitasJurnal
	// 	db.Table("identitas_jurnal").Select("id_identitas_jurnal").Find(&iden_jurnal)
	// 	for _, v := range iden_jurnal {
	// 		var file string = "./files/logo/" + strconv.Itoa(int(v.IdIdentitasJurnal)) + ".jpg"
	// 		fileName := strconv.Itoa(int(v.IdIdentitasJurnal)) + ".jpg"
	// 		exist := fileExists(file)
	// 		if exist {
	// 			update := db.Table("identitas_jurnal").Where("id_identitas_jurnal = ?", v.IdIdentitasJurnal).Update("image", fileName)
	// 			if update.Error != nil {
	// 				errorID = append(errorID, v.IdIdentitasJurnal)
	// 			}
	// 		}
	// 	}
	// 	return c.Status(fiber.StatusOK).JSON(errorID)
	// })

	// Static Folder files
	app.Static("/file/", "./files")

	RouteAPI := app.Group(docs.SwaggerInfo.BasePath)

	// Auth Routing
	auth := RouteAPI.Group("/auth")
	auth.Post("/login", controller.Login)
	auth.Post("/register", controller.Register)
	auth.Post("/forgot-password", controller.ForgotPassword)

	// Front Page Routing
	frontpage := RouteAPI.Group("/frontpage")
	frontpage.Get("/getNews", controller.FrontPageGetNews)
	frontpage.Get("/getDetailNews/:id", controller.FrontPageGetDetailNews)
	frontpage.Get("/getPopularNews", controller.FrontPageGetPopularNews)
	frontpage.Get("/getDownloadList", controller.FrontPageGetDownloadList)
	frontpage.Get("/getSliderList", controller.FrontPageGetSlider)

	frontpage.Get("/getJurnalList", controller.FrontPageGetListJurnal)
	frontpage.Get("/getJurnalDetail/:id", controller.FrontPageGetDetailJurnal)   //
	frontpage.Get("/getJurnalProgres/:id", controller.FrontPageGetJurnalProgres) //
	// frontpage.Get("/getJurnalAccreditationHistory/:id", controller.FrontPageGetJurnalAccreditationHistory)
	frontpage.Get("/getHistoryJurnalName/:id", controller.FrontPageGetHistoryJurnalName) //

	frontpage.Get("/getStatistikJurnal/barchart", controller.FrontPageGetStatistikJurnalBarChart)
	frontpage.Get("/getStatistikJurnal/polarchart", controller.FrontPageGetStatistikJurnalPolarChart)
	frontpage.Get("/getStatistikJurnal/count", controller.FrontPageGetStatistikJurnalCount)
	frontpage.Get("/getRekapUsulanJurnal", controller.FrontPageGetRekapUsulanJurnal)

	// Swagger
	app.Get("/swagger/*", fiberSwagger.WrapHandler)

	user := RouteAPI.Group("/user", middleware.AuthorizeJWT())
	user.Get("/", controller.GetAllUser)
	user.Get("/kewenangan", controller.GetAllKewenangan)
	user.Get("/assessor", controller.GetAllUserAssessor)
	user.Post("/", controller.SaveUser)
	user.Get("/:id", controller.GetUser)
	user.Put("/:id", controller.UpdateUser)
	user.Post("kewenangan/:id", controller.UpdateKewenanganUser)

	menu := RouteAPI.Group("/menu")
	menu.Get("/", controller.GetAllMenu)

	kontak := RouteAPI.Group("/kontak")
	kontak.Post("/send", controller.SendKontak)

	profile := RouteAPI.Group("/profile", middleware.AuthorizeJWT())
	profile.Get("/", controller.Profile)
	profile.Post("/", controller.UpdateProfile)
	profile.Post("/change-password", controller.ChangePassword)
	profile.Post("/foto", controller.UpdateFotoProfile)

	bidangilmu := RouteAPI.Group("/bidang-ilmu", middleware.AuthorizeJWT())
	bidangilmu.Get("/all", controller.GetAllListBidangIlmu)
	bidangilmu.Get("/", controller.GetAllRumpunIlmu)
	bidangilmu.Get("/:id", controller.GetAllBidangIlmu)
	bidangilmu.Get("/sub/:id", controller.GetAllSubBidangIlmu)

	utility := RouteAPI.Group("/utility", middleware.AuthorizeJWT())
	utility.Get("/bidang-fokus", controller.GetAllBidangFokus)
	utility.Get("/perguruan-tinggi", controller.GetAllPerguruanTinggi)
	utility.Post("/perguruan-tinggi", controller.CreatePerguruanTinggi)

	berita := RouteAPI.Group("/berita", middleware.AuthorizeJWT())
	berita.Get("/", controller.GetAllBerita)
	berita.Post("/", controller.SaveBerita)
	berita.Get("/:id", controller.GetBerita)
	berita.Put("/:id", controller.UpdateBerita)
	berita.Put("/status/:id", controller.UpdateStatusBerita)

	pengumuman := RouteAPI.Group("/pengumuman", middleware.AuthorizeJWT())
	pengumuman.Get("/", controller.GetAllPengumuman)
	pengumuman.Post("/", controller.SavePengumuman)
	pengumuman.Get("/:id", controller.GetPengumuman)
	pengumuman.Put("/:id", controller.UpdatePengumuman)
	pengumuman.Put("/status/:id", controller.UpdateStatusPengumuman)

	slider := RouteAPI.Group("/slider", middleware.AuthorizeJWT())
	slider.Get("/", controller.GetAllSlider)
	slider.Post("/", controller.SaveSlider)
	slider.Get("/:id", controller.GetSlider)
	slider.Put("/:id", controller.UpdateSlider)

	download := RouteAPI.Group("/download", middleware.AuthorizeJWT())
	download.Get("/", controller.GetAllDownload)
	download.Post("/", controller.SaveDownload)
	download.Get("/:id", controller.GetDownload)
	download.Put("/:id", controller.UpdateDownload)
	download.Put("/status/:id", controller.UpdateStatusDownload)

	config := RouteAPI.Group("/config", middleware.AuthorizeJWT())
	config.Get("/", controller.GetAllConfig)
	config.Post("/", controller.SaveConfig)
	config.Get("/:id", controller.GetConfig)
	config.Put("/:id", controller.UpdateConfig)
	config.Put("/active-status/:id", controller.UpdateStatusConfig)

	jurnal := RouteAPI.Group("/jurnal", middleware.AuthorizeJWT())
	jurnal.Get("/list-hasil-akreditasi-by-pic", controller.ListHasilAkreditasiByPic)
	jurnal.Get("/daftar-terbitan", controller.GetAllDaftarTerbitanJurnal)
	jurnal.Get("/daftar-usulan", controller.GetAllDaftarUsulanJurnal)
	jurnal.Get("/list-tersk", controller.ListJurnalTerSK)
	jurnal.Post("/bidang-ilmu-jurnal", controller.SaveBidangIlmuJurnal)
	jurnal.Delete("/bidang-ilmu-jurnal/:id", controller.DeleteBidangIlmuJurnal)
	jurnal.Post("/update-sk", controller.UpdateSKJurnal)
	jurnal.Post("/get-identitas-pic", controller.GetIdentitasPIC)
	jurnal.Post("/update-pic", controller.UpdatePICJurnal)
	jurnal.Post("/logo/:id", controller.UpdateLogoJurnal)
	jurnal.Post("/", controller.SaveJurnal)
	jurnal.Get("/list-by-sk/:id", controller.GetAllJurnalBySK)
	jurnal.Get("/:id", controller.GetJurnal)
	jurnal.Post("/:id", controller.UpdateJurnal)
	jurnal.Delete("/:id", controller.DeleteJurnal)
	jurnal.Get("/print-sk-jurnal/:id", controller.GetPrintSKPengelolaJurnal)

	usulan_akreditasi := RouteAPI.Group("/usulan-akreditasi", middleware.AuthorizeJWT())
	usulan_akreditasi.Get("/list-draft", controller.GetAllDraftUsulanAkreditasiJurnal)
	usulan_akreditasi.Get("/list-kemajuan/:id", controller.GetKemajuanUsulanAkreditasiJurnal)
	usulan_akreditasi.Get("/hasil-penilaian/:id", controller.GetHasilPenilaianAkreditasiJurnal)
	usulan_akreditasi.Get("/borang-akreditasi/:id", controller.GetBorangUsulanAkreditasiJurnal)
	usulan_akreditasi.Post("/borang-akreditasi", controller.SaveBorangUsulanAkreditasiJurnal)
	usulan_akreditasi.Post("/siap-akreditasi/:id", controller.SiapAkreditasiJurnal)
	usulan_akreditasi.Get("/:id", controller.GetUsulanAkreditasiJurnal)
	usulan_akreditasi.Post("/:id", controller.UpdateUsulanAkreditasiJurnal)
	usulan_akreditasi.Delete("/:id", controller.DeleteUsulanAkreditasiJurnal)

	dashboard := RouteAPI.Group("/dashboard", middleware.AuthorizeJWT())
	dashboard.Get("/pengusul/count", controller.DashboardPengusulCount)
	dashboard.Get("/pengusul/chart", controller.DashboardPengusulChart)
	dashboard.Get("/penilai/count", controller.DashboardPenilaiCount)
	dashboard.Get("/penilai/line-chart", controller.DashboardPenilaiLineChart)
	dashboard.Get("/penilai/bar-chart", controller.DashboardPenilaiBarChart)
	dashboard.Get("/distributor/count", controller.DashboardDistributorCount)
	dashboard.Get("/distributor/bar-chart/sk-terakhir", controller.DashboardDashboardBarChartSkTerakhir)
	dashboard.Get("/distributor/donut-chart/sebaran-jurnal", controller.DashboardDashboardDonutChartSebaranJurnal)
	dashboard.Get("/distributor/bar-chart/bidang-ilmu", controller.DashboardDashboardBarChartBidangIlmu)
	dashboard.Get("/distributor/rekap-akreditasi", controller.DashboardDistributorRekapAkreditasi)
	dashboard.Get("/distributor/detail-count", controller.DashboardDistributorDetailCount)
	dashboard.Get("/distributor/peta", controller.DashboardDistributorRekapJurnalPeta)
	dashboard.Get("/distributor/detail-sk", controller.DashboardDistributorDetailSKAkreditasi)
	dashboard.Get("/distributor/detail-usulan-akreditasi", controller.DashboardDistributorTabelRekapRekapituasiJurnal)
	dashboard.Get("/distributor/detail-bidang-ilmu", controller.DashboardDistributorDetailChartBidangIlmuInstitusi)
	dashboard.Get("/penilai/detail", controller.DashboardPenilaiDetail)
	dashboard.Get("/pengusul/detail", controller.DashboardPengusulDetail)

	lookup := RouteAPI.Group("/lookup", middleware.AuthorizeJWT())
	lookup.Get("/list-kota", controller.GetAllKota)
	lookup.Get("/list-country", controller.GetAllCountry)

	penilaian := RouteAPI.Group("/penilaian", middleware.AuthorizeJWT())
	penilaian.Get("/unsur-penilaian", controller.GetUnsusrPenilaian)
	penilaian.Get("/sub-unsur-penilaian/:id", controller.GetSubUnsusrPenilaian)
	penilaian.Get("/hasil-penilaian-sa/:id", controller.GetHasilPenilaianSa)
	penilaian.Post("/", controller.CreateUpdatePenilaianSa)
	penilaian.Get("/unsur-penilaian/penugasan-penilaian", controller.GetUnsusrPenilaianPenugasanPenilaian)
	penilaian.Get("/sub-unsur-penilaian/penugasan-penilaian/disinsentif", controller.GetSubUnsusrPenilaianPenugasanPenilaianDisinsentif)
	penilaian.Get("/sub-unsur-penilaian/penugasan-penilaian/:id", controller.GetSubUnsusrPenilaianPenugasanPenilaian)
	penilaian.Post("/penugasan-penilaian", controller.CreateUpdatePenilaianPenugasan)
	penilaian.Post("/penugasan-penilaian/disinsentif", controller.CreateUpdatePenilaianPenugasanDisinsentif)

	issue := RouteAPI.Group("/issue", middleware.AuthorizeJWT())
	issue.Get("/usulan-akreditasi", controller.GetAllIssueUsulanAkreditasi)
	issue.Get("/diajukan/usulan-akreditasi", controller.GetAllIssueDiajukanUsulanAkreditasi)
	issue.Post("/usulan-akreditasi", controller.SaveIssueUsulanAkreditasi)
	issue.Get("/usulan-akreditasi/:id", controller.GetIssueUsulanAkreditasi)
	issue.Post("/usulan-akreditasi/:id", controller.UpdateIssueUsulanAkreditasi)
	issue.Delete("/usulan-akreditasi/:id", controller.DeleteIssueUsulanAkreditasi)

	excel := RouteAPI.Group("/excel", middleware.AuthorizeJWT())
	excel.Get("/daftar-terbitan", controller.ExportDaftarTerbitanJurnal)
	excel.Get("/jurnal/daftar-usulan-terdaftar", controller.ExportDaftarUsulanJurnalTerdaftar)
	excel.Get("/kelola-usulan/akreditasi-baru", controller.ExportKelolaUsulanAkreditasiBaru)
	excel.Get("/kelola-usulan/evaluasi-dokumen", controller.ExportKelolaUsulanEvaluasiDokumen)
	excel.Post("/penilai/kinerja-penilai", controller.ExportKinerjaPenilai)
	excel.Get("/kelola-usulan/hasil-akreditasi", controller.ExportKelolaUsulanHasilAkreditasi)
	excel.Get("/sk-akreditasi/list-sk-akreditasi", controller.ExportSKAkreditasi)
	excel.Get("/jurnal/list-tersk", controller.ExportListJurnalTerSK)
	excel.Get("/dashboard/detail-chart-penilai", controller.ExportChartDetailPenilai)
	excel.Get("/dashboard/detail-chart-pengusul", controller.ExportChartDetailPengusul)

	penilai := RouteAPI.Group("/penilai", middleware.AuthorizeJWT())
	penilai.Post("/kinerja-penilai", controller.GetAllKinerjaPenilai)
	penilai.Post("/detail-jurnal/kinerja-penilai", controller.GetDetailListsJurnalKinerjaPenilai)
	penilai.Get("/detail/kinerja-penilai", controller.GetDetailKinerjaPenilai)
	penilai.Get("/detail/bar-chart", controller.GetDetailKinerjaPenilaiBarChart)
	penilai.Get("/detail/line-chart", controller.GetDetailKinerjaPenilaiLineChart)
	penilai.Get("/get-periode", controller.GetPeriodeKinerjaPenilai)
	penilai.Get("/penugasan-evaluator", controller.GetPenugasanPenilaiEvaluator)
	penilai.Get("/penugasan-issue", controller.GetPenugasanPenilaiIssue)
	penilai.Get("/penugasan-management", controller.GetPenugasanPenilaiManagement)
	penilai.Post("/penugasan-penilaian", controller.SavePenugasanPenilaian)
	penilai.Post("/delete-penugasan-penilaian", controller.DeletePenugasanPenilaian)

	kelola_usulan := RouteAPI.Group("/kelola-usulan", middleware.AuthorizeJWT())
	kelola_usulan.Get("/akreditasi-baru/list-usulan-item-penolakan", controller.ListUsulanItemPenolakan)
	kelola_usulan.Get("/akreditasi-baru", controller.GetAllUsulanAkreditasiBaru)
	kelola_usulan.Get("/akreditasi-baru/:id", controller.GetDetailUsulanAkreditasiBaru)
	kelola_usulan.Post("/akreditasi-baru", controller.UsulanAction)
	kelola_usulan.Post("/akreditasi-baru/cancel", controller.UsulanCancelAction)
	kelola_usulan.Get("/evaluasi-dokumen", controller.GetAllEvaluasiDokumen)
	kelola_usulan.Get("/distribusi-penilaian", controller.GetAllDistribusiPenilaian)
	kelola_usulan.Get("/hasil-akhir-penilaian", controller.GetAllHasiAkhirPenilaian)
	kelola_usulan.Get("/hasil-akreditasi", controller.GetAllHasiAkreditasi)
	kelola_usulan.Get("/hasil-akreditasi/:id", controller.GetDetailHasilAkreditasi)
	kelola_usulan.Put("/hasil-akreditasi/:id/permanent", controller.HasilAkreditasiPermanent)
	kelola_usulan.Put("/hasil-akreditasi/permanent-all-assesment", controller.HasilAkreditasiPermanentAllAssesment)
	kelola_usulan.Put("/hasil-akreditasi/:id/cancel-permanent", controller.HasilAkreditasiCancelPermanent)
	kelola_usulan.Get("/progres-penilaian", controller.GetAllProgresPenilaian)
	kelola_usulan.Get("/progres-penilaian/:id", controller.GetDetailProgresPenilaian)
	kelola_usulan.Post("/progres-penilaian/set-permanent", controller.ProgresPenilaianSetPermanent)
	kelola_usulan.Post("/progres-penilaian/cabut-permanent", controller.ProgresPenilaianCabutPermanent)
	kelola_usulan.Post("/progres-penilaian/penyesuaian-nilai", controller.ProgresPenilaianPenyesuaianNilai)
	kelola_usulan.Get("/penilaian-manajemen/:id", controller.DetailPenilaianManajemen)
	kelola_usulan.Get("/penilaian-substansi/:id", controller.DetailPenilaianSubstansi)
	kelola_usulan.Post("/progres-penilaian/penyesuaian-nilai/asessor", controller.ProgresPenilaianPenyesuaianNilaiAsessor)
	kelola_usulan.Get("/komentar-hasil-penilaian", controller.GetKomentarHasilPenilaianAkreditasi)

	sk_akreditasi := RouteAPI.Group("/sk-akreditasi", middleware.AuthorizeJWT())
	sk_akreditasi.Post("/set-sk-akreditasi", controller.SetSKAkreditasi)
	sk_akreditasi.Post("/unset-sk-akreditasi", controller.UnsetSKAkreditasi)
	sk_akreditasi.Get("/", controller.GetAllSKAkreditasi)
	sk_akreditasi.Get("/all", controller.GetAllListSKAkreditasi)
	sk_akreditasi.Post("/", controller.CreateSKAkreditasi)
	sk_akreditasi.Post("/publish/:id", controller.PublishSKAkreditasi)
	sk_akreditasi.Post("/unpublish/:id", controller.UnPublishSKAkreditasi)
	sk_akreditasi.Post("/:id", controller.UpdateSKAkreditasi)
	sk_akreditasi.Delete("/:id", controller.DeleteSKAkreditasi)
	sk_akreditasi.Get("/list-terakdetasi-belum-sk", controller.GetAllTerakdetasiBelumSK)

	penandatangan := RouteAPI.Group("/penandatangan", middleware.AuthorizeJWT())
	penandatangan.Get("/", controller.GetAllPenandatangan)
	penandatangan.Post("/", controller.SavePenandatangan)
	penandatangan.Get("/:id", controller.GetPenandatangan)
	penandatangan.Put("/:id", controller.UpdatePenandatangan)
	penandatangan.Put("/status/:id", controller.UpdateStatusPenandatangan)

	periode_akreditasi := RouteAPI.Group("/periode-akreditasi", middleware.AuthorizeJWT())
	periode_akreditasi.Get("/", controller.GetAllPeriodeAkreditasi)
	periode_akreditasi.Get("/active", controller.GetPeriodeAkreditasiActive)
	periode_akreditasi.Post("/", controller.SavePeriodeAkreditasi)
	periode_akreditasi.Get("/:id", controller.GetPeriodeAkreditasi)
	periode_akreditasi.Put("/:id", controller.UpdatePeriodeAkreditasi)
	periode_akreditasi.Put("/status/:id", controller.UpdateStatusPeriodeAkreditasi)

	kelola_usulan_penilaian := RouteAPI.Group("/kelola-usulan-penilaian", middleware.AuthorizeJWT())
	kelola_usulan_penilaian.Get("/penugasan-penilaian", controller.GetAllPenugasanPenilaian)
	kelola_usulan_penilaian.Put("/penugasan-penilaian/status/:id", controller.UpdateStatusPenugasanPenilaian)
	kelola_usulan_penilaian.Get("/proses-penilaian", controller.GetAllProsesPenilaiani)
	kelola_usulan_penilaian.Get("/penilaian-selesai", controller.GetAllPenilaianSelesai)
	kelola_usulan_penilaian.Get("/detail-penilaian/:id", controller.GetDetailPeninilaian)

	// 29982
	return app
}

func fileExists(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

// arjuna_list_penilaian_blm_selesai_with_filter
