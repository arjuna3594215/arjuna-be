package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Lists Penandatangan  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Penandatangan
// @Description  		Lists Penandatangan API calls
// @Tags         		Penandatangan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama"
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/penandatangan [get]
func GetAllPenandatangan(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	search := c.Query("search")
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListPenandatangan(search, page, row, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Save Penandatangan  			godoc
// @Security 				bearerAuth
// @Summary      		Save Penandatangan
// @Description  		Save Penandatangan API calls
// @Tags         		Penandatangan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		nama formData string true "Nama"
// @Param        		nip formData string true "NIP"
// @Param        		file formData file false "File"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/penandatangan [post]
func SavePenandatangan(c *fiber.Ctx) error {
	// Body To Struct
	penandatanganDto := new(entity.PenandatanganDTO)
	if err := c.BodyParser(penandatanganDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(penandatanganDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	file, _ := c.FormFile("file")
	penandatanganDto.File = file

	// Validasi type File
	if file != nil {
		if file.Header["Content-Type"][0] != "image/png" {
			res := helper.BuildSingleValidationResponse("File", "png", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}
	}

	// Insert Penandatangan
	data, err := service.InsertPenandatangan(*penandatanganDto, c)
	if err != nil {
		res := helper.BuildErrorResponse("Create Penandatangan Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Create Penandatangan Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Detail Penandatangan  	godoc
// @Security 				bearerAuth
// @Summary      		Detail Penandatangan
// @Description  		Detail Penandatangan API calls
// @Tags         		Penandatangan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path string true "ID Penandatangan"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/penandatangan/{id} [get]
func GetPenandatangan(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id := c.Params("id")
	data, err := service.ShowPenandatangan(id, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	if v, ok := data.(entity.Penandatangan); ok {
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update Penandatangan  			godoc
// @Security 				bearerAuth
// @Summary      		Update Penandatangan
// @Description  		Update Penandatangan API calls
// @Tags         		Penandatangan
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param  					id path string true "ID Penandatangan"
// @Param        		nama formData string true "Nama"
// @Param        		nip formData string true "NIP"
// @Param        		file formData file false "File"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/penandatangan/{id} [put]
func UpdatePenandatangan(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id := c.Params("id")
	// Body To Struct
	penandatanganDTO := new(entity.PenandatanganDTO)
	if err := c.BodyParser(penandatanganDTO); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(penandatanganDTO)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	file, _ := c.FormFile("file")
	penandatanganDTO.File = file

	// Validasi type File
	if file != nil {
		if file.Header["Content-Type"][0] != "image/png" {
			res := helper.BuildSingleValidationResponse("File", "png", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}
	}

	dataPenandatangan, err := service.ShowPenandatangan(id, baseUrl)
	if v, ok := dataPenandatangan.(entity.Penandatangan); ok {
		// Update Penandatangan
		data, err := service.UpdatePenandatangan(*penandatanganDTO, v, v.IDPenandaTanganSK, c)
		if err != nil {
			res := helper.BuildErrorResponse("Update Penandatangan Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Update Penandatangan Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update Status Penandatangan  			godoc
// @Security 				bearerAuth
// @Summary      		Update Status Penandatangan
// @Description  		Update Status Penandatangan API calls
// @Tags         		Penandatangan
// @Accept       		mpfd
// @Consume      		json
// @Produce      		json
// @Param  					id path string true "ID Penandatangan"
// @Param        		status formData string true "Status" Enums(1, 0)
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/penandatangan/status/{id} [put]
func UpdateStatusPenandatangan(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id := c.Params("id")
	// Body To Struct
	penandatanganDTO := new(entity.PenandatanganStatusDTO)
	if err := c.BodyParser(penandatanganDTO); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), penandatanganDTO)
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(penandatanganDTO)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	dataPenandatangan, err := service.ShowPenandatangan(id, baseUrl)
	if v, ok := dataPenandatangan.(entity.Penandatangan); ok {
		// Update Penandatangan
		data, err := service.UpdateStatusPenandatangan(*penandatanganDTO, v, v.IDPenandaTanganSK)
		if err != nil {
			res := helper.BuildErrorResponse("Update Status Penandatangan Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Update Status Penandatangan Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}
