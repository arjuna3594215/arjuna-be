package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/gofiber/fiber/v2"
)

// Lists Berita  	godoc
// @Summary      		Lists Berita
// @Description  		Lists Berita API calls
// @Tags         		Front Page Berita
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		row query string true "Row"
// @Param        		page query string true "Page"
// @Param        		search query string false "Search By Title"
// @Param        		kategori query string false "Sort By Kategori" Enums(berita, pengumuman, semua)
// @Param        		lang query string true "Sort By Bahasa"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/frontpage/getNews [get]
func FrontPageGetNews(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	row := c.Query("row")
	page := c.Query("page")
	search := c.Query("search")
	kategori := c.Query("kategori")
	lang := c.Query("lang")
	data, err := service.ListBeritaFrontPage(row, page, search, kategori, lang, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Detail Berita  	godoc
// @Summary      		Lists Detail Berita
// @Description  		Lists Detail Berita API calls
// @Tags         		Front Page Berita
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param  					id path int true "Berita ID"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/frontpage/getDetailNews/{id} [get]
func FrontPageGetDetailNews(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id, err := c.ParamsInt("id")
	data, err := service.ShowBeritaFrontPage(id, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	if v, ok := data.(entity.FrontPageBerita); ok {
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Lists Popular Berita  	godoc
// @Summary      		Lists Popular Berita
// @Description  		Lists Popular Berita API calls
// @Tags         		Front Page Berita
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		row query string true "Row"
// @Param        		lang query string true "Sort By Bahasa"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/frontpage/getPopularNews [get]
func FrontPageGetPopularNews(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	row := c.Query("row")
	lang := c.Query("lang")
	data, err := service.ListPopularBeritaFrontPage(row, lang, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Download  	godoc
// @Summary      		Lists Download
// @Description  		Lists Download API calls
// @Tags         		Front Page Berita
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		row query string true "Row"
// @Param        		page query string true "Page"
// @Param        		search query string false "Search By Title"
// @Param        		lang query string true "Sort By Bahasa"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/frontpage/getDownloadList [get]
func FrontPageGetDownloadList(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	row := c.Query("row")
	page := c.Query("page")
	search := c.Query("search")
	lang := c.Query("lang")
	data, err := service.ListBeritaFrontPage(row, page, search, "download", lang, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Slider  	godoc
// @Summary      		Lists Slider
// @Description  		Lists Slider API calls
// @Tags         		Front Page Slider
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/frontpage/getSliderList [get]
func FrontPageGetSlider(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	data, err := service.FrontPageListSlider(baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Statistik Jurnal  	godoc
// @Summary      		Lists Statistik Jurnal (Bar Chart)
// @Description  		Lists Statistik Jurnal API calls
// @Tags         		Front Page Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		fromyear query string false "From Year"
// @Param        		toyear query string false "To Year"
// @Param        		type query string false "Type" Enums(1, 2, 3, 4)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/frontpage/getStatistikJurnal/barchart [get]
func FrontPageGetStatistikJurnalBarChart(c *fiber.Ctx) error {
	types := c.Query("type")
	fromyear := c.Query("fromyear")
	toyear := c.Query("toyear")
	data, err := service.GetStatistikJurnalFrontPageBarChart(types, fromyear, toyear)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Statistik Jurnal  	godoc
// @Summary      		Lists Statistik Jurnal (Polar Chart)
// @Description  		Lists Statistik Jurnal API calls
// @Tags         		Front Page Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		fromyear query string false "From Year"
// @Param        		toyear query string false "To Year"
// @Param        		type query string false "Type" Enums(1, 2, 3, 4)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/frontpage/getStatistikJurnal/polarchart [get]
func FrontPageGetStatistikJurnalPolarChart(c *fiber.Ctx) error {
	types := c.Query("type")
	fromyear := c.Query("fromyear")
	toyear := c.Query("toyear")
	data, err := service.GetStatistikJurnalFrontPagePolarChart(types, fromyear, toyear)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Statistik Jurnal  	godoc
// @Summary      		Lists Statistik Jurnal (Count)
// @Description  		Lists Statistik Jurnal API calls
// @Tags         		Front Page Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		fromyear query string false "From Year"
// @Param        		toyear query string false "To Year"
// @Param        		type query string false "Type" Enums(1, 2, 3, 4)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/frontpage/getStatistikJurnal/count [get]
func FrontPageGetStatistikJurnalCount(c *fiber.Ctx) error {
	types := c.Query("type")
	fromyear := c.Query("fromyear")
	toyear := c.Query("toyear")
	data, err := service.GetStatistikJurnalFrontPageCount(types, fromyear, toyear)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// List Jurnal  	godoc
// @Summary      		Lists List Jurnal
// @Description  		Lists List Jurnal API calls
// @Tags         		Front Page Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama Jurnal / ISSN"
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Param        		order query string true "Oreder BY Tanggal Created" Enums(asc, desc)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/frontpage/getJurnalList [get]
func FrontPageGetListJurnal(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	search := c.Query("search")
	page := c.Query("page")
	row := c.Query("row")
	order := c.Query("order")
	data, err := service.ListJurnalFrontPage(search, page, row, order, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Detail Jurnal  	godoc
// @Summary      		Detail Jurnal
// @Description  		Detail Jurnal API calls
// @Tags         		Front Page Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param  					id path int true "ID Identitas Jurnal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/frontpage/getJurnalDetail/{id} [get]
func FrontPageGetDetailJurnal(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id, err := c.ParamsInt("id")
	data, err := service.DetailJurnalFrontPage(id, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}

	if v, ok := data.(entity.FrontPageListJurnal); ok {
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Jurnal Progres  	godoc
// @Summary      		Lists Jurnal Progres
// @Description  		Lists Jurnal Progres API calls
// @Tags         		Front Page Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param  					id path int true "ID Identitas Jurnal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/frontpage/getJurnalProgres/{id} [get]
func FrontPageGetJurnalProgres(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	data, err := service.JurnalProgresFrontPage(id)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// // Jurnal Accreditation History  	godoc
// // @Summary      		Lists Jurnal Accreditation History
// // @Description  		Lists Jurnal Accreditation History API calls
// // @Tags         		Front Page Jurnal
// // @Accept       		json
// // @Consume      		json
// // @Produce      		json
// // @Param  					id path int true "ID Identitas Jurnal"
// // @Success      		200  {object}  helper.Response
// // @Success      		500  {object}  helper.EmptyObj
// // @Router       		/frontpage/getJurnalAccreditationHistory/{id} [get]
// func FrontPageGetJurnalAccreditationHistory(c *fiber.Ctx) error {
// 	id, err := c.ParamsInt("id")
// 	data, err := service.JurnalAccreditationHistoryFrontPage(id)
// 	if err != nil {
// 		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
// 		return c.Status(fiber.StatusInternalServerError).JSON(res)
// 	}
// 	res := helper.BuildResponse(true, "Success", data)
// 	return c.Status(fiber.StatusOK).JSON(res)
// }

// History Jurnal Name  	godoc
// @Summary      		Lists History Jurnal Name
// @Description  		Lists History Jurnal Name API calls
// @Tags         		Front Page Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param  					id path int true "ID Jurnal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/frontpage/getHistoryJurnalName/{id} [get]
func FrontPageGetHistoryJurnalName(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	data, err := service.HistoryJurnalNameProgresFrontPage(id)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Rekap Usulan Jurnal   	godoc
// @Summary      		Rekap Usulan Jurnal (Count)
// @Description  		Rekap Usulan Jurnal  API calls
// @Tags         		Front Page Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/frontpage/getRekapUsulanJurnal [get]
func FrontPageGetRekapUsulanJurnal(c *fiber.Ctx) error {
	data, err := service.RekapUsulanJurnalFrontPage()
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}
