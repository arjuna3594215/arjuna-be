package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"
	"fmt"
	"strconv"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Lists Daftar Terbitan Jurnal  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Daftar Terbitan Jurnal
// @Description  		Lists Daftar Terbitan Jurnal API calls
// @Tags         		Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Title"
// @Param        		status query string false "Sort BY Status" Enums(1, 0)
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Param        		personal query string false "ID Personal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/jurnal/daftar-terbitan [get]
func GetAllDaftarTerbitanJurnal(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	search := c.Query("search")
	status := c.Query("status")
	page := c.Query("page")
	row := c.Query("row")
	personal := c.Query("personal")
	data, err := service.ListDaftarTerbitanJurnal(search, page, row, status, personal, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Save Jurnal  			godoc
// @Security 				bearerAuth
// @Summary      		Save Jurnal
// @Description  		Save Jurnal API calls
// @Tags         		Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		jurnal body entity.JurnalDTO true "Save Jurnal"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/jurnal [post]
func SaveJurnal(c *fiber.Ctx) error {
	// Body To Struct
	jurnalDto := new(entity.JurnalDTO)
	if err := c.BodyParser(jurnalDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(jurnalDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	// Check Duplicate EISSN
	if !service.CheckDuplicateEissnJurnal(jurnalDto.Eissn) {
		res := helper.BuildSingleValidationResponse("Eissn", "duplicate", "")
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Insert Jurnal
	id_personal := service.GetIDPersonalAuthenticated(c)
	data, err := service.InsertJurnal(*jurnalDto, id_personal)
	if err != nil {
		res := helper.BuildErrorResponse("Create Jurnal Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Create Jurnal Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Update Jurnal  			godoc
// @Security 				bearerAuth
// @Summary      		Update Jurnal
// @Description  		Update Jurnal API calls
// @Tags         		Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Identitas Jurnal"
// @Param        		jurnal body entity.JurnalUpdateDTO true "Update Jurnal"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/jurnal/{id} [post]
func UpdateJurnal(c *fiber.Ctx) error {
	// Body To Struct
	id, _ := c.ParamsInt("id")
	jurnalDto := new(entity.JurnalUpdateDTO)
	if err := c.BodyParser(jurnalDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(jurnalDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Update Jurnal
	id_personal := service.GetIDPersonalAuthenticated(c)
	data, err := service.UpdateJurnal(*jurnalDto, id_personal, id)
	if err != nil {
		res := helper.BuildErrorResponse("Update Jurnal Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Update Jurnal Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Detail Jurnal  	godoc
// @Security 				bearerAuth
// @Summary      		Detail Jurnal
// @Description  		Detail Jurnal API calls
// @Tags         		Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Identitas Jurnal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/jurnal/{id} [get]
func GetJurnal(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id, err := c.ParamsInt("id")
	data, err := service.ShowJurnal(id, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	if v, ok := data.(entity.DetailJurnal); ok {
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Delete Jurnal Jurnal  	godoc
// @Security 				bearerAuth
// @Summary      		Delete Jurnal Jurnal
// @Description  		Delete Jurnal Jurnal API calls
// @Tags         		Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Identitas Jurnal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/jurnal/{id} [delete]
func DeleteJurnal(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id, err := c.ParamsInt("id")

	data, err := service.ShowJurnal(id, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	if v, ok := data.(entity.DetailJurnal); ok {
		// Check Asessment Jurnal
		if service.CheckAssesmentJurnal(int(v.IdJurnal)) {
			res := helper.BuildSingleValidationResponse("Delete jurnal failed,", "This journal is currently in the assessment process", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}

		_, err := service.DeleteJurnal(v)
		if err != nil {
			res := helper.BuildErrorResponse("Delete Jurnal Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusInternalServerError).JSON(res)
		}
		res := helper.BuildResponse(true, "Delete Jurnal Success", helper.EmptyObj{})
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Get Identitas PIC  	godoc
// @Security 				bearerAuth
// @Summary      		Get Identitas PIC
// @Description  		Get Identitas PIC API calls
// @Tags         		Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		pic body entity.GetIdentitasPICDTO true "Identitas PIC"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/jurnal/get-identitas-pic [post]
func GetIdentitasPIC(c *fiber.Ctx) error {
	idenPicDto := new(entity.GetIdentitasPICDTO)
	if err := c.BodyParser(idenPicDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(idenPicDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	data, err := service.GetPersonalByEmail(idenPicDto.Email)
	if err != nil {
		res := helper.BuildResponse(false, "Not Found", err.Error())
		return c.Status(fiber.StatusOK).JSON(res)
	}
	if v, ok := data.(entity.Personal); ok {
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update PIC Jurnal  	godoc
// @Security 				bearerAuth
// @Summary      		Update PIC Jurnal
// @Description  		Update PIC Jurnal API calls
// @Tags         		Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		updatepicjurnal body entity.UpdatePICDTO true "Update PIC Jurnal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/jurnal/update-pic [post]
func UpdatePICJurnal(c *fiber.Ctx) error {
	updatePicDto := new(entity.UpdatePICDTO)
	if err := c.BodyParser(updatePicDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(updatePicDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	errs := service.UpdatePICJurnal(*updatePicDto)
	if errs != nil {
		res := helper.BuildErrorResponse("Failed", errs.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Update PIC Jurnal Success", helper.EmptyObj{})
	return c.Status(fiber.StatusOK).JSON(res)
}

// Update Logo Jurnal  			godoc
// @Security 				bearerAuth
// @Summary      		Update Logo Jurnal
// @Description  		Update Logo Jurnal API calls
// @Tags         		Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Identitas Jurnal"
// @Param        		file formData file true "File"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/jurnal/logo/{id} [post]
func UpdateLogoJurnal(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")
	var fileUpload []string
	form, err := c.MultipartForm()
	if form == nil {
		res := helper.BuildSingleValidationResponse("File", "required", "")
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	files := form.File["file"]
	for _, file := range files {
		// fmt.Println(file.Filename, file.Size, file.Header["Content-Type"][0])
		// => "tutorial.pdf" 360641 "application/pdf"

		file_type := file.Header["Content-Type"][0]
		if strings.Split(file_type, "/")[0] != "image" {
			res := helper.BuildSingleValidationResponse("File", "image", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}
		extension := strings.Split(file.Filename, ".")[len(strings.Split(file.Filename, "."))-1]
		nameFile := strconv.Itoa(id) + "." + extension
		// Save the files to disk:
		if err := c.SaveFile(file, fmt.Sprintf("./files/logo/%s", nameFile)); err != nil {
			res := helper.BuildErrorResponse("Upload Foto Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		fileUpload = append(fileUpload, nameFile)
	}

	if err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}

	errs := service.UpdateLogoJurnal(fileUpload, id)
	if errs != nil {
		res := helper.BuildErrorResponse("Update Logo Failed", errs.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Update Logo Success", helper.EmptyObj{})
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Daftar Usulan Jurnal Terdaftar  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Daftar Usulan Jurnal Terdaftar
// @Description  		Lists Daftar Usulan Jurnal Terdaftar API calls
// @Tags         		Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama Jurnal"
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/jurnal/daftar-usulan [get]
func GetAllDaftarUsulanJurnal(c *fiber.Ctx) error {
	search := c.Query("search")
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListDaftarUsulanJurnalTerdaftar(search, page, row)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Jurnal By SK  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Jurnal By SK
// @Description  		Lists Jurnal By SK API calls
// @Tags         		Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID SK Akreditasi"
// @Param        		search query string false "Search By Nama Jurnal"
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/jurnal/list-by-sk/{id} [get]
func GetAllJurnalBySK(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")
	search := c.Query("search")
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListJurnalBySK(id, search, page, row)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Update SK Jurnal  			godoc
// @Security 				bearerAuth
// @Summary      		Update SK Jurnal
// @Description  		Update SK Jurnal API calls
// @Tags         		Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		jurnal body entity.UpdateSKJurnalDTO true "Update SK Jurnal"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/jurnal/update-sk [post]
func UpdateSKJurnal(c *fiber.Ctx) error {
	// Body To Struct
	jurnalSKDto := new(entity.UpdateSKJurnalDTO)
	if err := c.BodyParser(jurnalSKDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(jurnalSKDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Update Jurnal
	data, err := service.UpdateSKJurnal(*jurnalSKDto)
	if err != nil {
		res := helper.BuildErrorResponse("Update SK Jurnal Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Update SK Jurnal Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// List Jurnal Ter-SK  	godoc
// @Security 				bearerAuth
// @Summary      		List Jurnal Ter-SK
// @Description  		List Jurnal Ter-SK API calls
// @Tags         		Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama Jurnal"
// @Param        		id_sk query string false "Sort By SK"
// @Param        		id_bidang_ilmu query string false "Sort By Bidang Ilmu"
// @Param        		grade query string false "Sort By Grade Akreditasi"
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/jurnal/list-tersk [get]
func ListJurnalTerSK(c *fiber.Ctx) error {
	search := c.Query("search")
	id_sk := c.Query("id_sk")
	id_bidang_ilmu := c.Query("id_bidang_ilmu")
	grade := c.Query("grade")
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListJurnalTerSK(search, page, row, id_sk, id_bidang_ilmu, grade)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Get Print SK Pengelola Jurnal  	godoc
// @Security 				bearerAuth
// @Summary      		Get Print SK Pengelola Jurnal
// @Description  		Get Print SK Pengelola Jurnal API calls
// @Tags         		Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Identitas Jurnal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/jurnal/print-sk-jurnal/{id} [get]
func GetPrintSKPengelolaJurnal(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")
	data, err := service.PrintSKPengelolaJurnal(id)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Hasil Akreditasi By Pic	godoc
// @Security 			bearerAuth
// @Summary      		Lists Hasil Akreditasi By Pic
// @Description  		Lists Hasil Akreditasi By Pic API calls
// @Tags         		Jurnal
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id query string true "ID Personal"
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/jurnal/list-hasil-akreditasi-by-pic [get]
func ListHasilAkreditasiByPic(c *fiber.Ctx) error {
	IdPersonal := c.Query("id")
	page := c.Query("page")
	row := c.Query("row")

	data, err := service.ListHasilJurnalAkreditasiByPic(IdPersonal, page, row)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}
