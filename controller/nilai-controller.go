package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Lists Unsur Penilaian  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Unsur Penilaian
// @Description  		Lists Unsur Penilaian API calls
// @Tags         		Penilaian
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		lang query string true "Sort By Lang" Enums(id,en)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/penilaian/unsur-penilaian [get]
func GetUnsusrPenilaian(c *fiber.Ctx) error {
	lang := c.Query("lang")
	data, err := service.ListUnsurPenilaian(lang, "")
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Sub Unsur Penilaian  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Sub Unsur Penilaian
// @Description  		Lists Sub Unsur Penilaian API calls
// @Tags         		Penilaian
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Unsur Penilaian"
// @Param        		lang query string true "Sort By Lang" Enums(id,en)
// @Param        		id_usulan_akreditasi query int true "ID Usulan Akreditasi"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/penilaian/sub-unsur-penilaian/{id} [get]
func GetSubUnsusrPenilaian(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	lang := c.Query("lang")
	id_usulan_akreditasi := c.Query("id_usulan_akreditasi")
	data, err := service.ListSubUnsurPenilaian(lang, id, id_usulan_akreditasi, "")
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Hasil Penilaian SA  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Hasil Penilaian SA
// @Description  		Lists Hasil Penilaian SA API calls
// @Tags         		Penilaian
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Sub Unsur Penilaian"
// @Param        		id_usulan_akreditasi query int true "Sort Usulan Akreditasi"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/penilaian/hasil-penilaian-sa/{id} [get]
func GetHasilPenilaianSa(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")
	id_usulan_akreditasi := c.Query("id_usulan_akreditasi")
	id_personal := service.GetIDPersonalAuthenticated(c)
	data, _ := service.ListHasilPenilaianSa(id, id_usulan_akreditasi, id_personal)
	// if err != nil {
	// 	res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
	// 	return c.Status(fiber.StatusInternalServerError).JSON(res)
	// }
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Create or Update Hasil Penilaian SA Jurnal  	godoc
// @Security 				bearerAuth
// @Summary      		Create or Update Hasil Penilaian SA Jurnal
// @Description  		Create or Update Hasil Penilaian SA Jurnal API calls
// @Tags         		Penilaian
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		penilaian_sa body entity.HasilPenilaianSaDTO true "Create or Update Hasil Penilaian SA Jurnal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/penilaian [post]
func CreateUpdatePenilaianSa(c *fiber.Ctx) error {
	penilaian_sa := new(entity.HasilPenilaianSaDTO)
	if err := c.BodyParser(penilaian_sa); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(penilaian_sa)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	errs := service.CreateUpdatePenilaianSa(*penilaian_sa)
	if errs != nil {
		res := helper.BuildErrorResponse("Failed", errs.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", helper.EmptyObj{})
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Unsur Penilaian Penugasan Penilaian  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Unsur Penilaian Penugasan Penilaian
// @Description  		Lists Unsur Penilaian Penugasan Penilaian API calls
// @Tags         		Penilaian
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		lang query string true "Sort By Lang" Enums(id,en)
// @Param        		penilai query string true "Sort By Penilai" Enums(issue, management, evaluator)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/penilaian/unsur-penilaian/penugasan-penilaian [get]
func GetUnsusrPenilaianPenugasanPenilaian(c *fiber.Ctx) error {
	lang := c.Query("lang")
	penilai := c.Query("penilai")
	var kel string
	if penilai == "management" || penilai == "evaluator" {
		kel = "2"
	} else if penilai == "issue" {
		kel = "1"
	}
	data, err := service.ListUnsurPenilaian(lang, kel)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Sub Unsur Penilaian Penugasan Penilaian  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Sub Unsur Penilaian Penugasan Penilaian
// @Description  		Lists Sub Unsur Penilaian Penugasan Penilaian API calls
// @Tags         		Penilaian
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Unsur Penilaian"
// @Param        		lang query string true "Sort By Lang" Enums(id,en)
// @Param        		penilai query string true "Sort By Penilai" Enums(issue, management, evaluator)
// @Param        		id_penugasan_penilaian query string true "ID Penugasan Penilaian"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/penilaian/sub-unsur-penilaian/penugasan-penilaian/{id} [get]
func GetSubUnsusrPenilaianPenugasanPenilaian(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	lang := c.Query("lang")
	penilai := c.Query("penilai")
	id_penugasan_penilaian := c.Query("id_penugasan_penilaian")
	data, err := service.ListSubUnsurPenilaianPP(lang, id, id_penugasan_penilaian, penilai)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Create or Update Penugasan Penilaian  	godoc
// @Security 				bearerAuth
// @Summary      		Create or Update Penugasan Penilaian
// @Description  		Create or Update Penugasan Penilaian API calls
// @Tags         		Penilaian
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		penilaian body entity.PenilaianPenugasanDTO true "Create or Update Penugasan Penilaian"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/penilaian/penugasan-penilaian [post]
func CreateUpdatePenilaianPenugasan(c *fiber.Ctx) error {
	penilaian := new(entity.PenilaianPenugasanDTO)
	if err := c.BodyParser(penilaian); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(penilaian)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	errs := service.CreateUpdatePenilaianPenugasan(*penilaian)
	if errs != nil {
		res := helper.BuildErrorResponse("Failed", errs.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", helper.EmptyObj{})
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Sub Unsur Penilaian Penugasan Penilaian Disinsentif  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Sub Unsur Penilaian Penugasan Penilaian Disinsentif
// @Description  		Lists Sub Unsur Penilaian Penugasan Penilaian Disinsentif API calls
// @Tags         		Penilaian
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		lang query string true "Sort By Lang" Enums(id,en)
// @Param        		penilai query string true "Sort By Penilai" Enums(issue)
// @Param        		id_penugasan_penilaian query string true "ID Penugasan Penilaian"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/penilaian/sub-unsur-penilaian/penugasan-penilaian/disinsentif [get]
func GetSubUnsusrPenilaianPenugasanPenilaianDisinsentif(c *fiber.Ctx) error {
	lang := c.Query("lang")
	penilai := c.Query("penilai")
	id_penugasan_penilaian := c.Query("id_penugasan_penilaian")
	data, err := service.ListSubUnsurPenilaianPPDisinsentif(lang, id_penugasan_penilaian, penilai)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Create or Update Penugasan Penilaian Disinsentif 	godoc
// @Security 				bearerAuth
// @Summary      		Create or Update Penugasan Penilaian Disinsentif
// @Description  		Create or Update Penugasan Penilaian Disinsentif API calls
// @Tags         		Penilaian
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		penilaian body entity.PenilaianPenugasanDisinsentifDTO true "Create or Update Penugasan Penilaian"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/penilaian/penugasan-penilaian/disinsentif [post]
func CreateUpdatePenilaianPenugasanDisinsentif(c *fiber.Ctx) error {
	penilaian := new(entity.PenilaianPenugasanDisinsentifDTO)
	if err := c.BodyParser(penilaian); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(penilaian)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	errs := service.CreateUpdatePenilaianPenugasanDisinsentif(*penilaian)
	if errs != nil {
		res := helper.BuildErrorResponse("Failed", errs.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", helper.EmptyObj{})
	return c.Status(fiber.StatusOK).JSON(res)
}
