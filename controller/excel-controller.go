package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/gofiber/fiber/v2"
)

// Export Excel Daftar Terbitan Jurnal  	godoc
// @Security 				bearerAuth
// @Summary      		Export Excel Daftar Terbitan Jurnal
// @Description  		Export Excel Daftar Terbitan Jurnal API calls
// @Tags         		Excel
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Title"
// @Param        		status query string false "Sort BY Status" Enums(1, 0)
// @Param        		personal query string false "ID Personal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/excel/daftar-terbitan [get]
func ExportDaftarTerbitanJurnal(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	search := c.Query("search")
	status := c.Query("status")
	personal := c.Query("personal")
	data, err := service.ExportDaftarTerbitanJurnal(search, status, personal, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Export Excel Daftar Usulan Jurnal Terdaftar  	godoc
// @Security 				bearerAuth
// @Summary      		Export Excel Daftar Usulan Jurnal Terdaftar
// @Description  		Export Excel Daftar Usulan Jurnal Terdaftar API calls
// @Tags         		Excel
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Title"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/excel/jurnal/daftar-usulan-terdaftar [get]
func ExportDaftarUsulanJurnalTerdaftar(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	search := c.Query("search")
	data, err := service.ExportDaftarUsulanJurnalTerdaftar(search, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Export Excel Usulan Akreditasi Baru  	godoc
// @Security 				bearerAuth
// @Summary      		Export Excel Usulan Akreditasi Baru
// @Description  		Export Excel Usulan Akreditasi Baru API calls
// @Tags         		Excel
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama Jurnal"
// @Param        		status query string true "Sort BY Status (1: >= 70 | 0: <70) " Enums(1, 0)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/excel/kelola-usulan/akreditasi-baru [get]
func ExportKelolaUsulanAkreditasiBaru(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	search := c.Query("search")
	status := c.Query("status")
	data, err := service.ExportKelolaUsulanAkreditasiBaru(search, status, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Export Excel Evaluasi Dokumen  	godoc
// @Security 				bearerAuth
// @Summary      		Export Excel Evaluasi Dokumen
// @Description  		Export Excel Evaluasi Dokumen API calls
// @Tags         		Excel
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama Jurnal"
// @Param        		status query string true "Sort BY Status (1: >= 70 | 0: <70) " Enums(1, 0)
//// @Param        		evaluasi query string true "Sort BY Status (1: Lulus | 0: Tidak Lulus) " Enums(1, 0)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/excel/kelola-usulan/evaluasi-dokumen [get]
func ExportKelolaUsulanEvaluasiDokumen(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	search := c.Query("search")
	evaluasi := c.Query("evaluasi")
	status := c.Query("status")
	data, err := service.ExportKelolaUsulanEvaluasiDokumen(search, status, baseUrl, evaluasi)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Export Excel Kinerja Penilaian  	godoc
// @Security 				bearerAuth
// @Summary      		Export Excel Kinerja Penilaian
// @Description  		Export Excel Kinerja Penilaian API calls
// @Tags         		Excel
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		kinerja body entity.KinerjaPenilaiDTO true "Get Kinerja Penilai"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/excel/penilai/kinerja-penilai [post]
func ExportKinerjaPenilai(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	p := new(entity.KinerjaPenilaiDTO)

	if err := c.BodyParser(p); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	data, err := service.ExportKinerjaPenilai(*p, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Export Excel Kelola Usulan Hasil Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		Export Excel Kelola Usulan Hasil Akreditasi
// @Description  		Export Excel Kelola Usulan Hasil Akreditasi API calls
// @Tags         		Excel
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama Jurnal"
// @Param        		status query string true "Sort BY Status (1: >= 70 | 0: <70) " Enums(1, 0)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/excel/kelola-usulan/hasil-akreditasi [get]
func ExportKelolaUsulanHasilAkreditasi(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	search := c.Query("search")
	status := c.Query("status")

	data, err := service.ExportKelolaUsulanHasilAkreditasi(search, status, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Export Excel SK Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		Export Excel SK Akreditasi
// @Description  		Export Excel SK Akreditasi API calls
// @Tags         		Excel
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		status query string false "Sort BY Status" Enums(1, 0)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/excel/sk-akreditasi/list-sk-akreditasi [get]
func ExportSKAkreditasi(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	status := c.Query("status")

	data, err := service.ExportSKAkreditasi(status, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Export Excel List Jurnal Ter-SK  	godoc
// @Security 				bearerAuth
// @Summary      		Export Excel List Jurnal Ter-SK
// @Description  		Export Excel List Jurnal Ter-SK API calls
// @Tags         		Excel
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama Jurnal"
// @Param        		id_sk query string false "Sort By SK"
// @Param        		id_bidang_ilmu query string false "Sort By Bidang Ilmu"
// @Param        		grade query string false "Sort By Grade Akreditasi"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/excel/jurnal/list-tersk [get]
func ExportListJurnalTerSK(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	search := c.Query("search")
	id_sk := c.Query("id_sk")
	id_bidang_ilmu := c.Query("id_bidang_ilmu")
	grade := c.Query("grade")

	data, err := service.ExportListJurnalTerSK(baseUrl, search, id_sk, id_bidang_ilmu, grade)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Export Detail Chart Penilai  	godoc
// @Security 				bearerAuth
// @Summary      		Export Detail Chart Penilai
// @Description  		Export Detail Chart Penilai API calls
// @Tags         		Excel
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		request query int true "Sort By Request" Enums(1,2,3)
// @Param        		method query int true "Method" Enums(1,2,3,4,5,6,7)
// @Param        		tahun query int false "Tahun"
// @Param        		id_personal query string true "id_personal"
// @Param        		record query int true "record"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/excel/dashboard/detail-chart-penilai [get]
func ExportChartDetailPenilai(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	request := c.Query("request")
	method := c.Query("method")
	bidangIlmu := c.Query("bidang_ilmu")
	tahun := c.Query("tahun")
	idPersonal := c.Query("id_personal")
	record := c.Query("record")

	data, err := service.ExportDetailChartPenilail(baseUrl, request, method, bidangIlmu, tahun, idPersonal, record)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Export Detail Chart Pengusul  	godoc
// @Security 				bearerAuth
// @Summary      		Export Detail Chart Pengusul
// @Description  		Export Detail Chart Pengusul API calls
// @Tags         		Excel
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		request query int true "Sort By Dashboard" Enums(1,2,3)
// @Param        		method query string false "Method"
// @Param        		tahun query int false "Tahun"
// @Param        		id_personal query string true "id_personal"
// @Param        		record query int true "record"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/excel/dashboard/detail-chart-pengusul [get]
func ExportChartDetailPengusul(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	request := c.Query("request")
	method := c.Query("method")
	bidangIlmu := c.Query("bidang_ilmu")
	tahun := c.Query("tahun")
	idPersonal := c.Query("id_personal")
	record := c.Query("record")

	data, err := service.ExportDetailPengusul(baseUrl, request, method, bidangIlmu, tahun, idPersonal, record)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}
