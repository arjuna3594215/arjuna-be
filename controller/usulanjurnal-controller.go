package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"
	"log"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Lists Draft Usulan Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Draft Usulan Akreditasi
// @Description  		Lists Draft Usulan Akreditasi API calls
// @Tags         		Usulan Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama Jurnal"
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Param        		personal query string true "ID Personal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/usulan-akreditasi/list-draft [get]
func GetAllDraftUsulanAkreditasiJurnal(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	search := c.Query("search")
	page := c.Query("page")
	row := c.Query("row")
	personal := c.Query("personal")
	data, err := service.ListDraftUsulanAkreditasiJurnal(search, page, row, personal, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Get Usulan Akreditasi (Borang Akreditasi)  	godoc
// @Security 				bearerAuth
// @Summary      		Get Usulan Akreditasi (Borang Akreditasi)
// @Description  		Get Usulan Akreditasi (Borang Akreditasi) API calls
// @Tags         		Usulan Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Identitas Jurnal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/usulan-akreditasi/borang-akreditasi/{id} [get]
func GetBorangUsulanAkreditasiJurnal(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id, err := c.ParamsInt("id")

	// Check Usulan Akreditasi Aktif
	checkAkreditasiAktif, message := service.CheckAkreditasiAktif(int64(id))
	if checkAkreditasiAktif == false {
		res := helper.BuildResponse(true, message, "redirect")
		return c.Status(fiber.StatusOK).JSON(res)
		// res := helper.BuildSingleValidationResponse(message, "", "")
		// return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	data, err := service.ShowJurnal(id, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	if v, ok := data.(entity.DetailJurnal); ok {
		log.Println(len(v.BidangIlmuJurnal))
		if v.Eissn == "" || v.NamaJurnal == "" { // Check ISSN dana Nama Jurnal
			res := helper.BuildSingleValidationResponse("Failed to add proposal! The required data for accreditation (e-ISSN and Journal Name) is empty. You may contact us via helpdesk.", "", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		} else if len(v.BidangIlmuJurnal) == 0 { // Check Bidang Jurnal
			//res := helper.BuildSingleValidationResponse("Journal field of area is empty.", "", "")
			res := helper.BuildSingleValidationResponse("Mohon lengkapi data Jurnal yang kosong pada halaman Edit Jurnal.", "", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		} else if v.EICJurnal.IdEic == 0 { // Insert Usulan Akreditasi Jurnal
			res := helper.BuildSingleValidationResponse("Please fill in the EiC data.", "", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Save Usulan Akreditasi (Borang Akreditasi) Jurnal  			godoc
// @Security 				bearerAuth
// @Summary      		Save Usulan Akreditasi (Borang Akreditasi) Jurnal
// @Description  		Save Usulan Akreditasi (Borang Akreditasi) Jurnal API calls
// @Tags         		Usulan Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		usulan_akreditasi body entity.SaveUsulanAkreditasiDTO true "Save Usulan Akreditasi (Borang Akreditasi) Jurnal"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/usulan-akreditasi/borang-akreditasi/ [post]
func SaveBorangUsulanAkreditasiJurnal(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	// Body To Struct
	usulanakDto := new(entity.SaveUsulanAkreditasiDTO)
	if err := c.BodyParser(usulanakDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(usulanakDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	// Check Usulan Akreditasi Aktif
	checkAkreditasiAktif, message := service.CheckAkreditasiAktif(usulanakDto.IdIdentitasJurnal)
	if checkAkreditasiAktif == false {
		res := helper.BuildResponse(true, message, "redirect")
		return c.Status(fiber.StatusOK).JSON(res)
		// res := helper.BuildSingleValidationResponse(message, "", "")
		// return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	checkIssnNamaJurnal, err := service.ShowJurnal(int(usulanakDto.IdIdentitasJurnal), baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	if v, ok := checkIssnNamaJurnal.(entity.DetailJurnal); ok {
		if v.Eissn == "" || v.NamaJurnal == "" { // Check ISSN dana Nama Jurnal
			res := helper.BuildSingleValidationResponse("Failed to add proposal! The required data for accreditation (e-ISSN and Journal Name) is empty. You may contact us via helpdesk.", "", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		} else if v.BidangIlmuJurnal == nil { // Check Bidang Jurnal
			res := helper.BuildSingleValidationResponse("Journal field of area is empty.", "", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		} else if v.EICJurnal.IdEic == 0 { // Insert Usulan Akreditasi Jurnal
			res := helper.BuildSingleValidationResponse("Please fill in the EiC data.", "", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}

		data, err := service.InsertUsulanAkreditasiJurnal(*usulanakDto, v.IdPic, v.EICJurnal.IdEic)
		if err != nil {
			res := helper.BuildErrorResponse("Create Usulan Akreditasi Jurnal Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Create Usulan Akreditasi Jurnal Success", data)
		return c.Status(fiber.StatusOK).JSON(res)

	}
	res := helper.BuildErrorResponse("Jurnal Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Detail Usulan Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		Detail Usulan Akreditasi
// @Description  		Detail Usulan Akreditasi API calls
// @Tags         		Usulan Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Usulan Akreditasi"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/usulan-akreditasi/{id} [get]
func GetUsulanAkreditasiJurnal(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")

	data := service.ShowUsulanAkreditasi(id)

	if v, ok := data.(entity.UsulanAkreditasi); ok {
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update Usulan Akreditasi  			godoc
// @Security 				bearerAuth
// @Summary      		Update Usulan Akreditasi
// @Description  		Update Usulan Akreditasi API calls
// @Tags         		Usulan Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Usulan Akreditasi"
// @Param        		usulanakreditasi body entity.UpdateUsulanAkreditasiDTO true "Update Usulan Akreditasi"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/usulan-akreditasi/{id} [post]
func UpdateUsulanAkreditasiJurnal(c *fiber.Ctx) error {
	// Body To Struct
	id, _ := c.ParamsInt("id")
	usulanakreditasiDto := new(entity.UpdateUsulanAkreditasiDTO)
	if err := c.BodyParser(usulanakreditasiDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(usulanakreditasiDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Update Usulan Akreditasi Jurnal
	data, err := service.UpdateUsulanAkreditasiJurnal(*usulanakreditasiDto, id)
	if err != nil {
		res := helper.BuildErrorResponse("Update Usulan Akreditasi Jurnal Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Update Usulan Akreditasi Jurnal Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Delete Usulan Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		Delete Usulan Akreditasi
// @Description  		Delete Usulan Akreditasi API calls
// @Tags         		Usulan Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Usulan Akreditasi"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/usulan-akreditasi/{id} [delete]
func DeleteUsulanAkreditasiJurnal(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")

	data := service.ShowUsulanAkreditasi(id)
	if v, ok := data.(entity.UsulanAkreditasi); ok {
		_, err := service.DeleteUsulanAkreditasiJurnal(v, id)
		if err != nil {
			res := helper.BuildErrorResponse("Delete Usulan Akreditasi Jurnal Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusInternalServerError).JSON(res)
		}
		res := helper.BuildResponse(true, "Delete Usulan Akreditasi Jurnal Success", helper.EmptyObj{})
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Siap Akreditasi  			godoc
// @Security 				bearerAuth
// @Summary      		Siap Akreditasi
// @Description  		Siap Akreditasi API calls
// @Tags         		Usulan Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Usulan Akreditasi"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/usulan-akreditasi/siap-akreditasi/{id} [post]
func SiapAkreditasiJurnal(c *fiber.Ctx) error {
	// Body To Struct
	id, err := c.ParamsInt("id")
	if err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}

	uslanAkreditasi := service.ShowUsulanAkreditasi(id)
	if v, ok := uslanAkreditasi.(entity.UsulanAkreditasi); ok {

		// Check Nilai Evaluasi Diri
		if !service.CheckNilaiEvaluasiDiri(int(v.IdUsulanAkreditasi)) {
			//res := helper.BuildSingleValidationResponse("Journal submission process failed. Make sure you have done a self-evaluation", "", "")
			res := helper.BuildSingleValidationResponse("Proses usulan akreditasi jurnal gagal. Pastikan Anda telah mengisi Instrumen Evaluasi Diri", "", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}

		// Check Jurnal Issue
		if !service.CheckIssueJurnal(int(v.IdUsulanAkreditasi)) {
			res := helper.BuildSingleValidationResponse("Journal submission process failed. Make sure you have filled out the journal issue", "", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}

		// Siap Akreditasi Jurnal
		err := service.SiapAkreditasiJurnal(v)
		if err != nil {
			res := helper.BuildErrorResponse("Update Usulan Akreditasi Jurnal Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Update Usulan Akreditasi Jurnal Success", helper.EmptyObj{})
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Usulan Akreditasi Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// List Kemajuan Usulan Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		List Kemajuan Usulan Akreditasi
// @Description  		List Kemajuan Usulan Akreditasi API calls
// @Tags         		Usulan Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Personal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/usulan-akreditasi/list-kemajuan/{id} [get]
func GetKemajuanUsulanAkreditasiJurnal(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")

	data, err := service.ListsKemajuanUsulanAkreditasi(id)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// List Hasil Penilaian Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		List Hasil Penilaian Akreditasi
// @Description  		List Hasil Penilaian Akreditasi API calls
// @Tags         		Usulan Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Param        		id path int true "ID Personal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/usulan-akreditasi/hasil-penilaian/{id} [get]
func GetHasilPenilaianAkreditasiJurnal(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListsHasilPenilaianAkreditasi(page, row, id)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}
