package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Lists Config  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Config
// @Description  		Lists Config API calls
// @Tags         		Config
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Title"
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/config [get]
func GetAllConfig(c *fiber.Ctx) error {
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListConfig(page, row)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Save Config  			godoc
// @Security 				bearerAuth
// @Summary      		Save Config
// @Description  		Save Config API calls
// @Tags         		Config
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		config body entity.ConfigDTO false "Insert Config"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/config [post]
func SaveConfig(c *fiber.Ctx) error {
	// Body To Struct
	configDto := new(entity.ConfigDTO)

	if err := c.BodyParser(configDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(configDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Insert Config
	data, err := service.InsertConfig(*configDto, c)
	if err != nil {
		res := helper.BuildErrorResponse("Create Config Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Create Config Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Detail Config  	godoc
// @Security 				bearerAuth
// @Summary      		Detail Config
// @Description  		Detail Config API calls
// @Tags         		Config
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Config"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/config/{id} [get]
func GetConfig(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	data, err := service.ShowConfig(id)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	if v, ok := data.(entity.Config); ok {
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update Config  			godoc
// @Security 				bearerAuth
// @Summary      		Update Config
// @Description  		Update Config API calls
// @Tags         		Config
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param  					id path int true "ID Config"
// @Param        		config body entity.ConfigDTO false "Update Config"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/config/{id} [put]
func UpdateConfig(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")
	// Body To Struct
	configDto := new(entity.ConfigDTO)
	if err := c.BodyParser(configDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(configDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	dataConfig, err := service.ShowConfig(id)
	if v, ok := dataConfig.(entity.Config); ok {
		// Update Config
		data, err := service.UpdateConfig(*configDto, v, int(v.IDConfigurasi))
		if err != nil {
			res := helper.BuildErrorResponse("Update Config Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Update Config Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update Config  			godoc
// @Security 				bearerAuth
// @Summary      		Update Config
// @Description  		Update Config API calls
// @Tags         		Config
// @Accept       		mpfd
// @Consume      		json
// @Produce      		json
// @Param  					id path int true "ID Config"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/config/active-status/{id} [put]
func UpdateStatusConfig(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")

	dataConfig, err := service.ShowConfig(id)
	if v, ok := dataConfig.(entity.Config); ok {
		// Update Config
		data, err := service.UpdateStatusConfig(v, int(v.IDConfigurasi))
		if err != nil {
			res := helper.BuildErrorResponse("Update Status Config Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Update Status Config Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}
