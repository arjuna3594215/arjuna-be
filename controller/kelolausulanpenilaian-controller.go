package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Lists Penugasan Penilaian  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Penugasan Penilaian
// @Description  		Lists Penugasan Penilaian API calls
// @Tags         		Kelola Usulan Penilaian
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama Jurnal"
// @Param        		id_personal query int true "Sort By ID Personal"
// @Param        		penilai query string true "Sort By Penilai" Enums(issue, management, evaluator)
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan-penilaian/penugasan-penilaian [get]
func GetAllPenugasanPenilaian(c *fiber.Ctx) error {
	search := c.Query("search")
	penilai := c.Query("penilai")
	id_personal := c.Query("id_personal")
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListPenugasanPenilaian(search, page, row, penilai, id_personal)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Update Status Penugasan Penilaian  			godoc
// @Security 				bearerAuth
// @Summary      		Update Status Penugasan Penilaian
// @Description  		Update Status Penugasan Penilaian API calls
// @Tags         		Kelola Usulan Penilaian
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param  					id path string true "ID Penugasan Penilaian"
// @Param        		penugasan_penilaian body entity.UpdateStatusPenugasanPenilaianDTO true "Status Penugasan Penilaian"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/kelola-usulan-penilaian/penugasan-penilaian/status/{id} [put]
func UpdateStatusPenugasanPenilaian(c *fiber.Ctx) error {
	id := c.Params("id")
	// Body To Struct
	updateStatusPenugasanPenilaianDTO := new(entity.UpdateStatusPenugasanPenilaianDTO)
	if err := c.BodyParser(updateStatusPenugasanPenilaianDTO); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(updateStatusPenugasanPenilaianDTO)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	dataPenugasanPenilaian, err := service.ShowPenugasanPenilaian(id, updateStatusPenugasanPenilaianDTO.Penilai)
	if v, ok := dataPenugasanPenilaian.(entity.PenugasanPenilaian); ok {
		// Update Penugasan Penilaian
		data, err := service.UpdateStatusPenugasanPenilaian(*updateStatusPenugasanPenilaianDTO, v.IDPenugasanPenilaian)
		if err != nil {
			res := helper.BuildErrorResponse("Update Penugasan Penilaian Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Update Penugasan Penilaian Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Lists Proses Penilaian  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Proses Penilaian
// @Description  		Lists Proses Penilaian API calls
// @Tags         		Kelola Usulan Penilaian
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama Jurnal"
// @Param        		id_personal query int true "Sort By ID Personal"
// @Param        		penilai query string true "Sort By Penilai" Enums(issue, management, evaluator)
// @Param        		proses query string true "Sort By Proses Penilai" Enums(Semua Proses, Belum Dinilai, Proses Penilaian, Penyesuaian Penilaian)
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan-penilaian/proses-penilaian [get]
func GetAllProsesPenilaiani(c *fiber.Ctx) error {
	search := c.Query("search")
	penilai := c.Query("penilai")
	proses := c.Query("proses")
	id_personal := c.Query("id_personal")
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListProsesPenilaian(search, page, row, penilai, id_personal, proses)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Penilaian Selesai  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Penilaian Selesai
// @Description  		Lists Penilaian Selesai API calls
// @Tags         		Kelola Usulan Penilaian
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama Jurnal"
// @Param        		id_personal query int true "Sort By ID Personal"
// @Param        		penilai query string true "Sort By Penilai" Enums(issue, management, evaluator)
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan-penilaian/penilaian-selesai [get]
func GetAllPenilaianSelesai(c *fiber.Ctx) error {
	search := c.Query("search")
	penilai := c.Query("penilai")
	id_personal := c.Query("id_personal")
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListPenilaianSelesai(search, page, row, penilai, id_personal)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Detail Penilaian Proses  	godoc
// @Security 				bearerAuth
// @Summary      		Detail Penilaian Proses
// @Description  		Detail Penilaian Proses API calls
// @Tags         		Kelola Usulan Penilaian
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path string true "ID Penugasan Penilaian"
// @Param        		penilai query string true "Sort By Penilai" Enums(issue, management, evaluator)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/kelola-usulan-penilaian/detail-penilaian/{id} [get]
func GetDetailPeninilaian(c *fiber.Ctx) error {
	id := c.Params("id")
	penilai := c.Query("penilai")
	data, err := service.DetailPenilaian(id, penilai)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), id)
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}

	if penilai == "issue" {
		if v, ok := data.(entity.DetailPenilaianIssue); ok {
			res := helper.BuildResponse(true, "Success", v)
			return c.Status(fiber.StatusOK).JSON(res)
		}
	} else {
		if v, ok := data.(entity.DetailPenilaian); ok {
			res := helper.BuildResponse(true, "Success", v)
			return c.Status(fiber.StatusOK).JSON(res)
		}
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}
