package controller

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"
	"fmt"
	"strconv"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Profile  	godoc
// @Security 				bearerAuth
// @Summary      		Profile
// @Description  		Profile API calls
// @Tags         		Profile
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/profile [get]
func Profile(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id_personal := service.GetIDPersonalAuthenticated(c)
	dataPersonal, err := service.ShowPersonal(baseUrl, id_personal)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	if v, ok := dataPersonal.(entity.Personal); ok {
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildResponse(false, "User Not Found", helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update Profile  			godoc
// @Security 				bearerAuth
// @Summary      		Update Profile
// @Description  		Update Profile API calls
// @Tags         		Profile
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		updateProfile body entity.ProfileDTO true "Update Profile"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/profile [post]
func UpdateProfile(c *fiber.Ctx) error {
	// Body To Struct
	profileDto := new(entity.ProfileDTO)
	if err := c.BodyParser(profileDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(profileDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	id_personal := service.GetIDPersonalAuthenticated(c)
	data, err := service.UpdateProfile(*profileDto, id_personal)
	if err != nil {
		res := helper.BuildErrorResponse("Update Profile Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Update Profile Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Update Foto Profile  			godoc
// @Security 				bearerAuth
// @Summary      		Update Foto Profile
// @Description  		Update Foto Profile API calls
// @Tags         		Profile
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		file formData file true "File"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/profile/foto [post]
func UpdateFotoProfile(c *fiber.Ctx) error {
	var fileUpload []string
	form, err := c.MultipartForm()
	id_personal := service.GetIDPersonalAuthenticated(c)
	if form == nil {
		res := helper.BuildSingleValidationResponse("File", "required", "")
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	files := form.File["file"]
	for _, file := range files {
		// fmt.Println(file.Filename, file.Size, file.Header["Content-Type"][0])
		// => "tutorial.pdf" 360641 "application/pdf"

		file_type := file.Header["Content-Type"][0]
		if strings.Split(file_type, "/")[0] != "image" {
			res := helper.BuildSingleValidationResponse("File", "image", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}
		extension := strings.Split(file.Filename, ".")[len(strings.Split(file.Filename, "."))-1]
		nameFile := strconv.Itoa(id_personal) + "." + extension
		// Save the files to disk:
		if err := c.SaveFile(file, fmt.Sprintf("./files/photos/%s", nameFile)); err != nil {
			res := helper.BuildErrorResponse("Upload Foto Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		fileUpload = append(fileUpload, nameFile)
	}

	if err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}

	errs := service.UpdateFotoProfile(fileUpload, id_personal)
	if errs != nil {
		res := helper.BuildErrorResponse("Update Foto Failed", errs.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Update Foto Success", helper.EmptyObj{})
	return c.Status(fiber.StatusOK).JSON(res)
}

// Change Password  			godoc
// @Security 				bearerAuth
// @Summary      		Change Password
// @Description  		Change Password API calls
// @Tags         		Profile
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		changepassword body entity.ChangePasswordDTO true "Change Password"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/profile/change-password [post]
func ChangePassword(c *fiber.Ctx) error {
	// Body To Struct
	changepasswordDto := new(entity.ChangePasswordDTO)
	if err := c.BodyParser(changepasswordDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(changepasswordDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	id_peengguna := service.GetIDPenggunaAuthenticated(c)
	dataPengguna, _ := service.ShowPengguna(id_peengguna)
	if v, ok := dataPengguna.(entity.Pengguna); ok {
		// Check Old Password
		if config.EncryptPassword(changepasswordDto.OldPassword) != v.Password {
			res := helper.BuildSingleValidationResponse("OldPassword", "oldsamepass", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}
		// Check New Password And Confirm Password
		if changepasswordDto.NewPassword != changepasswordDto.ConfirmNewPassword {
			res := helper.BuildSingleValidationResponse("NewPassword", "newsamepass", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}

		err := service.ChangePassword(int(v.IDPengguna), changepasswordDto.ConfirmNewPassword)

		if err != nil {
			res := helper.BuildErrorResponse("Change Password Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Change Password Success", helper.EmptyObj{})
		return c.Status(fiber.StatusOK).JSON(res)

	}
	res := helper.BuildResponse(false, "User Not Found", helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}
