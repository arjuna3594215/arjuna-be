package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Authenticate  godoc
// @Summary      Login
// @Description  Authenticates a user and providers a JWT to Authorize API calls
// @ID           Authentication
// @Tags         Auth
// @Accept       json
// @Consume      json
// @Produce      json
// @Param        login body entity.LoginDTO true "Login"
// @Success      200  {object}  helper.Response
// @Failure      422  {object}  helper.EmptyObj
// @Failure      401  {object}  helper.Response
// @Failure      400  {object}  helper.Response
// @Router       /auth/login [post]
func Login(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	// Body To Struct
	loginDTO := new(entity.LoginDTO)
	if err := c.BodyParser(loginDTO); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(loginDTO)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	// Login
	data := service.Login(*loginDTO, baseUrl)
	if v, ok := data.(entity.Login); ok {
		v.Token = service.GenerateToken(v)
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}

	res := helper.BuildErrorResponse("Please check again your credential", "Invalid credentials", helper.EmptyObj{})
	return c.Status(fiber.StatusUnauthorized).JSON(res)
}

// Register  godoc
// @Summary      Login
// @Description  Register
// @ID           Register
// @Tags         Auth
// @Accept       json
// @Consume      json
// @Produce      json
// @Param        Register body entity.UserDTO true "Register"
// @Success      200  {object}  helper.Response
// @Failure      422  {object}  helper.EmptyObj
// @Failure      401  {object}  helper.Response
// @Failure      400  {object}  helper.Response
// @Router       /auth/register [post]
func Register(c *fiber.Ctx) error {
	// Body To Struct
	userDTO := new(entity.UserDTO)
	if err := c.BodyParser(userDTO); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(userDTO)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	// Check Duplicate Email Personal
	if !service.CheckDuplicateEmailPersonal(userDTO.Email) {
		res := helper.BuildSingleValidationResponse("Email", "duplicate", "")
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	// Check Duplicate Username Pengguna
	if !service.CheckDuplicateUsernamePengguna(userDTO.Username) {
		res := helper.BuildSingleValidationResponse("Username", "duplicate", "")
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Register
	data, err := service.InsertUser(*userDTO)
	if err != nil {
		res := helper.BuildErrorResponse("Register Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Register Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Forgot Password  godoc
// @Summary      Forgot Password
// @Description  Forgot Password
// @ID           Forgot Password
// @Tags         Auth
// @Accept       json
// @Consume      json
// @Produce      json
// @Param        ForgotPassword body entity.ForgotPassDTO true "Forgot Password"
// @Success      200  {object}  helper.Response
// @Failure      422  {object}  helper.EmptyObj
// @Failure      401  {object}  helper.Response
// @Failure      400  {object}  helper.Response
// @Failure      500  {object}  helper.Response
// @Router       /auth/forgot-password [post]
func ForgotPassword(c *fiber.Ctx) error {
	// Body To Struct
	fogotpassDTO := new(entity.ForgotPassDTO)
	if err := c.BodyParser(fogotpassDTO); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(fogotpassDTO)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	// Check Email Personal
	if service.CheckDuplicateEmailPersonal(fogotpassDTO.Email) {
		res := helper.BuildSingleValidationResponse("Email", "notfound", "")
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	datapersonal, err := service.GetPersonalByEmail(fogotpassDTO.Email)
	if v, ok := datapersonal.(entity.Personal); ok {
		datapengguna, err := service.GetPenggunaByIdPersonal(int(v.IDPersonal))
		if vpeng, okpeng := datapengguna.(entity.Pengguna); okpeng {
			_, err := service.ForgotPassword(v, vpeng)
			if err != nil {
				res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
				return c.Status(fiber.StatusInternalServerError).JSON(res)
			}
			res := helper.BuildResponse(true, "Success", fiber.Map{
				"message": "Password berhasil direset, silahkan cek email anda",
			})
			return c.Status(fiber.StatusOK).JSON(res)
		}
		res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusNotFound).JSON(res)
	}

	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}
