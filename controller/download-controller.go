package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Lists Download  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Download
// @Description  		Lists Download API calls
// @Tags         		Download
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Title"
// @Param        		status query string false "Sort By Status" Enums(1,0)
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/download [get]
func GetAllDownload(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	search := c.Query("search")
	status := c.Query("status")
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListDownload(search, page, row, baseUrl, status)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Save Download  			godoc
// @Security 				bearerAuth
// @Summary      		Save Download
// @Description  		Save Download API calls
// @Tags         		Download
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		title formData string true "Title"
// @Param        		lang formData string true "Lang" Enums(id, en)
// @Param        		image formData file true "Image"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/download [post]
func SaveDownload(c *fiber.Ctx) error {
	// Body To Struct
	DownloadDto := new(entity.DownloadDTO)
	if err := c.BodyParser(DownloadDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(DownloadDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	image, _ := c.FormFile("image")
	DownloadDto.Image = image

	// Validasi type File
	if image != nil {
		if strings.Split(image.Header["Content-Type"][0], "/")[0] != "image" {
			res := helper.BuildSingleValidationResponse("File", "image", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}
	} else {
		res := helper.BuildSingleValidationResponse("File", "required", "")
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Insert Download
	data, err := service.InsertDownload(*DownloadDto, c)
	if err != nil {
		res := helper.BuildErrorResponse("Create Download Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Create Download Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Detail Download  	godoc
// @Security 				bearerAuth
// @Summary      		Detail Download
// @Description  		Detail Download API calls
// @Tags         		Download
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Download"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/download/{id} [get]
func GetDownload(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id, err := c.ParamsInt("id")
	data, err := service.ShowDownload(id, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	if v, ok := data.(entity.Download); ok {
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update Download  			godoc
// @Security 				bearerAuth
// @Summary      		Update Download
// @Description  		Update Download API calls
// @Tags         		Download
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param  					id path int true "ID Download"
// @Param        		title formData string true "Title"
// @Param        		lang formData string true "Lang" Enums(id, en)
// @Param        		image formData file false "Image"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/download/{id} [put]
func UpdateDownload(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id, _ := c.ParamsInt("id")
	// Body To Struct
	downloadDto := new(entity.DownloadDTO)
	if err := c.BodyParser(downloadDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(downloadDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	image, _ := c.FormFile("image")
	downloadDto.Image = image

	// Validasi type File
	if image != nil {
		if strings.Split(image.Header["Content-Type"][0], "/")[0] != "image" {
			res := helper.BuildSingleValidationResponse("File", "image", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}
	}

	dataDownload, err := service.ShowDownload(id, baseUrl)
	if v, ok := dataDownload.(entity.Download); ok {
		// Update Download
		data, err := service.UpdateDownload(*downloadDto, v, int(v.ID), c)
		if err != nil {
			res := helper.BuildErrorResponse("Update Download Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Update Download Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update Download  			godoc
// @Security 				bearerAuth
// @Summary      		Update Download
// @Description  		Update Download API calls
// @Tags         		Download
// @Accept       		mpfd
// @Consume      		json
// @Produce      		json
// @Param  					id path int true "ID Download"
// @Param        		status formData string true "Status" Enums(1, 0)
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/download/status/{id} [put]
func UpdateStatusDownload(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id, _ := c.ParamsInt("id")
	// Body To Struct
	downloadDto := new(entity.DownloadStatusDTO)
	if err := c.BodyParser(downloadDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), downloadDto)
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(downloadDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	dataDownload, err := service.ShowDownload(id, baseUrl)
	if v, ok := dataDownload.(entity.Download); ok {
		// Update Download
		data, err := service.UpdateStatusDownload(*downloadDto, v, int(v.ID))
		if err != nil {
			res := helper.BuildErrorResponse("Update Status Download Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Update Status Download Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}
