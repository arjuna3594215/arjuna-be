package controller

import (
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/gofiber/fiber/v2"
)

// Dashboard Pengusul   	godoc
// @Security 				bearerAuth
// @Summary      		Dashboard Pengusul (Count)
// @Description  		Dashboard Pengusul  API calls
// @Tags         		Dashboard
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/dashboard/pengusul/count [get]
func DashboardPengusulCount(c *fiber.Ctx) error {
	id_personal := service.GetIDPersonalAuthenticated(c)
	data, err := service.DashboardPengusulCount(id_personal)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Dashboard Pengusul  	godoc
// @Security 				bearerAuth
// @Summary      		Dashboard Pengusul (Bar Chart)
// @Description  		Dashboard Pengusul API calls
// @Tags         		Dashboard
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		type query string false "Type" Enums(1, 2)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/dashboard/pengusul/chart [get]
func DashboardPengusulChart(c *fiber.Ctx) error {
	types := c.Query("type")
	id_personal := service.GetIDPersonalAuthenticated(c)
	data, err := service.DashboardPengusulChart(types, int64(id_personal))
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Dashboard Penilai   	godoc
// @Security 				bearerAuth
// @Summary      		Dashboard Penilai (Count)
// @Description  		Dashboard Penilai  API calls
// @Tags         		Dashboard
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		penilai query string true "Sort By Penilai" Enums(issue, management, evaluator)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/dashboard/penilai/count [get]
func DashboardPenilaiCount(c *fiber.Ctx) error {
	penilai := c.Query("penilai")
	id_personal := service.GetIDPersonalAuthenticated(c)
	data, err := service.DashboardPenilaiCount(id_personal, penilai)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Dashboard Penilai   	godoc
// @Security 				bearerAuth
// @Summary      		Dashboard Penilai (Line Chart)
// @Description  		Dashboard Penilai  API calls
// @Tags         		Dashboard
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		penilai query string true "Sort By Penilai" Enums(issue, management, evaluator)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/dashboard/penilai/line-chart [get]
func DashboardPenilaiLineChart(c *fiber.Ctx) error {
	penilai := c.Query("penilai")
	id_personal := service.GetIDPersonalAuthenticated(c)
	data, err := service.DashboardPenilaiLineChart(id_personal, penilai)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Dashboard Penilai   	godoc
// @Security 				bearerAuth
// @Summary      		Dashboard Penilai (Bar Chart)
// @Description  		Dashboard Penilai  API calls
// @Tags         		Dashboard
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		penilai query string true "Sort By Penilai" Enums(issue, management, evaluator)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/dashboard/penilai/bar-chart [get]
func DashboardPenilaiBarChart(c *fiber.Ctx) error {
	penilai := c.Query("penilai")
	id_personal := service.GetIDPersonalAuthenticated(c)
	data, err := service.DashboardPenilaiBarChart(id_personal, penilai)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Dashboard Distributor   	godoc
// @Security 				bearerAuth
// @Summary      		Dashboard Distributor (Count)
// @Description  		Dashboard Distributor  API calls
// @Tags         		Dashboard
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/dashboard/distributor/count [get]
func DashboardDistributorCount(c *fiber.Ctx) error {
	data, err := service.DashboardDistributorCount()
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Dashboard Distributor SK Terakhir   	godoc
// @Security 				bearerAuth
// @Summary      		Dashboard Distributor SK Terakhir (Donut Chart)
// @Description  		Dashboard Distributor SK Terakhir  API calls
// @Tags         		Dashboard
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		type query string true "Sort By Penilai" Enums(1, 2, 3)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/dashboard/distributor/donut-chart/sebaran-jurnal [get]
func DashboardDashboardDonutChartSebaranJurnal(c *fiber.Ctx) error {
	types := c.Query("type")
	data, err := service.DashboardDashboardDonutChartSebaranJurnal(types)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Dashboard Distributor Sebaran Jurnal   	godoc
// @Security 				bearerAuth
// @Summary      		Dashboard Distributor Sebaran Jurnal (Bar Chart)
// @Description  		Dashboard Distributor Sebaran Jurnal  API calls
// @Tags         		Dashboard
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/dashboard/distributor/bar-chart/sk-terakhir [get]
func DashboardDashboardBarChartSkTerakhir(c *fiber.Ctx) error {
	data, err := service.DashboardDistributorBarChartSkTerakhir()
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Dashboard Distributor Bidang Ilmu   	godoc
// @Security 				bearerAuth
// @Summary      		Dashboard Distributor Bidang Ilmu (Bar Chart)
// @Description  		Dashboard Distributor Bidang Ilmu  API calls
// @Tags         		Dashboard
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		type query string true "Sort By Penilai" Enums(1, 2, 3)
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/dashboard/distributor/bar-chart/bidang-ilmu [get]
func DashboardDashboardBarChartBidangIlmu(c *fiber.Ctx) error {
	types := c.Query("type")
	data, err := service.DashboardDashboardBarChartBidangIlmu(types)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Dashboard Distributor Reakap AKreditasi   	godoc
// @Security 				bearerAuth
// @Summary      		Dashboard Distributor Reakap AKreditasi
// @Description  		Dashboard Distributor Reakap AKreditasi  API calls
// @Tags         		Dashboard
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/dashboard/distributor/rekap-akreditasi [get]
func DashboardDistributorRekapAkreditasi(c *fiber.Ctx) error {
	data, err := service.DashboardDistributorRekapAkreditasi()
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Dashboard Distributor Detail  	godoc
// @Security 				bearerAuth
// @Summary      		Dashboard Distributor Detail (Count)
// @Description  		Dashboard Distributor Detail  API calls
// @Tags         		Dashboard
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		types query string true "Sort By Usulan(1-> Usulan Jurnal, 2->Jurnal Terakreditasi, 3->Usulan Ditolak)" Enums(1, 2, 3)
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/dashboard/distributor/detail-count [get]
func DashboardDistributorDetailCount(c *fiber.Ctx) error {
	types := c.Query("types")
	pages := c.Query("page")
	rows := c.Query("row")

	data, err := service.DashboardDistributorDetailCount(types, pages, rows)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}

	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Dashboard Distributor Rekap Peta  	godoc
// @Security 				bearerAuth
// @Summary      		Dashboard Distributor Rekap Peta
// @Description  		Dashboard Distributor Rekap Peta  API calls
// @Tags         		Dashboard
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		kota query string true "kota"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/dashboard/distributor/peta [get]
func DashboardDistributorRekapJurnalPeta(c *fiber.Ctx) error {
	kota := c.Query("kota")

	data, err := service.DashboardDistributorRekapPeta(kota)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}

	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Dashboard Distributor Detail SK Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		Dashboard Distributor Detail SK Akreditasi
// @Description  		Dashboard Distributor Detail SK Akreditasi  API calls
// @Tags         		Dashboard
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id_sk_akreditasi query int true "id_sk_akreditasi"
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/dashboard/distributor/detail-sk [get]
func DashboardDistributorDetailSKAkreditasi(c *fiber.Ctx) error {
	id_akreditasi := c.Query("id_sk_akreditasi")
	pages := c.Query("page")
	rows := c.Query("row")

	data, err := service.DashboardDistributorDetailSK(id_akreditasi, pages, rows)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Dashboard Distributor Detail Tabel Rekapitulasi Jurnal	godoc
// @Security 				bearerAuth
// @Summary      		Dashboard Distributor Detail Tabel Rekapitulasi Jurnal
// @Description  		Dashboard Distributor Detail Tabel Rekapitulasi Jurnal  API calls
// @Tags         		Dashboard
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		request query string true "Sort By Request(1-> Total Usulan, 2-> Baru, 3-> Reakreditasi, 4-> Belum Diproses, 5-> Ditolak, 6-> Lolos Evaluasi, 7-> Belum dinilai, 8-> Penilaian)" Enums(1, 2, 3, 4, 5, 6, 7, 8)
// @Param        		method query string true "Sort By Method(1-> >= 70, 2-> <= 70, 3-> Total)" Enums(1, 2, 3)
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/dashboard/distributor/detail-usulan-akreditasi [get]
func DashboardDistributorTabelRekapRekapituasiJurnal(c *fiber.Ctx) error {
	request := c.Query("request")
	method := c.Query("method")
	pages := c.Query("page")
	rows := c.Query("row")

	data, err := service.DashboardDistributorTabelRekapitulasi(request, method, pages, rows)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}

	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Dashboard Distributor Detail Bidang Ilmu  	godoc
// @Security 				bearerAuth
// @Summary      		Dashboard Distributor Detail Bidang Ilmu
// @Description  		Dashboard Distributor Detail Bidang Ilmu API calls
// @Tags         		Dashboard
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		request query string true "Sort By Request" Enums(bidang_ilmu, institusi)
// @Param        		bidang_ilmu query string true "Bidang Ilmu"
// @Param        		method query string true "method"
// @Param        		institusi query string false "Institusi"
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/dashboard/distributor/detail-bidang-ilmu [get]
func DashboardDistributorDetailChartBidangIlmuInstitusi(c *fiber.Ctx) error {
	request := c.Query("request")
	bidang_ilmu := c.Query("bidang_ilmu")
	method := c.Query("method")
	institusi := c.Query("institusi")
	pages := c.Query("page")
	rows := c.Query("row")

	data, err := service.DashboardDistributorBidangIlmu(request, bidang_ilmu, method, institusi, pages, rows)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Dashboard Penilai Detail  	godoc
// @Security 				bearerAuth
// @Summary      		Dashboard Penilai Detail
// @Description  		Dashboard Penilai Detail API calls
// @Tags         		Dashboard
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		request query string true "Sort By Request" Enums(penilai konten, penilai manajemen, evaluator)
// @Param        		method query string true "Sort By Method" Enums(Total Penugasan, Penugasan Diterima, Penugasan Ditolak, Penugasan Belum Diputuskan, Dalam Proses Penilaian, Penilaian Selesai, Rekapitulasi Penilaian Tahunan, Rekapitulasi Penilaian berdasarkan Bidang Keilmuan)
// @Param        		bidang_ilmu query string false "Bidang Ilmu"
// @Param        		tahun query string false "Tahun"
// @Param        		id_personal query string true "id_personal"
// @Param        		page query string false "Page"
// @Param        		row query string false "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/dashboard/penilai/detail [get]
func DashboardPenilaiDetail(c *fiber.Ctx) error {
	request := c.Query("request")
	method := c.Query("method")
	bidang_ilmu := c.Query("bidang_ilmu")
	tahun := c.Query("tahun")
	id_personal := c.Query("id_personal")
	pages := c.Query("page")
	rows := c.Query("row")

	data, err := service.DashboardDetailChartPenilai(request, method, bidang_ilmu, tahun, id_personal, pages, rows)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}

	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Dashboard Pengusul Detail  	godoc
// @Security 				bearerAuth
// @Summary      		Dashboard Pengusul Detail
// @Description  		Dashboard Pengusul Detail API calls
// @Tags         		Dashboard
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		request query string true "Sort By Dashboard" Enums(dashboard header, dashboard chart rekap jurnal pertahun, dashboard rekap akreditasi tahunan)
// @Param        		method query string false "Method"
// @Param        		tahun query int false "Tahun"
// @Param        		id_personal query string true "id_personal"
// @Param        		page query string false "Page"
// @Param        		row query string false "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/dashboard/pengusul/detail [get]
func DashboardPengusulDetail(c *fiber.Ctx) error {
	request := c.Query("request")
	method := c.Query("method")
	tahun := c.Query("tahun")
	id_personal := c.Query("id_personal")
	pages := c.Query("page")
	rows := c.Query("row")

	data, err := service.DashboardDetailPengusul(request, method, tahun, id_personal, pages, rows)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}

	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}
