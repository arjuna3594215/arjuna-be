package controller

import (
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/gofiber/fiber/v2"
)

// Lists Kewenangan  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Kewenangan
// @Description  		Lists Kewenangan API calls
// @Tags         		User
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/app/user/kewenangan [get]
func GetAllKewenangan(c *fiber.Ctx) error {
	data, err := service.ListKewenangan()
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Add Kewenangan Pengguna  			godoc
// @Security 				bearerAuth
// @Summary      		Add Kewenangan Pengguna
// @Description  		Add Kewenangan Pengguna API calls
// @Tags         		User
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param  					id_pengguna path int true "ID Pengguna"
// @Param  					id_kewenangan path int true "ID Kewenangan"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/app/user [post]
// func SaveKewenanganPengguna(c *fiber.Ctx) error {
// 	id_pengguna, _ := c.ParamsInt("id_pengguna")
// 	id_kewenangan, _ := c.ParamsInt("id_kewenangan")
// 	// Check Kewenangan Pengguna
// 	if !service.CheckKewenanganPengguna(id_pengguna, id_kewenangan, "1") {
// 		res := helper.BuildErrorResponse("The user is already included in this authority", "", helper.EmptyObj{})
// 		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
// 	}

// 	// Insert Kewenagan Pengguna
// 	data, err := service.InsertKewenaganPengguna(id_pengguna, id_kewenangan)
// 	if err != nil {
// 		res := helper.BuildErrorResponse("Create User Failed", err.Error(), helper.EmptyObj{})
// 		return c.Status(fiber.StatusBadRequest).JSON(res)
// 	}
// 	res := helper.BuildResponse(true, "Create User Success", data)
// 	return c.Status(fiber.StatusOK).JSON(res)
// }
