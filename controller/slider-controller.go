package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Lists Slider  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Slider
// @Description  		Lists Slider API calls
// @Tags         		Slider
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Title"
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/slider [get]
func GetAllSlider(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	search := c.Query("search")
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListSlider(search, page, row, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Save Slider  			godoc
// @Security 				bearerAuth
// @Summary      		Save Slider
// @Description  		Save Slider API calls
// @Tags         		Slider
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		title formData string true "Title"
// @Param        		sub_title formData string true "Sub Title"
// @Param        		url formData string false "Url"
// @Param        		image formData file true "Image"
// @Param        		date_last formData string false "Date Last"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/slider [post]
func SaveSlider(c *fiber.Ctx) error {
	// Body To Struct
	sliderDto := new(entity.SliderDTO)

	if err := c.BodyParser(sliderDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(sliderDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	image, _ := c.FormFile("image")
	sliderDto.Imagename = image

	// Validasi type File
	if image != nil {
		if strings.Split(image.Header["Content-Type"][0], "/")[0] != "image" {
			res := helper.BuildSingleValidationResponse("File", "image", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}
	} else {
		res := helper.BuildSingleValidationResponse("File", "required", "")
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Insert Slider
	data, err := service.InsertSlider(*sliderDto, c)
	if err != nil {
		res := helper.BuildErrorResponse("Create Slider Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Create Slider Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Detail Slider  	godoc
// @Security 				bearerAuth
// @Summary      		Detail Slider
// @Description  		Detail Slider API calls
// @Tags         		Slider
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Slider"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/slider/{id} [get]
func GetSlider(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id, err := c.ParamsInt("id")
	data, err := service.ShowSlider(id, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	if v, ok := data.(entity.Slider); ok {
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update Slider  			godoc
// @Security 				bearerAuth
// @Summary      		Update Slider
// @Description  		Update Slider API calls
// @Tags         		Slider
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param  					id path int true "ID Slider"
// @Param        		title formData string true "Title"
// @Param        		sub_title formData string true "Sub Title"
// @Param        		url formData string false "Url"
// @Param        		image formData file false "Image"
// @Param        		date_last formData string false "Date Last"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/slider/{id} [put]
func UpdateSlider(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id, _ := c.ParamsInt("id")
	// Body To Struct
	sliderDto := new(entity.SliderDTO)
	if err := c.BodyParser(sliderDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(sliderDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	image, _ := c.FormFile("image")
	sliderDto.Imagename = image

	// Validasi type File
	if image != nil {
		if strings.Split(image.Header["Content-Type"][0], "/")[0] != "image" {
			res := helper.BuildSingleValidationResponse("File", "image", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}
	}

	dataSlider, err := service.ShowSlider(id, baseUrl)
	if v, ok := dataSlider.(entity.Slider); ok {
		// Update Slider
		data, err := service.UpdateSlider(*sliderDto, v, int(v.ID), c)
		if err != nil {
			res := helper.BuildErrorResponse("Update Slider Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Update Slider Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}
