package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Lists Kinerja Penilai  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Kinerja Penilai
// @Description  		Lists Kinerja Penilai API calls
// @Tags         		Penilai
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		kinerja body entity.KinerjaPenilaiDTO true "Get Kinerja Penilai"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/penilai/kinerja-penilai [post]
func GetAllKinerjaPenilai(c *fiber.Ctx) error {
	p := new(entity.KinerjaPenilaiDTO)

	if err := c.BodyParser(p); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	data, err := service.ListKinerjaPenilaian(*p)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Detail Lists Jurnal Kinerja Penilai  	godoc
// @Security 				bearerAuth
// @Summary      		Detail Lists Jurnal Kinerja Penilai
// @Description  		Detail Lists Jurnal Kinerja Penilai API calls
// @Tags         		Penilai
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		kinerja body entity.KinerjaPenilaiDTO true "Get Kinerja Penilai"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/penilai/detail-jurnal/kinerja-penilai [post]
func GetDetailListsJurnalKinerjaPenilai(c *fiber.Ctx) error {
	p := new(entity.KinerjaPenilaiDTO)

	if err := c.BodyParser(p); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	data, err := service.DetailListJurnalKinerjaPenilaian(*p)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Detail Kinerja Penilai  	godoc
// @Security 				bearerAuth
// @Summary      		Detail Kinerja Penilai
// @Description  		Detail Kinerja Penilai API calls
// @Tags         		Penilai
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		penilai query string true "Sort By Penilai" Enums(issue, management, evaluator)
// @Param        		personal query string false "personal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/penilai/detail/kinerja-penilai [get]
func GetDetailKinerjaPenilai(c *fiber.Ctx) error {
	penilai := c.Query("penilai")
	personal := c.Query("personal")
	data, err := service.DetailKinerjaPenilaian(penilai, personal)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Detail Kinerja Bar Chart  	godoc
// @Security 				bearerAuth
// @Summary      		Detail Kinerja Bar Chart (Bar Chart)
// @Description  		Detail Kinerja Bar Chart API calls
// @Tags         		Penilai
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		penilai query string true "Sort By Penilai" Enums(issue, management, evaluator)
// @Param        		personal query string false "personal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/penilai/detail/bar-chart [get]
func GetDetailKinerjaPenilaiBarChart(c *fiber.Ctx) error {
	penilai := c.Query("penilai")
	personal := c.Query("personal")
	data, err := service.DetailKinerjaPenilaianBarChart(penilai, personal)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Detail Kinerja Line Chart  	godoc
// @Security 				bearerAuth
// @Summary      		Detail Kinerja Line Chart (Line Chart)
// @Description  		Detail Kinerja Line Chart API calls
// @Tags         		Penilai
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		penilai query string true "Sort By Penilai" Enums(issue, management, evaluator)
// @Param        		personal query string false "personal"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/penilai/detail/line-chart [get]
func GetDetailKinerjaPenilaiLineChart(c *fiber.Ctx) error {
	penilai := c.Query("penilai")
	personal := c.Query("personal")
	data, err := service.DetailKinerjaPenilaianLineChart(penilai, personal)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Get Periode Kinerja Penilai  	godoc
// @Security 				bearerAuth
// @Summary      		Get Periode Kinerja Penilai
// @Description  		Get Periode Kinerja Penilai API calls
// @Tags         		Penilai
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/penilai/get-periode [get]
func GetPeriodeKinerjaPenilai(c *fiber.Ctx) error {
	data, err := service.GetPeriodeKinerjaPenilai()
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Get Penugasan Penilai Evaluator  	godoc
// @Security 				bearerAuth
// @Summary      		Get Penugasan Penilai Evaluator
// @Description  		Get Penugasan Penilai Evaluator API calls
// @Tags         		Penilai
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id_usulan_akreditasi query string true "ID Usulan Akreditasi"
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/penilai/penugasan-evaluator [get]
func GetPenugasanPenilaiEvaluator(c *fiber.Ctx) error {
	page := c.Query("page")
	row := c.Query("row")
	id_usulan_akreditasi := c.Query("id_usulan_akreditasi")
	data, err := service.GetListPenugasanPenilaiEvaluator(id_usulan_akreditasi, page, row)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Get Penugasan Penilai Issue  	godoc
// @Security 				bearerAuth
// @Summary      		Get Penugasan Penilai Issue
// @Description  		Get Penugasan Penilai Issue API calls
// @Tags         		Penilai
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id_usulan_akreditasi query string true "ID Usulan Akreditasi"
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/penilai/penugasan-issue [get]
func GetPenugasanPenilaiIssue(c *fiber.Ctx) error {
	page := c.Query("page")
	row := c.Query("row")
	id_usulan_akreditasi := c.Query("id_usulan_akreditasi")
	data, err := service.GetListPenugasanPenilaiIssue(id_usulan_akreditasi, page, row)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Get Penugasan Penilai Management  	godoc
// @Security 				bearerAuth
// @Summary      		Get Penugasan Penilai Management
// @Description  		Get Penugasan Penilai Management API calls
// @Tags         		Penilai
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id_usulan_akreditasi query string true "ID Usulan Akreditasi"
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/penilai/penugasan-management [get]
func GetPenugasanPenilaiManagement(c *fiber.Ctx) error {
	page := c.Query("page")
	row := c.Query("row")
	id_usulan_akreditasi := c.Query("id_usulan_akreditasi")
	data, err := service.GetListPenugasanPenilaiManagement(id_usulan_akreditasi, page, row)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Save Penugasan Penilai  			godoc
// @Security 				bearerAuth
// @Summary      		Save Penugasan Penilai
// @Description  		Save Penugasan Penilai API calls
// @Tags         		Penilai
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		penugasan_penilaian body entity.SavePengusulPenilaiDTO true "Assessor = Manajemen, Issue, Evaluator"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/penilai/penugasan-penilaian [post]
func SavePenugasanPenilaian(c *fiber.Ctx) error {
	// Body To Struct
	penugasanpenilaianDto := new(entity.SavePengusulPenilaiDTO)
	if err := c.BodyParser(penugasanpenilaianDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(penugasanpenilaianDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	// Check Duplicate Penugasan Penilaian
	if !service.CheckDuplicatePenugasanPenilaian(penugasanpenilaianDto.IDPersonal, penugasanpenilaianDto.IDUsulanAkreditasi, penugasanpenilaianDto.Assessor) {
		res := helper.BuildSingleValidationResponse("Assessor", "duplicate", "")
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Insert Penugasan Penilaian
	data, err := service.InsertPenugasanPenilaian(*penugasanpenilaianDto)
	if err != nil {
		res := helper.BuildErrorResponse("Create Penugasan Penilaian Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Create Penugasan Penilaian Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Delete Penugasan Penilai  	godoc
// @Security 				bearerAuth
// @Summary      		Delete Penugasan Penilai
// @Description  		Delete Penugasan Penilai API calls
// @Tags         		Penilai
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		penugasan_penilaian body entity.DeletePengusulPenilaiDTO true "Assessor = Manajemen, Issue, Evaluator"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/penilai/delete-penugasan-penilaian [post]
func DeletePenugasanPenilaian(c *fiber.Ctx) error {
	// Body To Struct
	penugasanpenilaianDto := new(entity.DeletePengusulPenilaiDTO)
	if err := c.BodyParser(penugasanpenilaianDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(penugasanpenilaianDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	data, err := service.DeletePenugasanPenilaian(*penugasanpenilaianDto)
	if err != nil {
		res := helper.BuildErrorResponse("Delete Penugasan Penilaian Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Delete Penugasan Penilaian Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}
