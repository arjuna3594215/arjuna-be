package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Lists Berita  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Berita
// @Description  		Lists Berita API calls
// @Tags         		Berita
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Title"
// @Param        		status query string false "Sort By Status" Enums(1,0)
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/berita [get]
func GetAllBerita(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	search := c.Query("search")
	status := c.Query("status")
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListBerita(search, page, row, baseUrl, status)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Save Berita  			godoc
// @Security 				bearerAuth
// @Summary      		Save Berita
// @Description  		Save Berita API calls
// @Tags         		Berita
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		title formData string true "Title"
// @Param        		isi formData string true "isi"
// @Param        		lang formData string true "Lang" Enums(id, en)
// @Param        		image formData file false "Image"
// @Param        		lampiran1 formData file false "Lampiran 1"
// @Param        		lampiran2 formData file false "Lampiran 2"
// @Param        		lampiran3 formData file false "Lampiran 3"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/berita [post]
func SaveBerita(c *fiber.Ctx) error {
	// Body To Struct
	beritaDto := new(entity.BeritaDTO)
	if err := c.BodyParser(beritaDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(beritaDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	image, _ := c.FormFile("image")
	lampiran1, _ := c.FormFile("lampiran1")
	lampiran2, _ := c.FormFile("lampiran2")
	lampiran3, _ := c.FormFile("lampiran3")
	beritaDto.Image = image
	beritaDto.Lampiran1 = lampiran1
	beritaDto.Lampiran2 = lampiran2
	beritaDto.Lampiran3 = lampiran3

	// Validasi type File
	if image != nil {
		if strings.Split(image.Header["Content-Type"][0], "/")[0] != "image" {
			res := helper.BuildSingleValidationResponse("File", "image", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}
	}
	// if lampiran1 != nil {
	// 	if strings.Split(lampiran1.Header["Content-Type"][0], "/")[0] != "application" {
	// 		res := helper.BuildSingleValidationResponse("File", "image", "")
	// 		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	// 	}
	// }

	// Insert Berita
	data, err := service.InsertBerita(*beritaDto, c)
	if err != nil {
		res := helper.BuildErrorResponse("Create Berita Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Create Berita Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Detail Berita  	godoc
// @Security 				bearerAuth
// @Summary      		Detail Berita
// @Description  		Detail Berita API calls
// @Tags         		Berita
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Berita"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/berita/{id} [get]
func GetBerita(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id, err := c.ParamsInt("id")
	data, err := service.ShowBerita(id, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	if v, ok := data.(entity.Berita); ok {
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update Berita  			godoc
// @Security 				bearerAuth
// @Summary      		Update Berita
// @Description  		Update Berita API calls
// @Tags         		Berita
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param  					id path int true "ID Berita"
// @Param        		title formData string true "Title"
// @Param        		isi formData string true "isi"
// @Param        		lang formData string true "Lang" Enums(id, en)
// @Param        		image formData file false "Image"
// @Param        		lampiran1 formData file false "Lampiran 1"
// @Param        		lampiran2 formData file false "Lampiran 2"
// @Param        		lampiran3 formData file false "Lampiran 3"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/berita/{id} [put]
func UpdateBerita(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id, _ := c.ParamsInt("id")
	// Body To Struct
	beritaDTO := new(entity.BeritaDTO)
	if err := c.BodyParser(beritaDTO); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(beritaDTO)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	image, _ := c.FormFile("image")
	lampiran1, _ := c.FormFile("lampiran1")
	lampiran2, _ := c.FormFile("lampiran2")
	lampiran3, _ := c.FormFile("lampiran3")
	beritaDTO.Image = image
	beritaDTO.Lampiran1 = lampiran1
	beritaDTO.Lampiran2 = lampiran2
	beritaDTO.Lampiran3 = lampiran3

	// Validasi type File
	if image != nil {
		if strings.Split(image.Header["Content-Type"][0], "/")[0] != "image" {
			res := helper.BuildSingleValidationResponse("File", "image", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}
	}

	dataBerita, err := service.ShowBerita(id, baseUrl)
	if v, ok := dataBerita.(entity.Berita); ok {
		// Update Berita
		data, err := service.UpdateBerita(*beritaDTO, v, int(v.ID), c)
		if err != nil {
			res := helper.BuildErrorResponse("Update Berita Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Update Berita Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update Berita  			godoc
// @Security 				bearerAuth
// @Summary      		Update Berita
// @Description  		Update Berita API calls
// @Tags         		Berita
// @Accept       		mpfd
// @Consume      		json
// @Produce      		json
// @Param  					id path int true "ID Berita"
// @Param        		status formData string true "Status" Enums(1, 0)
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/berita/status/{id} [put]
func UpdateStatusBerita(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id, _ := c.ParamsInt("id")
	// Body To Struct
	beritaDTO := new(entity.BeritaStatusDTO)
	if err := c.BodyParser(beritaDTO); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), beritaDTO)
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(beritaDTO)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	dataBerita, err := service.ShowBerita(id, baseUrl)
	if v, ok := dataBerita.(entity.Berita); ok {
		// Update Berita
		data, err := service.UpdateStatusBerita(*beritaDTO, v, int(v.ID))
		if err != nil {
			res := helper.BuildErrorResponse("Update Status Berita Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Update Status Berita Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}
