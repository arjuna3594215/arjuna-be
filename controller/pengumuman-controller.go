package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Lists Pengumuman  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Pengumuman
// @Description  		Lists Pengumuman API calls
// @Tags         		Pengumuman
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Title"
// @Param        		status query string false "Sort By Status" Enums(1,0)
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/pengumuman [get]
func GetAllPengumuman(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	search := c.Query("search")
	status := c.Query("status")
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListPengumuman(search, page, row, baseUrl, status)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Save Pengumuman  			godoc
// @Security 				bearerAuth
// @Summary      		Save Pengumuman
// @Description  		Save Pengumuman API calls
// @Tags         		Pengumuman
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		title formData string true "Title"
// @Param        		isi formData string true "isi"
// @Param        		lang formData string true "Lang" Enums(id, en)
// @Param        		image formData file false "Image"
// @Param        		lampiran1 formData file false "Lampiran 1"
// @Param        		lampiran2 formData file false "Lampiran 2"
// @Param        		lampiran3 formData file false "Lampiran 3"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/pengumuman [post]
func SavePengumuman(c *fiber.Ctx) error {
	// Body To Struct
	pengumumanDto := new(entity.PengumumanDTO)
	if err := c.BodyParser(pengumumanDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(pengumumanDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	image, _ := c.FormFile("image")
	lampiran1, _ := c.FormFile("lampiran1")
	lampiran2, _ := c.FormFile("lampiran2")
	lampiran3, _ := c.FormFile("lampiran3")
	pengumumanDto.Image = image
	pengumumanDto.Lampiran1 = lampiran1
	pengumumanDto.Lampiran2 = lampiran2
	pengumumanDto.Lampiran3 = lampiran3

	// Validasi type File
	if image != nil {
		if strings.Split(image.Header["Content-Type"][0], "/")[0] != "image" {
			res := helper.BuildSingleValidationResponse("File", "image", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}
	}
	// if lampiran1 != nil {
	// 	if strings.Split(lampiran1.Header["Content-Type"][0], "/")[0] != "application" {
	// 		res := helper.BuildSingleValidationResponse("File", "image", "")
	// 		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	// 	}
	// }

	// Insert Pengumuman
	data, err := service.InsertPengumuman(*pengumumanDto, c)
	if err != nil {
		res := helper.BuildErrorResponse("Create Pengumuman Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Create Pengumuman Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Detail Pengumuman  	godoc
// @Security 				bearerAuth
// @Summary      		Detail Pengumuman
// @Description  		Detail Pengumuman API calls
// @Tags         		Pengumuman
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Pengumuman"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/pengumuman/{id} [get]
func GetPengumuman(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id, err := c.ParamsInt("id")
	data, err := service.ShowPengumuman(id, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	if v, ok := data.(entity.Pengumuman); ok {
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update Pengumuman  			godoc
// @Security 				bearerAuth
// @Summary      		Update Pengumuman
// @Description  		Update Pengumuman API calls
// @Tags         		Pengumuman
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param  					id path int true "ID Pengumuman"
// @Param        		title formData string true "Title"
// @Param        		isi formData string true "isi"
// @Param        		lang formData string true "Lang" Enums(id, en)
// @Param        		image formData file false "Image"
// @Param        		lampiran1 formData file false "Lampiran 1"
// @Param        		lampiran2 formData file false "Lampiran 2"
// @Param        		lampiran3 formData file false "Lampiran 3"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/pengumuman/{id} [put]
func UpdatePengumuman(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id, _ := c.ParamsInt("id")
	// Body To Struct
	pengumumanDTO := new(entity.PengumumanDTO)
	if err := c.BodyParser(pengumumanDTO); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(pengumumanDTO)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	image, _ := c.FormFile("image")
	lampiran1, _ := c.FormFile("lampiran1")
	lampiran2, _ := c.FormFile("lampiran2")
	lampiran3, _ := c.FormFile("lampiran3")
	pengumumanDTO.Image = image
	pengumumanDTO.Lampiran1 = lampiran1
	pengumumanDTO.Lampiran2 = lampiran2
	pengumumanDTO.Lampiran3 = lampiran3

	// Validasi type File
	if image != nil {
		if strings.Split(image.Header["Content-Type"][0], "/")[0] != "image" {
			res := helper.BuildSingleValidationResponse("File", "image", "")
			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}
	}

	dataPengumuman, err := service.ShowPengumuman(id, baseUrl)
	if v, ok := dataPengumuman.(entity.Pengumuman); ok {
		// Update Pengumuman
		data, err := service.UpdatePengumuman(*pengumumanDTO, v, int(v.ID), c)
		if err != nil {
			res := helper.BuildErrorResponse("Update Pengumuman Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Update Pengumuman Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update Pengumuman  			godoc
// @Security 				bearerAuth
// @Summary      		Update Pengumuman
// @Description  		Update Pengumuman API calls
// @Tags         		Pengumuman
// @Accept       		mpfd
// @Consume      		json
// @Produce      		json
// @Param  					id path int true "ID Pengumuman"
// @Param        		status formData string true "Status" Enums(1, 0)
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/pengumuman/status/{id} [put]
func UpdateStatusPengumuman(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id, _ := c.ParamsInt("id")
	// Body To Struct
	pengumumanDTO := new(entity.PengumumanStatusDTO)
	if err := c.BodyParser(pengumumanDTO); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), pengumumanDTO)
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(pengumumanDTO)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	dataPengumuman, err := service.ShowPengumuman(id, baseUrl)
	if v, ok := dataPengumuman.(entity.Pengumuman); ok {
		// Update Pengumuman
		data, err := service.UpdateStatusPengumuman(*pengumumanDTO, v, int(v.ID))
		if err != nil {
			res := helper.BuildErrorResponse("Update Status Pengumuman Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Update Status Pengumuman Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}
