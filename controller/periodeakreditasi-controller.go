package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Lists Periode Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Periode Akreditasi
// @Description  		Lists Periode Akreditasi API calls
// @Tags         		Periode Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Periode"
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/periode-akreditasi [get]
func GetAllPeriodeAkreditasi(c *fiber.Ctx) error {
	search := c.Query("search")
	page := c.Query("page")
	row := c.Query("row")
	data, err := service.ListPeriodeAkreditasi(search, page, row)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Get Periode Akreditasi Active  	godoc
// @Security 				bearerAuth
// @Summary      		Get Periode Akreditasi Active
// @Description  		Get Periode Akreditasi Active API calls
// @Tags         		Periode Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/periode-akreditasi/active [get]
func GetPeriodeAkreditasiActive(c *fiber.Ctx) error {
	data, err := service.GetPeriodeAkreditasiActive()
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Save Periode Akreditasi  			godoc
// @Security 				bearerAuth
// @Summary      		Save Periode Akreditasi
// @Description  		Save Periode Akreditasi API calls
// @Tags         		Periode Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		periode_akrediasi body entity.PeriodeAkreditasiDTO true "Save Periode Akreditasi"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/periode-akreditasi [post]
func SavePeriodeAkreditasi(c *fiber.Ctx) error {
	// Body To Struct
	PeriodeakreditasiDto := new(entity.PeriodeAkreditasiDTO)
	if err := c.BodyParser(PeriodeakreditasiDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(PeriodeakreditasiDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	// Insert PeriodeAkreditasi
	data, err := service.InsertPeriodeAkreditasi(*PeriodeakreditasiDto)
	if err != nil {
		res := helper.BuildErrorResponse("Create PeriodeAkreditasi Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	res := helper.BuildResponse(true, "Create PeriodeAkreditasi Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Detail Periode Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		Detail Periode Akreditasi
// @Description  		Detail Periode Akreditasi API calls
// @Tags         		Periode Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path string true "ID Periode Akreditasi"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/periode-akreditasi/{id} [get]
func GetPeriodeAkreditasi(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id := c.Params("id")
	data, err := service.ShowPeriodeAkreditasi(id, baseUrl)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	if v, ok := data.(entity.PeriodeAkreditasi); ok {
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update Periode Akreditasi  			godoc
// @Security 				bearerAuth
// @Summary      		Update Periode Akreditasi
// @Description  		Update Periode Akreditasi API calls
// @Tags         		Periode Akreditasi
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param  					id path string true "ID Periode Akreditasi"
// @Param        		periode_akrediasi body entity.PeriodeAkreditasiDTO true "Save Periode Akreditasi"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/periode-akreditasi/{id} [put]
func UpdatePeriodeAkreditasi(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id := c.Params("id")
	// Body To Struct
	periodeakreditasiDTO := new(entity.PeriodeAkreditasiDTO)
	if err := c.BodyParser(periodeakreditasiDTO); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(periodeakreditasiDTO)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	dataPeriodeAkreditasi, err := service.ShowPeriodeAkreditasi(id, baseUrl)
	if v, ok := dataPeriodeAkreditasi.(entity.PeriodeAkreditasi); ok {
		// Update Periode Akreditasi
		data, err := service.UpdatePeriodeAkreditasi(*periodeakreditasiDTO, v, v.IDPeriodeAkreditasi)
		if err != nil {
			res := helper.BuildErrorResponse("Update Periode Akreditasi Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Update Periode Akreditasi Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update Status Periode Akreditasi  			godoc
// @Security 				bearerAuth
// @Summary      		Update Status Periode Akreditasi
// @Description  		Update Status Periode Akreditasi API calls
// @Tags         		Periode Akreditasi
// @Accept       		mpfd
// @Consume      		json
// @Produce      		json
// @Param  					id path string true "ID Periode Akreditasi"
// @Param        		status formData string true "Status" Enums(1, 0)
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Failure      		404  {object}  helper.Response
// @Router       		/periode-akreditasi/status/{id} [put]
func UpdateStatusPeriodeAkreditasi(c *fiber.Ctx) error {
	baseUrl := c.BaseURL()
	id := c.Params("id")
	// Body To Struct
	periodeakreditasiDTO := new(entity.PeriodeAkreditasiStatusDTO)
	if err := c.BodyParser(periodeakreditasiDTO); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), periodeakreditasiDTO)
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(periodeakreditasiDTO)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	dataPeriodeAkreditasi, err := service.ShowPeriodeAkreditasi(id, baseUrl)
	if v, ok := dataPeriodeAkreditasi.(entity.PeriodeAkreditasi); ok {
		// Update Periode Akreditasi
		data, err := service.UpdateStatusPeriodeAkreditasi(*periodeakreditasiDTO, v, v.IDPeriodeAkreditasi)
		if err != nil {
			res := helper.BuildErrorResponse("Update Status Periode Akreditasi Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Update Status Periode Akreditasi Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}
