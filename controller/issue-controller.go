package controller

import (
	"arjuna-api/entity"
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// Lists Issue Usulan Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Issue Usulan Akreditasi
// @Description  		Lists Issue Usulan Akreditasi API calls
// @Tags         		Issue
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		search query string false "Search By Nama Judul"
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Param        		id_usulan_akreditasi query string true "ID Usulan Akreditasi"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/issue/usulan-akreditasi [get]
func GetAllIssueUsulanAkreditasi(c *fiber.Ctx) error {
	search := c.Query("search")
	page := c.Query("page")
	row := c.Query("row")
	id_usulan_akreditasi := c.Query("id_usulan_akreditasi")
	data, err := service.ListIssueUsulanAkreditasi(search, page, row, id_usulan_akreditasi)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Issue Diajukan Usulan Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Issue Diajukan Usulan Akreditasi
// @Description  		Lists Issue Diajukan Usulan Akreditasi API calls
// @Tags         		Issue
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		page query string true "Page"
// @Param        		row query string true "Row"
// @Param        		id_usulan_akreditasi query string true "ID Usulan Akreditasi"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/issue/diajukan/usulan-akreditasi [get]
func GetAllIssueDiajukanUsulanAkreditasi(c *fiber.Ctx) error {
	page := c.Query("page")
	row := c.Query("row")
	id_usulan_akreditasi := c.Query("id_usulan_akreditasi")
	data, err := service.ListIssueDiajukanUsulanAkreditasi(page, row, id_usulan_akreditasi)
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Save Issue Usulan Akreditasi  			godoc
// @Security 				bearerAuth
// @Summary      		Save Issue Usulan Akreditasi
// @Description  		Save Issue Usulan Akreditasi API calls
// @Tags         		Issue
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		issue body entity.IssueDTO true "Save Issue Usulan Akreditasi"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/issue/usulan-akreditasi/ [post]
func SaveIssueUsulanAkreditasi(c *fiber.Ctx) error {
	// Body To Struct
	issueDto := new(entity.IssueDTO)
	if err := c.BodyParser(issueDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(issueDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}
	// Check Duplicate URL Issue
	if !service.CheckDuplicateUrlIssue(issueDto.IdUsulanAkreditasi, issueDto.UrlIssue) {
		res := helper.BuildSingleValidationResponse("URLIssue", "duplicate", "")
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	uslanAkreditasi := service.ShowUsulanAkreditasi(int(issueDto.IdUsulanAkreditasi))

	if v, ok := uslanAkreditasi.(entity.UsulanAkreditasi); ok {

		// Chech CCanAddNewIssue
		if !service.CheckCanAddNewIssue(issueDto.IdUsulanAkreditasi, v.IdIdentitasJurnal) {
			//edit by agust 2023-04-12 
			//res := helper.BuildSingleValidationResponse("This journal has been accredited, re-accreditation process only require 1 issue.", "", "") 
			res := helper.BuildSingleValidationResponse("Jurnal ini telah terakreditasi, proses Re-Akreditasi hanya membutuhkan 1 terbitan.", "", "")

			return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
		}

		data, err := service.InsertIssueUsulanAkreditasi(*issueDto, v.IdIdentitasJurnal, v.IdJurnal)
		if err != nil {
			res := helper.BuildErrorResponse("Create Issue Usulan Akreditasi Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusBadRequest).JSON(res)
		}
		res := helper.BuildResponse(true, "Create Issue Usulan Akreditasi Success", data)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Usulan Akreditasi Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Detail Issue Usulan Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		Detail Issue Usulan Akreditasi
// @Description  		Detail Issue Usulan Akreditasi API calls
// @Tags         		Issue
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Issue"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/issue/usulan-akreditasi/{id} [get]
func GetIssueUsulanAkreditasi(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")

	data := service.ShowIssueUsulanAkreditasi(id)
	if v, ok := data.(entity.Issue); ok {
		res := helper.BuildResponse(true, "Success", v)
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Update Issue Usulan Akreditasi  			godoc
// @Security 				bearerAuth
// @Summary      		Update Issue Usulan Akreditasi
// @Description  		Update Issue Usulan Akreditasi API calls
// @Tags         		Issue
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Issue"
// @Param        		issue body entity.IssueDTO true "Update Issue Usulan Akreditasi"
// @Success      		200  {object}  helper.Response
// @Failure      		422  {object}  helper.EmptyObj
// @Failure      		401  {object}  helper.Response
// @Failure      		400  {object}  helper.Response
// @Router       		/issue/usulan-akreditasi/{id} [post]
func UpdateIssueUsulanAkreditasi(c *fiber.Ctx) error {
	id, _ := c.ParamsInt("id")
	// Body To Struct
	issueDto := new(entity.IssueDTO)
	if err := c.BodyParser(issueDto); err != nil {
		res := helper.BuildErrorResponse("Bad Request", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusBadRequest).JSON(res)
	}
	// Validasi
	validate := validator.New()
	err := validate.Struct(issueDto)
	if err != nil {
		res := helper.BuildValidationResponse(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
	}

	issue := service.ShowIssueUsulanAkreditasi(id)
	if item, ok := issue.(entity.Issue); ok {

		if item.UrlIssue != issueDto.UrlIssue {
			// Check Duplicate URL Issue
			if !service.CheckDuplicateUrlIssue(issueDto.IdUsulanAkreditasi, issueDto.UrlIssue) {
				res := helper.BuildSingleValidationResponse("URLIssue", "duplicate", "")
				return c.Status(fiber.StatusUnprocessableEntity).JSON(res)
			}
		}

		uslanAkreditasi := service.ShowUsulanAkreditasi(int(issueDto.IdUsulanAkreditasi))
		if v, ok := uslanAkreditasi.(entity.UsulanAkreditasi); ok {

			data, err := service.UpdateIssueUsulanAkreditasi(*issueDto, item.IDIssue, v.IdIdentitasJurnal, v.IdJurnal)
			if err != nil {
				res := helper.BuildErrorResponse("Update Issue Usulan Akreditasi Failed", err.Error(), helper.EmptyObj{})
				return c.Status(fiber.StatusBadRequest).JSON(res)
			}
			res := helper.BuildResponse(true, "Update Issue Usulan Akreditasi Success", data)
			return c.Status(fiber.StatusOK).JSON(res)
		}
		res := helper.BuildErrorResponse("Usulan Akreditasi Not Found", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusNotFound).JSON(res)
	}
	res := helper.BuildErrorResponse("Issue Usulan Akreditasi Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}

// Delete Issue Usulan Akreditasi  	godoc
// @Security 				bearerAuth
// @Summary      		Delete Issue Usulan Akreditasi
// @Description  		Delete Issue Usulan Akreditasi API calls
// @Tags         		Issue
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Param        		id path int true "ID Issue"
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Success      		404  {object}  helper.EmptyObj
// @Router       		/issue/usulan-akreditasi/{id} [delete]
func DeleteIssueUsulanAkreditasi(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")

	data := service.ShowIssueUsulanAkreditasi(id)
	if v, ok := data.(entity.Issue); ok {
		_, err := service.DeleteIssueUsulanAkreditasi(v, id)
		if err != nil {
			res := helper.BuildErrorResponse("Delete Issue Usulan Akreditasi Failed", err.Error(), helper.EmptyObj{})
			return c.Status(fiber.StatusInternalServerError).JSON(res)
		}
		res := helper.BuildResponse(true, "Delete Issue Usulan Akreditasi Success", helper.EmptyObj{})
		return c.Status(fiber.StatusOK).JSON(res)
	}
	res := helper.BuildErrorResponse("Not Found", err.Error(), helper.EmptyObj{})
	return c.Status(fiber.StatusNotFound).JSON(res)
}
