package controller

import (
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/gofiber/fiber/v2"
)

// Lists Kota  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Kota
// @Description  		Lists Kota API calls
// @Tags         		Lookup
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/lookup/list-kota [get]
func GetAllKota(c *fiber.Ctx) error {
	data, err := service.ListKota()
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}

// Lists Country  	godoc
// @Security 				bearerAuth
// @Summary      		Lists Country
// @Description  		Lists Country API calls
// @Tags         		Lookup
// @Accept       		json
// @Consume      		json
// @Produce      		json
// @Success      		200  {object}  helper.Response
// @Success      		500  {object}  helper.EmptyObj
// @Router       		/lookup/list-country [get]
func GetAllCountry(c *fiber.Ctx) error {
	data, err := service.ListCountry()
	if err != nil {
		res := helper.BuildErrorResponse("Failed", err.Error(), helper.EmptyObj{})
		return c.Status(fiber.StatusInternalServerError).JSON(res)
	}
	res := helper.BuildResponse(true, "Success", data)
	return c.Status(fiber.StatusOK).JSON(res)
}
