package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/vcraescu/go-paginator/v2"
	"github.com/vcraescu/go-paginator/v2/adapter"
	"github.com/vcraescu/go-paginator/v2/view"
)

func ListDownload(search string, page string, row string, baseUrl string, status string) (helper.ResponsePagination, error) {
	var download []entity.Download

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("berita as download")
	data.Select("download.*, download_lang.title, download_lang.isi, download_lang.lang")
	data.Joins("join berita_lang as download_lang on download_lang.id = download.id")
	data.Where("download.kategori = ?", "download")
	if status != "" {
		data.Where("download.status = ?", status)
	}
	if search != "" {
		data.Where("lower(download_lang) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Order("download.date_created DESC")
	data.Find(&download)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(download)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&download); err != nil {
		panic(err)
	}

	for i, v := range download {
		path := baseUrl + "/file/download/"
		if v.Imagename != "" {
			download[i].PathImagename = path + v.Imagename
		}
	}

	res := helper.BuildPaginationResponse(download, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func InsertDownload(req entity.DownloadDTO, c *fiber.Ctx) (entity.Download, error) {
	var download entity.Download
	var download_lang entity.DownloadLang
	db := config.Con()
	defer config.CloseCon(db)

	download.DateCreated = time.Now()
	download.Kategori = "download"
	if req.Image != nil {
		filename, _ := helper.UploadFile("./files/download", req.Image, c)
		download.Imagename = filename
	}

	saveDownload := db.Table("berita").Save(&download)

	if saveDownload.Error != nil {
		return entity.Download{}, saveDownload.Error
	}

	download_lang.ID = download.ID
	download_lang.Isi = download.Imagename + ";" + strconv.Itoa(int(req.Image.Size)) + ";" + req.Image.Header["Content-Type"][0]
	download_lang.Lang = req.Lang
	download_lang.Snapshot = req.Title
	download_lang.Title = req.Title

	saveDownloadLang := db.Table("berita_lang").Save(&download_lang)

	if saveDownloadLang.Error != nil {
		return entity.Download{}, saveDownloadLang.Error
	}

	download.Title = download_lang.Title
	download.Isi = download_lang.Isi
	download.Lang = download_lang.Lang

	return download, nil
}

func ShowDownload(id int, baseUrl string) (interface{}, error) {
	var download entity.Download
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("berita as download")
	data.Select("download.*, download_lang.title, download_lang.isi, download_lang.lang, download_lang.id_lang")
	data.Joins("join berita_lang as download_lang on download_lang.id = download.id")
	data.Where("download.id = ?", id)
	data.Take(&download)
	if data.Error != nil {
		return nil, data.Error
	}

	path := baseUrl + "/file/download/"
	if download.Imagename != "" {
		download.PathImagename = path + download.Imagename
	}
	return download, nil
}

func UpdateDownload(req entity.DownloadDTO, old entity.Download, id int, c *fiber.Ctx) (entity.Download, error) {
	var download entity.Download
	var download_lang entity.DownloadLang
	var isi string
	db := config.Con()
	defer config.CloseCon(db)

	download.ID = int64(id)
	download.DateCreated = time.Now()
	download.Kategori = "download"
	if req.Image != nil {
		os.Remove("./files/download/" + old.Imagename)
		filename, _ := helper.UploadFile("./files/download", req.Image, c)
		download.Imagename = filename
		isi = filename + ";" + strconv.Itoa(int(req.Image.Size)) + ";" + req.Image.Header["Content-Type"][0]
	} else {
		download.Imagename = old.Imagename
		isi = old.Isi
	}

	saveDownload := db.Table("berita").Save(&download)

	if saveDownload.Error != nil {
		return entity.Download{}, saveDownload.Error
	}

	// download_lang.IDLang = old.IDLang
	download_lang.ID = download.ID
	download_lang.Isi = isi
	download_lang.Lang = req.Lang
	download_lang.Snapshot = req.Title
	download_lang.Title = req.Title

	saveDownloadLang := db.Table("berita_lang").Save(&download_lang)

	if saveDownloadLang.Error != nil {
		return entity.Download{}, saveDownloadLang.Error
	}

	download.Title = download_lang.Title
	download.Isi = download_lang.Isi
	download.Lang = download_lang.Lang

	return download, nil
}

func UpdateStatusDownload(req entity.DownloadStatusDTO, old entity.Download, id int) (entity.Download, error) {
	db := config.Con()
	defer config.CloseCon(db)

	updateStatus := db.Table("berita").Where("id = ?", id).Update("status", req.Status)
	if updateStatus.Error != nil {
		return entity.Download{}, updateStatus.Error
	}
	old.Status = req.Status
	return old, nil
}
