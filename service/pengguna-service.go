package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
)

func CheckDuplicateUsernamePengguna(username string) bool {
	var pengguna entity.Pengguna

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("pengguna").Where("username = ?", username).Take(&pengguna)
	return !(data.Error == nil)

}

func GetPenggunaByIdPersonal(id_personal int) (res interface{}, err error) {
	var pengguna entity.Pengguna

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("pengguna").Where("id_personal = ?", id_personal).Take(&pengguna)
	if data.Error != nil {
		return nil, data.Error
	}
	return pengguna, nil
}

func ShowPengguna(id_pengguna int) (res interface{}, err error) {
	var pengguna entity.Pengguna

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("pengguna").Where("id_pengguna = ?", id_pengguna).Take(&pengguna)
	if data.Error != nil {
		return nil, data.Error
	}
	return pengguna, nil
}

func InsertPengguna(data entity.Pengguna) (res interface{}, err error) {
	db := config.Con()
	defer config.CloseCon(db)

	save := db.Table("pengguna").Create(&data)
	if save.Error != nil {
		return nil, save.Error
	}
	return data, nil
}

func UpdatePengguna(data entity.Pengguna) (res interface{}, err error) {
	db := config.Con()
	defer config.CloseCon(db)

	save := db.Table("pengguna").Where("id_pengguna = ?", data.IDPengguna).Updates(map[string]interface{}{"password": data.Password, "sts_aktif_pengguna": data.StsAktifPengguna})
	if save.Error != nil {
		return nil, save.Error
	}
	return data, nil
}
