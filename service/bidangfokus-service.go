package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
)

func GetBidangFokus() ([]entity.BidangFokus, error) {
	var res []entity.BidangFokus

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("bidang_fokus")
	data.Order("id_bidang_fokus asc")
	data.Find(&res)
	if data.Error != nil {
		return nil, data.Error
	}
	return res, nil
}
