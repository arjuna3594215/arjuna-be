package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"strconv"
	"strings"
	"time"

	"github.com/vcraescu/go-paginator/v2"
	"github.com/vcraescu/go-paginator/v2/adapter"
	"github.com/vcraescu/go-paginator/v2/view"
)

func ListDraftUsulanAkreditasiJurnal(search string, page string, row string, personal string, baseUrl string) (helper.ResponsePagination, error) {
	var usulan_akreditasi []entity.UsulanAkreditasi

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	progres := db.Table("usulan_akreditasi as ua")
	progres.Select("progres.progres")
	// progres.Where("ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	// progres.Order("ua.id_usulan_akreditasi DESC")
	// progres.Joins("Left JOIN progres_usulan_akreditasi as pua ON pua.id_usulan_akreditasi = ua.id_usulan_akreditasi AND ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	progres.Joins("Left JOIN progres_usulan_akreditasi as pua ON pua.id_usulan_akreditasi = ua.id_usulan_akreditasi")
	progres.Where("ua.id_usulan_akreditasi = temp.id_usulan_akreditasi")
	progres.Joins("Left JOIN progres ON progres.id_progres = pua.id_progres")
	progres.Limit(1)

	nilai_total := db.Table("usulan_akreditasi as ua")
	nilai_total.Select("ha.nilai_total")
	nilai_total.Joins("JOIN hasil_akreditasi as ha on ha.id_usulan_akreditasi = ua.id_usulan_akreditasi")
	nilai_total.Where("ua.id_usulan_akreditasi = temp.id_usulan_akreditasi")
	nilai_total.Limit(1)

	//nilai_total := db.Table("hasil_akreditasi as ha")
	//nilai_total.Select("ha.nilai_total")
	//// nilai_total.Joins("JOIN hasil_akreditasi as ha on ha.id_usulan_akreditasi = ua.id_usulan_akreditasi AND ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	//// nilai_total.Order("ua.id_usulan_akreditasi DESC")
	//nilai_total.Where("ha.id_usulan_akreditasi = ua.id_usulan_akreditasi")
	//nilai_total.Limit(1)

	grade_akreditasi := db.Table("usulan_akreditasi as ua")
	grade_akreditasi.Select("ha.grade_akreditasi")
	grade_akreditasi.Joins("JOIN hasil_akreditasi as ha ON ha.id_usulan_akreditasi = ua.id_usulan_akreditasi")
	grade_akreditasi.Where("ua.id_usulan_akreditasi = temp.id_usulan_akreditasi")
	grade_akreditasi.Limit(1)
	
	status_usulan_akreditasi := db.Raw("SELECT arjuna_check_status_reakreditasi2 as status_usulan_akreditasi FROM arjuna_check_status_reakreditasi2(ua.id_usulan_akreditasi:: INTEGER)")

	data := db.Table("usulan_akreditasi as ua")
	data.Distinct()

	data.Select("ua.*,ua.id_usulan_akreditasi, ua.tgl_updated as tgl_usulan, ij.nama_jurnal, hasil_sa.nilai_total as nilai_total_sa, ij.id_jurnal, j.alamat, ij.frekuensi_terbitan, ij.eissn, ij.pissn, j.tahun_1_terbit as tahun_terbit, j.tgl_akhir_terakreditasi, j.tgl_usulan_akreditasi_terakhir, hasil_sa.grade_akreditasi as grade_akreditasi_sa, (?) as status_usulan_akreditasi", status_usulan_akreditasi)
	//data.Select("ua.*, ij.nama_jurnal, hasil_sa.nilai_total as nilai_total_sa, ij.id_jurnal, j.alamat, ij.frekuensi_terbitan, ij.eissn, ij.pissn, j.tahun_1_terbit as tahun_terbit, j.tgl_akhir_terakreditasi, j.tgl_usulan_akreditasi_terakhir, hasil_sa.grade_akreditasi as grade_akreditasi_sa")

	data.Joins("JOIN pic AS pic ON pic.id_identitas_jurnal = ua.id_identitas_jurnal")
	data.Joins("JOIN identitas_jurnal AS ij ON ij.id_identitas_jurnal = ua.id_identitas_jurnal")
	data.Joins("JOIN jurnal AS j ON j.id_jurnal = ij.id_jurnal")
	data.Joins("Left JOIN hasil_sa ON hasil_sa.id_usulan_akreditasi = ua.id_usulan_akreditasi ")
	// data.Joins("JOIN pengguna ON pengguna.id_personal = pic.id_personal ")
	// data.Joins("JOIN personal ON personal.id_personal = pic.id_personal ")
	data.Where("ua.sts_pengajuan_usulan = ?", "0")
	data.Where("ij.sts_aktif_identitas_jurnal = ?", "1")
	if personal != "" {
		data.Where("pic.id_personal = ?", personal)
	}
	if search != "" {
		data.Where("lower(ij.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	}

	dataTemp := db.Table("(?) as temp", data)
	//dataTemp.Select("temp.*, (?) as progres, (?) as nilai_total, (?) as grade_akreditasi", progres, nilai_total, grade_akreditasi)
	dataTemp.Select("temp.*, (?) as progres, (?) as nilai_total, (?) as grade_akreditasi ", progres, nilai_total, grade_akreditasi)
	dataTemp.Order("temp.id_usulan_akreditasi DESC")
	data.Find(&usulan_akreditasi)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(usulan_akreditasi)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&usulan_akreditasi); err != nil {
		panic(err)
	}

	for i, v := range usulan_akreditasi {
		if v.GradeAkreditasiSa == "A" {
			usulan_akreditasi[i].GradeAkreditasiSa = "A"
		} else if v.GradeAkreditasiSa == "B" {
			usulan_akreditasi[i].GradeAkreditasiSa = "B"
		} else if v.GradeAkreditasiSa == "" {
			usulan_akreditasi[i].GradeAkreditasiSa = "Blm dilakukan"
		} else {
			usulan_akreditasi[i].GradeAkreditasiSa = "Tdk terakreditasi"
		}
		if v.NilaiTotalSa >= 85 {
			usulan_akreditasi[i].PeringkatGradeAkreditasiSa = "Peringkat 1"
		} else if v.NilaiTotalSa >= 70 {
			usulan_akreditasi[i].PeringkatGradeAkreditasiSa = "Peringkat 2"
		} else if v.NilaiTotalSa >= 60 {
			usulan_akreditasi[i].PeringkatGradeAkreditasiSa = "Peringkat 3"
		} else if v.NilaiTotalSa >= 50 {
			usulan_akreditasi[i].PeringkatGradeAkreditasiSa = "Peringkat 4"
		} else if v.NilaiTotalSa >= 40 {
			usulan_akreditasi[i].PeringkatGradeAkreditasiSa = "Peringkat 5"
		} else if v.NilaiTotalSa >= 30 {
			usulan_akreditasi[i].PeringkatGradeAkreditasiSa = "Peringkat 6"
		} else {
			usulan_akreditasi[i].PeringkatGradeAkreditasiSa = "Belum dilakukan"
		}
		//edit by agus 2023-04-08
		 if v.Progres == "" {
			//usulan_akreditasi[i].Progres = "Belum pernah diajukan"
			usulan_akreditasi[i].TglUsulanAkreditasiTerakhir = "Belum pernah diajukan"
		}
		 
		if v.StatusUsulanAkreditasi == true {
			usulan_akreditasi[i].Progres = "Re-Akreditasi"
		} else {
			usulan_akreditasi[i].Progres = "Usulan Baru"
		}
	}

	res := helper.BuildPaginationResponse(usulan_akreditasi, view)
	res.PerPage = rows
	res.Total = total
	return res, nil

}

func ShowUsulanAkreditasi(id_usulan_akreditasi int) interface{} {
	var usulan_akreditasi entity.UsulanAkreditasi

	db := config.Con()
	defer config.CloseCon(db)

	//check_sts_akreditasi := db.Raw("SELECT * FROM arjuna_check_status_reakreditasi2(ua.id_usulan_akreditasi)")
	//
	//data := db.Table("usulan_akreditasi as ua")
	//data.Select("ua.*, ij.nama_jurnal, ij.id_jurnal, ij.frekuensi_terbitan, ij.pissn, ij.eissn ( ? ) as sts_akreditasi", check_sts_akreditasi)
	//data.Joins("JOIN identitas_jurnal AS ij ON ij.id_identitas_jurnal = ua.id_identitas_jurnal")
	//data.Where("ua.id_usulan_akreditasi = ?", id_usulan_akreditasi)
	
	//edit by agust 2023-04-09
	//data := db.Raw("SELECT ua.*, ij.nama_jurnal, ij.id_jurnal, ij.frekuensi_terbitan, ij.pissn, ij.eissn, arjuna_check_status_reakreditasi2(ua.id_usulan_akreditasi) as sts_akreditasi FROM usulan_akreditasi as ua JOIN identitas_jurnal AS ij ON ij.id_identitas_jurnal = ua.id_identitas_jurnal WHERE ua.id_usulan_akreditasi = ?", id_usulan_akreditasi)
	data := db.Raw("SELECT ua.*, ij.nama_jurnal, ij.id_jurnal, ij.frekuensi_terbitan, ij.pissn, ij.eissn,arjuna_check_status_reakreditasi2(ua.id_usulan_akreditasi) as sts_akreditasi, arjuna_check_status_reakreditasi2(ua.id_usulan_akreditasi) as status_usulan_akreditasi FROM usulan_akreditasi as ua JOIN identitas_jurnal AS ij ON ij.id_identitas_jurnal = ua.id_identitas_jurnal WHERE ua.id_usulan_akreditasi = ?", id_usulan_akreditasi)

	data.Limit(1)
	//data.Where("ua.id_usulan_akreditasi = ?", id_usulan_akreditasi)
	data.Take(&usulan_akreditasi)

	if data.Error != nil {
		return nil
	}
	return usulan_akreditasi
	data.Take(&usulan_akreditasi)

	if data.Error != nil {
		return nil
	}
	return usulan_akreditasi
}

func CheckAkreditasiAktif(id_identitas_jurnal int64) (bool, string) {
	var usulan_akreditasi entity.UsulanAkreditasi
	var count_penetapan_akreditasi int64
	var count_penetapan_desk_evaluasi int64

	db := config.Con()
	defer config.CloseCon(db)

	checkUsulanAkreditasi := db.Table("usulan_akreditasi as ua")
	checkUsulanAkreditasi.Where("ua.id_identitas_jurnal = ?", id_identitas_jurnal)
	checkUsulanAkreditasi.Order("ua.id_usulan_akreditasi desc")
	checkUsulanAkreditasi.Take(&usulan_akreditasi)

	if checkUsulanAkreditasi.Error != nil {
		return true, ""
	}

	if usulan_akreditasi.StsPengajuanUsulan == "0" {
		return false, "The submitted accreditation on behalf of the journal already exists."
	} else {
		penetapanAkreditasi := db.Table("penetapan_akreditasi")
		penetapanAkreditasi.Where("id_usulan_akreditasi = ?", usulan_akreditasi.IdUsulanAkreditasi)
		penetapanAkreditasi.Count(&count_penetapan_akreditasi)

		penetapanDeskEvaliasi := db.Table("penetapan_desk_evaluasi")
		penetapanDeskEvaliasi.Where("id_usulan_akreditasi = ?", usulan_akreditasi.IdUsulanAkreditasi)
		penetapanDeskEvaliasi.Where("sts_hasil_desk_evaluasi = ?", "0")
		penetapanDeskEvaliasi.Count(&count_penetapan_desk_evaluasi)

		if count_penetapan_akreditasi > 0 {
			// jika ada , berarti usulan_akreditasi yang lama sudah selesai/tidak aktif
			return true, ""
		} else if count_penetapan_desk_evaluasi > 0 {
			// sedang dinilai
			return true, ""
		} else {
			// jika tidak, berarti usulan_akreditasi yang masih dalam proses penilaian
			return false, "The submitted accreditation on behalf of the journal is in the process of being assessed" // sedang dinilai
		}
	}
}

func CheckNilaiEvaluasiDiri(id_usulan_akreditasi int) bool {
	var hasil_sa entity.HasilSa

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("hasil_sa").Where("id_usulan_akreditasi = ?", id_usulan_akreditasi).Take(&hasil_sa)
	return (data.Error == nil)
}

func CheckIssueJurnal(id_usulan_akreditasi int) bool {
	var issue entity.Issue

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("issue").Where("id_usulan_akreditasi = ?", id_usulan_akreditasi).Take(&issue)
	return (data.Error == nil)
}

func InsertUsulanAkreditasiJurnal(req entity.SaveUsulanAkreditasiDTO, id_pic int64, id_eic int64) (interface{}, error) {
	var usulan_akreditasi entity.UsulanAkreditasi

	db := config.Con()
	defer config.CloseCon(db)

	usulan_akreditasi.IdIdentitasJurnal = req.IdIdentitasJurnal
	usulan_akreditasi.IdPic = id_pic
	usulan_akreditasi.IdEic = id_eic
	usulan_akreditasi.UserPenilai = req.UserPenilai
	usulan_akreditasi.PasswdPenilai = req.PasswdPenilai

	save := db.Table("usulan_akreditasi").Create(&usulan_akreditasi)
	if save.Error != nil {
		return nil, save.Error
	}
	return usulan_akreditasi, nil
}

func UpdateUsulanAkreditasiJurnal(req entity.UpdateUsulanAkreditasiDTO, id_usulan_akreditasi int) (interface{}, error) {
	db := config.Con()
	defer config.CloseCon(db)

	update := db.Table("usulan_akreditasi").Where("id_usulan_akreditasi = ?", id_usulan_akreditasi).Updates(map[string]interface{}{"user_penilai": req.UserPenilai, "passwd_penilai": req.PasswdPenilai, "tgl_updated": time.Now()})
	if update.Error != nil {
		return nil, update.Error
	}
	return true, nil
}

func DeleteUsulanAkreditasiJurnal(req entity.UsulanAkreditasi, id_usulan_akreditasi int) (interface{}, error) {
	db := config.Con()
	defer config.CloseCon(db)

	delete := db.Table("usulan_akreditasi").Where("id_usulan_akreditasi = ?", id_usulan_akreditasi).Delete(&entity.UsulanAkreditasi{})
	if delete.Error != nil {
		return nil, delete.Error
	}
	return true, nil
}

func SiapAkreditasiJurnal(req entity.UsulanAkreditasi) error {
	var progres_usulan_akreditasi entity.ProgresUsulanAkreditasi
	var pengajuan_usulan_akreditasi entity.PengajuanUsulanAkreditasi
	var resubmit entity.Resubmit
	var check_pua int64
	var check_resubmit int64

	db := config.Con()
	defer config.CloseCon(db)

	update_usulan_akreditasi := db.Model(&entity.UsulanAkreditasi{}).Table("usulan_akreditasi").Where("id_usulan_akreditasi = ?", req.IdUsulanAkreditasi).Updates(map[string]interface{}{"sts_pengajuan_usulan": "1", "tgl_updated": time.Now()})
	if update_usulan_akreditasi.Error != nil {
		return update_usulan_akreditasi.Error
	}
	update_jurnal := db.Model(&entity.Jurnal{}).Table("jurnal").Where("id_jurnal = ?", req.IdJurnal).Updates(map[string]interface{}{"tgl_usulan_akreditasi_terakhir": time.Now(), "tgl_updated": time.Now()})
	if update_jurnal.Error != nil {
		return update_jurnal.Error
	}

	db.Table("progres_usulan_akreditasi").Where("id_usulan_akreditasi = ?", req.IdUsulanAkreditasi).Count(&check_pua)
	if check_pua > 0 {
		update_pua := db.Model(&entity.ProgresUsulanAkreditasi{}).Table("progres_usulan_akreditasi").Where("id_usulan_akreditasi = ?", req.IdUsulanAkreditasi).Updates(map[string]interface{}{"id_progres": "1", "tgl_updated": time.Now()})
		if update_pua.Error != nil {
			return update_pua.Error
		}
	} else {
		progres_usulan_akreditasi.IdUsulanAkreditasi = req.IdUsulanAkreditasi
		progres_usulan_akreditasi.IdProgres = 1
		progres_usulan_akreditasi.TglCreated = time.Now()
		save_pua := db.Table("progres_usulan_akreditasi").Create(&progres_usulan_akreditasi)
		if save_pua.Error != nil {
			return save_pua.Error
		}
	}

	//check resubmit
	db.Table("resubmit").Where("id_usulan_akreditasi = ?", req.IdUsulanAkreditasi).Where("kd_sts_resubmit = ?", "1").Count(&check_resubmit)
	if check_resubmit > 0 {
		db.Table("resubmit").Where("id_usulan_akreditasi = ?", req.IdUsulanAkreditasi).Where("kd_sts_resubmit = ?", "1").Order("tgl_created").Take(&resubmit)
		pengajuan_usulan_akreditasi.IdResubmit = resubmit.IdResubmit
		pengajuan_usulan_akreditasi.TglPengajuan = time.Now()
		pengajuan_usulan_akreditasi.IdUsulanAkreditasi = req.IdUsulanAkreditasi
		save_pea := db.Table("pengajuan_usulan_akreditasi").Create(&pengajuan_usulan_akreditasi)
		if save_pea.Error != nil {
			return save_pea.Error
		}
		update_resubmit := db.Table("resubmit").Where("id_usulan_akreditasi = ?", req.IdUsulanAkreditasi).Updates(map[string]interface{}{"kd_sts_resubmit": "0", "tgl_updated": time.Now()})
		if update_resubmit.Error != nil {
			return update_resubmit.Error
		}
	} else {
		pengajuan_usulan_akreditasi.TglPengajuan = time.Now()
		pengajuan_usulan_akreditasi.IdUsulanAkreditasi = req.IdUsulanAkreditasi
		save_pea := db.Table("pengajuan_usulan_akreditasi").Create(&pengajuan_usulan_akreditasi)
		if save_pea.Error != nil {
			return save_pea.Error
		}
	}

	update_epb := db.Table("eksepsi_pengajuan_baru").Where("id_identitas_jurnal = ?", req.IdIdentitasJurnal).Where("kd_sts_eksepsi = ?", "0").Updates(map[string]interface{}{"kd_sts_eksepsi": "1", "tgl_eksekusi_eksepsi": time.Now(), "tgl_updated": time.Now()})
	if update_epb.Error != nil {
		return update_epb.Error
	}

	update_uadt := db.Table("usulan_akreditasi_ditolak").Where("id_usulan_akreditasi = ?", req.IdUsulanAkreditasi).Updates(map[string]interface{}{"kd_sts_aktif": "0", "tgl_updated": time.Now()})
	if update_uadt.Error != nil {
		return update_uadt.Error
	}

	delete_pde := db.Table("penetapan_desk_evaluasi").Where("id_usulan_akreditasi = ?", req.IdUsulanAkreditasi).Delete(&entity.PenetapanDeskEvaluasi{})
	if delete_pde.Error != nil {
		return delete_pde.Error
	}

	return nil
}

func ListsKemajuanUsulanAkreditasi(id_personal int) ([]entity.FrontPageJurnalProgres, error) {
	var jurnal []entity.FrontPageJurnalProgres

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("usulan_akreditasi AS t1 ")
	data.Select("t1.id_usulan_akreditasi,t3.id_identitas_jurnal, t3.eissn, t3.pissn, t1.tgl_created AS tgl_created_pengajuan_dlm_progress, t1.tgl_updated AS tgl_updated_pengajuan_dlm_progress, t4.nilai_total AS nilai_total_sa, t4.grade_akreditasi AS grade_akreditasi_sa, t6.sts_hasil_desk_evaluasi, t6.tgl_updated AS tgl_penetapan_deskevaluasi, t9.nilai_total, t9.grade_akreditasi, t7.sts_hasil_akreditasi, t7.tgl_created AS tgl_penetapan_akreditasi, t8.no_sk, t8.judul_sk, t8.tgl_sk, t8.tgl_updated AS tgl_pembuatan_sk, t5.tgl_usulan_akreditasi_terakhir, t3.nama_jurnal")
	data.Joins("INNER JOIN pic AS t2 ON t2.id_identitas_jurnal = t1.id_identitas_jurnal")
	data.Joins("INNER JOIN identitas_jurnal AS t3 ON t3.id_identitas_jurnal = t1.id_identitas_jurnal")
	data.Joins("LEFT JOIN hasil_sa AS t4 ON t4.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("INNER JOIN jurnal AS t5 ON t5.id_jurnal = t3.id_jurnal")

	data.Joins("LEFT JOIN penetapan_desk_evaluasi AS t6 ON t6.id_usulan_akreditasi = t1.id_usulan_akreditasi")

	data.Joins("LEFT JOIN penetapan_akreditasi t7 ON t7.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("LEFT JOIN sk_akreditasi t8 ON t8.id_sk_akreditasi = t7.id_sk_akreditasi AND t8.sts_published = ?", "1")
	data.Joins("LEFT JOIN hasil_akreditasi t9 ON t9.id_hasil_akreditasi = t7.id_hasil_akreditasi ")

	data.Where("t3.sts_aktif_identitas_jurnal = ?", "1")
	data.Where("t1.sts_pengajuan_usulan = ?", "1")
	data.Where("t2.id_personal = ?", id_personal)
	data.Order("t1.tgl_updated DESC")
	data.Find(&jurnal)

	for i, v := range jurnal {
		if v.NilaiTotalSa >= 85 {
			jurnal[i].GradeAkreditasiSa = "Peringkat 1"
		} else if v.NilaiTotalSa >= 70 {
			jurnal[i].GradeAkreditasiSa = "Peringkat 2"
		} else if v.NilaiTotalSa >= 60 {
			jurnal[i].GradeAkreditasiSa = "Peringkat 3"
		} else if v.NilaiTotalSa >= 50 {
			jurnal[i].GradeAkreditasiSa = "Peringkat 4"
		} else if v.NilaiTotalSa >= 40 {
			jurnal[i].GradeAkreditasiSa = "Peringkat 5"
		} else if v.NilaiTotalSa >= 30 {
			jurnal[i].GradeAkreditasiSa = "Peringkat 6"
		} else if v.GradeAkreditasiSa == "" {
			jurnal[i].GradeAkreditasiSa = "Blm dilakukan"
		} else {
			jurnal[i].GradeAkreditasiSa = "Tdk terakreditasi"
		}

		if v.StsHasilDeskEvaluasi == "1" {
			jurnal[i].HasilDeskEvaluasi = "Lolos desk evaluasi"
		} else if v.StsHasilDeskEvaluasi == "0" {
			jurnal[i].HasilDeskEvaluasi = "Tdk lolos desk evaluasi"
		} else {
			jurnal[i].HasilDeskEvaluasi = "Dlm proses penilaian"
		}
		jurnal[i].DataPenugasanPenerimaanManajemen = getPenugasanPenerimaanManajemen(int(v.IdUsulanAkreditasi))
		jurnal[i].DataPenugasanPenerimaanEvaluator = getPenugasanPenerimaanEvaluator(int(v.IdUsulanAkreditasi))
	}

	if data.Error != nil {
		return nil, data.Error
	}

	return jurnal, nil
}

func ListsHasilPenilaianAkreditasi(page string, row string, id_personal int) (helper.ResponsePagination, error) {
	var hasil_penilaian_akreditasi []entity.HasilPenilaianAkreditasi

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("progres_usulan_akreditasi as t1")
	//data.Select("t1.id_usulan_akreditasi, t2.id_identitas_jurnal, t2.id_pic, t2.id_eic, t4.nama_jurnal, t4.eissn, t4.pissn, t10.url_statistik_pengunjung, t10.url_jurnal, t10.url_contact, t10.url_editor, t4.alamat_oai, t4.doi_jurnal, t2.tgl_updated::date as tgl_updated_usulan, t5.grade_akreditasi as grade_akreditasi_hasil_sa, t8.nilai_total as nilai_total_akreditasi, t8.grade_akreditasi, t7.tgl_updated::date as tgl_pencatatan_sk, t6.sts_hasil_akreditasi, t9.sts_hasil_desk_evaluasi, t9.tgl_updated::date as tgl_penetapan_deskevaluasi, t11.alasan_penolakan as komentar_penolakan")
	data.Select("t1.id_usulan_akreditasi, t2.id_identitas_jurnal, t2.id_pic, t2.id_eic, t2.tgl_updated as tgl_diusulkan, t4.nama_jurnal, t4.eissn, t4.pissn, t10.url_statistik_pengunjung, t10.url_jurnal, t10.url_contact, t10.url_editor, t4.alamat_oai, t4.doi_jurnal, t2.tgl_updated::date as tgl_updated_usulan, t5.grade_akreditasi as grade_akreditasi_hasil_sa, t8.nilai_total as nilai_total_akreditasi, t8.grade_akreditasi, t7.tgl_updated::date as tgl_pencatatan_sk, t6.sts_hasil_akreditasi, t9.sts_hasil_desk_evaluasi, t9.tgl_updated::date as tgl_penetapan_deskevaluasi, t11.alasan_penolakan as komentar_penolakan")
	data.Joins("INNER JOIN usulan_akreditasi t2 ON t1.id_usulan_akreditasi = t2.id_usulan_akreditasi")
	data.Joins("INNER JOIN pic t3 ON t3.id_identitas_jurnal = t2.id_identitas_jurnal")
	data.Joins("INNER JOIN identitas_jurnal t4 ON t4.id_identitas_jurnal = t2.id_identitas_jurnal")
	data.Joins("INNER JOIN hasil_sa t5 ON t1.id_usulan_akreditasi = t5.id_usulan_akreditasi")
	data.Joins("LEFT JOIN penetapan_akreditasi t6 ON t1.id_usulan_akreditasi = t6.id_usulan_akreditasi")
	data.Joins("LEFT JOIN sk_akreditasi t7 ON t6.id_sk_akreditasi = t7.id_sk_akreditasi AND t7.sts_published = '1'")
	data.Joins("LEFT JOIN hasil_akreditasi t8 ON t6.id_hasil_akreditasi = t8.id_hasil_akreditasi")
	data.Joins("INNER JOIN penetapan_desk_evaluasi t9 ON t1.id_usulan_akreditasi = t9.id_usulan_akreditasi")
	data.Joins("INNER JOIN jurnal t10 ON t4.id_jurnal = t10.id_jurnal")
	data.Joins("LEFT JOIN usulan_akreditasi_ditolak as t11 ON t11.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Where("t1.id_progres >= ?", "3")
	data.Where("t2.sts_pengajuan_usulan = ?", "1")
	data.Where("t3.id_personal = ?", id_personal)
	data.Where("t4.sts_aktif_identitas_jurnal = ?", "1")
	// data.Where("t7.sts_published = ?", "1")
	data.Order("t4.nama_jurnal ASC")
	data.Find(&hasil_penilaian_akreditasi)

	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(hasil_penilaian_akreditasi)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&hasil_penilaian_akreditasi); err != nil {
		panic(err)
	}

	for i, v := range hasil_penilaian_akreditasi {
		if v.GradeAkreditasiHasilAa == "A" {
			hasil_penilaian_akreditasi[i].GradeAkreditasiSa = "1"
		} else if v.GradeAkreditasiHasilAa == "B" {
			hasil_penilaian_akreditasi[i].GradeAkreditasiSa = "2"
		} else if v.GradeAkreditasiHasilAa == "" {
			hasil_penilaian_akreditasi[i].GradeAkreditasiSa = "Blm dilakukan"
		} else {
			hasil_penilaian_akreditasi[i].GradeAkreditasiSa = v.GradeAkreditasi
		}

		if v.GradeAkreditasi == "A" {
			hasil_penilaian_akreditasi[i].GradeAkreditasi = "1"
		} else if v.GradeAkreditasi == "B" {
			hasil_penilaian_akreditasi[i].GradeAkreditasi = "2"
		} else if v.GradeAkreditasi == "" {
			hasil_penilaian_akreditasi[i].GradeAkreditasi = "Blm dilakukan"
		} else {
			hasil_penilaian_akreditasi[i].GradeAkreditasi = v.GradeAkreditasi
		}

		if v.StsHasilDeskEvaluasi == "1" {
			hasil_penilaian_akreditasi[i].HasilDeskEvaluasi = "Lolos desk evaluasi"
		} else if v.StsHasilDeskEvaluasi == "0" {
			hasil_penilaian_akreditasi[i].HasilDeskEvaluasi = "Tdk Lolos desk evaluasi"
		} else if v.StsHasilDeskEvaluasi == "" {
			hasil_penilaian_akreditasi[i].HasilDeskEvaluasi = "Dlm proses penilaian"
		}

	}

	res := helper.BuildPaginationResponse(hasil_penilaian_akreditasi, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}
