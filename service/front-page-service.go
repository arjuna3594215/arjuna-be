package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"strconv"
	"strings"

	"github.com/vcraescu/go-paginator/v2"
	"github.com/vcraescu/go-paginator/v2/adapter"
	"github.com/vcraescu/go-paginator/v2/view"
)

func FrontPageListSlider(baseUrl string) (resdata []entity.FrontPageSlider, err error) {
	var slider []entity.FrontPageSlider

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("sliders").Find(&slider)

	if data.Error != nil {
		return nil, err
	}
	for i, v := range slider {
		slider[i].Imagename = baseUrl + "/file/slider/" + v.Imagename
	}

	return slider, nil
}

// ======================================================= Berita ========================================================

func ListBeritaFrontPage(row string, page string, search string, kategori string, lang string, baseUrl string) (resData helper.ResponsePagination, err error) {
	var berita []entity.FrontPageBerita
	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("berita as t1")
	data.Select("t1.id, t2.title, t2.snapshot, t2.isi, t1.date_created, t1.imagename, t1.torder,t1.most_viewed, t1.kategori, t1.lampiran1, t1.lampiran2, t1.lampiran3")
	data.Joins("left join berita_lang as t2 on t1.id = t2.id")
	data.Where("t1.status = ?", 1)
	data.Where("t2.lang = ?", lang)
	if search != "" {
		data.Where("lower(t2.title) LIKE ?", "%"+strings.ToLower(search)+"%")
		// data.Or("t2.isi LIKE ?", "%"+search+"%")
	}
	if kategori != "semua" {
		data.Where("t1.kategori = ?", kategori)
	} else {
		data.Where("(t1.kategori) IN ?", [][]interface{}{{"berita"}, {"pengumuman"}})
	}
	data.Order("t1.date_created desc")
	data.Find(&berita)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}
	total := len(berita)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&berita); err != nil {
		return helper.ResponsePagination{}, data.Error
	}
	for i, v := range berita {
		var path string
		if v.Kategori == "berita" {
			path = baseUrl + "/file/berita/"
		} else {
			path = baseUrl + "/file/info/"
		}
		if v.Imagename != "" {
			berita[i].Imagename = path + v.Imagename
		}
		if v.Lampiran1 != "" {
			berita[i].Lampiran1 = path + v.Lampiran1
		}
		if v.Lampiran2 != "" {
			berita[i].Lampiran2 = path + v.Lampiran2
		}
		if v.Lampiran3 != "" {
			berita[i].Lampiran3 = path + v.Lampiran3
		}
	}
	res := helper.BuildPaginationResponse(berita, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func ListPopularBeritaFrontPage(row string, lang string, baseUrl string) (res []entity.FrontPageBerita, err error) {
	var berita []entity.FrontPageBerita
	var interval_mostpopular int64
	rows, _ := strconv.Atoi(row)
	db := config.Con()
	defer config.CloseCon(db)

	db.Table("configurasi").Select("interval_mostpopular").Where("sts_aktif_configurasi = ?", "1").Scan(&interval_mostpopular)

	data := db.Table("berita as t1")
	data.Select("t1.id, t2.title, t2.snapshot, t2.isi, t1.date_created, t1.imagename, t1.torder,t1.most_viewed, t1.kategori, t1.lampiran1, t1.lampiran2, t1.lampiran3")
	data.Joins("inner join berita_lang as t2 on t1.id = t2.id")
	// data.Where("t1.kategori IN ?", [][]interface{}{{"berita"}, {"pengumuman"}})
	data.Where("t1.status = ?", "1")
	data.Where("t2.lang = ?", lang)
	data.Where("t1.kategori = ?", "berita")
	data.Or("t1.kategori = ?", "pengumuman")
	data.Where("t1.date_created > ( now()::date - " + strconv.Itoa(int(interval_mostpopular)) + " )")
	data.Limit(rows)
	data.Order("t1.most_viewed desc")
	data.Find(&berita)
	for i, v := range berita {
		var path string
		if v.Kategori == "berita" {
			path = baseUrl + "/file/berita/"
		} else {
			path = baseUrl + "/file/info/"
		}
		if v.Imagename != "" {
			berita[i].Imagename = path + v.Imagename
		}
		if v.Lampiran1 != "" {
			berita[i].Lampiran1 = path + v.Lampiran1
		}
		if v.Lampiran2 != "" {
			berita[i].Lampiran2 = path + v.Lampiran2
		}
		if v.Lampiran3 != "" {
			berita[i].Lampiran3 = path + v.Lampiran3
		}
	}
	if data.Error != nil {
		return nil, data.Error
	}
	return berita, nil
}

func ShowBeritaFrontPage(id int, baseUrl string) (res interface{}, err error) {
	var berita entity.FrontPageBerita
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("berita as t1")
	data.Select("t1.id, t2.title, t2.snapshot, t2.isi, t1.date_created, t1.imagename, t1.torder,t1.most_viewed, t1.kategori, t1.lampiran1, t1.lampiran2, t1.lampiran3")
	data.Joins("left join berita_lang as t2 on t1.id = t2.id")
	data.Where("t1.id = ?", id)
	data.Take(&berita)
	var path string
	if berita.Kategori == "berita" {
		path = baseUrl + "/file/berita/"
	} else {
		path = baseUrl + "/file/info/"
	}
	if berita.Imagename != "" {
		berita.Imagename = path + berita.Imagename
	}
	if berita.Lampiran1 != "" {
		berita.Lampiran1 = path + berita.Lampiran1
	}
	if berita.Lampiran2 != "" {
		berita.Lampiran2 = path + berita.Lampiran2
	}
	if berita.Lampiran3 != "" {
		berita.Lampiran3 = path + berita.Lampiran3
	}
	if data.Error != nil {
		return nil, data.Error
	}
	most_viewed := berita.MostViewed + 1
	db.Table("berita").Where("id = ?", id).Updates(map[string]interface{}{"most_viewed": most_viewed})
	return berita, nil
}

// ======================================================= Jurnal ========================================================

func GetStatistikJurnalFrontPageBarChart(types string, fromyear string, toyear string) (interface{}, error) {
	var res interface{}
	toyearint, _ := strconv.Atoi(toyear)
	plus1year := toyearint + 1
	if types == "1" || types == "" {
		res = barChart(getStatistikBidangIlmuUsulanTerbanyak(fromyear, strconv.Itoa(plus1year)), "Bidang Ilmu Dengan Usulan Terbanyak")
	} else if types == "2" {
		res = barChart(getStatistikBidangIlmuTerakreditasiTerbanyak(fromyear, strconv.Itoa(plus1year)), "Bidang Ilmu Dengan Akreditasi Terbanyak")
	} else if types == "3" {
		res = barChart(getStatistikInstitusiUsulanJurnalTerbanyak(fromyear, strconv.Itoa(plus1year)), "Institusi Dengan Usulan Jurnal Terbanyak")
	} else if types == "4" {
		res = barChart(getStatistikInstitusiUsulanTerakreditasiTerbanyak(fromyear, strconv.Itoa(plus1year)), "Institusi Dengan Jurnal Terakreditasi Terbanyak")
	}
	return res, nil
}

func GetStatistikJurnalFrontPagePolarChart(types string, fromyear string, toyear string) (interface{}, error) {
	var res interface{}
	toyearint, _ := strconv.Atoi(toyear)
	plus1year := toyearint + 1
	if types == "1" || types == "" {
		res = polarChart(getStatistikBidangIlmuUsulanTerbanyak(fromyear, strconv.Itoa(plus1year)), "Bidang Ilmu Dengan Usulan Terbanyak")
	} else if types == "2" {
		res = polarChart(getStatistikBidangIlmuTerakreditasiTerbanyak(fromyear, strconv.Itoa(plus1year)), "Bidang Ilmu Dengan Akreditasi Terbanyak")
	} else if types == "3" {
		res = polarChart(getStatistikInstitusiUsulanJurnalTerbanyak(fromyear, strconv.Itoa(plus1year)), "Institusi Dengan Usulan Jurnal Terbanyak")
	} else if types == "4" {
		res = polarChart(getStatistikInstitusiUsulanTerakreditasiTerbanyak(fromyear, strconv.Itoa(plus1year)), "Institusi Dengan Jurnal Terakreditasi Terbanyak")
	}
	return res, nil
}

func GetStatistikJurnalFrontPageCount(types string, fromyear string, toyear string) (interface{}, error) {
	var res interface{}
	toyearint, _ := strconv.Atoi(toyear)
	plus1year := toyearint + 1
	if types == "1" || types == "" {
		res = sumCountChart(getStatistikBidangIlmuUsulanTerbanyak(fromyear, strconv.Itoa(plus1year)))
	} else if types == "2" {
		res = sumCountChart(getStatistikBidangIlmuTerakreditasiTerbanyak(fromyear, strconv.Itoa(plus1year)))
	} else if types == "3" {
		res = sumCountChart(getStatistikInstitusiUsulanJurnalTerbanyak(fromyear, strconv.Itoa(plus1year)))
	} else if types == "4" {
		res = sumCountChart(getStatistikInstitusiUsulanTerakreditasiTerbanyak(fromyear, strconv.Itoa(plus1year)))
	}
	return res, nil
}

func ListJurnalFrontPage(search string, page string, row string, order string, baseUrl string) (l_jurnal helper.ResponsePagination, err error) {
	var jurnal []entity.FrontPageListJurnal
	pages, _ := strconv.Atoi(page)
	rows, _ := strconv.Atoi(row)

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("pic").Distinct()
	data.Select("ij.eissn, ij.id_identitas_jurnal, j.id_jurnal, j.nama_awal_jurnal, ij.nama_jurnal, ij.publisher, j.tgl_pembuatan, j.url_jurnal, pa.url_sinta, j.tgl_created, j.url_contact, j.url_editor, ij.image")
	// data.Joins("join pengguna as p on p.id_personal = pic.id_personal")
	data.Joins("join identitas_jurnal as ij on ij.id_identitas_jurnal = pic.id_identitas_jurnal")
	data.Joins("join jurnal as j on j.id_jurnal = ij.id_jurnal")
	data.Joins("left join penetapan_akreditasi as pa on pa.id_identitas_jurnal = ij.id_identitas_jurnal")
	data.Where("pic.sts_aktif_pic = ?", "1")
	data.Where("ij.sts_aktif_identitas_jurnal = ?", "1")
	if search != "" {
		data.Where("lower(ij.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
		data.Or("ij.eissn = ?", search)
	}
	data.Order("j.tgl_created " + order + "")
	data.Find(&jurnal)

	if err != nil {
		return helper.ResponsePagination{}, err
	}

	total := len(jurnal)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&jurnal); err != nil {
		panic(err)
	}

	for i, v := range jurnal {
		if v.Image != "" {
			jurnal[i].Image = baseUrl + "/file/logo/" + v.Image
		} else {
			jurnal[i].Image = baseUrl + "/file/logo/no_image.jpg"
		}
	}

	res := helper.BuildPaginationResponse(jurnal, view)
	res.PerPage = rows
	res.Total = total

	return res, nil
}

func DetailJurnalFrontPage(id int, baseUrl string) (d_jurnal interface{}, err error) {
	var jurnal entity.FrontPageListJurnal

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("jurnal as j").Distinct()
	data.Select("ij.eissn, ij.id_identitas_jurnal, j.id_jurnal, j.nama_awal_jurnal, ij.nama_jurnal, ij.publisher, j.tgl_pembuatan, j.url_jurnal, pa.url_sinta, j.tgl_created, j.url_contact, j.url_editor, ij.image")
	data.Joins("join identitas_jurnal as ij on ij.id_jurnal = j.id_jurnal")
	data.Joins("left join penetapan_akreditasi as pa on pa.id_identitas_jurnal = ij.id_identitas_jurnal")
	data.Where("ij.id_identitas_jurnal = ?", id)
	data.Take(&jurnal)
	if data.Error != nil {
		return nil, err
	}
	if jurnal.Image != "" {
		jurnal.Image = baseUrl + "/file/logo/" + jurnal.Image
	} else {
		jurnal.Image = baseUrl + "/file/logo/no_image.jpg"
	}
	return jurnal, nil
}

func JurnalProgresFrontPage(id int) (p_jurnal entity.FrontPageJurnalProgres, err error) {
	var jurnal entity.FrontPageJurnalProgres

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("usulan_akreditasi as ua")
	data.Select("ua.id_usulan_akreditasi, ij.id_identitas_jurnal, ij.eissn, ij.pissn, ua.tgl_created AS tgl_created_pengajuan_dlm_progress, ua.tgl_updated as tgl_updated_pengajuan_dlm_progress, hs.nilai_total as nilai_total_sa, hs.grade_akreditasi as grade_akreditasi_sa, pde.sts_hasil_desk_evaluasi, pde.tgl_updated as tgl_penetapan_deskevaluasi, ha.nilai_total, ha.grade_akreditasi, pa.sts_hasil_akreditasi, pa.tgl_created as tgl_penetapan_akreditasi, sa.no_sk, sa.judul_sk, sa.tgl_sk, sa.tgl_updated as tgl_pembuatan_sk, j.tgl_usulan_akreditasi_terakhir")
	data.Joins("inner join identitas_jurnal as ij on ij.id_identitas_jurnal = ua.id_identitas_jurnal")
	data.Joins("left join hasil_sa as hs on hs.id_usulan_akreditasi = ua.id_usulan_akreditasi")
	data.Joins("inner join jurnal as j on j.id_jurnal = ij.id_jurnal")

	data.Joins("left join penetapan_desk_evaluasi as pde on pde.id_identitas_jurnal = ij.id_identitas_jurnal")

	data.Joins("left join penetapan_akreditasi as pa on pa.id_identitas_jurnal = ij.id_identitas_jurnal")
	data.Joins("left join sk_akreditasi as sa on sa.id_sk_akreditasi = pa.id_sk_akreditasi AND sa.sts_published = ?", "1")
	data.Joins("left join hasil_akreditasi as ha on ha.id_hasil_akreditasi = pa.id_hasil_akreditasi")

	// data.Joins("left join ")
	data.Where("ua.sts_pengajuan_usulan = ?", "1")
	data.Where("ij.sts_aktif_identitas_jurnal = ?", "1")
	data.Where("ij.id_identitas_jurnal = ?", id)
	data.Take(&jurnal)

	if jurnal.NilaiTotalSa >= 85 {
		jurnal.GradeAkreditasiSa = "Peringkat 1"
	} else if jurnal.NilaiTotalSa >= 70 {
		jurnal.GradeAkreditasiSa = "Peringkat 2"
	} else if jurnal.NilaiTotalSa >= 60 {
		jurnal.GradeAkreditasiSa = "Peringkat 3"
	} else if jurnal.NilaiTotalSa >= 50 {
		jurnal.GradeAkreditasiSa = "Peringkat 4"
	} else if jurnal.NilaiTotalSa >= 40 {
		jurnal.GradeAkreditasiSa = "Peringkat 5"
	} else if jurnal.NilaiTotalSa >= 30 {
		jurnal.GradeAkreditasiSa = "Peringkat 6"
	} else if jurnal.GradeAkreditasiSa == "" {
		jurnal.GradeAkreditasiSa = "Blm dilakukan"
	} else {
		jurnal.GradeAkreditasiSa = "Tdk terakreditasi"
	}

	if jurnal.StsHasilDeskEvaluasi == "1" {
		jurnal.HasilDeskEvaluasi = "Lolos desk evaluasi"
	} else if jurnal.StsHasilDeskEvaluasi == "0" {
		jurnal.HasilDeskEvaluasi = "Tdk lolos desk evaluasi"
	} else {
		jurnal.HasilDeskEvaluasi = "Dlm proses penilaian"
	}
	jurnal.DataPenugasanPenerimaanManajemen = getPenugasanPenerimaanManajemen(int(jurnal.IdUsulanAkreditasi))
	jurnal.DataPenugasanPenerimaanEvaluator = getPenugasanPenerimaanEvaluator(int(jurnal.IdUsulanAkreditasi))

	if data.Error != nil {
		return entity.FrontPageJurnalProgres{}, err
	}

	return jurnal, nil
}

func getPenugasanPenerimaanManajemen(id_usulan_akreditasi int) []entity.PenugasanPenerimaanManajemen {
	var res []entity.PenugasanPenerimaanManajemen

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penugasan_penilaian_mgmt")
	data.Select("penugasan_penilaian_mgmt.*, p.nama as penilai")
	data.Joins("left join personal as p on p.id_personal = penugasan_penilaian_mgmt.id_personal")
	data.Where("id_usulan_akreditasi = ?", id_usulan_akreditasi)
	data.Find(&res)
	return res
}

func getPenugasanPenerimaanEvaluator(id_usulan_akreditasi int) []entity.PenugasanPenerimaanEvaluator {
	var res []entity.PenugasanPenerimaanEvaluator

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penugasan_penilaian_evaluator")
	data.Select("penugasan_penilaian_evaluator.*, p.nama as penilai")
	data.Joins("left join personal as p on p.id_personal = penugasan_penilaian_evaluator.id_personal")
	data.Where("id_usulan_akreditasi = ?", id_usulan_akreditasi)
	data.Find(&res)
	return res
}

// func JurnalAccreditationHistoryFrontPage(id int) (p_jurnal []entity.FrontPageJurnalProgres, err error) {
// 	var jurnal []entity.FrontPageJurnalProgres

// 	db := config.Con()
// 	defer config.CloseCon(db)

// 	data := db.Table("v_get_detail_jurnal_front_detail as jurnal")
// 	data.Where("jurnal.id_identitas_jurnal = ?", id)
// 	data.Take(&jurnal)

// 	if data.Error != nil {
// 		return nil, err
// 	}

// 	return jurnal, nil
// }

func HistoryJurnalNameProgresFrontPage(id int) (p_jurnal []entity.FrontPageJurnalNameHistory, err error) {
	var jurnal []entity.FrontPageJurnalNameHistory

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("v_get_jurnal_name_history as jurnal")
	data.Where("jurnal.id_jurnal = ?", id)
	data.Find(&jurnal)

	if data.Error != nil {
		return nil, err
	}

	return jurnal, nil
}

func RekapUsulanJurnalFrontPage() (ru_jurnal entity.FrontPageRekapUsulan, err error) {
	var jurnal entity.FrontPageRekapUsulan

	jurnal.TotalUsulanJurnal = countRekapUsulanJurnal("TotalUsulanJurnal")
	jurnal.UsulanBaru = countRekapUsulanJurnal("UsulanBaru")
	jurnal.ReAkreditasi = countRekapUsulanJurnal("ReAkreditasi")
	jurnal.TotalUsulanJurnalL70 = countRekapUsulanJurnal("TotalUsulanJurnalL70")
	jurnal.UsulanBaruL70 = countRekapUsulanJurnal("UsulanBaruL70")
	jurnal.ReAkreditasiL70 = countRekapUsulanJurnal("ReAkreditasiL70")
	jurnal.TotalUsulanJurnaK70 = countRekapUsulanJurnal("TotalUsulanJurnaK70")
	jurnal.UsulanBaruK70 = countRekapUsulanJurnal("UsulanBaruK70")
	jurnal.ReAkreditasiK70 = countRekapUsulanJurnal("ReAkreditasiK70")

	return jurnal, nil

}

func countRekapUsulanJurnal(types string) int64 {
	var res int64

	db := config.Con()
	defer config.CloseCon(db)

	sub_query := db.Table("issue").Select("COUNT(*)").Where("issue.id_usulan_akreditasi = ua.id_usulan_akreditasi")

	query := db.Table("usulan_akreditasi as ua")
	query.Select("ua.id_identitas_jurnal, ua.id_usulan_akreditasi, hs.nilai_total")
	query.Joins("JOIN hasil_sa AS hs ON hs.id_usulan_akreditasi = ua.id_usulan_akreditasi")
	query.Where("ua.sts_pengajuan_usulan = ?", "1")
	if types == "TotalUsulanJurnal" {
		// Total Usulan Jurnal
		query.Count(&res)
	} else if types == "UsulanBaru" {
		// Usulan Baru
		query.Where("(?) = ?", sub_query, "0")
		query.Count(&res)
	} else if types == "ReAkreditasi" {
		// Re-Akreditasi
		query.Where("(?) >= ?", sub_query, "1")
		query.Count(&res)
	} else if types == "TotalUsulanJurnalL70" {
		// Total Usulan Jurnal >= 70
		query.Where("hs.nilai_total >= ?", 70)
		query.Count(&res)
	} else if types == "UsulanBaruL70" {
		// Usulan Baru >= 70
		query.Where("hs.nilai_total >= ?", 70)
		query.Where("(?) = ?", sub_query, "0")
		query.Count(&res)
	} else if types == "ReAkreditasiL70" {
		// Re-Akreditasi >= 70
		query.Where("hs.nilai_total >= ?", 70)
		query.Where("(?) >= ?", sub_query, "1")
		query.Count(&res)
	} else if types == "TotalUsulanJurnaK70" {
		// Total Usulan Jurnal < 70
		query.Where("hs.nilai_total < ?", 70)
		query.Count(&res)
	} else if types == "UsulanBaruK70" {
		// Usulan Baru < 70
		query.Where("hs.nilai_total < ?", 70)
		query.Where("(?) = ?", sub_query, "0")
		query.Count(&res)
	} else if types == "ReAkreditasiK70" {
		// Re-Akreditasi < 70
		query.Where("hs.nilai_total < ?", 70)
		query.Where("(?) >= ?", sub_query, "1")
		query.Count(&res)
	}
	return res
}
