package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"fmt"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
)

type JWTService interface {
	GenerateToken(userID string) string
	ValidateToken(token string) (*jwt.Token, error)
}

type jwtCustomClaim struct {
	IDPengguna string `json:"id_pengguna"`
	IDPersonal string `json:"id_personal"`
	// DataUser entity.Login `json:"data_user"`
	jwt.StandardClaims
}

func GenerateToken(data entity.Login) string {
	claims := &jwtCustomClaim{
		strconv.Itoa(int(data.IDPengguna)),
		strconv.Itoa(int(data.IDPersonal)),
		// data,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 24).Unix(), // 1 hari expired
			// ExpiresAt: time.Now().AddDate(1, 0, 0).Unix(),
			Issuer:   "*!@#$%arjuna^&*()*",
			IssuedAt: time.Now().Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	t, err := token.SignedString([]byte(config.GetSecretKey()))
	if err != nil {
		panic(err)
	}
	return t
}

func ValidateToken(token string) (*jwt.Token, error) {
	return jwt.Parse(token, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method %v", t.Header["alg"])
		}
		return []byte(config.GetSecretKey()), nil
	})
}

func GetIDPenggunaAuthenticated(c *fiber.Ctx) (id int) {
	const BEARER_SCHEMA = "Bearer "
	authHeader := c.GetReqHeaders()
	token_string := authHeader["Authorization"][len(BEARER_SCHEMA):]
	token, _ := ValidateToken(token_string)
	if token.Valid {
		claims := token.Claims.(jwt.MapClaims)
		data := claims["id_pengguna"].(string)
		id, _ := strconv.Atoi(data)
		return id
	} else {
		return
	}
}

func GetIDPersonalAuthenticated(c *fiber.Ctx) (id int) {
	const BEARER_SCHEMA = "Bearer "
	authHeader := c.GetReqHeaders()
	token_string := authHeader["Authorization"][len(BEARER_SCHEMA):]
	token, _ := ValidateToken(token_string)
	if token.Valid {
		claims := token.Claims.(jwt.MapClaims)
		data := claims["id_personal"].(string)
		id, _ := strconv.Atoi(data)
		return id
	} else {
		return
	}
}
