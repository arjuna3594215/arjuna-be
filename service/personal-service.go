package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
)

func CheckDuplicateEmailPersonal(email string) bool {
	var personal entity.Personal

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("personal").Where("email = ?", email).Take(&personal)
	return !(data.Error == nil)
}

func GetPersonalByEmail(email string) (res interface{}, err error) {
	var personal entity.Personal

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("personal").Where("email = ?", email).Take(&personal)
	if data.Error != nil {
		return nil, data.Error
	}

	return personal, nil
}

func ShowPersonal(baseURL string, id int) (res interface{}, err error) {
	var personal entity.Personal

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("personal").Where("id_personal = ?", id).Take(&personal)
	personal.Image = baseURL + "/file/photos/" + personal.Image
	personal.BidangIlmuInterest = ListBidangIlmuByPersonal(int(personal.IDPersonal))
	if data.Error != nil {
		return nil, data.Error
	}

	return personal, nil
}

func InsertPersonal(data entity.Personal) (res interface{}, err error) {
	db := config.Con()
	defer config.CloseCon(db)

	save := db.Table("personal").Create(&data)
	if save.Error != nil {
		return nil, save.Error
	}
	return data, nil
}

func UpdatePersonal(data entity.Personal) (res interface{}, err error) {
	db := config.Con()
	defer config.CloseCon(db)
	save := db.Table("personal").Model(&data).Where("id_personal = ?", data.IDPersonal)
	if data.StsAktifPersonal != "" {
		save.Updates(map[string]interface{}{"nama": data.Nama, "nama_institusi": data.NamaInstitusi, "email": data.Email, "no_hand_phone": data.NOHandPhone, "sts_aktif_personal": data.StsAktifPersonal})
	} else {
		save.Updates(map[string]interface{}{"nama": data.Nama, "nama_institusi": data.NamaInstitusi, "email": data.Email, "no_hand_phone": data.NOHandPhone})
	}

	if save.Error != nil {
		return nil, save.Error
	}
	return data, nil
}
