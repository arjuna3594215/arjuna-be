package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"time"
)

func ListUnsurPenilaian(lang string, kelompok string) ([]entity.UnsurPenilaian, error) {
	var unsur_penilaian []entity.UnsurPenilaian

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("unsur_penilaian as up").Distinct()
	data.Select("up.id_unsur_penilaian, up.no_urut, upl.unsur_penilaian")
	data.Joins("JOIN unsur_penilaian_lang as upl on upl.id_unsur_penilaian = up.id_unsur_penilaian")
	data.Where("upl.lang = ?", lang)
	data.Where("up.sts_aktif_unsur_penilaian = ?", "1")
	if kelompok != "" {
		data.Where("? = ANY(up.id_kel_unsur_penilaian)", kelompok)
	}
	data.Order("up.no_urut asc")
	data.Find(&unsur_penilaian)

	if data.Error != nil {
		return nil, data.Error
	}
	return unsur_penilaian, nil
}

func ListSubUnsurPenilaian(lang string, id_unsur_penilaian int, id string, penilai string) ([]entity.SubUnsurPenilaian, error) {
	var sub_unsur_penilaian []entity.SubUnsurPenilaian

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("sub_unsur_penilaian as sup")
	data.Select("sup.id_sub_unsur_penilaian, sup.no_urut, supl.sub_unsur_penilaian")
	data.Joins("JOIN sub_unsur_penilaian_lang as supl on supl.id_sub_unsur_penilaian = sup.id_sub_unsur_penilaian")
	data.Where("supl.lang = ?", lang)
	data.Where("sup.id_unsur_penilaian = ?", id_unsur_penilaian)
	if penilai == "management" {
		data.Where("sup.id_kelompok_unsur_penilaian = ?", "2")
	} else if penilai == "issue" {
		data.Where("sup.id_kelompok_unsur_penilaian = ?", "1")
	}
	data.Where("sup.sts_aktif_sub_unsur_penilaian = ?", "1")
	data.Order("sup.no_urut asc")
	data.Find(&sub_unsur_penilaian)

	if data.Error != nil {
		return nil, data.Error
	}

	for i, v := range sub_unsur_penilaian {
		var answer entity.HasilPenilaianSa
		var komentar string
		var jmlSampleArtikel int64
		if penilai == "" {
			db.Table("hasil_penilaian_sa").Select("hasil_penilaian_sa.*").Where("id_usulan_akreditasi = ?", id).Where("id_sub_unsur_penilaian = ?", v.IDSubUnsurPenilaian).Find(&answer)
		} else if penilai == "management" {
			db.Table("hasil_penilaian_mgmt").Select("hasil_penilaian_mgmt.*").Where("id_penugasan_penilaian_manajemen = ?", id).Where("id_sub_unsur_penilaian = ?", v.IDSubUnsurPenilaian).Find(&answer)
			db.Table("komentar_penilaian_mgmt").Select("komentar").Where("id_penugasan_penilaian_manajemen = ?", id).Where("id_unsur_penilaian = ?", id_unsur_penilaian).Find(&komentar)
		} else if penilai == "evaluator" {
			db.Table("hasil_penilaian_evaluator").Select("hasil_penilaian_evaluator.*").Where("id_penugasan_penilaian_evaluator = ?", id).Where("id_sub_unsur_penilaian = ?", v.IDSubUnsurPenilaian).Find(&answer)
			db.Table("komentar_penilaian_evaluator").Select("komentar").Where("id_penugasan_penilaian_evaluator = ?", id).Where("id_unsur_penilaian = ?", id_unsur_penilaian).Find(&komentar)
		} else if penilai == "issue" {
			db.Table("hasil_penilaian_issue").Select("hasil_penilaian_issue.*").Where("id_penugasan_penilaian_issue = ?", id).Where("id_sub_unsur_penilaian = ?", v.IDSubUnsurPenilaian).Find(&answer)
			db.Table("komentar_penilaian_issue").Select("komentar").Where("id_penugasan_penilaian_issue = ?", id).Where("id_unsur_penilaian = ?", id_unsur_penilaian).Find(&komentar)
			db.Table("penugasan_penilaian_issue").Select("jumlah_sampel").Where("id_penugasan_penilaian_issue = ?", id).Find(&jmlSampleArtikel)
		}
		sub_unsur_penilaian[i].Answer = listIndikatorPenilaianAnswer(lang, int(v.IDSubUnsurPenilaian), answer.Nilai)
		sub_unsur_penilaian[i].IndikatorPenilaian = listIndikatorPenilaian(lang, int(v.IDSubUnsurPenilaian))
	}

	return sub_unsur_penilaian, nil
}

func ListSubUnsurPenilaianPP(lang string, id_unsur_penilaian int, id string, penilai string) (entity.SubUnsurPenilaianPenugasanPenilaian, error) {
	var sub_unsur_penilaian entity.SubUnsurPenilaianPenugasanPenilaian
	var komentar string
	var jmlSampleArtikel int64

	db := config.Con()
	defer config.CloseCon(db)

	if penilai == "management" {
		db.Table("komentar_penilaian_mgmt").Select("komentar").Where("id_penugasan_penilaian_manajemen = ?", id).Where("id_unsur_penilaian = ?", id_unsur_penilaian).Find(&komentar)
	} else if penilai == "evaluator" {
		db.Table("komentar_penilaian_evaluator").Select("komentar").Where("id_penugasan_penilaian_evaluator = ?", id).Where("id_unsur_penilaian = ?", id_unsur_penilaian).Find(&komentar)
	} else if penilai == "issue" {
		db.Table("komentar_penilaian_issue").Select("komentar").Where("id_penugasan_penilaian_issue = ?", id).Where("id_unsur_penilaian = ?", id_unsur_penilaian).Find(&komentar)
		db.Table("penugasan_penilaian_issue").Select("jumlah_sampel").Where("id_penugasan_penilaian_issue = ?", id).Find(&jmlSampleArtikel)
	}

	sub_unsur_p, _ := ListSubUnsurPenilaian(lang, id_unsur_penilaian, id, penilai)
	sub_unsur_penilaian.SubUnsurPenilaian = sub_unsur_p
	sub_unsur_penilaian.Komentar = komentar
	sub_unsur_penilaian.JmlSampleArtikel = jmlSampleArtikel

	return sub_unsur_penilaian, nil
}

func ListSubUnsurPenilaianPPDisinsentif(lang string, id string, penilai string) (entity.SubUnsurPenilaianPenugasanPenilaianDisinsentif, error) {
	var sub_unsur_penilaian entity.SubUnsurPenilaianPenugasanPenilaianDisinsentif
	var sub_unsur []entity.SubUnsurPenilaianDisinsentif
	var komentar string
	var jmlSampleArtikel int64

	db := config.Con()
	defer config.CloseCon(db)

	data_sub := db.Table("sub_unsur_disinsentif as t1")
	data_sub.Select("t1.id_sub_unsur_disinsentif as id_sub_unsur_penilaian_disinsentif, t1.no_urut, t2.sub_unsur_disinsentif as sub_unsur_penilaian_disinsentif")
	data_sub.Joins("INNER JOIN sub_unsur_disinsentif_lang t2 ON t1.id_sub_unsur_disinsentif = t2.id_sub_unsur_disinsentif")
	data_sub.Where("t2.lang = ?", lang)
	data_sub.Where("sts_aktif_sub_unsur_disinsentif = ?", "1")
	data_sub.Where("t1.id_kelompok_unsur_penilaian = ?", 1)
	data_sub.Order("t1.no_urut")
	data_sub.Find(&sub_unsur)

	for i, v := range sub_unsur {
		var disinsentif_penilaian_issue entity.DisintensifPenilaianIssue
		var indikator_penilaian []entity.IndikatorPenilaianDisinsentif
		var answer entity.IndikatorPenilaianDisinsentif
		db.Table("disinsentif_penilaian_issue").Where("id_penugasan_penilaian_issue = ?", id).Where("id_sub_unsur_disinsentif = ?", v.IDSubUnsurPenilaianDisinsentif).Take(&disinsentif_penilaian_issue)
		idikator_penilaian := db.Table("indikator_disinsentif as t1")
		idikator_penilaian.Select("t1.id_indikator_disinsentif as id_indikator_Penilaian_disinsentif, t1.no_urut, t1.bobot, t2.indikator_disinsentif as indikator_Penilaian_disinsentif, t1.id_sub_unsur_disinsentif as id_sub_unsur_penilaian_disinsentif")
		idikator_penilaian.Joins("INNER JOIN indikator_disinsentif_lang t2 ON t1.id_indikator_disinsentif = t2.id_indikator_disinsentif")
		idikator_penilaian.Where("t1.id_sub_unsur_disinsentif = ?", v.IDSubUnsurPenilaianDisinsentif)
		idikator_penilaian.Where("t1.sts_aktif_indikator_disinsentif = ?", "1")
		idikator_penilaian.Where("t2.lang = ?", lang)
		idikator_penilaian.Order("t1.no_urut")
		idikator_penilaian.Order("t1.no_urut")
		idikator_penilaian.Find(&indikator_penilaian)

		data_answer := db.Table("indikator_disinsentif as t1")
		data_answer.Select("t1.id_indikator_disinsentif as id_indikator_Penilaian_disinsentif, t1.no_urut, t1.bobot, t2.indikator_disinsentif as indikator_Penilaian_disinsentif, t1.id_sub_unsur_disinsentif as id_sub_unsur_penilaian_disinsentif")
		data_answer.Joins("INNER JOIN indikator_disinsentif_lang t2 ON t1.id_indikator_disinsentif = t2.id_indikator_disinsentif")
		data_answer.Where("t1.id_sub_unsur_disinsentif = ?", v.IDSubUnsurPenilaianDisinsentif)
		data_answer.Where("t1.bobot = ?", disinsentif_penilaian_issue.Nilai)
		data_answer.Where("t1.sts_aktif_indikator_disinsentif = ?", "1")
		data_answer.Where("t2.lang = ?", lang)
		data_answer.Order("t1.no_urut")
		data_answer.Order("t1.no_urut")
		data_answer.Find(&answer)

		sub_unsur[i].Answer = answer
		sub_unsur[i].IndikatorPenilaian = indikator_penilaian
	}

	// if penilai == "management" {
	// 	db.Table("komentar_penilaian_mgmt").Select("komentar").Where("id_penugasan_penilaian_manajemen = ?", id).Where("id_unsur_penilaian = ?", id_unsur_penilaian).Find(&komentar)
	// } else if penilai == "evaluator" {
	// 	db.Table("komentar_penilaian_evaluator").Select("komentar").Where("id_penugasan_penilaian_evaluator = ?", id).Where("id_unsur_penilaian = ?", id_unsur_penilaian).Find(&komentar)
	// } else
	if penilai == "issue" {
		db.Table("komentar_disinsentif_issue").Select("komentar").Where("id_penugasan_penilaian_issue = ?", id).Find(&komentar)
		// db.Table("penugasan_penilaian_issue").Select("jumlah_sampel").Where("id_penugasan_penilaian_issue = ?", id).Find(&jmlSampleArtikel)
	}

	sub_unsur_penilaian.SubUnsurPenilaian = sub_unsur
	sub_unsur_penilaian.Komentar = komentar
	sub_unsur_penilaian.JmlSampleArtikel = jmlSampleArtikel

	return sub_unsur_penilaian, nil
}

func listIndikatorPenilaian(lang string, id_sub_unsur_penilaian int) []entity.IndikatorPenilaian {
	var indikator_penilaian []entity.IndikatorPenilaian

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("indikator_penilaian as ip")
	data.Select("ip.id_indikator_penilaian, ip.no_urut, ip.bobot, ipl.indikator_penilaian, ip.id_sub_unsur_penilaian")
	data.Joins("JOIN indikator_penilaian_lang as ipl on ipl.id_indikator_penilaian = ip.id_indikator_penilaian")
	data.Where("ip.id_sub_unsur_penilaian = ?", id_sub_unsur_penilaian)
	data.Where("ip.sts_aktif_indikator_penilaian = ?", "1")
	data.Where("ipl.lang = ?", lang)
	data.Order("ip.no_urut asc")
	data.Find(&indikator_penilaian)

	if data.Error != nil {
		return nil
	}
	return indikator_penilaian
}

func listIndikatorPenilaianAnswer(lang string, id_sub_unsur_penilaian int, nilai float64) interface{} {
	var indikator_penilaian entity.IndikatorPenilaian

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("indikator_penilaian as ip")
	data.Select("ip.id_indikator_penilaian, ip.no_urut, ip.bobot, ipl.indikator_penilaian, ip.id_sub_unsur_penilaian")
	data.Joins("JOIN indikator_penilaian_lang as ipl on ipl.id_indikator_penilaian = ip.id_indikator_penilaian")
	data.Where("ip.id_sub_unsur_penilaian = ?", id_sub_unsur_penilaian)
	data.Where("ip.sts_aktif_indikator_penilaian = ?", "1")
	data.Where("ipl.lang = ?", lang)
	data.Where("ip.bobot = ?", nilai)
	data.Order("ip.no_urut asc")
	data.Take(&indikator_penilaian)

	if data.Error != nil {
		return nil
	}
	return indikator_penilaian
}

func ListHasilPenilaianSa(id_sub_unsur_penilaian int, id_usulan_akreditasi string, id_personal int) (interface{}, error) {
	var penilaian_sa []entity.HasilPenilaianSa

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("hasil_penilaian_sa as hps")
	data.Select("hps.*")
	data.Joins("JOIN usulan_akreditasi as ua on ua.id_usulan_akreditasi = hps.id_usulan_akreditasi")
	data.Joins("JOIN pic on pic.id_pic = ua.id_pic")
	data.Where("pic.sts_aktif_pic = ?", "1")
	data.Where("pic.id_personal = ?", id_personal)
	data.Where("hps.id_usulan_akreditasi = ?", id_usulan_akreditasi)
	data.Where("hps.id_sub_unsur_penilaian = ?", id_sub_unsur_penilaian)
	data.Order("hps.id_sub_unsur_penilaian asc")
	data.Take(&penilaian_sa)

	if data.Error != nil {
		return nil, data.Error
	}
	return penilaian_sa, nil
}

func CreateUpdatePenilaianSa(req entity.HasilPenilaianSaDTO) error {
	var penilaian_sa entity.HasilPenilaianSa
	var hasil_sa entity.HasilSa
	var check int64
	var check_hasil_sa int64
	var nilai_total float64
	var grade_akreditasi string
	configurasi := getAktifConfiguration()

	db := config.Con()
	defer config.CloseCon(db)

	for i, v := range req.IdSubUnsurPenilaian {
		db.Table("hasil_penilaian_sa").Where("id_usulan_akreditasi = ?", req.IdUsulanAkreditasi).Where("id_sub_unsur_penilaian = ?", v).Count(&check)
		if check > 0 {
			update := db.Model(&entity.HasilPenilaianSa{}).Table("hasil_penilaian_sa").Where("id_usulan_akreditasi = ?", req.IdUsulanAkreditasi).Where("id_sub_unsur_penilaian = ?", v).Update("nilai", req.Nilai[i])
			if update.Error != nil {
				return update.Error
			}
		} else {
			var last_id int64
			db.Table("hasil_penilaian_sa").Select("id_hasil_penilaian_sa").Order("id_hasil_penilaian_sa desc").Limit(1).Scan(&last_id)
			penilaian_sa.IdHasilPenilaianSa = last_id + 1
			penilaian_sa.IdUsulanAkreditasi = req.IdUsulanAkreditasi
			penilaian_sa.IdSubUnsurPenilaian = v
			penilaian_sa.Nilai = req.Nilai[i]
			save := db.Table("hasil_penilaian_sa").Create(&penilaian_sa)
			if save.Error != nil {
				return save.Error
			}
		}
		db.Table("hasil_penilaian_sa").Select("COALESCE(sum(nilai)::numeric(5,2), 0.0) as nilai_total").Where("id_usulan_akreditasi = ?", req.IdUsulanAkreditasi).Find(&nilai_total)
		if nilai_total >= configurasi.MinimalNilaiA {
			grade_akreditasi = "A"
		} else if nilai_total >= configurasi.MinimalNilaiB {
			grade_akreditasi = "B"
		} else {
			grade_akreditasi = "T"
		}

		db.Table("hasil_sa").Where("id_usulan_akreditasi = ?", req.IdUsulanAkreditasi).Count(&check_hasil_sa)
		if check_hasil_sa > 0 {
			update := db.Model(&entity.HasilSa{}).Table("hasil_sa").Where("id_usulan_akreditasi = ?", req.IdUsulanAkreditasi).Updates(map[string]interface{}{"nilai_total": nilai_total, "grade_akreditasi": grade_akreditasi})
			if update.Error != nil {
				return update.Error
			}
		} else {
			hasil_sa.IdUsulanAkreditasi = req.IdUsulanAkreditasi
			hasil_sa.NilaiTotal = nilai_total
			hasil_sa.GradeAkreditasi = grade_akreditasi
			hasil_sa.IDConfigurasi = configurasi.IDConfigurasi
			save := db.Table("hasil_sa").Create(&hasil_sa)
			if save.Error != nil {
				return save.Error
			}
		}
	}

	return nil
}

func CreateUpdatePenilaianPenugasan(req entity.PenilaianPenugasanDTO) error {
	var check_hasil_penilaian int64
	var check_komentar int64
	var last_id int64
	var komentar_penilaian_mgmt entity.KomentarPenilaianMgmt
	var hasil_penilaian_mgmt entity.HasilPenilaianMgmt
	var komentar_penilaian_evaluator entity.KomentarPenilaianEvaluator
	var hasil_penilaian_evaluator entity.HasilPenilaianEvaluator
	var komentar_penilaian_issue entity.KomentarPenilaianIssue
	var hasil_penilaian_issue entity.HasilPenilaianIssue

	db := config.Con()
	defer config.CloseCon(db)

	for i, v := range req.IDSubUnsurPenilaian {
		if req.Penilai == "management" {
			// Management
			db.Table("hasil_penilaian_mgmt").Where("id_penugasan_penilaian_manajemen = ?", req.IDPenugasanPenilaian).Where("id_sub_unsur_penilaian = ?", v).Count(&check_hasil_penilaian)
			if check_hasil_penilaian > 0 {
				update := db.Model(&entity.HasilPenilaianMgmt{}).Table("hasil_penilaian_mgmt").Where("id_penugasan_penilaian_manajemen = ?", req.IDPenugasanPenilaian).Where("id_sub_unsur_penilaian = ?", v).Updates(map[string]interface{}{"nilai": req.Nilai[i], "id_indikator_penilaian": req.IDIndikatorPenilaian[i], "tgl_updated": time.Now()})
				if update.Error != nil {
					return update.Error
				}
			} else {
				db.Table("hasil_penilaian_mgmt").Select("id_hasil_penilaian_manajemen").Order("id_hasil_penilaian_manajemen desc").Limit(1).Scan(&last_id)
				hasil_penilaian_mgmt.IDHasilPenilaianManajemen = last_id + 1
				hasil_penilaian_mgmt.IDPenugasanPenilaianManajemen = req.IDPenugasanPenilaian
				hasil_penilaian_mgmt.IDSubUnsurPenilaian = v
				hasil_penilaian_mgmt.Nilai = req.Nilai[i]
				hasil_penilaian_mgmt.IDIndikatorPenilaian = req.IDIndikatorPenilaian[i]
				hasil_penilaian_mgmt.TglCreated = time.Now()
				save := db.Table("hasil_penilaian_mgmt").Create(&hasil_penilaian_mgmt)
				if save.Error != nil {
					return save.Error
				}
			}
			//
		} else if req.Penilai == "evaluator" {
			// Evaluator
			db.Table("hasil_penilaian_evaluator").Where("id_penugasan_penilaian_evaluator = ?", req.IDPenugasanPenilaian).Where("id_sub_unsur_penilaian = ?", v).Count(&check_hasil_penilaian)
			if check_hasil_penilaian > 0 {
				update := db.Model(&entity.HasilPenilaianEvaluator{}).Table("hasil_penilaian_evaluator").Where("id_penugasan_penilaian_evaluator = ?", req.IDPenugasanPenilaian).Where("id_sub_unsur_penilaian = ?", v).Updates(map[string]interface{}{"nilai": req.Nilai[i], "id_indikator_penilaian": req.IDIndikatorPenilaian[i], "tgl_updated": time.Now()})
				if update.Error != nil {
					return update.Error
				}
			} else {
				db.Table("hasil_penilaian_evaluator").Select("id_hasil_penilaian_evaluator").Order("id_hasil_penilaian_evaluator desc").Limit(1).Scan(&last_id)
				hasil_penilaian_evaluator.IDHasilPenilaianEvaluator = last_id + 1
				hasil_penilaian_evaluator.IDPenugasanPenilaianEvaluator = req.IDPenugasanPenilaian
				hasil_penilaian_evaluator.IDSubUnsurPenilaian = v
				hasil_penilaian_evaluator.Nilai = req.Nilai[i]
				hasil_penilaian_evaluator.IDIndikatorPenilaian = req.IDIndikatorPenilaian[i]
				hasil_penilaian_evaluator.TglCreated = time.Now()
				save := db.Table("hasil_penilaian_evaluator").Create(&hasil_penilaian_evaluator)
				if save.Error != nil {
					return save.Error
				}
			}
			//
		} else if req.Penilai == "issue" {
			// Issue
			db.Table("hasil_penilaian_issue").Where("id_penugasan_penilaian_issue = ?", req.IDPenugasanPenilaian).Where("id_sub_unsur_penilaian = ?", v).Count(&check_hasil_penilaian)
			if check_hasil_penilaian > 0 {
				update := db.Model(&entity.HasilPenilaianIssue{}).Table("hasil_penilaian_issue").Where("id_penugasan_penilaian_issue = ?", req.IDPenugasanPenilaian).Where("id_sub_unsur_penilaian = ?", v).Updates(map[string]interface{}{"nilai": req.Nilai[i], "id_indikator_penilaian": req.IDIndikatorPenilaian[i], "tgl_updated": time.Now()})
				if update.Error != nil {
					return update.Error
				}
			} else {
				db.Table("hasil_penilaian_issue").Select("id_hasil_penilaian_issue").Order("id_hasil_penilaian_issue desc").Limit(1).Scan(&last_id)
				hasil_penilaian_issue.IDHasilPenilaianIssue = last_id + 1
				hasil_penilaian_issue.IDPenugasanPenilaianIssue = req.IDPenugasanPenilaian
				hasil_penilaian_issue.IDIssue = req.IDIssue
				hasil_penilaian_issue.IDSubUnsurPenilaian = v
				hasil_penilaian_issue.Nilai = req.Nilai[i]
				hasil_penilaian_issue.IDIndikatorPenilaian = req.IDIndikatorPenilaian[i]
				hasil_penilaian_issue.TglCreated = time.Now()
				save := db.Table("hasil_penilaian_issue").Create(&hasil_penilaian_issue)
				if save.Error != nil {
					return save.Error
				}
			}
			//
		}
	}

	if req.Penilai == "management" {
		// Management
		db.Table("komentar_penilaian_mgmt").Where("id_penugasan_penilaian_manajemen = ?", req.IDPenugasanPenilaian).Where("id_unsur_penilaian = ?", req.IDUnsurPenilaian).Count(&check_komentar)
		if check_komentar > 0 {
			update := db.Model(&entity.KomentarPenilaianMgmt{}).Table("komentar_penilaian_mgmt").Where("id_penugasan_penilaian_manajemen = ?", req.IDPenugasanPenilaian).Where("id_unsur_penilaian = ?", req.IDUnsurPenilaian).Updates(map[string]interface{}{"komentar": req.Komentar, "tgl_updated": time.Now()})
			if update.Error != nil {
				return update.Error
			}
		} else {
			komentar_penilaian_mgmt.IDPenugasanPenilaianManajemen = req.IDPenugasanPenilaian
			komentar_penilaian_mgmt.IDUnsurPenilaian = req.IDUnsurPenilaian
			komentar_penilaian_mgmt.Komentar = req.Komentar
			komentar_penilaian_mgmt.TglCreated = time.Now()
			save := db.Table("komentar_penilaian_mgmt").Create(&komentar_penilaian_mgmt)
			if save.Error != nil {
				return save.Error
			}
		}
		//
	} else if req.Penilai == "evaluator" {
		// Evaluator
		db.Table("komentar_penilaian_evaluator").Where("id_penugasan_penilaian_evaluator = ?", req.IDPenugasanPenilaian).Where("id_unsur_penilaian = ?", req.IDUnsurPenilaian).Count(&check_komentar)
		if check_komentar > 0 {
			update := db.Model(&entity.KomentarPenilaianEvaluator{}).Table("komentar_penilaian_evaluator").Where("id_penugasan_penilaian_evaluator = ?", req.IDPenugasanPenilaian).Where("id_unsur_penilaian = ?", req.IDUnsurPenilaian).Updates(map[string]interface{}{"komentar": req.Komentar, "tgl_update": time.Now()})
			if update.Error != nil {
				return update.Error
			}
		} else {
			komentar_penilaian_evaluator.IDPenugasanPenilaianEvaluator = req.IDPenugasanPenilaian
			komentar_penilaian_evaluator.IDUnsurPenilaian = req.IDUnsurPenilaian
			komentar_penilaian_evaluator.Komentar = req.Komentar
			komentar_penilaian_evaluator.TglCreated = time.Now()
			save := db.Table("komentar_penilaian_evaluator").Create(&komentar_penilaian_evaluator)
			if save.Error != nil {
				return save.Error
			}
		}
		//
	} else if req.Penilai == "issue" {
		// Issue
		db.Table("komentar_penilaian_issue").Where("id_penugasan_penilaian_issue = ?", req.IDPenugasanPenilaian).Where("id_unsur_penilaian = ?", req.IDUnsurPenilaian).Count(&check_komentar)
		if check_komentar > 0 {
			update := db.Model(&entity.KomentarPenilaianIssue{}).Table("komentar_penilaian_issue").Where("id_penugasan_penilaian_issue = ?", req.IDPenugasanPenilaian).Where("id_unsur_penilaian = ?", req.IDUnsurPenilaian).Updates(map[string]interface{}{"komentar": req.Komentar, "tgl_updated": time.Now()})
			if update.Error != nil {
				return update.Error
			}
		} else {
			komentar_penilaian_issue.IDPenugasanPenilaianIssue = req.IDPenugasanPenilaian
			komentar_penilaian_issue.IDUnsurPenilaian = req.IDUnsurPenilaian
			komentar_penilaian_issue.Komentar = req.Komentar
			komentar_penilaian_issue.TglCreated = time.Now()
			save := db.Table("komentar_penilaian_issue").Create(&komentar_penilaian_issue)
			if save.Error != nil {
				return save.Error
			}
		}
		db.Model(&entity.PenugasanPenilaianIssue{}).Table("penugasan_penilaian_issue").Where("id_penugasan_penilaian_issue = ?", req.IDPenugasanPenilaian).Updates(map[string]interface{}{"jumlah_sampel": req.JumlahSample, "tgl_updated": time.Now()})
		//
	}

	return nil
}

func CreateUpdatePenilaianPenugasanDisinsentif(req entity.PenilaianPenugasanDisinsentifDTO) error {
	var check_hasil_penilaian int64
	var check_komentar int64
	var last_id int64
	// var komentar_penilaian_mgmt entity.KomentarPenilaianMgmt
	// var hasil_penilaian_mgmt entity.HasilPenilaianMgmt
	// var komentar_penilaian_evaluator entity.KomentarPenilaianEvaluator
	// var hasil_penilaian_evaluator entity.HasilPenilaianEvaluator
	var komentar_penilaian_issue entity.KomentarDisintensifPenilaianIssue
	var hasil_penilaian_issue entity.DisintensifPenilaianIssue

	db := config.Con()
	defer config.CloseCon(db)

	for i, v := range req.IDSubUnsurPenilaian {
		if req.Penilai == "issue" {
			// Issue
			db.Table("disinsentif_penilaian_issue").Where("id_penugasan_penilaian_issue = ?", req.IDPenugasanPenilaian).Where("id_sub_unsur_disinsentif = ?", v).Count(&check_hasil_penilaian)
			if check_hasil_penilaian > 0 {
				update := db.Model(&entity.DisintensifPenilaianIssue{}).Table("disinsentif_penilaian_issue").Where("id_penugasan_penilaian_issue = ?", req.IDPenugasanPenilaian).Where("id_sub_unsur_disinsentif = ?", v).Updates(map[string]interface{}{"nilai": req.Nilai[i], "id_indikator_disinsentif": req.IDIndikatorPenilaian[i], "tgl_updated": time.Now()})
				if update.Error != nil {
					return update.Error
				}
			} else {
				db.Table("disinsentif_penilaian_issue").Select("id_disinsentif_penilaian_issue").Order("id_disinsentif_penilaian_issue desc").Limit(1).Scan(&last_id)
				hasil_penilaian_issue.IDDisinsentifPenilaianIssue = last_id + 1
				hasil_penilaian_issue.IDPenugasanPenilaianIssue = req.IDPenugasanPenilaian
				hasil_penilaian_issue.IDSubUnsurDisinsentif = v
				hasil_penilaian_issue.Nilai = req.Nilai[i]
				hasil_penilaian_issue.IDIndikatorDisinsentif = req.IDIndikatorPenilaian[i]
				hasil_penilaian_issue.TglCreated = time.Now()
				save := db.Table("disinsentif_penilaian_issue").Create(&hasil_penilaian_issue)
				if save.Error != nil {
					return save.Error
				}
			}
			db.Table("komentar_disinsentif_issue").Where("id_penugasan_penilaian_issue = ?", req.IDPenugasanPenilaian).Where("id_sub_unsur_disinsentif = ?", v).Count(&check_komentar)
			if check_komentar > 0 {
				update := db.Model(&entity.KomentarDisintensifPenilaianIssue{}).Table("komentar_disinsentif_issue").Where("id_penugasan_penilaian_issue = ?", req.IDPenugasanPenilaian).Where("id_sub_unsur_disinsentif = ?", v).Updates(map[string]interface{}{"komentar": req.Komentar, "tgl_updaated": time.Now()})
				if update.Error != nil {
					return update.Error
				}
			} else {
				komentar_penilaian_issue.IDPenugasanPenilaianIssue = req.IDPenugasanPenilaian
				komentar_penilaian_issue.IDSubUnsurDisinsentif = v
				komentar_penilaian_issue.Komentar = req.Komentar
				komentar_penilaian_issue.TglCreated = time.Now()
				save := db.Table("komentar_disinsentif_issue").Create(&komentar_penilaian_issue)
				if save.Error != nil {
					return save.Error
				}
			}
			//
		}
	}

	return nil
}
