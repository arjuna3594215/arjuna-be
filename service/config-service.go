package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/vcraescu/go-paginator/v2"
	"github.com/vcraescu/go-paginator/v2/adapter"
	"github.com/vcraescu/go-paginator/v2/view"
)

func ListConfig(page string, row string) (helper.ResponsePagination, error) {
	var configurasi []entity.Config

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("configurasi")
	data.Order("tgl_created desc")
	data.Find(&configurasi)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(configurasi)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&configurasi); err != nil {
		panic(err)
	}
	res := helper.BuildPaginationResponse(configurasi, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func InsertConfig(req entity.ConfigDTO, c *fiber.Ctx) (entity.Config, error) {
	var configurasi entity.Config
	db := config.Con()
	defer config.CloseCon(db)

	db.Table("configurasi").Where("sts_aktif_configurasi = ?", "1").Update("sts_aktif_configurasi", "0")

	tglPendaftaranmulai, errors := time.Parse("2006-01-02", req.TglPendaftaranMulai)
	tglPendaftaranakhir, errors := time.Parse("2006-01-02", req.TglPendaftaranAkhir)
	if errors != nil {
		return entity.Config{}, errors
	}

	configurasi.Durasi = req.Durasi
	configurasi.MinimalNilaiA = req.MinimalNilaiA
	configurasi.MinimalNilaiB = req.MinimalNilaiB
	configurasi.MasaTungguMinimal = req.MasaTungguMinimal
	configurasi.MasaPengingat = req.MasaPengingat
	configurasi.BatasPerbedaanNilaiMgmt = req.BatasPerbedaanNilaiMgmt
	configurasi.BatasPerbedaanNilaiArtikel = req.BatasPerbedaanNilaiArtikel
	configurasi.JmlAsessorMgmt = req.JmlAsessorMgmt
	configurasi.JmlAsessorArtikel = req.JmlAsessorArtikel
	configurasi.TglPendaftaranMulai = tglPendaftaranmulai
	configurasi.TglPendaftaranAkhir = tglPendaftaranakhir
	configurasi.TglCreated = time.Now()
	configurasi.JmlAsessorIssue = req.JmlAsessorIssue
	configurasi.IntervalMostpopular = 100

	saveConfig := db.Table("configurasi").Create(&configurasi)

	if saveConfig.Error != nil {
		return entity.Config{}, saveConfig.Error
	}

	return configurasi, nil
}

func ShowConfig(id int) (interface{}, error) {
	var configurasi entity.Config
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("configurasi")
	data.Where("id_configurasi = ?", id)
	data.Take(&configurasi)
	if data.Error != nil {
		return nil, data.Error
	}
	return configurasi, nil
}

func UpdateConfig(req entity.ConfigDTO, old entity.Config, id int) (entity.Config, error) {
	var configurasi entity.Config
	db := config.Con()
	defer config.CloseCon(db)

	tglPendaftaranmulai, errors := time.Parse("2006-01-02", req.TglPendaftaranMulai)
	tglPendaftaranakhir, errors := time.Parse("2006-01-02", req.TglPendaftaranAkhir)
	if errors != nil {
		return entity.Config{}, errors
	}
	configurasi.IDConfigurasi = int64(id)
	configurasi.Durasi = req.Durasi
	configurasi.MinimalNilaiA = req.MinimalNilaiA
	configurasi.MinimalNilaiB = req.MinimalNilaiB
	configurasi.MasaTungguMinimal = req.MasaTungguMinimal
	configurasi.MasaPengingat = req.MasaPengingat
	configurasi.BatasPerbedaanNilaiMgmt = req.BatasPerbedaanNilaiMgmt
	configurasi.BatasPerbedaanNilaiArtikel = req.BatasPerbedaanNilaiArtikel
	configurasi.JmlAsessorMgmt = req.JmlAsessorMgmt
	configurasi.JmlAsessorArtikel = req.JmlAsessorArtikel
	configurasi.TglPendaftaranMulai = tglPendaftaranmulai
	configurasi.TglPendaftaranAkhir = tglPendaftaranakhir
	configurasi.TglCreated = old.TglCreated
	configurasi.JmlAsessorIssue = req.JmlAsessorIssue
	configurasi.IntervalMostpopular = 100

	saveConfig := db.Table("configurasi").Where("id_configurasi = ?", id).Save(&configurasi)

	if saveConfig.Error != nil {
		return entity.Config{}, saveConfig.Error
	}

	return configurasi, nil
}

func UpdateStatusConfig(old entity.Config, id int) (entity.Config, error) {
	db := config.Con()
	defer config.CloseCon(db)

	db.Table("configurasi").Where("sts_aktif_configurasi = ?", "1").Update("sts_aktif_configurasi", "0")
	updateStatus := db.Table("configurasi").Where("id_configurasi = ?", id).Update("sts_aktif_configurasi", "1")
	if updateStatus.Error != nil {
		return entity.Config{}, updateStatus.Error
	}
	old.StsAktifConfigurasi = "1"
	return old, nil
}

func getAktifConfiguration() entity.Config {
	var configurasi entity.Config

	db := config.Con()
	defer config.CloseCon(db)

	db.Table("configurasi").Where("sts_aktif_configurasi = ?", "1").Take(&configurasi)
	return configurasi
}
