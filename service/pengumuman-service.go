package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/vcraescu/go-paginator/v2"
	"github.com/vcraescu/go-paginator/v2/adapter"
	"github.com/vcraescu/go-paginator/v2/view"
)

func ListPengumuman(search string, page string, row string, baseUrl string, status string) (helper.ResponsePagination, error) {
	var pengumuman []entity.Pengumuman

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("berita as pengumuman")
	data.Select("pengumuman.*, pengumuman_lang.title, pengumuman_lang.isi, pengumuman_lang.lang")
	data.Joins("join berita_lang as pengumuman_lang on pengumuman_lang.id = pengumuman.id")
	data.Where("pengumuman.kategori = ?", "pengumuman")
	if status != "" {
		data.Where("pengumuman.status = ?", status)
	}
	if search != "" {
		data.Where("lower(pengumuman_lang) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Order("pengumuman.date_created DESC")
	data.Find(&pengumuman)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(pengumuman)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&pengumuman); err != nil {
		panic(err)
	}

	for i, v := range pengumuman {
		path := baseUrl + "/file/info/"
		if v.Imagename != "" {
			pengumuman[i].PathImagename = path + v.Imagename
		}
		if v.Lampiran1 != "" {
			pengumuman[i].PathLampiran1 = path + v.Lampiran1
		}
		if v.Lampiran2 != "" {
			pengumuman[i].PathLampiran2 = path + v.Lampiran2
		}
		if v.Lampiran3 != "" {
			pengumuman[i].PathLampiran3 = path + v.Lampiran3
		}
	}

	res := helper.BuildPaginationResponse(pengumuman, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func InsertPengumuman(req entity.PengumumanDTO, c *fiber.Ctx) (entity.Pengumuman, error) {
	var pengumuman entity.Pengumuman
	var pengumuman_lang entity.PengumumanLang
	db := config.Con()
	defer config.CloseCon(db)

	pengumuman.DateCreated = time.Now()
	pengumuman.Kategori = "pengumuman"
	if req.Image != nil {
		filename, _ := helper.UploadFile("./files/info", req.Image, c)
		pengumuman.Imagename = filename
	}

	if req.Lampiran1 != nil {
		filename, _ := helper.UploadFile("./files/info", req.Lampiran1, c)
		pengumuman.Lampiran1 = filename
	}

	if req.Lampiran2 != nil {
		filename, _ := helper.UploadFile("./files/info", req.Lampiran2, c)
		pengumuman.Lampiran2 = filename
	}

	if req.Lampiran3 != nil {
		filename, _ := helper.UploadFile("./files/info", req.Lampiran3, c)
		pengumuman.Lampiran3 = filename
	}

	savePengumuman := db.Table("berita").Save(&pengumuman)

	if savePengumuman.Error != nil {
		return entity.Pengumuman{}, savePengumuman.Error
	}

	pengumuman_lang.ID = pengumuman.ID
	pengumuman_lang.Isi = req.Isi
	pengumuman_lang.Lang = req.Lang
	pengumuman_lang.Snapshot = req.Title
	pengumuman_lang.Title = req.Title

	savePengumumanLang := db.Table("berita_lang").Save(&pengumuman_lang)

	if savePengumumanLang.Error != nil {
		return entity.Pengumuman{}, savePengumumanLang.Error
	}

	pengumuman.Title = pengumuman_lang.Title
	pengumuman.Isi = pengumuman_lang.Isi
	pengumuman.Lang = pengumuman_lang.Lang

	return pengumuman, nil
}

func ShowPengumuman(id int, baseUrl string) (interface{}, error) {
	var pengumuman entity.Pengumuman
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("berita as pengumuman")
	data.Select("pengumuman.*, pengumuman_lang.title, pengumuman_lang.isi, pengumuman_lang.lang, pengumuman_lang.id_lang")
	data.Joins("join berita_lang as pengumuman_lang on pengumuman_lang.id = pengumuman.id")
	data.Where("pengumuman.id = ?", id)
	data.Take(&pengumuman)
	if data.Error != nil {
		return nil, data.Error
	}

	path := baseUrl + "/file/info/"
	if pengumuman.Imagename != "" {
		pengumuman.PathImagename = path + pengumuman.Imagename
	}
	if pengumuman.Lampiran1 != "" {
		pengumuman.PathLampiran1 = path + pengumuman.Lampiran1
	}
	if pengumuman.Lampiran2 != "" {
		pengumuman.PathLampiran2 = path + pengumuman.Lampiran2
	}
	if pengumuman.Lampiran3 != "" {
		pengumuman.PathLampiran3 = path + pengumuman.Lampiran3
	}
	return pengumuman, nil
}

func UpdatePengumuman(req entity.PengumumanDTO, old entity.Pengumuman, id int, c *fiber.Ctx) (entity.Pengumuman, error) {
	var pengumuman entity.Pengumuman
	var pengumuman_lang entity.PengumumanLang
	db := config.Con()
	defer config.CloseCon(db)

	pengumuman.ID = int64(id)
	pengumuman.DateCreated = time.Now()
	pengumuman.Kategori = "pengumuman"
	if req.Image != nil {
		os.Remove("./files/info/" + old.Imagename)
		filename, _ := helper.UploadFile("./files/info", req.Image, c)
		pengumuman.Imagename = filename
	} else {
		pengumuman.Imagename = old.Imagename
	}

	if req.Lampiran1 != nil {
		os.Remove("./files/info/" + old.Lampiran1)
		filename, _ := helper.UploadFile("./files/info", req.Lampiran1, c)
		pengumuman.Lampiran1 = filename
	} else {
		pengumuman.Lampiran1 = old.Lampiran1
	}

	if req.Lampiran2 != nil {
		os.Remove("./files/info/" + old.Lampiran2)
		filename, _ := helper.UploadFile("./files/info", req.Lampiran2, c)
		pengumuman.Lampiran2 = filename
	} else {
		pengumuman.Lampiran2 = old.Lampiran2
	}

	if req.Lampiran3 != nil {
		os.Remove("./files/info/" + old.Lampiran3)
		filename, _ := helper.UploadFile("./files/info", req.Lampiran3, c)
		pengumuman.Lampiran3 = filename
	} else {
		pengumuman.Lampiran3 = old.Lampiran3
	}

	savePengumuman := db.Table("berita").Save(&pengumuman)

	if savePengumuman.Error != nil {
		return entity.Pengumuman{}, savePengumuman.Error
	}

	pengumuman_lang.IDLang = old.IDLang
	pengumuman_lang.ID = pengumuman.ID
	pengumuman_lang.Isi = req.Isi
	pengumuman_lang.Lang = req.Lang
	pengumuman_lang.Snapshot = req.Title
	pengumuman_lang.Title = req.Title

	savePengumumanLang := db.Table("berita_lang").Save(&pengumuman_lang)

	if savePengumumanLang.Error != nil {
		return entity.Pengumuman{}, savePengumumanLang.Error
	}

	pengumuman.Title = pengumuman_lang.Title
	pengumuman.Isi = pengumuman_lang.Isi
	pengumuman.Lang = pengumuman_lang.Lang

	return pengumuman, nil
}

func UpdateStatusPengumuman(req entity.PengumumanStatusDTO, old entity.Pengumuman, id int) (entity.Pengumuman, error) {
	db := config.Con()
	defer config.CloseCon(db)

	updateStatus := db.Table("berita").Where("id = ?", id).Update("status", req.Status)
	if updateStatus.Error != nil {
		return entity.Pengumuman{}, updateStatus.Error
	}
	old.Status = req.Status
	return old, nil
}
