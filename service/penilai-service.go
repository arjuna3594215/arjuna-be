package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"os"
	"strconv"
	"time"

	"github.com/vcraescu/go-paginator/v2"
	"github.com/vcraescu/go-paginator/v2/adapter"
	"github.com/vcraescu/go-paginator/v2/view"
)

func ListKinerjaPenilaian(req entity.KinerjaPenilaiDTO) ([]entity.KinerjaPenilai, error) {
	var res []entity.KinerjaPenilai
	var err error
	if req.Penilai == "management" {
		data, errs := getKinerjaPenilaiManagement(req.Periode)
		res = data
		err = errs
	} else if req.Penilai == "issue" {
		data, errs := getKinerjaPenilaiIssue(req.Periode)
		res = data
		err = errs
	} else if req.Penilai == "evaluator" {
		data, errs := getKinerjaPenilaiEvaluator(req.Periode)
		res = data
		err = errs
	}
	return res, err
}

func getKinerjaPenilaiEvaluator(periode []string) ([]entity.KinerjaPenilai, error) {
	var kinerja []entity.KinerjaPenilai

	db := config.Con()
	defer config.CloseCon(db)

	hasil_penilaian := db.Table("penugasan_penilaian_evaluator t1")
	hasil_penilaian.Select("ta.id_penugasan_penilaian_evaluator,ta.sts_penetapan_penilaian,sum(ta.nilai )")
	hasil_penilaian.Joins("LEFT JOIN public.hasil_penilaian_evaluator ta ON ta.id_penugasan_penilaian_evaluator = t1.id_penugasan_penilaian_evaluator")
	hasil_penilaian.Where("to_char(t1.tgl_created, 'MM-YYYY') IN ?", periode)
	hasil_penilaian.Group("ta.id_penugasan_penilaian_evaluator,ta.sts_penetapan_penilaian")

	data := db.Table("penugasan_penilaian_evaluator AS t1")
	data.Select("t1.id_personal, t5.nama, COUNT( t1.id_penugasan_penilaian_evaluator ) AS jml_ditawarkan, SUM (CASE WHEN t1.sts_penerimaan_penugasan IS NULL THEN 1 ELSE 0 END) AS jml_blm_diputuskan, SUM (CASE WHEN t1.sts_penerimaan_penugasan = '0' THEN 1  ELSE 0 END) AS jml_ditolak, SUM (CASE WHEN t1.sts_penerimaan_penugasan = '1' THEN 1  ELSE 0 END) AS jml_diterima, SUM (CASE WHEN (ta.id_penugasan_penilaian_evaluator IS NULL AND t1.sts_penerimaan_penugasan = '1') THEN 1 ELSE 0 END) AS jml_belumdilaksanakan, SUM (CASE WHEN (ta.id_penugasan_penilaian_evaluator IS NOT NULL) THEN 1 ELSE 0 END) AS jml_dilaksanakan, SUM (CASE WHEN ta.sts_penetapan_penilaian = '1' THEN 1 END) AS jml_ditetapkan")
	data.Joins("INNER JOIN usulan_akreditasi AS t2 ON t2.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("INNER JOIN pic AS t3 ON t3.id_identitas_jurnal = t2.id_identitas_jurnal AND t3.sts_aktif_pic = '1'")
	data.Joins("INNER JOIN eic AS t4 ON t4.id_identitas_jurnal = t2.id_identitas_jurnal AND t4.sts_aktif_eic = '1'")
	data.Joins("INNER JOIN personal AS t5 ON t5.id_personal = t1.id_personal")
	data.Joins("LEFT JOIN ( ? ) AS ta ON ta.id_penugasan_penilaian_evaluator = t1.id_penugasan_penilaian_evaluator", hasil_penilaian)
	data.Where("to_char(t1.tgl_created, 'MM-YYYY') IN ?", periode)
	data.Group("t1.id_personal, t5.nama")
	data.Order("t5.nama")
	data.Find(&kinerja)

	if data.Error != nil {
		return nil, data.Error
	}
	return kinerja, nil
}

func getKinerjaPenilaiIssue(periode []string) ([]entity.KinerjaPenilai, error) {
	var kinerja []entity.KinerjaPenilai

	db := config.Con()
	defer config.CloseCon(db)

	hasil_penilaian := db.Table("penugasan_penilaian_issue t1")
	hasil_penilaian.Select("ta.id_penugasan_penilaian_issue,ta.sts_penetapan_penilaian,sum(ta.nilai )")
	hasil_penilaian.Joins("LEFT JOIN public.hasil_penilaian_issue ta ON ta.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue")
	hasil_penilaian.Where("to_char(t1.tgl_created, 'MM-YYYY') IN ?", periode)
	hasil_penilaian.Group("ta.id_penugasan_penilaian_issue,ta.sts_penetapan_penilaian")

	data := db.Table("penugasan_penilaian_issue AS t1")
	data.Select("t1.id_personal, t5.nama, COUNT( t1.id_penugasan_penilaian_issue ) AS jml_ditawarkan, SUM (CASE WHEN t1.sts_penerimaan_penugasan IS NULL THEN 1 ELSE 0 END) AS jml_blm_diputuskan, SUM (CASE WHEN t1.sts_penerimaan_penugasan = '0' THEN 1  ELSE 0 END) AS jml_ditolak, SUM (CASE WHEN t1.sts_penerimaan_penugasan = '1' THEN 1  ELSE 0 END) AS jml_diterima, SUM (CASE WHEN (ta.id_penugasan_penilaian_issue IS NULL AND t1.sts_penerimaan_penugasan = '1') THEN 1 ELSE 0 END) AS jml_belumdilaksanakan, SUM (CASE WHEN (ta.id_penugasan_penilaian_issue IS NOT NULL) THEN 1 ELSE 0 END) AS jml_dilaksanakan, SUM (CASE WHEN ta.sts_penetapan_penilaian = '1' THEN 1 END) AS jml_ditetapkan")
	data.Joins("INNER JOIN usulan_akreditasi AS t2 ON t2.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("INNER JOIN pic AS t3 ON t3.id_identitas_jurnal = t2.id_identitas_jurnal AND t3.sts_aktif_pic = '1'")
	data.Joins("INNER JOIN eic AS t4 ON t4.id_identitas_jurnal = t2.id_identitas_jurnal AND t4.sts_aktif_eic = '1'")
	data.Joins("INNER JOIN personal AS t5 ON t5.id_personal = t1.id_personal")
	data.Joins("LEFT JOIN ( ? ) AS ta ON ta.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue", hasil_penilaian)
	data.Where("to_char(t1.tgl_created, 'MM-YYYY') IN ?", periode)
	data.Group("t1.id_personal, t5.nama")
	data.Order("t5.nama")
	data.Find(&kinerja)

	if data.Error != nil {
		return nil, data.Error
	}
	return kinerja, nil
}

func getKinerjaPenilaiManagement(periode []string) ([]entity.KinerjaPenilai, error) {
	var kinerja []entity.KinerjaPenilai

	db := config.Con()
	defer config.CloseCon(db)

	hasil_penilaian := db.Table("penugasan_penilaian_mgmt t1")
	hasil_penilaian.Select("ta.id_penugasan_penilaian_manajemen,ta.sts_penetapan_penilaian,sum(ta.nilai )")
	hasil_penilaian.Joins("LEFT JOIN public.hasil_penilaian_mgmt ta ON ta.id_penugasan_penilaian_manajemen = t1.id_penugasan_penilaian_manajemen")
	hasil_penilaian.Where("to_char(t1.tgl_created, 'MM-YYYY') IN ?", periode)
	hasil_penilaian.Group("ta.id_penugasan_penilaian_manajemen,ta.sts_penetapan_penilaian")

	data := db.Table("penugasan_penilaian_mgmt AS t1")
	data.Select("t1.id_personal, t5.nama, COUNT( t1.id_penugasan_penilaian_manajemen ) AS jml_ditawarkan, SUM (CASE WHEN t1.sts_penerimaan_penugasan IS NULL THEN 1 ELSE 0 END) AS jml_blm_diputuskan, SUM (CASE WHEN t1.sts_penerimaan_penugasan = '0' THEN 1  ELSE 0 END) AS jml_ditolak, SUM (CASE WHEN t1.sts_penerimaan_penugasan = '1' THEN 1  ELSE 0 END) AS jml_diterima, SUM (CASE WHEN (ta.id_penugasan_penilaian_manajemen IS NULL AND t1.sts_penerimaan_penugasan = '1') THEN 1 ELSE 0 END) AS jml_belumdilaksanakan, SUM (CASE WHEN (ta.id_penugasan_penilaian_manajemen IS NOT NULL) THEN 1 ELSE 0 END) AS jml_dilaksanakan, SUM (CASE WHEN ta.sts_penetapan_penilaian = '1' THEN 1 END) AS jml_ditetapkan")
	data.Joins("INNER JOIN usulan_akreditasi AS t2 ON t2.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("INNER JOIN pic AS t3 ON t3.id_identitas_jurnal = t2.id_identitas_jurnal AND t3.sts_aktif_pic = '1'")
	data.Joins("INNER JOIN eic AS t4 ON t4.id_identitas_jurnal = t2.id_identitas_jurnal AND t4.sts_aktif_eic = '1'")
	data.Joins("INNER JOIN personal AS t5 ON t5.id_personal = t1.id_personal")
	data.Joins("LEFT JOIN ( ? ) AS ta ON ta.id_penugasan_penilaian_manajemen = t1.id_penugasan_penilaian_manajemen", hasil_penilaian)
	data.Where("to_char(t1.tgl_created, 'MM-YYYY') IN ?", periode)
	data.Group("t1.id_personal, t5.nama")
	data.Order("t5.nama")
	data.Find(&kinerja)

	if data.Error != nil {
		return nil, data.Error
	}
	return kinerja, nil
}

func DetailListJurnalKinerjaPenilaian(req entity.KinerjaPenilaiDTO) ([]entity.DetailListJurnalKinerjaPenilai, error) {
	var res []entity.DetailListJurnalKinerjaPenilai
	var err error
	if req.Penilai == "management" {
		data, errs := getDetailListJurnalKinerjaPenilaiManagement(req.Periode, req.Status, req.Personal)
		res = data
		err = errs
	} else if req.Penilai == "issue" {
		data, errs := getDetailListJurnalKinerjaPenilaiIssue(req.Periode, req.Status, req.Personal)
		res = data
		err = errs
	} else if req.Penilai == "evaluator" {
		data, errs := getDetailListJurnalKinerjaPenilaiEvaluator(req.Periode, req.Status, req.Personal)
		res = data
		err = errs
	}
	return res, err
}

func getDetailListJurnalKinerjaPenilaiManagement(periode []string, status string, personal string) ([]entity.DetailListJurnalKinerjaPenilai, error) {
	var res []entity.DetailListJurnalKinerjaPenilai

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penugasan_penilaian_mgmt AS t1").Distinct()
	data.Select("t1.id_personal, ij.nama_jurnal, t1.id_penugasan_penilaian_manajemen as id_penugasan_penilaian")
	data.Joins("INNER JOIN usulan_akreditasi AS t2 ON t2.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("INNER JOIN identitas_jurnal AS ij ON ij.id_identitas_jurnal = t2.id_identitas_jurnal")
	data.Joins("INNER JOIN pic AS t3 ON t3.id_identitas_jurnal = t2.id_identitas_jurnal AND t3.sts_aktif_pic = '1'")
	data.Joins("INNER JOIN eic AS t4 ON t4.id_identitas_jurnal = t2.id_identitas_jurnal AND t4.sts_aktif_eic = '1'")
	data.Joins("INNER JOIN personal AS t5 ON t5.id_personal = t1.id_personal")
	data.Where("to_char(t1.tgl_created, 'MM-YYYY') IN ?", periode)
	data.Where("t1.id_personal = ?", personal)
	if status == "2" {
		data.Where("t1.sts_penerimaan_penugasan IS NULL")
	} else if status == "3" {
		data.Where("t1.sts_penerimaan_penugasan = ?", "0")
	} else if status == "4" {
		data.Where("t1.sts_penerimaan_penugasan = ?", "1")
	} else if status == "5" {
		data.Joins("LEFT JOIN hasil_penilaian_mgmt ta ON ta.id_penugasan_penilaian_manajemen = t1.id_penugasan_penilaian_manajemen")
		data.Where("ta.id_penugasan_penilaian_manajemen IS NULL")
		data.Where("t1.sts_penerimaan_penugasan = ?", "1")
	} else if status == "6" {
		data.Joins("LEFT JOIN hasil_penilaian_mgmt ta ON ta.id_penugasan_penilaian_manajemen = t1.id_penugasan_penilaian_manajemen")
		data.Where("ta.id_penugasan_penilaian_manajemen IS NOT NULL")
	} else if status == "7" {
		data.Joins("LEFT JOIN hasil_penilaian_mgmt ta ON ta.id_penugasan_penilaian_manajemen = t1.id_penugasan_penilaian_manajemen")
		data.Where("ta.sts_penetapan_penilaian = ?", "1")
	}
	data.Order("ij.nama_jurnal")
	data.Find(&res)

	if data.Error != nil {
		return nil, data.Error
	}
	return res, nil
}

func getDetailListJurnalKinerjaPenilaiIssue(periode []string, status string, personal string) ([]entity.DetailListJurnalKinerjaPenilai, error) {
	var res []entity.DetailListJurnalKinerjaPenilai

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penugasan_penilaian_issue AS t1").Distinct()
	data.Select("t1.id_personal, ij.nama_jurnal, t1.id_penugasan_penilaian_issue as id_penugasan_penilaian")
	data.Joins("INNER JOIN usulan_akreditasi AS t2 ON t2.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("INNER JOIN identitas_jurnal AS ij ON ij.id_identitas_jurnal = t2.id_identitas_jurnal")
	data.Joins("INNER JOIN pic AS t3 ON t3.id_identitas_jurnal = t2.id_identitas_jurnal AND t3.sts_aktif_pic = '1'")
	data.Joins("INNER JOIN eic AS t4 ON t4.id_identitas_jurnal = t2.id_identitas_jurnal AND t4.sts_aktif_eic = '1'")
	data.Joins("INNER JOIN personal AS t5 ON t5.id_personal = t1.id_personal")
	data.Where("to_char(t1.tgl_created, 'MM-YYYY') IN ?", periode)
	data.Where("t1.id_personal = ?", personal)
	if status == "2" {
		data.Where("t1.sts_penerimaan_penugasan IS NULL")
	} else if status == "3" {
		data.Where("t1.sts_penerimaan_penugasan = ?", "0")
	} else if status == "4" {
		data.Where("t1.sts_penerimaan_penugasan = ?", "1")
	} else if status == "5" {
		data.Joins("LEFT JOIN hasil_penilaian_issue ta ON ta.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue")
		data.Where("ta.id_penugasan_penilaian_issue IS NULL")
		data.Where("t1.sts_penerimaan_penugasan = ?", "1")
	} else if status == "6" {
		data.Joins("LEFT JOIN hasil_penilaian_issue ta ON ta.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue")
		data.Where("ta.id_penugasan_penilaian_issue IS NOT NULL")
	} else if status == "7" {
		data.Joins("LEFT JOIN hasil_penilaian_issue ta ON ta.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue")
		data.Where("ta.sts_penetapan_penilaian = ?", "1")
	}
	data.Order("ij.nama_jurnal")
	data.Find(&res)

	if data.Error != nil {
		return nil, data.Error
	}
	return res, nil
}

func getDetailListJurnalKinerjaPenilaiEvaluator(periode []string, status string, personal string) ([]entity.DetailListJurnalKinerjaPenilai, error) {
	var res []entity.DetailListJurnalKinerjaPenilai

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penugasan_penilaian_evaluator AS t1").Distinct()
	data.Select("t1.id_personal, ij.nama_jurnal, t1.id_penugasan_penilaian_evaluator as id_penugasan_penilaian")
	data.Joins("INNER JOIN usulan_akreditasi AS t2 ON t2.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("INNER JOIN identitas_jurnal AS ij ON ij.id_identitas_jurnal = t2.id_identitas_jurnal")
	data.Joins("INNER JOIN pic AS t3 ON t3.id_identitas_jurnal = t2.id_identitas_jurnal AND t3.sts_aktif_pic = '1'")
	data.Joins("INNER JOIN eic AS t4 ON t4.id_identitas_jurnal = t2.id_identitas_jurnal AND t4.sts_aktif_eic = '1'")
	data.Joins("INNER JOIN personal AS t5 ON t5.id_personal = t1.id_personal")
	data.Where("to_char(t1.tgl_created, 'MM-YYYY') IN ?", periode)
	data.Where("t1.id_personal = ?", personal)
	if status == "2" {
		data.Where("t1.sts_penerimaan_penugasan IS NULL")
	} else if status == "3" {
		data.Where("t1.sts_penerimaan_penugasan = ?", "0")
	} else if status == "4" {
		data.Where("t1.sts_penerimaan_penugasan = ?", "1")
	} else if status == "5" {
		data.Joins("LEFT JOIN hasil_penilaian_evaluator ta ON ta.id_penugasan_penilaian_evaluator = t1.id_penugasan_penilaian_evaluator")
		data.Where("ta.id_penugasan_penilaian_evaluator IS NULL")
		data.Where("t1.sts_penerimaan_penugasan = ?", "1")
	} else if status == "6" {
		data.Joins("LEFT JOIN hasil_penilaian_evaluator ta ON ta.id_penugasan_penilaian_evaluator = t1.id_penugasan_penilaian_evaluator")
		data.Where("ta.id_penugasan_penilaian_evaluator IS NOT NULL")
	} else if status == "7" {
		data.Joins("LEFT JOIN hasil_penilaian_evaluator ta ON ta.id_penugasan_penilaian_evaluator = t1.id_penugasan_penilaian_evaluator")
		data.Where("ta.sts_penetapan_penilaian = ?", "1")
	}
	data.Order("ij.nama_jurnal")
	data.Find(&res)

	if data.Error != nil {
		return nil, data.Error
	}
	return res, nil
}

func DetailKinerjaPenilaian(penilai string, personal string) (interface{}, error) {
	var res entity.DetailKinerjaPenilai
	var err error
	if penilai == "management" {
		data, errs := getDetailPenilaiManagement(personal)
		res = data
		err = errs
	} else if penilai == "issue" {
		data, errs := getDetailKinerjaPenilaiIssue(personal)
		res = data
		err = errs
	} else if penilai == "evaluator" {
		data, errs := getDetailKinerjaPenilaiEvaluator(personal)
		res = data
		err = errs
	}
	return res, err
}

func getDetailKinerjaPenilaiIssue(personal string) (entity.DetailKinerjaPenilai, error) {
	var res entity.DetailKinerjaPenilai
	db := config.Con()
	defer config.CloseCon(db)

	db.Table("personal").Select("nama").Where("id_personal = ?", personal).Scan(&res.Nama)

	total_penugasan := db.Table("penugasan_penilaian_issue")
	total_penugasan.Where("id_personal = ?", personal)
	total_penugasan.Count(&res.TotalPenugasan)

	if total_penugasan.Error != nil {
		return entity.DetailKinerjaPenilai{}, total_penugasan.Error
	}

	total_usulan_ditolak := db.Table("penugasan_penilaian_issue")
	total_usulan_ditolak.Where("id_personal = ?", personal)
	total_usulan_ditolak.Where("sts_penerimaan_penugasan = ?", "0")
	total_usulan_ditolak.Count(&res.TotalUsulanDitolak)

	if total_usulan_ditolak.Error != nil {
		return entity.DetailKinerjaPenilai{}, total_usulan_ditolak.Error
	}

	total_usulan_selesai := db.Table("penugasan_penilaian_issue")
	total_usulan_selesai.Where("id_personal = ?", personal)
	total_usulan_selesai.Where("sts_penerimaan_penugasan = ?", "1")
	total_usulan_selesai.Where("sts_pelaksanaan_tugas = ?", "1")
	total_usulan_selesai.Count(&res.TotalUsulanSelesai)

	if total_usulan_selesai.Error != nil {
		return entity.DetailKinerjaPenilai{}, total_usulan_selesai.Error
	}

	total_jurnal_usulan_selesai := db.Table("penugasan_penilaian_issue")
	total_jurnal_usulan_selesai.Where("id_personal = ?", personal)
	total_jurnal_usulan_selesai.Where("sts_penerimaan_penugasan = ?", "1")
	total_jurnal_usulan_selesai.Where("sts_pelaksanaan_tugas = ?", "1")
	total_jurnal_usulan_selesai.Count(&res.TotalJurnalUsulanSelesai)

	if total_jurnal_usulan_selesai.Error != nil {
		return entity.DetailKinerjaPenilai{}, total_jurnal_usulan_selesai.Error
	}

	usulan_on_progres := db.Table("penugasan_penilaian_issue")
	usulan_on_progres.Where("id_personal = ?", personal)
	usulan_on_progres.Where("sts_penerimaan_penugasan = ?", "1")
	usulan_on_progres.Where("sts_pelaksanaan_tugas = ?", "0")
	usulan_on_progres.Count(&res.UsulanOnProgres)

	if usulan_on_progres.Error != nil {
		return entity.DetailKinerjaPenilai{}, usulan_on_progres.Error
	}

	var id []int64
	sub_q := db.Table("hasil_penilaian_issue as t1").Distinct()
	sub_q.Select("t1.id_penugasan_penilaian_issue")
	sub_q.Joins("INNER JOIN penugasan_penilaian_issue as t2 on t2.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue")
	sub_q.Where("t2.id_personal = ?", personal)
	sub_q.Where("t2.sts_penerimaan_penugasan = ?", "1")
	sub_q.Find(&id)

	usulan_belum_dinilai := db.Table("penugasan_penilaian_issue")
	usulan_belum_dinilai.Where("id_personal = ?", personal)
	usulan_belum_dinilai.Where("sts_penerimaan_penugasan = ?", "1")
	usulan_belum_dinilai.Where("id_penugasan_penilaian_issue NOT IN ?", id)
	usulan_belum_dinilai.Count(&res.UsulanBelumDinilai)

	if usulan_belum_dinilai.Error != nil {
		return entity.DetailKinerjaPenilai{}, usulan_belum_dinilai.Error
	}

	usulan_belum_diterima := db.Table("penugasan_penilaian_issue")
	usulan_belum_diterima.Where("id_personal = ?", personal)
	usulan_belum_diterima.Where("sts_penerimaan_penugasan IS NULL")
	usulan_belum_diterima.Count(&res.UsulanBelumDiterima)

	if usulan_belum_diterima.Error != nil {
		return entity.DetailKinerjaPenilai{}, usulan_belum_diterima.Error
	}
	return res, nil
}

func getDetailPenilaiManagement(personal string) (entity.DetailKinerjaPenilai, error) {
	var res entity.DetailKinerjaPenilai
	db := config.Con()
	defer config.CloseCon(db)

	db.Table("personal").Select("nama").Where("id_personal = ?", personal).Scan(&res.Nama)

	total_penugasan := db.Table("penugasan_penilaian_mgmt")
	total_penugasan.Where("id_personal = ?", personal)
	total_penugasan.Count(&res.TotalPenugasan)

	if total_penugasan.Error != nil {
		return entity.DetailKinerjaPenilai{}, total_penugasan.Error
	}

	total_usulan_ditolak := db.Table("penugasan_penilaian_mgmt")
	total_usulan_ditolak.Where("id_personal = ?", personal)
	total_usulan_ditolak.Where("sts_penerimaan_penugasan = ?", "0")
	total_usulan_ditolak.Count(&res.TotalUsulanDitolak)

	if total_usulan_ditolak.Error != nil {
		return entity.DetailKinerjaPenilai{}, total_usulan_ditolak.Error
	}

	total_usulan_selesai := db.Table("penugasan_penilaian_mgmt")
	total_usulan_selesai.Where("id_personal = ?", personal)
	total_usulan_selesai.Where("sts_penerimaan_penugasan = ?", "1")
	total_usulan_selesai.Where("sts_pelaksanaan_tugas = ?", "1")
	total_usulan_selesai.Count(&res.TotalUsulanSelesai)

	if total_usulan_selesai.Error != nil {
		return entity.DetailKinerjaPenilai{}, total_usulan_selesai.Error
	}

	total_jurnal_usulan_selesai := db.Table("penugasan_penilaian_mgmt")
	total_jurnal_usulan_selesai.Where("id_personal = ?", personal)
	total_jurnal_usulan_selesai.Where("sts_penerimaan_penugasan = ?", "1")
	total_jurnal_usulan_selesai.Where("sts_pelaksanaan_tugas = ?", "1")
	total_jurnal_usulan_selesai.Count(&res.TotalJurnalUsulanSelesai)

	if total_jurnal_usulan_selesai.Error != nil {
		return entity.DetailKinerjaPenilai{}, total_jurnal_usulan_selesai.Error
	}

	usulan_on_progres := db.Table("penugasan_penilaian_mgmt")
	usulan_on_progres.Where("id_personal = ?", personal)
	usulan_on_progres.Where("sts_penerimaan_penugasan = ?", "1")
	usulan_on_progres.Where("sts_pelaksanaan_tugas = ?", "0")
	usulan_on_progres.Count(&res.UsulanOnProgres)

	if usulan_on_progres.Error != nil {
		return entity.DetailKinerjaPenilai{}, usulan_on_progres.Error
	}

	var id []int64
	sub_q := db.Table("hasil_penilaian_mgmt as t1").Distinct()
	sub_q.Select("t1.id_penugasan_penilaian_manajemen")
	sub_q.Joins("INNER JOIN penugasan_penilaian_mgmt as t2 on t2.id_penugasan_penilaian_manajemen = t1.id_penugasan_penilaian_manajemen")
	sub_q.Where("t2.id_personal = ?", personal)
	sub_q.Where("t2.sts_penerimaan_penugasan = ?", "1")
	sub_q.Find(&id)

	usulan_belum_dinilai := db.Table("penugasan_penilaian_mgmt")
	usulan_belum_dinilai.Where("id_personal = ?", personal)
	usulan_belum_dinilai.Where("sts_penerimaan_penugasan = ?", "1")
	usulan_belum_dinilai.Where("id_penugasan_penilaian_manajemen NOT IN ?", id)
	usulan_belum_dinilai.Count(&res.UsulanBelumDinilai)

	if usulan_belum_dinilai.Error != nil {
		return entity.DetailKinerjaPenilai{}, usulan_belum_dinilai.Error
	}

	usulan_belum_diterima := db.Table("penugasan_penilaian_mgmt")
	usulan_belum_diterima.Where("id_personal = ?", personal)
	usulan_belum_diterima.Where("sts_penerimaan_penugasan IS NULL")
	usulan_belum_diterima.Count(&res.UsulanBelumDiterima)

	if usulan_belum_diterima.Error != nil {
		return entity.DetailKinerjaPenilai{}, usulan_belum_diterima.Error
	}
	return res, nil
}

func getDetailKinerjaPenilaiEvaluator(personal string) (entity.DetailKinerjaPenilai, error) {
	var res entity.DetailKinerjaPenilai
	db := config.Con()
	defer config.CloseCon(db)

	db.Table("personal").Select("nama").Where("id_personal = ?", personal).Scan(&res.Nama)

	total_penugasan := db.Table("penugasan_penilaian_evaluator")
	total_penugasan.Where("id_personal = ?", personal)
	total_penugasan.Count(&res.TotalPenugasan)

	if total_penugasan.Error != nil {
		return entity.DetailKinerjaPenilai{}, total_penugasan.Error
	}

	total_usulan_ditolak := db.Table("penugasan_penilaian_evaluator")
	total_usulan_ditolak.Where("id_personal = ?", personal)
	total_usulan_ditolak.Where("sts_penerimaan_penugasan = ?", "0")
	total_usulan_ditolak.Count(&res.TotalUsulanDitolak)

	if total_usulan_ditolak.Error != nil {
		return entity.DetailKinerjaPenilai{}, total_usulan_ditolak.Error
	}

	total_usulan_selesai := db.Table("penugasan_penilaian_evaluator")
	total_usulan_selesai.Where("id_personal = ?", personal)
	total_usulan_selesai.Where("sts_penerimaan_penugasan = ?", "1")
	total_usulan_selesai.Where("sts_pelaksanaan_tugas = ?", "1")
	total_usulan_selesai.Count(&res.TotalUsulanSelesai)

	if total_usulan_selesai.Error != nil {
		return entity.DetailKinerjaPenilai{}, total_usulan_selesai.Error
	}

	total_jurnal_usulan_selesai := db.Table("penugasan_penilaian_evaluator")
	total_jurnal_usulan_selesai.Where("id_personal = ?", personal)
	total_jurnal_usulan_selesai.Where("sts_penerimaan_penugasan = ?", "1")
	total_jurnal_usulan_selesai.Where("sts_pelaksanaan_tugas = ?", "1")
	total_jurnal_usulan_selesai.Count(&res.TotalJurnalUsulanSelesai)

	if total_jurnal_usulan_selesai.Error != nil {
		return entity.DetailKinerjaPenilai{}, total_jurnal_usulan_selesai.Error
	}

	usulan_on_progres := db.Table("penugasan_penilaian_evaluator")
	usulan_on_progres.Where("id_personal = ?", personal)
	usulan_on_progres.Where("sts_penerimaan_penugasan = ?", "1")
	usulan_on_progres.Where("sts_pelaksanaan_tugas = ?", "0")
	usulan_on_progres.Count(&res.UsulanOnProgres)

	if usulan_on_progres.Error != nil {
		return entity.DetailKinerjaPenilai{}, usulan_on_progres.Error
	}

	var id []int64
	sub_q := db.Table("hasil_penilaian_evaluator as t1").Distinct()
	sub_q.Select("t1.id_penugasan_penilaian_evaluator")
	sub_q.Joins("INNER JOIN penugasan_penilaian_evaluator as t2 on t2.id_penugasan_penilaian_evaluator = t1.id_penugasan_penilaian_evaluator")
	sub_q.Where("t2.id_personal = ?", personal)
	sub_q.Where("t2.sts_penerimaan_penugasan = ?", "1")
	sub_q.Find(&id)

	usulan_belum_dinilai := db.Table("penugasan_penilaian_evaluator")
	usulan_belum_dinilai.Where("id_personal = ?", personal)
	usulan_belum_dinilai.Where("sts_penerimaan_penugasan = ?", "1")
	usulan_belum_dinilai.Where("id_penugasan_penilaian_evaluator NOT IN ?", id)
	usulan_belum_dinilai.Count(&res.UsulanBelumDinilai)

	if usulan_belum_dinilai.Error != nil {
		return entity.DetailKinerjaPenilai{}, usulan_belum_dinilai.Error
	}

	usulan_belum_diterima := db.Table("penugasan_penilaian_evaluator")
	usulan_belum_diterima.Where("id_personal = ?", personal)
	usulan_belum_diterima.Where("sts_penerimaan_penugasan IS NULL")
	usulan_belum_diterima.Count(&res.UsulanBelumDiterima)

	if usulan_belum_diterima.Error != nil {
		return entity.DetailKinerjaPenilai{}, usulan_belum_diterima.Error
	}
	return res, nil
}

func DetailKinerjaPenilaianBarChart(penilai string, personal string) (entity.BarChart, error) {
	var res entity.BarChart
	if penilai == "management" {
		res = barChart(getDetailKinerjaDataBarChartManagement(personal), "Rekapitulasi Penilaian berdasarkan Bidang Keilmuan")
	} else if penilai == "issue" {
		res = barChart(getDetailKinerjaDataBarChartIssue(personal), "Rekapitulasi Penilaian berdasarkan Bidang Keilmuan")
	} else if penilai == "evaluator" {
		res = barChart(getDetailKinerjaDataBarChartEvaluator(personal), "Rekapitulasi Penilaian berdasarkan Bidang Keilmuan")
	}
	return res, nil
}

func DetailKinerjaPenilaianLineChart(penilai string, personal string) (entity.LineChart, error) {
	var res entity.LineChart
	if penilai == "management" {
		res = lineChart(getDetailKinerjaDataLineChartManagement(personal), "Rekapitulasi Penilaian Tahunan")
	} else if penilai == "issue" {
		res = lineChart(getDetailKinerjaDataLineChartIssue(personal), "Rekapitulasi Penilaian Tahunan")
	} else if penilai == "evaluator" {
		res = lineChart(getDetailKinerjaDataLineChartEvaluator(personal), "Rekapitulasi Penilaian Tahunan")
	}
	return res, nil
}

func GetPeriodeKinerjaPenilai() ([]entity.ParentPeriodeKinerjaPenilai, error) {
	var parent []entity.ParentPeriodeKinerjaPenilai

	db := config.Con()
	defer config.CloseCon(db)

	periode_mgmt := db.Table("penugasan_penilaian_mgmt as t1")
	periode_mgmt.Select("extract(year from t1.tgl_created) as tahun, extract(month from t1.tgl_created) as bulan, to_char(t1.tgl_created,'MM-YYYY') periode, to_char(t1.tgl_created,'MON YYYY') periode_text")
	periode_mgmt.Group("1,2,3,4")
	periode_mgmt.Order("1")

	periode_issue := db.Table("penugasan_penilaian_issue as t1")
	periode_issue.Select("extract(year from t1.tgl_created) as tahun, extract(month from t1.tgl_created) as bulan, to_char(t1.tgl_created,'MM-YYYY') periode, to_char(t1.tgl_created,'MON YYYY') periode_text")
	periode_issue.Group("1,2,3,4")
	periode_issue.Order("1")

	periode_evaluator := db.Table("penugasan_penilaian_evaluator as t1")
	periode_evaluator.Select("extract(year from t1.tgl_created) as tahun, extract(month from t1.tgl_created) as bulan, to_char(t1.tgl_created,'MM-YYYY') periode, to_char(t1.tgl_created,'MON YYYY') periode_text")
	periode_evaluator.Group("1,2,3,4")
	periode_evaluator.Order("1")

	periode := db.Raw(" ( ? ) UNION ( ? ) UNION ( ? ) ", periode_mgmt, periode_issue, periode_evaluator)

	db.Table("( ? ) as tb", periode).Select("tb.tahun").Group("tb.tahun").Scan(&parent)

	for i, v := range parent {
		var child []entity.ChildPeriodeKinerjaPenilai
		db.Table("( ? ) as tb", periode).Select("*").Where("tb.tahun = ?", v.Tahun).Scan(&child)
		parent[i].Bulan = child
	}

	return parent, nil
}

func GetListPenugasanPenilaiEvaluator(id_usulan_akreditasi string, page string, row string) (helper.ResponsePagination, error) {
	var penugasan_penilaian_evaluator []entity.ListPenugasanPenilaiEvaluator

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penugasan_penilaian_evaluator as t1")
	data.Select("t1.id_penugasan_penilaian_evaluator, t2.id_personal, t2.nama as nama_penilai, t1.alasan_tolak, t1.tgl_created as tgl_penugasan, t1.sts_penerimaan_penugasan")
	data.Joins("INNER JOIN personal t2 ON t1.id_personal = t2.id_personal")
	data.Where("t1.id_usulan_akreditasi = ?", id_usulan_akreditasi)
	data.Order("t1.tgl_created DESC")
	data.Find(&penugasan_penilaian_evaluator)

	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(penugasan_penilaian_evaluator)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&penugasan_penilaian_evaluator); err != nil {
		panic(err)
	}

	for i, v := range penugasan_penilaian_evaluator {
		if v.StsPenerimaanPenugasan == "1" {
			penugasan_penilaian_evaluator[i].StsPenerimaanPenugasan = "Diterima"
		} else if v.StsPenerimaanPenugasan == "0" {
			penugasan_penilaian_evaluator[i].StsPenerimaanPenugasan = "Ditolak"
		} else {
			penugasan_penilaian_evaluator[i].StsPenerimaanPenugasan = "Blm. diputuskan"
		}
	}

	res := helper.BuildPaginationResponse(penugasan_penilaian_evaluator, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func GetListPenugasanPenilaiIssue(id_usulan_akreditasi string, page string, row string) (helper.ResponsePagination, error) {
	var penugasan_penilaian_issue []entity.ListPenugasanPenilaiIssue

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penugasan_penilaian_issue as t1")
	data.Distinct("t2.id_personal, t2.nama as nama_penilai, t1.tgl_created as tgl_penugasan, t1.sts_penerimaan_penugasan")
	data.Joins("INNER JOIN personal t2 ON t1.id_personal = t2.id_personal")
	data.Joins("LEFT JOIN public.usulan_akreditasi t3 ON t3.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Where("t1.id_usulan_akreditasi = ?", id_usulan_akreditasi)
	data.Where("t1.tgl_created >= t3.tgl_created")
	data.Order("t2.nama")
	data.Find(&penugasan_penilaian_issue)

	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(penugasan_penilaian_issue)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&penugasan_penilaian_issue); err != nil {
		panic(err)
	}

	for i, v := range penugasan_penilaian_issue {
		if v.StsPenerimaanPenugasan == "1" {
			penugasan_penilaian_issue[i].StsPenerimaanPenugasan = "Diterima"
		} else if v.StsPenerimaanPenugasan == "0" {
			penugasan_penilaian_issue[i].StsPenerimaanPenugasan = "Ditolak"
		} else {
			penugasan_penilaian_issue[i].StsPenerimaanPenugasan = "Blm. diputuskan"
		}
	}

	res := helper.BuildPaginationResponse(penugasan_penilaian_issue, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func GetListPenugasanPenilaiManagement(id_usulan_akreditasi string, page string, row string) (helper.ResponsePagination, error) {
	var penugasan_penilaian_mgmt []entity.ListPenugasanPenilaiManagement

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penugasan_penilaian_mgmt as t1")
	data.Select("t1.id_penugasan_penilaian_manajemen, t2.id_personal, t2.nama as nama_penilai, t1.tgl_created as tgl_penugasan, t1.sts_penerimaan_penugasan")
	data.Joins("INNER JOIN personal t2 ON t1.id_personal = t2.id_personal")
	data.Joins("LEFT JOIN public.usulan_akreditasi t3 ON t3.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Where("t1.id_usulan_akreditasi = ?", id_usulan_akreditasi)
	data.Where("t1.tgl_created >= t3.tgl_created")
	data.Order("t1.tgl_created DESC")
	data.Find(&penugasan_penilaian_mgmt)

	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(penugasan_penilaian_mgmt)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&penugasan_penilaian_mgmt); err != nil {
		panic(err)
	}

	for i, v := range penugasan_penilaian_mgmt {
		if v.StsPenerimaanPenugasan == "1" {
			penugasan_penilaian_mgmt[i].StsPenerimaanPenugasan = "Diterima"
		} else if v.StsPenerimaanPenugasan == "0" {
			penugasan_penilaian_mgmt[i].StsPenerimaanPenugasan = "Ditolak"
		} else {
			penugasan_penilaian_mgmt[i].StsPenerimaanPenugasan = "Blm. diputuskan"
		}
	}

	res := helper.BuildPaginationResponse(penugasan_penilaian_mgmt, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func CheckDuplicatePenugasanPenilaian(id_personal int64, id_usulan_akreditasi int64, assessor string) bool {
	var penilai_mgmt entity.PenugasanPenilaianManajemen
	var penilai_evaluator entity.PenugasanPenilaianEvaluator

	db := config.Con()
	defer config.CloseCon(db)

	if assessor == "Manajemen" {
		data := db.Table("penugasan_penilaian_mgmt").Where("id_usulan_akreditasi = ?", id_usulan_akreditasi).Where("id_personal = ?", id_personal).Take(&penilai_mgmt)
		if data.Error != nil {
			return true
		}
	} else if assessor == "Issue" {
		return true
	} else if assessor == "Evaluator" {
		data := db.Table("penugasan_penilaian_evaluator").Where("id_usulan_akreditasi = ?", id_usulan_akreditasi).Where("id_personal = ?", id_personal).Take(&penilai_evaluator)
		if data.Error != nil {
			return true
		}
	}

	return false
}

func InsertPenugasanPenilaian(req entity.SavePengusulPenilaiDTO) (interface{}, error) {
	var penilai_mgmt entity.PenugasanPenilaianManajemen
	var penilai_issue entity.PenugasanPenilaianIssue
	var penilai_evaluator entity.PenugasanPenilaianEvaluator
	var data_personal entity.Personal

	db := config.Con()
	defer config.CloseCon(db)

	if req.Assessor == "Manajemen" {
		penilai_mgmt.IDPersonal = req.IDPersonal
		penilai_mgmt.IDUsulanAkreditasi = req.IDUsulanAkreditasi
		save := db.Table("penugasan_penilaian_mgmt").Create(&penilai_mgmt)
		if save.Error != nil {
			return false, nil
		}
	} else if req.Assessor == "Issue" {
		for _, v := range req.IDIssue {
			var count int64
			var last_id int64
			db.Table("penugasan_penilaian_issue").Where("id_issue = ?", v).Where("id_personal = ?", req.IDPersonal).Count(&count)
			db.Table("penugasan_penilaian_issue").Select("id_penugasan_penilaian_issue").Order("id_penugasan_penilaian_issue desc").Limit(1).Scan(&last_id)
			if count < 1 {
				penilai_issue.IDPenugasanPenilaianIssue = last_id + 1
				penilai_issue.IDUsulanAkreditasi = req.IDUsulanAkreditasi
				penilai_issue.IDPersonal = req.IDPersonal
				penilai_issue.IDIssue = v
				db.Table("penugasan_penilaian_issue").Create(&penilai_issue)
			}
		}
		return true, nil
	} else if req.Assessor == "Evaluator" {
		penilai_evaluator.IDPersonal = req.IDPersonal
		penilai_evaluator.IDUsulanAkreditasi = req.IDUsulanAkreditasi
		save := db.Table("penugasan_penilaian_evaluator").Create(&penilai_evaluator)
		if save.Error != nil {
			return false, nil
		}
	}

	err := db.Table("personal").Where("id_personal=?", req.IDPersonal).Scan(&data_personal).Error
	if err != nil {
		return false, nil
	}
	penugasan_baru_mail := entity.PenugasanBaruMail{
		NamaPIC:    data_personal.Nama,
		HostArjuna: os.Getenv("HOST_ARJUNA"),
		EmailPIC:   data_personal.Email,
		Tahun:      time.Now().Year(),
	}
	helper.SendPenugasanBaruMail(penugasan_baru_mail)

	return true, nil
}

func DeletePenugasanPenilaian(req entity.DeletePengusulPenilaiDTO) (bool, error) {
	// var penilai_mgmt entity.PenugasanPenilaianManajemen
	// var penilai_issue entity.PenugasanPenilaianIssue
	// var penilai_evaluator entity.PenugasanPenilaianEvaluator
	var count int64

	db := config.Con()
	defer config.CloseCon(db)

	if req.Assessor == "Manajemen" {
		db.Raw("DELETE FROM penyesuaian_nilai_asessor_mgmt WHERE id_penugasan_penilaian_manajemen = ?", req.IDPenugasan)
		db.Raw("DELETE FROM disinsentif_penilaian_mgmt WHERE id_penugasan_penilaian_manajemen = ?", req.IDPenugasan)
		db.Raw("DELETE FROM komentar_disinsentif_mgmt WHERE id_penugasan_penilaian_manajemen = ?", req.IDPenugasan)
		db.Raw("DELETE FROM hasil_penilaian_mgmt WHERE id_penugasan_penilaian_manajemen = ?", req.IDPenugasan)
		db.Table("penugasan_penilaian_mgmt").Where("id_penugasan_penilaian_manajemen = ?", req.IDPenugasan).Delete(&entity.PenugasanPenilaianManajemen{})
	} else if req.Assessor == "Issue" {
		db.Table("penugasan_penilaian_issue").Where("id_personal = ?", req.IDPersonal).Where("id_usulan_akreditasi = ?", req.IDUsulanAkreditasi).Count(&count)
		if count > 0 {
			db.Table("penugasan_penilaian_issue").Where("id_personal = ?", req.IDPersonal).Where("id_usulan_akreditasi = ?", req.IDUsulanAkreditasi).Delete(&entity.PenugasanPenilaianIssue{})
		}
	} else if req.Assessor == "Evaluator" {
		db.Raw("DELETE FROM penyesuaian_nilai_asessor_evaluator WHERE id_penugasan_penilaian_evaluator = ?", req.IDPenugasan)
		db.Raw("DELETE FROM disinsentif_penilaian_evaluator WHERE id_penugasan_penilaian_evaluator = ?", req.IDPenugasan)
		db.Raw("DELETE FROM komentar_disinsentif_evaluator WHERE id_penugasan_penilaian_evaluator = ?", req.IDPenugasan)
		db.Raw("DELETE FROM hasil_penilaian_evaluator WHERE id_penugasan_penilaian_evaluator = ?", req.IDPenugasan)
		db.Table("penugasan_penilaian_evaluator").Where("id_penugasan_penilaian_evaluator = ?", req.IDPenugasan).Delete(&entity.PenugasanPenilaianEvaluator{})
	}

	return true, nil
}
