package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
)

func SendKontak(req entity.KontakDTO) (interface{}, error) {
	var kontak entity.Kontak
	db := config.Con()
	defer config.CloseCon(db)
	kontak.FirstName = req.FirstName
	kontak.LastName = req.LastName
	kontak.Email = req.Email
	kontak.Phone = req.Phone
	kontak.Subject = req.Subject
	kontak.Message = req.Message
	save := db.Save(&kontak)
	if save.Error != nil {
		return nil, save.Error
	}
	errorsendmail := helper.SendKontakMail(kontak)

	if errorsendmail != nil {
		return false, errorsendmail
	}
	return req, nil
}
