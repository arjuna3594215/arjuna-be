package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"errors"
	"strconv"
	"strings"
	"time"

	"github.com/vcraescu/go-paginator/v2"
	"github.com/vcraescu/go-paginator/v2/adapter"
	"github.com/vcraescu/go-paginator/v2/view"
)

func ListUsulanAkreditasiBaru(search string, page string, row string, status string) (helper.ResponsePagination, error) {
	var akreditasi_baru []entity.UsulanAkreditasiBaru

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	sub_q := db.Table("penetapan_desk_evaluasi")
	sub_q.Select("id_usulan_akreditasi")

	bidang_ilmu := db.Table("bidang_ilmu_jurnal ta")
	bidang_ilmu.Select("ta.id_jurnal, array_to_string(ARRAY_AGG(tb.bidang_ilmu), ', ') as bidang_ilmu")
	bidang_ilmu.Joins("INNER JOIN public.bidang_ilmu tb ON ta.id_bidang_ilmu = tb.id_bidang_ilmu")
	bidang_ilmu.Where("ta.sts_aktif_bidang_ilmu_jurnal = ?", "1")
	bidang_ilmu.Group("ta.id_jurnal")

	alasan_penolakan := db.Table("usulan_akreditasi_ditolak as uad")
	alasan_penolakan.Select("uad.alasan_penolakan")
	alasan_penolakan.Joins("LEFT JOIN usulan_akreditasi as ua on ua.id_usulan_akreditasi = uad.id_usulan_akreditasi")
	alasan_penolakan.Where("ua.id_identitas_jurnal = t2.id_identitas_jurnal")
	alasan_penolakan.Order("uad.tgl_created desc")
	alasan_penolakan.Limit(1)

	pemberi_komentar := db.Table("usulan_akreditasi_ditolak as uad")
	pemberi_komentar.Select("p.nama as pemberi_komentar")
	pemberi_komentar.Joins("LEFT JOIN usulan_akreditasi as ua on ua.id_usulan_akreditasi = uad.id_usulan_akreditasi")
	pemberi_komentar.Joins("LEFT JOIN personal as p on p.id_personal = uad.id_personal")
	pemberi_komentar.Where("ua.id_identitas_jurnal = t2.id_identitas_jurnal")
	pemberi_komentar.Order("uad.tgl_created desc")
	pemberi_komentar.Limit(1)

	status_usulan_akreditasi := db.Raw("SELECT arjuna_check_status_reakreditasi2 as status_usulan_akreditasi FROM arjuna_check_status_reakreditasi2(t2.id_usulan_akreditasi)")

	q := db.Table("usulan_akreditasi AS t2")
	q.Select("t2.id_usulan_akreditasi, t2.id_identitas_jurnal, t3.nama_jurnal, t2.id_pic, t2.id_eic, t2.user_penilai, t2.passwd_penilai, t2.file_dokumen_usulan, t2.tgl_updated AS tgl_pengajuan_usulan, t3.eissn, t3.pissn, t3.publisher, t3.society, t3.alamat_oai, t3.doi_jurnal, t5.nama_awal_jurnal, t5.tgl_pembuatan, t5.url_jurnal, t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat AS alamat_jurnal, t5.no_telepon AS no_telepon_jurnal, t5.alamat_surel AS alamat_surel_jurnal, t5.tgl_akhir_terakreditasi, t5.url_statistik_pengunjung, t7.nama AS nama_pic, t7.nama_institusi, t7.email AS email_pic, t7.no_hand_phone AS no_hand_phone_pic, t8.nama_eic, t8.no_telepon AS no_telepon_eic, t8.alamat_surel AS alamat_surel_eic, t1.nilai_total, t9.bidang_ilmu, ( ? ) as alasan_penolakan, ( ? ) as pemberi_komentar, ( ? ) as status_usulan_akreditasi", alasan_penolakan, pemberi_komentar, status_usulan_akreditasi)
	q.Joins("INNER JOIN hasil_sa as t1 on t1.id_usulan_akreditasi = t2.id_usulan_akreditasi")
	q.Joins("INNER JOIN identitas_jurnal AS t3 ON t3.id_identitas_jurnal = t2.id_identitas_jurnal")
	q.Joins("INNER JOIN progres_usulan_akreditasi AS t4 ON t4.id_usulan_akreditasi = t2.id_usulan_akreditasi")
	q.Joins("INNER JOIN jurnal AS t5 ON t5.id_jurnal = t3.id_jurnal")
	q.Joins("INNER JOIN pic AS t6 ON t6.id_pic = t2.id_pic")
	q.Joins("INNER JOIN personal AS t7 ON t7.id_personal = t6.id_personal")
	q.Joins("INNER JOIN eic AS t8 ON t8.id_eic = t2.id_eic ")
	q.Joins("LEFT JOIN ( ? ) as t9 ON t9.id_jurnal = t5.id_jurnal", bidang_ilmu)
	q.Where("t2.id_usulan_akreditasi NOT IN ( ? )", sub_q)
	q.Where("t2.sts_pengajuan_usulan = ?", "1")
	// if status == "1" {
	// 	// q.Where("t1.nilai_total >= ?", "70")
	// } else {
	// 	// q.Where("t1.nilai_total < ?", "70")
	// }
	q.Where("t3.sts_aktif_identitas_jurnal = ?", "1")
	//q.Where("t4.id_progres = ?", "1") * kondisinya dipindah ke bawah 
	q.Where("t3.nama_jurnal IS NOT NULL")
	if search != "" {
		//remark & edit by agus 2023-03-24 (tambah param search dgn eissn)
		//q.Where("lower(t3.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
		q.Where("lower(t3.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%").Or("lower(t3.eissn) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	q.Where("t4.id_progres = ?", "1")
	q.Order("t2.tgl_updated asc")
	data := db.Table("( ? ) as temp", q)

	//if status == "1" {
	//	data.Where("temp.status_usulan_akreditasi = ?", 0)
	//} else {
	//	data.Where("temp.status_usulan_akreditasi = ?", 1)
	//}
	if status == "0" {
		data.Where("temp.status_usulan_akreditasi = ?", 1)
	}
	data.Find(&akreditasi_baru)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(akreditasi_baru)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&akreditasi_baru); err != nil {
		panic(err)
	}

	res := helper.BuildPaginationResponse(akreditasi_baru, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func DetailUsulanAkreditasiBaru(id_usulan_akreditasi int) (interface{}, error) {
	var res entity.DetailUsulanAkreditasiBaru

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("usulan_akreditasi as t1")
	data.Select("t1.id_usulan_akreditasi, t1.id_identitas_jurnal, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society, t5.nama_awal_jurnal, t5.tgl_pembuatan, t5.url_jurnal, t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat AS alamat_jurnal, t5.no_telepon AS no_telepon_jurnal, t5.alamat_surel AS alamat_surel_jurnal, t5.tgl_akhir_terakreditasi, t1.id_pic, t7.nama AS nama_pic, t7.nama_institusi, t7.email AS email_pic, t7.no_hand_phone AS no_hand_phone_pic, t1.id_eic, t8.nama_eic, t8.no_telepon AS no_telepon_eic, t8.alamat_surel AS alamat_surel_eic, t4.alamat_oai, t4.doi_jurnal, t5.url_statistik_pengunjung, t4.url_etikapublikasi, t1.user_penilai, t1.passwd_penilai, t1.file_dokumen_usulan, t1.tgl_updated AS tgl_pengajuan_usulan, t2.nilai_total,t2.grade_akreditasi,t10.bidang_ilmu")
	data.Joins("LEFT JOIN hasil_sa AS t2 ON t2.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("INNER JOIN identitas_jurnal AS t4 ON t4.id_identitas_jurnal = t1.id_identitas_jurnal")
	data.Joins("INNER JOIN jurnal t5 ON t4.id_jurnal = t5.id_jurnal")
	data.Joins("INNER JOIN pic t6 ON t6.id_pic = t1.id_pic")
	data.Joins("INNER JOIN personal t7 ON t6.id_personal = t7.id_personal")
	data.Joins("INNER JOIN eic t8 ON t1.id_eic = t8.id_eic")
	data.Joins("LEFT JOIN bidang_ilmu_jurnal AS t9 ON t9.id_jurnal = t4.id_jurnal AND t9.sts_aktif_bidang_ilmu_jurnal = ?", "1")
	data.Joins("LEFT JOIN bidang_ilmu AS t10 ON t10.id_bidang_ilmu = t9.id_bidang_ilmu AND t10.sts_aktif_bidang_ilmu = ?", "1")
	data.Where("t1.id_usulan_akreditasi = ?", id_usulan_akreditasi)
	data.Where("t4.sts_aktif_identitas_jurnal = ?", "1")
	data.Take(&res)

	res.Issue = listIssueByUsulanAkreditasi(res.IDUsulanAkreditasi)
	res.IssueSebelumnya = listIssueByUsulanAkreditasiDiajukanSebelumnya(res.IDUsulanAkreditasi)

	if data.Error != nil {
		return nil, data.Error
	}
	return res, nil
}

func UsulanAction(req entity.UsulanActionDTO, id_personal int) (interface{}, error) {
	var count_usulan_akreditasi_ditolak int64
	var count_penetapan_desk_evaluasi int64
	var id_periode_akreditasi int64
	var id_identitas_jurnal int64
	var usulan_akreditasi_ditolak entity.UsulanAkreditasiDitolak
	var penetapan_desk_evaluasi entity.PenetapanDeskEvaluasi
	var data_mail_reject_usulan entity.DataMailRejectUsulan

	db := config.Con()
	defer config.CloseCon(db)

	db.Table("periode_akreditasi").Select("id_periode_akreditasi").Where("sts_aktif_periode_akreditasi = ?", "1").Scan(&id_periode_akreditasi)
	db.Table("usulan_akreditasi").Select("id_identitas_jurnal").Where("id_usulan_akreditasi = ?", req.IDUsulanAkreditasi).Scan(&id_identitas_jurnal)

	if req.StsHasilDeskEvaluasi == "0" {
		db.Table("usulan_akreditasi_ditolak").Where("id_usulan_akreditasi = ?", req.IDUsulanAkreditasi).Count(&count_usulan_akreditasi_ditolak)
		if count_usulan_akreditasi_ditolak > 0 {
			update_uad := db.Model(&entity.UsulanAkreditasiDitolak{}).Table("usulan_akreditasi_ditolak").Where("id_usulan_akreditasi = ?", req.IDUsulanAkreditasi).Updates(map[string]interface{}{"alasan_penolakan": req.AlasanPenolakan, "kd_sts_aktif": "1", "id_periode_akreditasi": id_periode_akreditasi, "tgl_updated": time.Now()})
			if update_uad.Error != nil {
				return nil, update_uad.Error
			}
		} else {
			usulan_akreditasi_ditolak.IDUsulanAkreditasi = req.IDUsulanAkreditasi
			usulan_akreditasi_ditolak.IDPersonal = int64(id_personal)
			usulan_akreditasi_ditolak.AlasanPenolakan = req.AlasanPenolakan
			usulan_akreditasi_ditolak.IDPeriodeAkreditasi = id_periode_akreditasi
			usulan_akreditasi_ditolak.TglCreated = time.Now()
			save_uad := db.Table("usulan_akreditasi_ditolak").Create(&usulan_akreditasi_ditolak)
			if save_uad.Error != nil {
				return nil, save_uad.Error
			}
		}
		update_progres_usulan_akreditasi := db.Table("progres_usulan_akreditasi").Where("id_usulan_akreditasi = ?", req.IDUsulanAkreditasi).Where("id_progres > ?", "0").Updates(map[string]interface{}{"id_progres": "4", "tgl_updated": time.Now()})
		if update_progres_usulan_akreditasi.Error != nil {
			return nil, update_progres_usulan_akreditasi.Error
		}

		penetapan_desk_evaluasi.IdIdentitasJurnal = id_identitas_jurnal
		penetapan_desk_evaluasi.IdUsulanAkreditasi = req.IDUsulanAkreditasi
		penetapan_desk_evaluasi.StsHasilDeskEvaluasi = "0"
		penetapan_desk_evaluasi.IdPersonal = int64(id_personal)
		penetapan_desk_evaluasi.IdPeriodeAkreditasi = id_periode_akreditasi
		penetapan_desk_evaluasi.TglCreated = time.Now()
		save_pde := db.Table("penetapan_desk_evaluasi").Create(&penetapan_desk_evaluasi)
		if save_pde.Error != nil {
			return nil, save_pde.Error
		}

		update_usulan_akreditasi := db.Table("usulan_akreditasi").Where("id_usulan_akreditasi = ?", req.IDUsulanAkreditasi).Updates(map[string]interface{}{"id_periode_akreditasi": id_periode_akreditasi, "tgl_updated": time.Now()})
		if update_usulan_akreditasi.Error != nil {
			return nil, update_usulan_akreditasi.Error
		}

		data := db.Table("usulan_akreditasi as t1")
		data.Select("to_char(t1.tgl_updated, 'YYYY-MM-DD') AS tgl_usulan, t4.nama_jurnal, t4.eissn, t5.nama_awal_jurnal, t7.nama AS nama_pic, t7.email AS email_pic")
		data.Joins("INNER JOIN identitas_jurnal AS t4 ON t4.id_identitas_jurnal = t1.id_identitas_jurnal")
		data.Joins("INNER JOIN jurnal t5 ON t4.id_jurnal = t5.id_jurnal")
		data.Joins("INNER JOIN pic t6 ON t6.id_pic = t1.id_pic")
		data.Joins("INNER JOIN personal t7 ON t6.id_personal = t7.id_personal")
		data.Where("t1.id_usulan_akreditasi = ?", req.IDUsulanAkreditasi)
		data.Take(&data_mail_reject_usulan)
		data_mail_reject_usulan.Comment = req.AlasanPenolakan

		errorsendmail := helper.SendRejectUsulanMail(data_mail_reject_usulan)

		if errorsendmail != nil {
			return false, errorsendmail
		}

	} else {
		db.Table("penetapan_desk_evaluasi").Where("id_usulan_akreditasi = ?", req.IDUsulanAkreditasi).Count(&count_penetapan_desk_evaluasi)

		if count_penetapan_desk_evaluasi > 0 {
			update_pde := db.Table("penetapan_desk_evaluasi").Where("id_usulan_akreditasi = ?", req.IDUsulanAkreditasi).Updates(map[string]interface{}{"sts_hasil_desk_evaluasi": req.StsHasilDeskEvaluasi, "id_personal": id_personal, "id_periode_akreditasi": id_periode_akreditasi, "tgl_updated": time.Now()})
			if update_pde.Error != nil {
				return nil, update_pde.Error
			}
		} else {
			penetapan_desk_evaluasi.IdIdentitasJurnal = id_identitas_jurnal
			penetapan_desk_evaluasi.IdUsulanAkreditasi = req.IDUsulanAkreditasi
			penetapan_desk_evaluasi.StsHasilDeskEvaluasi = req.StsHasilDeskEvaluasi
			penetapan_desk_evaluasi.IdPersonal = int64(id_personal)
			penetapan_desk_evaluasi.IdPeriodeAkreditasi = id_periode_akreditasi
			penetapan_desk_evaluasi.TglCreated = time.Now()
			save_pde := db.Table("penetapan_desk_evaluasi").Create(&penetapan_desk_evaluasi)
			if save_pde.Error != nil {
				return nil, save_pde.Error
			}
		}

		delete_usulan_akreditasi_ditolak := db.Table("usulan_akreditasi_ditolak").Where("id_usulan_akreditasi", req.IDUsulanAkreditasi).Delete(&entity.UsulanAkreditasiDitolak{})
		if delete_usulan_akreditasi_ditolak.Error != nil {
			return nil, delete_usulan_akreditasi_ditolak.Error
		}

		update_progres_usulan_akreditasi := db.Table("progres_usulan_akreditasi").Where("id_usulan_akreditasi = ?", req.IDUsulanAkreditasi).Where("id_progres > ?", "0").Updates(map[string]interface{}{"id_progres": "2", "tgl_updated": time.Now()})
		if update_progres_usulan_akreditasi.Error != nil {
			return nil, update_progres_usulan_akreditasi.Error
		}

		update_usulan_akreditasi := db.Table("usulan_akreditasi").Where("id_usulan_akreditasi = ?", req.IDUsulanAkreditasi).Updates(map[string]interface{}{"id_periode_akreditasi": id_periode_akreditasi, "tgl_updated": time.Now()})
		if update_usulan_akreditasi.Error != nil {
			return nil, update_usulan_akreditasi.Error
		}
	}
	return true, nil
}

func UsulanCancelAction(req entity.UsulanActionDTO, id_personal int) (interface{}, error) {
	var count_penetapan_desk_evaluasi int64
	var count_usulan_akreditasi_ditolak int64

	db := config.Con()
	defer config.CloseCon(db)

	if req.StsHasilDeskEvaluasi == "1" {
		db.Table("penetapan_desk_evaluasi").Where("id_usulan_akreditasi = ?", req.IDUsulanAkreditasi).Count(&count_penetapan_desk_evaluasi)
		if count_penetapan_desk_evaluasi > 0 {
			db.Table("penetapan_desk_evaluasi").Where("id_usulan_akreditasi = ?", req.IDUsulanAkreditasi).Delete(&entity.PenetapanDeskEvaluasi{})
		}
		db.Table("progres_usulan_akreditasi").Where("id_usulan_akreditasi = ?", req.IDUsulanAkreditasi).Updates(map[string]interface{}{"id_progres": 1, "tgl_updated": time.Now()})
	} else {
		db.Table("usulan_akreditasi_ditolak").Where("id_usulan_akreditasi = ?", req.IDUsulanAkreditasi).Count(&count_usulan_akreditasi_ditolak)
		if count_penetapan_desk_evaluasi > 0 {
			db.Table("usulan_akreditasi_ditolak").Where("id_usulan_akreditasi = ?", req.IDUsulanAkreditasi).Delete(&entity.UsulanAkreditasiDitolak{})
		}
		db.Table("usulan_akreditasi").Where("id_usulan_akreditasi = ?", req.IDUsulanAkreditasi).Updates(map[string]interface{}{"sts_pengajuan_usulan": 1, "tgl_updated": time.Now()})
	}

	return true, nil
}

//======================================================================================================================

func ListEvaluasiDokumen(search string, page string, row string, status string, evaluasi string) (helper.ResponsePagination, error) {
	var evaluasi_dokumen []entity.UsulanAkreditasiBaru

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	resubmit := db.Table("resubmit as ta")
	resubmit.Select("ta.id_resubmit, ta.id_usulan_akreditasi, ta.alasan_resubmit, ta.tgl_batas_resubmit")
	resubmit.Where("ta.kd_sts_resubmit = ?", "1")
	resubmit.Where("ta.tgl_batas_resubmit >= now()")

	status_usulan_akreditasi := db.Raw("SELECT arjuna_check_status_reakreditasi2 as status_usulan_akreditasi FROM arjuna_check_status_reakreditasi2(t1.id_usulan_akreditasi)")

	q := db.Table("usulan_akreditasi AS t1").Distinct()
	q.Select("t1.id_usulan_akreditasi, t1.id_identitas_jurnal, t3.nama_jurnal, t3.eissn, t3.pissn, t3.publisher, t3.society, t4.nama_awal_jurnal, t4.tgl_pembuatan, t4.url_jurnal, t4.url_contact, t4.url_editor, t4.country, t4.city, t4.alamat AS alamat_jurnal, t4.no_telepon AS no_telepon_jurnal, t4.alamat_surel AS alamat_surel_jurnal, t4.tgl_akhir_terakreditasi, t1.id_pic, t6.nama AS nama_pic, t6.nama_institusi, t6.email AS email_pic,t6.no_hand_phone AS no_hand_phone_pic, t1.id_eic, t7.nama_eic, t7.no_telepon AS no_telepon_eic, t7.alamat_surel AS alamat_surel_eic, t4.url_statistik_pengunjung, t3.alamat_oai, t3.doi_jurnal, t3.url_googlescholar, t1.user_penilai, t1.passwd_penilai, t1.file_dokumen_usulan, t1.tgl_updated AS tgl_pengajuan_usulan, t10.alasan_penolakan, t11.id_resubmit, t11.alasan_resubmit, t11.tgl_batas_resubmit, t9.bidang_ilmu, t2.nilai_total, t12.nama as pemberi_komentar, ( ? ) as status_usulan_akreditasi", status_usulan_akreditasi)
	q.Joins("INNER JOIN hasil_sa AS t2 ON t2.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	q.Joins("INNER JOIN identitas_jurnal AS t3 ON t3.id_identitas_jurnal = t1.id_identitas_jurnal")
	q.Joins("INNER JOIN jurnal AS t4 ON t4.id_jurnal = t3.id_jurnal")
	q.Joins("INNER JOIN pic AS t5 ON t5.id_pic = t1.id_pic")
	q.Joins("INNER JOIN personal AS t6 ON t6.id_personal = t5.id_personal")
	q.Joins("INNER JOIN eic AS t7 ON t7.id_eic = t1.id_eic")
	q.Joins("INNER JOIN bidang_ilmu_jurnal AS t8 ON t8.id_jurnal = t3.id_jurnal")
	q.Joins("INNER JOIN bidang_ilmu AS t9 ON t9.id_bidang_ilmu = t8.id_bidang_ilmu AND t8.sts_aktif_bidang_ilmu_jurnal = '1' AND t9.sts_aktif_bidang_ilmu = '1'")
	q.Joins("INNER JOIN usulan_akreditasi_ditolak AS t10 ON t10.id_usulan_akreditasi = t1.id_usulan_akreditasi AND t10.kd_sts_aktif = '1'")
	q.Joins("LEFT JOIN ( ? ) as t11 on t11.id_usulan_akreditasi = t1.id_usulan_akreditasi", resubmit)
	q.Joins("INNER JOIN personal as t12 on t12.id_personal = t10.id_personal")
	q.Where("t3.sts_aktif_identitas_jurnal = ?", "1")
	if search != "" {
		q.Where("lower(t3.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	// if status == "1" {
	// 	q.Where("t2.nilai_total >= ?", "70")
	// } else {
	// 	q.Where("t2.nilai_total < ?", "70")
	// }
	q.Where("t3.nama_jurnal IS NOT NULL")
	q.Order("tgl_pengajuan_usulan DESC")
	data := db.Table("( ? ) as temp", q)

	//if status == "1" {
	//	data.Where("temp.status_usulan_akreditasi = ?", 0)
	//} else {
	//	data.Where("temp.status_usulan_akreditasi = ?", 1)
	//}
	if status == "0" {
		data.Where("temp.status_usulan_akreditasi = ?", 1)
	}
	data.Find(&evaluasi_dokumen)

	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(evaluasi_dokumen)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&evaluasi_dokumen); err != nil {
		panic(err)
	}

	res := helper.BuildPaginationResponse(evaluasi_dokumen, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

//======================================================================================================================

func ListDistribusiPenilaian(search string, page string, row string, status string) (helper.ResponsePagination, error) {
	var res helper.ResponsePagination
	var err error

	data, errs := getDistribusiPenilaianLebihDari70(search, page, row, status)
	res = data
	err = errs
	// if status == "1" {
	// 	data, errs := getDistribusiPenilaianLebihDari70(search, page, row)
	// 	res = data
	// 	err = errs
	// } else {
	// 	data, errs := getDistribusiPenilaianKurangDari70(search, page, row)
	// 	res = data
	// 	err = errs
	// }

	if err != nil {
		return helper.ResponsePagination{}, err
	}

	return res, nil
}

func getDistribusiPenilaianLebihDari70(search string, page string, row string, status string) (helper.ResponsePagination, error) {
	var distribusi_penilaian []entity.DistribusiPenilaian
	var err error

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	// query := db.Raw("SELECT * from arjuna_list_penilaian_belum_mulai(( ? ), '1', ( ? ), ( ? ))", (pages - 1), search, rows)
	query := db.Raw("SELECT * from z_arjuna_list_penilaian_belum_mulai(( ? ), '1', ( ? ), ( ? ))", (pages - 1), search, rows)
	if status == "1" {
		query.Where("sts_akreditasi = ?", 0)
	} else {
		query.Where("sts_akreditasi = ?", 1)
	}

	data := db.Table("( ? ) as tb_temp", query)
	// if search != "" {
	// 	data.Where("lower(tb_temp.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	// }
	data.Scan(&distribusi_penilaian)

	// configuration := db.Table("configurasi")
	// configuration.Where("sts_aktif_configurasi = ?", "1")

	// sub_q_where_1 := db.Table("penetapan_desk_evaluasi as temp_1")
	// sub_q_where_1.Select("temp_1.id_usulan_akreditasi")
	// sub_q_where_1.Where("temp_1.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	// sub_q_where_1.Where("sts_hasil_desk_evaluasi = ?", "1")

	// data_issue := db.Table("usulan_akreditasi as t1")
	// data_issue.Select("t1.id_usulan_akreditasi, CASE WHEN SUM(CASE WHEN sts_penerimaan_penugasan = '1' THEN 1 ELSE 0 END) >= jml_asessor_issue THEN '1' ELSE '0' END as sts_distribusi")
	// data_issue.Joins("INNER JOIN penetapan_desk_evaluasi t2 ON t1.id_usulan_akreditasi = t2.id_usulan_akreditasi AND sts_hasil_desk_evaluasi = ?", "1")
	// data_issue.Joins("INNER JOIN issue t3 ON t1.id_usulan_akreditasi = t3.id_usulan_akreditasi")
	// data_issue.Joins("INNER JOIN penetapan_akreditasi t4 ON t1.id_usulan_akreditasi = t4.id_usulan_akreditasi AND t4.sts_hasil_akreditasi = ?", "1")
	// data_issue.Joins("LEFT JOIN penugasan_penilaian_issue as t5 on t5.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	// data_issue.Joins("CROSS JOIN ( SELECT tb.jml_asessor_issue FROM configurasi tb WHERE tb.sts_aktif_configurasi = '1' ) as di_c")
	// // data_issue.Where("sts_distribusi = ?", "0")
	// data_issue.Group("t1.id_usulan_akreditasi, di_c.jml_asessor_issue")

	// data_mgmt := db.Table("penugasan_penilaian_mgmt as t1")
	// data_mgmt.Select("t1.id_usulan_akreditasi, CASE WHEN (SUM(CASE WHEN t1.sts_penerimaan_penugasan = '1' THEN 1 ELSE 0 END) >= jml_asessor_mgmt) AND  (SUM(CASE WHEN t1.sts_penerimaan_penugasan = '1' THEN 1 ELSE 0 END)>=count(*)) THEN '1' ELSE '0' END as sts_distribusi")
	// data_mgmt.Joins("INNER JOIN penetapan_akreditasi as t4 ON t1.id_usulan_akreditasi = t4.id_usulan_akreditasi AND t4.sts_hasil_akreditasi = ?", "1")
	// data_mgmt.Joins("CROSS JOIN ( SELECT tb.jml_asessor_mgmt FROM public.configurasi tb WHERE tb.sts_aktif_configurasi = '1' ) as dm_c")
	// // data_mgmt.Where("t1.sts_penerimaan_penugasan = ?", "0")
	// data_mgmt.Group("t1.id_usulan_akreditasi, jml_asessor_mgmt")

	// data_issue_where := db.Table("( ? ) as tb_di", data_issue).Select("tb_di.id_usulan_akreditasi").Where("sts_distribusi = ?", "0")
	// data_mgmt_where := db.Table("( ? ) as tb_dm", data_mgmt).Select("tb_dm.id_usulan_akreditasi").Where("sts_distribusi != ?", "1")

	// sub_q_where_2 := db.Raw(" ( ? ) UNION ( ? ) ", data_issue_where, data_mgmt_where)

	// data := db.Table("usulan_akreditasi as t1")
	// // data.Select("")
	// data.Joins("INNER JOIN hasil_sa as hs on hs.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	// data.Joins("INNER JOIN identitas_jurnal t4 ON t1.id_identitas_jurnal = t4.id_identitas_jurnal")
	// data.Joins("INNER JOIN jurnal t5 ON t4.id_jurnal = t5.id_jurnal")
	// data.Joins("INNER JOIN pic t6 ON t6.id_identitas_jurnal = t4.id_identitas_jurnal")
	// data.Joins("INNER JOIN personal t7 ON t6.id_personal = t7.id_personal")
	// data.Joins("INNER JOIN eic t8 ON t1.id_eic = t8.id_eic")
	// data.Joins("CROSS JOIN ( ? ) as t13", configuration)
	// data.Joins("LEFT JOIN penetapan_desk_evaluasi t2 ON t1.id_usulan_akreditasi = t2.id_usulan_akreditasi")
	// data.Joins("INNER JOIN progres_usulan_akreditasi tp ON tp.id_usulan_akreditasi = t1.id_usulan_akreditasi")

	// data.Where("hs.nilai_total >= ?", 70)
	// data.Where("t4.sts_aktif_identitas_jurnal = ?", "1")
	// data.Where("t6.sts_aktif_pic = ?", "1")

	// data.Where("t1.id_usulan_akreditasi IN ( ? )", sub_q_where_1)
	// data.Where("t1.id_usulan_akreditasi IN ( ? )", sub_q_where_2)

	// data.Where("tp.id_progres != ?", 4)
	// data.Order("t1.tgl_updated asc")
	// data.Count(&count)

	if err != nil {
		return helper.ResponsePagination{}, err
	}

	// total := len(distribusi_penilaian)
	// p := paginator.New(adapter.NewGORMAdapter(data), rows)
	// p.SetPage(pages)
	// view := view.New(p)
	// if err := p.Results(&distribusi_penilaian); err != nil {
	// 	panic(err)
	// }

	// res := helper.BuildPaginationResponse(distribusi_penilaian, view)
	// res.PerPage = rows
	// res.Total = total
	// return res, nil

	var total int
	if len(distribusi_penilaian) > 0 {
		total = int(distribusi_penilaian[0].JmlRecord)
	} else {
		total = 0
	}
	res := helper.ResponsePagination{}
	res.Data = distribusi_penilaian
	res.CurrentPage = pages
	res.LastPage = ((total / rows) + 1)
	res.PerPage = rows
	res.Total = total

	return res, nil
}

func getDistribusiPenilaianKurangDari70(search string, page string, row string) (helper.ResponsePagination, error) {
	var distribusi_penilaian []entity.DistribusiPenilaian
	var err error

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	query := db.Raw("SELECT * from public.z_lists_distribusi_penilaian_kd_70('0', '')")

	data := db.Table("( ? ) as tb_temp", query)
	if search != "" {
		data.Where("lower(tb_temp.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Scan(&distribusi_penilaian)

	if err != nil {
		return helper.ResponsePagination{}, err
	}

	total := len(distribusi_penilaian)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&distribusi_penilaian); err != nil {
		panic(err)
	}

	res := helper.BuildPaginationResponse(distribusi_penilaian, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

//=======================================================================================================================

func ListsHasilAkhirPenilaian(search string, page string, row string, status string) (helper.ResponsePagination, error) {
	var res helper.ResponsePagination
	var err error

	data, errs := getHasilAkhirPenilaianLebihDari70(search, page, row, status)
	res = data
	err = errs
	// if status == "1" {
	// 	data, errs := getHasilAkhirPenilaianLebihDari70(search, page, row)
	// 	res = data
	// 	err = errs
	// } else {
	// 	data, errs := getHasilAkhirPenilaianKurangDari70(search, page, row)
	// 	res = data
	// 	err = errs
	// }

	if err != nil {
		return helper.ResponsePagination{}, err
	}

	return res, nil
}

func getHasilAkhirPenilaianLebihDari70(search string, page string, row string, status string) (helper.ResponsePagination, error) {
	var hasil_akhir_penilaian []entity.HasilPenilaianLebihDari70

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	db := config.Con()
	defer config.CloseCon(db)

	query := db.Raw("SELECT * from public.z_new_arjuna_list_penilaian_selesai_lb_70('1', '')")

	status_usulan_akreditasi := db.Raw("SELECT arjuna_check_status_reakreditasi2 as status_usulan_akreditasi FROM arjuna_check_status_reakreditasi2(tb_temp.id_usulan_akreditasi :: INTEGER)")

	q := db.Table("( ? ) as tb_temp", query)
	q.Select("tb_temp.*, ( ? ) as sts_akreditasi", status_usulan_akreditasi)
	data := db.Table("( ? ) as tb_temp", q)
	if status == "1" {
		data.Where("tb_temp.sts_akreditasi = ?", 0)
	} else {
		data.Where("tb_temp.sts_akreditasi = ?", 1)
	}
	if search != "" {
		data.Where("lower(tb_temp.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Scan(&hasil_akhir_penilaian)

	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(hasil_akhir_penilaian)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&hasil_akhir_penilaian); err != nil {
		panic(err)
	}

	res := helper.BuildPaginationResponse(hasil_akhir_penilaian, view)
	res.PerPage = rows
	res.Total = total

	return res, nil
}

func getHasilAkhirPenilaianKurangDari70(search string, page string, row string) (helper.ResponsePagination, error) {
	var hasil_akhir_penilaian []entity.HasilPenilaianKurangDari70

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	db := config.Con()
	defer config.CloseCon(db)

	query := db.Raw("SELECT * from public.z_new_arjuna_list_penilaian_selesai_kd_70('')")

	data := db.Table("( ? ) as tb_temp", query)
	if search != "" {
		data.Where("lower(tb_temp.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Scan(&hasil_akhir_penilaian)

	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(hasil_akhir_penilaian)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&hasil_akhir_penilaian); err != nil {
		panic(err)
	}

	res := helper.BuildPaginationResponse(hasil_akhir_penilaian, view)
	res.PerPage = rows
	res.Total = total

	return res, nil
}

//======================================================================================================================

func ListsHasilAkreditasi(search string, page string, row string, status string) (helper.ResponsePagination, error) {
	var hasil_akreditasi []entity.HasilAkreditasi

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	db := config.Con()
	defer config.CloseCon(db)

	// penugasan_penilaian_mgmt := db.Table("penugasan_penilaian_mgmt")
	// penugasan_penilaian_mgmt.Select("id_usulan_akreditasi")
	// penugasan_penilaian_mgmt.Where("sts_penerimaan_penugasan = ?", "1")
	// penugasan_penilaian_mgmt.Where("sts_pelaksanaan_tugas = ?", "1")

	// penugasan_penilaian_issue := db.Table("penugasan_penilaian_issue")
	// penugasan_penilaian_issue.Select("id_usulan_akreditasi")
	// penugasan_penilaian_issue.Where("sts_penerimaan_penugasan = ?", "1")
	// penugasan_penilaian_issue.Where("sts_pelaksanaan_tugas = ?", "1")

	// db.Table(" ( ( ? ) INTERSECT ( ? ) ) as t1", penugasan_penilaian_mgmt, penugasan_penilaian_issue).Distinct().Select("t1.id_usulan_akreditasi").Count(&hasil_akreditasi)
	status_usulan_akreditasi := db.Raw("SELECT arjuna_check_status_reakreditasi2 as status_usulan_akreditasi FROM arjuna_check_status_reakreditasi2(t1.id_usulan_akreditasi :: INTEGER)")

	q := db.Table("hasil_akreditasi as t1")
	q.Distinct("t1.id_usulan_akreditasi, t1.tgl_updated :: date AS tgl_penetapan_akreditasi, t5.url_statistik_pengunjung, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society, t5.nama_awal_jurnal,t5.tgl_pembuatan, t5.url_jurnal, t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat, t5.no_telepon, t5.alamat_surel, t4.alamat_oai, t4.doi_jurnal, t5.tgl_akhir_terakreditasi, t7.nama AS nama_pic, t7.nama_institusi, t8.nama_eic, t8.alamat_surel AS alamat_surel_eic, t1.nilai_total, t1.grade_akreditasi, t10.id_sk_akreditasi, t10.sts_published, ( ? ) as sts_akreditasi", status_usulan_akreditasi)
	q.Joins("INNER JOIN usulan_akreditasi AS t3 ON t3.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	q.Joins("INNER JOIN identitas_jurnal AS t4 ON t4.id_identitas_jurnal = t3.id_identitas_jurnal")
	q.Joins("INNER JOIN jurnal AS t5 ON t5.id_jurnal = t4.id_jurnal")
	q.Joins("INNER JOIN pic AS t6 ON t6.id_pic = t3.id_pic")
	q.Joins("INNER JOIN personal AS t7 ON t7.id_personal = t6.id_personal")
	q.Joins("INNER JOIN eic AS t8 ON t8.id_eic = t3.id_eic")
	q.Joins("LEFT JOIN penetapan_akreditasi AS t9 ON t9.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	q.Joins("LEFT JOIN sk_akreditasi AS t10 ON t10.id_sk_akreditasi = t9.id_sk_akreditasi")
	q.Where("t4.nama_jurnal IS NOT NULL")
	// if status == "1" {
	// 	q.Where("t1.nilai_total >= ?", 70)
	// } else {
	// 	q.Where("t1.nilai_total < ?", 70)
	// }
	if search != "" {
		q.Where("lower(t4.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	q.Order("tgl_penetapan_akreditasi DESC")
	data := db.Table("( ? ) as tb_temp", q)
	if status == "1" {
		data.Where("tb_temp.sts_akreditasi = ?", 0)
	} else {
		data.Where("tb_temp.sts_akreditasi = ?", 1)
	}
	data.Find(&hasil_akreditasi)

	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(hasil_akreditasi)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&hasil_akreditasi); err != nil {
		panic(err)
	}

	res := helper.BuildPaginationResponse(hasil_akreditasi, view)
	res.PerPage = rows
	res.Total = total

	return res, nil
}

func DetailHasilAkreditasi(id_sk int) (interface{}, error) {
	var res entity.DetailHasilAkreditasi

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("sk_akreditasi")
	data.Where("id_sk_akreditasi = ?", id_sk)
	data.Take(&res)

	if data.Error != nil {
		return nil, data.Error
	}
	return res, nil
}

func HasilAkreditasiPermanent(id_usulan_akreditasi int) (interface{}, error) {
	var res string

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Raw("select * from arjuna_set_permanen_penilaian( ? )", id_usulan_akreditasi).Scan(&res)

	if data.Error != nil {
		return nil, data.Error
	}
	return true, nil
}

func HasilAkreditasiPermanentAllAssesment() (interface{}, error) {
	var res string

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Raw("select * from arjuna_set_permanen_penilaian_all_running()").Scan(&res)

	if data.Error != nil {
		return nil, data.Error
	}
	return true, nil
}

func HasilAkreditasiCancelPermanent(id_usulan_akreditasi int) (interface{}, error) {
	var penugasan_mgmt []entity.PenugasanPenilaian
	var penugasan_issue []entity.PenugasanPenilaian

	db := config.Con()
	defer config.CloseCon(db)

	db.Table("penugasan_penilaian_mgmt").Select("id_penugasan_penilaian_manajemen as id_penugasan_penilaian, id_personal").Where("id_usulan_akreditasi = ?", id_usulan_akreditasi).Find(&penugasan_mgmt)
	db.Table("penugasan_penilaian_issue").Select("id_penugasan_penilaian_issue as id_penugasan_penilaian, id_personal").Where("id_usulan_akreditasi = ?", id_usulan_akreditasi).Find(&penugasan_issue)

	for _, v := range penugasan_mgmt {
		sub_q_where := db.Table("penugasan_penilaian_mgmt").Select("id_penugasan_penilaian_manajemen").Where("id_penugasan_penilaian_manajemen = ?", v.IDPenugasanPenilaian).Where("id_personal = ?", v.IDPersonal)
		db.Table("hasil_penilaian_mgmt").Model(&entity.HasilPenilaianMgmt{}).Where("id_penugasan_penilaian_manajemen IN ( ? )", sub_q_where).Updates(map[string]interface{}{"sts_penetapan_penilaian": "0", "tgl_updated": time.Now()})
		db.Table("disinsentif_penilaian_mgmt").Model(&entity.HasilPenilaianMgmt{}).Where("id_penugasan_penilaian_manajemen IN ( ? )", sub_q_where).Updates(map[string]interface{}{"sts_penetapan_penilaian": "0", "tgl_updated": time.Now()})
		db.Table("penugasan_penilaian_mgmt").Model(&entity.PenugasanPenilaianManajemen{}).Where("id_penugasan_penilaian_manajemen = ?", v.IDPenugasanPenilaian).Updates(map[string]interface{}{"sts_pelaksanaan_tugas": "0", "tgl_pelaksanaan_tugas": time.Now(), "tgl_updated": time.Now()})
	}

	for _, v := range penugasan_issue {
		sub_q_where := db.Table("penugasan_penilaian_issue").Select("id_penugasan_penilaian_issue").Where("id_penugasan_penilaian_issue = ?", v.IDPenugasanPenilaian).Where("id_personal = ?", v.IDPersonal)
		db.Table("hasil_penilaian_issue").Model(&entity.HasilPenilaianIssue{}).Where("id_penugasan_penilaian_issue IN ( ? )", sub_q_where).Updates(map[string]interface{}{"sts_penetapan_penilaian": "0", "tgl_updated": time.Now()})
		db.Table("disinsentif_penilaian_issue").Model(&entity.HasilPenilaianIssue{}).Where("id_penugasan_penilaian_issue IN ( ? )", sub_q_where).Updates(map[string]interface{}{"sts_penetapan_penilaian": "0", "tgl_updated": time.Now()})
		db.Table("penugasan_penilaian_issue").Model(&entity.PenugasanPenilaianIssue{}).Where("id_penugasan_penilaian_issue = ?", v.IDPenugasanPenilaian).Updates(map[string]interface{}{"sts_pelaksanaan_tugas": "0", "tgl_pelaksanaan_tugas": time.Now(), "tgl_updated": time.Now()})
	}

	return true, nil
}

//=======================================================================================================================

func ListsProgresPenilaian(search string, page string, row string, status string, proses string) (helper.ResponsePagination, error) {
	var res helper.ResponsePagination
	var err error
	var kode_proses string

	if proses == "Semua Proses" {
		kode_proses = "-1"
	} else if proses == "Belum Dinilai" {
		kode_proses = "0"
	} else if proses == "Perlu Penyesuaian" {
		kode_proses = "1"
	} else if proses == "Siap Permanen" {
		kode_proses = "2"
	}

	data, errs := getProgresPenilaianLebihDari70(search, page, row, kode_proses, status)
	res = data
	err = errs
	// if status == "1" {
	// 	data, errs := getProgresPenilaianLebihDari70(search, page, row, kode_proses)
	// 	res = data
	// 	err = errs
	// } else {
	// 	data, errs := getProgresPenilaianKurangDari70(search, page, row, kode_proses)
	// 	res = data
	// 	err = errs
	// }

	if err != nil {
		return helper.ResponsePagination{}, err
	}

	return res, nil
}

func getProgresPenilaianLebihDari70(search string, page string, row string, kode_proses string, status string) (helper.ResponsePagination, error) {
	var progres_penilaian []entity.ProgresPenilaian
	// var jml_record int64

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	db := config.Con()
	defer config.CloseCon(db)

	// penugasan_penilaian_mgmt := db.Table("penugasan_penilaian_mgmt")
	// penugasan_penilaian_mgmt.Select("id_usulan_akreditasi")
	// penugasan_penilaian_mgmt.Where("sts_penerimaan_penugasan = ?", "1")
	// penugasan_penilaian_mgmt.Where("sts_pelaksanaan_tugas = ?", "0")

	// penugasan_penilaian_issue := db.Table("penugasan_penilaian_issue")
	// penugasan_penilaian_issue.Select("id_usulan_akreditasi")
	// penugasan_penilaian_issue.Where("sts_penerimaan_penugasan = ?", "1")
	// penugasan_penilaian_issue.Where("sts_pelaksanaan_tugas = ?", "0")

	// sub_query_Not_IN := db.Table("hasil_akreditasi as t13")
	// sub_query_Not_IN.Select("t13.id_usulan_akreditasi")
	// sub_query_Not_IN.Joins("LEFT JOIN usulan_akreditasi tk ON t13.id_usulan_akreditasi = tk.id_usulan_akreditasi")
	// sub_query_Not_IN.Where("t13.tgl_created > tk.tgl_updated")

	// total_nilai_mgmt := db.Table("penugasan_penilaian_mgmt as t2")
	// total_nilai_mgmt.Select("SUM( t3.nilai ) AS total")
	// total_nilai_mgmt.Joins("INNER JOIN hasil_penilaian_mgmt t3 ON t2.id_penugasan_penilaian_manajemen = t3.id_penugasan_penilaian_manajemen")
	// total_nilai_mgmt.Joins("INNER JOIN sub_unsur_penilaian t5 ON t3.id_sub_unsur_penilaian = t5.id_sub_unsur_penilaian")
	// total_nilai_mgmt.Joins("INNER JOIN unsur_penilaian t6 ON t5.id_unsur_penilaian = t6.id_unsur_penilaian")
	// total_nilai_mgmt.Joins("LEFT JOIN public.usulan_akreditasi t7 ON t7.id_usulan_akreditasi = t2.id_usulan_akreditasi ")
	// total_nilai_mgmt.Where("t6.id_kelompok_unsur_perhitungan_nilai = 2")
	// total_nilai_mgmt.Where("t2.sts_penerimaan_penugasan != '0' ")
	// total_nilai_mgmt.Where("t2.tgl_created > t6.tgl_updated ")
	// total_nilai_mgmt.Where("t2.id_usulan_akreditasi = t0.id_usulan_akreditasi")

	// total_nilai_issue := db.Table("penugasan_penilaian_issue as t2")
	// total_nilai_issue.Select("SUM( t3.nilai ) AS total")
	// total_nilai_issue.Joins("INNER JOIN hasil_penilaian_issue t3 ON t2.id_penugasan_penilaian_issue = t3.id_penugasan_penilaian_issue")
	// total_nilai_issue.Joins("INNER JOIN sub_unsur_penilaian t5 ON t3.id_sub_unsur_penilaian = t5.id_sub_unsur_penilaian")
	// total_nilai_issue.Joins("INNER JOIN unsur_penilaian t6 ON t5.id_unsur_penilaian = t6.id_unsur_penilaian")
	// total_nilai_issue.Joins("LEFT JOIN public.usulan_akreditasi t7 ON t7.id_usulan_akreditasi = t2.id_usulan_akreditasi ")
	// total_nilai_issue.Where("t6.id_kelompok_unsur_perhitungan_nilai = 1")
	// total_nilai_issue.Where("t2.sts_penerimaan_penugasan != '0' ")
	// total_nilai_issue.Where("t2.tgl_created > t6.tgl_updated ")
	// total_nilai_issue.Where("t2.id_usulan_akreditasi = t0.id_usulan_akreditasi")

	// data_usulan := db.Raw("( ? ) UNION ( ? )", penugasan_penilaian_mgmt, penugasan_penilaian_issue)

	// query := db.Table("( ? ) as t0", data_usulan)
	// query.Select("t3.id_usulan_akreditasi, t4.nama_jurnal, ( ? ) as total_nilai_mgmt, ( ? ) as total_nilai_issue", total_nilai_mgmt, total_nilai_issue)
	// query.Joins("INNER JOIN hasil_sa as hs on hs.id_usulan_akreditasi = t0.id_usulan_akreditasi AND hs.nilai_total >= ?", 70)
	// query.Joins("INNER JOIN usulan_akreditasi t3 ON t3.id_usulan_akreditasi = t0.id_usulan_akreditasi ")
	// query.Joins("INNER JOIN identitas_jurnal t4 ON t3.id_identitas_jurnal = t4.id_identitas_jurnal")
	// query.Where("t0.id_usulan_akreditasi NOT IN ( ? )", sub_query_Not_IN)
	// query.Where("t3.sts_pengajuan_usulan = ?", "1")
	// query.Order("t3.tgl_updated desc")

	// data := db.Table("( ? ) as t1", query)
	// // data.Select("t1.*")
	// if proses == "Belum Dinilai" {
	// 	data.Where("t1.total_nilai_mgmt IS NULL").Or("t1.total_nilai_issue IS NULL")
	// }
	// data.Count(&progres_penilaian)
	// data.Find(&progres_penilaian)

	// if search != "" {
	// 	data.Where("lower(tb_temp.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	// }
	// data.Scan(&hasil_akhir_penilaian)

	// db.Raw("SELECT jml_record FROM arjuna_list_penilaian_blm_selesai_with_filter(0,  ( ? ), 1, '1', '')", kode_proses).Scan(&jml_record)

	// query := db.Raw("SELECT * from arjuna_list_penilaian_blm_selesai_with_filter(( ? ),  ( ? ), ( ? ), '1', ( ? ))", (pages - 1), kode_proses, rows, search)
	query := db.Raw("SELECT * from z_arjuna_list_penilaian_blm_selesai_with_filter(( ? ),  ( ? ), ( ? ), '1', ( ? ))", (pages - 1), kode_proses, rows, search)
	if status == "1" {
		query.Where("sts_akreditasi = ?", 0)
	} else {
		query.Where("sts_akreditasi = ?", 1)
	}
	data := db.Table("( ? ) as tb_temp", query)
	data.Scan(&progres_penilaian)

	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	var total int
	if len(progres_penilaian) > 0 {
		total = int(progres_penilaian[0].JmlRecord)
	} else {
		total = 0
	}
	// p := paginator.New(adapter.NewGORMAdapter(data), rows)
	// p.SetPage(pages)
	// view := view.New(p)
	// if err := p.Results(&progres_penilaian); err != nil {
	// 	panic(err)
	// }

	// res := helper.BuildPaginationResponse(progres_penilaian, view)
	res := helper.ResponsePagination{}
	res.Data = progres_penilaian
	res.CurrentPage = pages
	res.LastPage = ((total / rows) + 1)
	res.PerPage = rows
	res.Total = total

	return res, nil
}

func DetailListHasilPenilaianManajemen(id_usulan_akreditasi int, lang string) (interface{}, error) {
	var detail_penilaian_mgmt []entity.DetailtListHasilPenilaianManajemen
	db := config.Con()
	defer config.CloseCon(db)

	query := db.Raw("SELECT * from arjuna_list_hasil_penilaian_manajemen( ( ? ), ( ? ))", id_usulan_akreditasi, lang)
	if lang == "en" {
		lang = "en"
	} else {
		lang = "id"
	}
	data := db.Table("( ? ) as tb_temp", query)
	data.Scan(&detail_penilaian_mgmt)

	if data.Error != nil {
		return nil, data.Error
	}
	return detail_penilaian_mgmt, nil
}

func DetailListHasilPenilaianSubstansi(id_usulan_akreditasi int, lang string) (interface{}, error) {
	var detail_penilaian_substansi []entity.DetailtListHasilPenilaianSubstansi
	db := config.Con()
	defer config.CloseCon(db)

	query := db.Raw("SELECT * from arjuna_list_hasil_penilaian_substansi( ( ? ), ( ? ))", id_usulan_akreditasi, lang)
	if lang == "en" {
		lang = "en"
	} else {
		lang = "id"
	}
	data := db.Table("( ? ) as tb_temp", query)
	data.Scan(&detail_penilaian_substansi)

	if data.Error != nil {
		return nil, data.Error
	}
	return detail_penilaian_substansi, nil
}

func getProgresPenilaianKurangDari70(search string, page string, row string, kode_proses string) (helper.ResponsePagination, error) {
	var progres_penilaian []entity.ProgresPenilaian
	// var jml_record int64

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	db := config.Con()
	defer config.CloseCon(db)

	query := db.Raw("SELECT * from arjuna_list_penilaian_blm_selesai70_with_filter(( ? ),  ( ? ), ( ? ), '0', '')", (pages - 1), kode_proses, rows)

	data := db.Table("( ? ) as tb_temp", query)
	if search != "" {
		data.Where("lower(tb_temp.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Scan(&progres_penilaian)

	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	var total int
	if len(progres_penilaian) > 0 {
		total = int(progres_penilaian[0].JmlRecord)
	} else {
		total = 0
	}
	// p := paginator.New(adapter.NewGORMAdapter(data), rows)
	// p.SetPage(pages)
	// view := view.New(p)
	// if err := p.Results(&progres_penilaian); err != nil {
	// 	panic(err)
	// }

	// res := helper.BuildPaginationResponse(progres_penilaian, view)
	res := helper.ResponsePagination{}
	res.Data = progres_penilaian
	res.CurrentPage = pages
	res.LastPage = ((total / rows) + 1)
	res.PerPage = rows
	res.Total = total

	return res, nil
}

func GetDetailProgresPenilaianManagement(id_usulan_akreditasi int, row string, page string, id_personal string, status string) (helper.ResponsePagination, error) {
	var detail_proses_penilaian []entity.DetailProgresPenilaianManagement

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	db := config.Con()
	defer config.CloseCon(db)

	sub_q := db.Table("hasil_penilaian_mgmt as ta")
	sub_q.Select("ta.id_penugasan_penilaian_manajemen")
	sub_q.Where("ta.sts_penetapan_penilaian = ?", "1")

	nilai_disinsentif := db.Table("disinsentif_penilaian_mgmt as nilai_disinsentif")
	nilai_disinsentif.Select("nilai_disinsentif.nilai")
	nilai_disinsentif.Where("nilai_disinsentif.id_penugasan_penilaian_manajemen = t1.id_penugasan_penilaian_manajemen")

	nilai_mgmt := db.Table("hasil_penilaian_mgmt as nilai_mgmt")
	nilai_mgmt.Select("SUM(nilai_mgmt.nilai) as nilai")
	nilai_mgmt.Where("nilai_mgmt.id_penugasan_penilaian_manajemen = t1.id_penugasan_penilaian_manajemen")

	penyesuaian_nilai := db.Table("penyesuaian_nilai_asessor_mgmt as temp")
	penyesuaian_nilai.Select("temp.id_penyesuaian_nilai_mgmt")
	penyesuaian_nilai.Where("temp.id_penugasan_penilaian_manajemen = t1.id_penugasan_penilaian_manajemen")
	penyesuaian_nilai.Where("status_penyesuaian = ?", true)

	selisih_nilai := db.Raw("select array_to_json(array_agg(ttemp.*)) from public.arjuna_check_status_selisih_nilai_mgmt_by_asessor( ? ) as ttemp", id_usulan_akreditasi)

	data := db.Table("penugasan_penilaian_mgmt as t1")
	data.Select("t1.id_penugasan_penilaian_manajemen as id_penugasan_penilaian, t1.id_usulan_akreditasi, t1.tgl_created::date as tgl_penugasan, t1.tgl_updated::date as tgl_penerimaan, t5.url_statistik_pengunjung, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society, t5.nama_awal_jurnal, t5.tgl_pembuatan, t5.url_jurnal, t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat, t5.no_telepon, t5.alamat_surel, t5.tgl_akhir_terakreditasi, t7.nama as nama_pic, t7.nama_institusi, t8.nama_eic, t8.alamat_surel as alamat_surel_eic, t4.alamat_oai, t4.doi_jurnal, t3.user_penilai, t3.passwd_penilai, t9.nama as nama_penilai, ( ? ) as nilai, ( ? ) as nilai_disinsentif, ( ? ) as id_penyesuaian_nilai, ( ? ) as selisih_penilaian", nilai_mgmt, nilai_disinsentif, penyesuaian_nilai, selisih_nilai)
	data.Joins("INNER JOIN usulan_akreditasi as t3 ON t1.id_usulan_akreditasi = t3.id_usulan_akreditasi")
	data.Joins("INNER JOIN identitas_jurnal as t4 ON t3.id_identitas_jurnal = t4.id_identitas_jurnal")
	data.Joins("INNER JOIN jurnal as t5 ON t4.id_jurnal = t5.id_jurnal")
	data.Joins("INNER JOIN pic as t6 ON t3.id_pic = t6.id_pic")
	data.Joins("INNER JOIN personal as t7 ON t6.id_personal = t7.id_personal")
	data.Joins("INNER JOIN eic as t8 ON t3.id_eic = t8.id_eic")
	data.Joins("INNER JOIN personal as t9 ON t9.id_personal = t1.id_personal")
	data.Where("t1.id_personal = ?", id_personal)
	data.Where("t1.sts_penerimaan_penugasan = ?", "1")
	if status == "Progres" {
		data.Where("t1.id_penugasan_penilaian_manajemen NOT IN ( ? )", sub_q)
	} else {
		data.Where("t1.id_penugasan_penilaian_manajemen IN ( ? )", sub_q)
	}
	data.Where("t1.id_usulan_akreditasi = ?", id_usulan_akreditasi)
	data.Order("t1.tgl_updated DESC")
	data.Find(&detail_proses_penilaian)

	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(detail_proses_penilaian)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&detail_proses_penilaian); err != nil {
		panic(err)
	}

	for i, v := range detail_proses_penilaian {
		detail_proses_penilaian[i].NilaiTotal = v.NilaiDisinsentif + v.Nilai
	}

	res := helper.BuildPaginationResponse(detail_proses_penilaian, view)
	res.PerPage = rows
	res.Total = total

	return res, nil
}

func GetDetailProgresPenilaianIssue(id_usulan_akreditasi int, row string, page string, id_personal string, status string) (helper.ResponsePagination, error) {
	var res helper.ResponsePagination
	var err error

	if status == "Progres" {
		data, errs := getDetailProgresPenilaianIssueProses(id_usulan_akreditasi, row, page, id_personal)
		res = data
		err = errs
	} else {
		data, errs := getDetailProgresPenilaianIssueRevoke(id_usulan_akreditasi, row, page, id_personal)
		res = data
		err = errs
	}

	if err != nil {
		return helper.ResponsePagination{}, err
	}

	return res, nil
}

func getDetailProgresPenilaianIssueProses(id_usulan_akreditasi int, row string, page string, id_personal string) (helper.ResponsePagination, error) {
	var detail_proses_penilaian []entity.DetailProgresPenilaianIssue

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	db := config.Con()
	defer config.CloseCon(db)

	sub_q := db.Table("hasil_penilaian_issue as ta")
	sub_q.Select("ta.id_penugasan_penilaian_issue")
	sub_q.Where("ta.sts_penetapan_penilaian = ?", "1")

	nilai_disinsentif := db.Table("disinsentif_penilaian_issue as nilai_disinsentif")
	nilai_disinsentif.Select("nilai_disinsentif.nilai")
	nilai_disinsentif.Where("nilai_disinsentif.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue")

	nilai_mgmt := db.Table("hasil_penilaian_issue as nilai_issue")
	nilai_mgmt.Select("SUM(nilai_issue.nilai) as nilai")
	nilai_mgmt.Where("nilai_issue.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue")

	penyesuaian_nilai := db.Table("penyesuaian_nilai_asessor_issue as temp")
	penyesuaian_nilai.Select("temp.id_penyesuaian_nilai_issue")
	penyesuaian_nilai.Where("temp.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue")
	penyesuaian_nilai.Order("tgl_created desc")
	penyesuaian_nilai.Limit(1)

	selisih_nilai := db.Raw("select array_to_json(array_agg(ttemp.*)) from public.arjuna_check_status_selisih_nilai_by_id_issue( t1.id_issue::integer ) as ttemp")

	data := db.Table("penugasan_penilaian_issue as t1")
	data.Select("t1.id_penugasan_penilaian_issue, t1.id_usulan_akreditasi, t1.tgl_created as tgl_penugasan, t1.tgl_penerimaan_penugasan::date as tgl_penerimaan, t5.url_statistik_pengunjung, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society, t5.nama_awal_jurnal, t5.tgl_pembuatan, t5.url_jurnal, t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat, t5.no_telepon, t5.alamat_surel, t5.tgl_akhir_terakreditasi, t7.nama as nama_pic, t7.nama_institusi, t8.nama_eic, t8.alamat_surel as alamat_surel_eic, t2.url_issue,t2.id_issue, t2.judul_issue, t2.volume_issue, t2.nomor_issue, t2.tahun_issue, t9a.id_bidang_ilmu as id_bidang_ilmu_issue, t10.bidang_ilmu as bidang_ilmu_issue, t10.level_taksonomi as level_taksonomi_issue, t4.alamat_oai, t4.doi_jurnal, t9.user_penilai, t9.passwd_penilai, ( ? ) as nilai, ( ? ) as nilai_disinsentif, ( ? ) as id_penyesuaian_nilai, ( ? ) as selisih_penilaian", nilai_mgmt, nilai_disinsentif, penyesuaian_nilai, selisih_nilai)
	data.Joins("INNER JOIN issue t2 ON t1.id_issue = t2.id_issue")
	data.Joins("LEFT JOIN public.identitas_jurnal t4 ON t2.id_identitas_jurnal = t4.id_identitas_jurnal")
	data.Joins("INNER JOIN public.jurnal t5 ON t4.id_jurnal = t5.id_jurnal")
	data.Joins("LEFT JOIN public.pic t6 ON t4.id_identitas_jurnal = t6.id_identitas_jurnal AND t6.sts_aktif_pic = ?", "1")
	data.Joins("INNER JOIN public.personal t7 ON t6.id_personal = t7.id_personal")
	data.Joins("INNER JOIN public.eic t8 ON t4.id_identitas_jurnal = t8.id_identitas_jurnal AND t8.sts_aktif_eic = ?", "1")
	data.Joins("LEFT JOIN public.usulan_akreditasi t9 ON t1.id_usulan_akreditasi = t9.id_usulan_akreditasi")
	data.Joins("INNER JOIN public.bidang_ilmu_jurnal t9a ON t5.id_jurnal = t9a.id_jurnal AND t9a.sts_aktif_bidang_ilmu_jurnal = ?", "1")
	data.Joins("INNER JOIN public.bidang_ilmu t10 ON t9a.id_bidang_ilmu = t10.id_bidang_ilmu")
	// data.Joins("LEFT JOIN public.bidang_ilmu_jurnal t9a ON t5.id_jurnal = t9a.id_jurnal AND t9a.sts_aktif_bidang_ilmu_jurnal = ?", "1")
	// data.Joins("LEFT JOIN public.bidang_ilmu t10 ON t9a.id_bidang_ilmu = t10.id_bidang_ilmu")
	data.Joins("LEFT JOIN public.hasil_penilaian_issue ta ON ta.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue AND ta.sts_penetapan_penilaian = ?", "1")

	data.Where("t1.id_personal = ?", id_personal)
	data.Where("t1.sts_penerimaan_penugasan = ?", "1")
	data.Where("t1.sts_pelaksanaan_tugas = ?", "0")
	data.Where("t1.id_usulan_akreditasi = ?", id_usulan_akreditasi)
	data.Order("t1.tgl_created DESC")
	data.Find(&detail_proses_penilaian)

	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(detail_proses_penilaian)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&detail_proses_penilaian); err != nil {
		panic(err)
	}

	for i, v := range detail_proses_penilaian {
		detail_proses_penilaian[i].NilaiTotal = v.NilaiDisinsentif + v.Nilai
	}

	res := helper.BuildPaginationResponse(detail_proses_penilaian, view)
	res.PerPage = rows
	res.Total = total

	return res, nil
}

func getDetailProgresPenilaianIssueRevoke(id_usulan_akreditasi int, row string, page string, id_personal string) (helper.ResponsePagination, error) {
	var detail_proses_penilaian []entity.DetailProgresPenilaianManagement

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	db := config.Con()
	defer config.CloseCon(db)

	sub_q := db.Table("hasil_penilaian_issue as ta")
	sub_q.Select("ta.id_penugasan_penilaian_issue")
	sub_q.Where("ta.sts_penetapan_penilaian = ?", "1")

	nilai_disinsentif := db.Table("disinsentif_penilaian_issue as nilai_disinsentif")
	nilai_disinsentif.Select("nilai_disinsentif.nilai")
	nilai_disinsentif.Where("nilai_disinsentif.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue")

	nilai_issue := db.Table("hasil_penilaian_issue as nilai_issue")
	nilai_issue.Select("SUM(nilai_issue.nilai) as nilai")
	nilai_issue.Where("nilai_issue.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue")

	data := db.Table("penugasan_penilaian_issue as t1")
	data.Select("t1.id_penugasan_penilaian_issue as id_penugasan_penilaian, t1.id_usulan_akreditasi, t1.tgl_created::date as tgl_penugasan, t1.tgl_updated::date as tgl_penerimaan, t5.url_statistik_pengunjung, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society, t5.nama_awal_jurnal, t5.tgl_pembuatan, t5.url_jurnal, t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat, t5.no_telepon, t5.alamat_surel, t5.tgl_akhir_terakreditasi, t7.nama as nama_pic, t7.nama_institusi, t8.nama_eic, t8.alamat_surel as alamat_surel_eic, t4.alamat_oai, t4.doi_jurnal, t3.user_penilai, t3.passwd_penilai, t9.nama as nama_penilai, ( ? ) as nilai, ( ? ) as nilai_disinsentif", nilai_issue, nilai_disinsentif)
	data.Joins("INNER JOIN usulan_akreditasi as t3 ON t1.id_usulan_akreditasi = t3.id_usulan_akreditasi")
	data.Joins("INNER JOIN identitas_jurnal as t4 ON t3.id_identitas_jurnal = t4.id_identitas_jurnal")
	data.Joins("INNER JOIN jurnal as t5 ON t4.id_jurnal = t5.id_jurnal")
	data.Joins("INNER JOIN pic as t6 ON t3.id_pic = t6.id_pic")
	data.Joins("INNER JOIN personal as t7 ON t6.id_personal = t7.id_personal")
	data.Joins("INNER JOIN eic as t8 ON t3.id_eic = t8.id_eic")
	data.Joins("INNER JOIN personal as t9 ON t9.id_personal = t1.id_personal")
	data.Where("t1.id_personal = ?", id_personal)
	data.Where("t1.sts_penerimaan_penugasan = ?", "1")
	data.Where("t1.id_penugasan_penilaian_issue IN ( ? )", sub_q)
	data.Where("t1.id_usulan_akreditasi = ?", id_usulan_akreditasi)
	data.Order("t1.tgl_updated DESC")
	data.Find(&detail_proses_penilaian)

	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(detail_proses_penilaian)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&detail_proses_penilaian); err != nil {
		panic(err)
	}

	for i, v := range detail_proses_penilaian {
		detail_proses_penilaian[i].NilaiTotal = v.NilaiDisinsentif + v.Nilai
	}

	res := helper.BuildPaginationResponse(detail_proses_penilaian, view)
	res.PerPage = rows
	res.Total = total

	return res, nil
}

func ProgresPenilaianSetPermanent(req entity.ProgresPenilaianSetPermanentDTO) (interface{}, error) {
	db := config.Con()
	defer config.CloseCon(db)

	if req.Penilai == "Management" {
		sub_q := db.Table("penugasan_penilaian_mgmt")
		sub_q.Select("id_penugasan_penilaian_manajemen")
		sub_q.Where("id_penugasan_penilaian_manajemen = ?", req.IDPenugasan)
		sub_q.Where("id_personal = ?", req.IDPersonal)
		db.Table("hasil_penilaian_mgmt").Model(&entity.HasilPenilaianMgmt{}).Where("id_penugasan_penilaian_manajemen IN ( ? )", sub_q).Updates(map[string]interface{}{"sts_penetapan_penilaian": "1", "tgl_updated": time.Now()})
		db.Table("disinsentif_penilaian_mgmt").Model(&entity.HasilPenilaianMgmt{}).Where("id_penugasan_penilaian_manajemen IN ( ? )", sub_q).Updates(map[string]interface{}{"sts_penetapan_penilaian": "1", "tgl_updated": time.Now()})
		db.Table("penugasan_penilaian_mgmt").Model(&entity.PenugasanPenilaianManajemen{}).Where("id_penugasan_penilaian_manajemen = ?", req.IDPenugasan).Updates(map[string]interface{}{"sts_pelaksanaan_tugas": "1", "tgl_pelaksanaan_tugas": time.Now(), "tgl_updated": time.Now()})
	} else if req.Penilai == "Issue" {
		sub_q := db.Table("penugasan_penilaian_issue")
		sub_q.Select("id_penugasan_penilaian_issue")
		sub_q.Where("id_penugasan_penilaian_issue = ?", req.IDPenugasan)
		sub_q.Where("id_personal = ?", req.IDPersonal)
		db.Table("hasil_penilaian_issue").Model(&entity.HasilPenilaianIssue{}).Where("id_penugasan_penilaian_issue IN ( ? )", sub_q).Updates(map[string]interface{}{"sts_penetapan_penilaian": "1", "tgl_updated": time.Now()})
		db.Table("disinsentif_penilaian_issue").Model(&entity.HasilPenilaianIssue{}).Where("id_penugasan_penilaian_issue IN ( ? )", sub_q).Updates(map[string]interface{}{"sts_penetapan_penilaian": "1", "tgl_updated": time.Now()})
		db.Table("penugasan_penilaian_issue").Model(&entity.PenugasanPenilaianIssue{}).Where("id_penugasan_penilaian_issue = ?", req.IDPenugasan).Updates(map[string]interface{}{"sts_pelaksanaan_tugas": "1", "tgl_pelaksanaan_tugas": time.Now(), "tgl_updated": time.Now()})
	}

	return true, nil
}

func CountUnsurPenilaian(req entity.ProgresPenilaianSetPermanentDTO) (int64, error) {
	var res int64

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("hasil_penilaian_issue as tb")
	data.Distinct("tb.id_sub_unsur_penilaian")
	data.Joins("INNER JOIN sub_unsur_penilaian as tb2 on tb2.id_sub_unsur_penilaian = tb.id_sub_unsur_penilaian")
	data.Joins("INNER JOIN penugasan_penilaian_issue as tb3 on tb3.id_penugasan_penilaian_issue = tb.id_penugasan_penilaian_issue")
	data.Where("tb.id_penugasan_penilaian_issue = ?", req.IDPenugasan)
	data.Where("tb3.id_personal = ?", req.IDPersonal)
	data.Count(&res)

	if data.Error != nil {
		return 0, data.Error
	}

	return res, nil
}

func ProgresPenilaianCabutPermanent(req entity.ProgresPenilaianSetPermanentDTO) (interface{}, error) {
	db := config.Con()
	defer config.CloseCon(db)

	if req.Penilai == "Management" {
		sub_q := db.Table("penugasan_penilaian_mgmt")
		sub_q.Select("id_penugasan_penilaian_manajemen")
		sub_q.Where("id_penugasan_penilaian_manajemen = ?", req.IDPenugasan)
		sub_q.Where("id_personal = ?", req.IDPersonal)
		db.Table("hasil_penilaian_mgmt").Model(&entity.HasilPenilaianMgmt{}).Where("id_penugasan_penilaian_manajemen IN ( ? )", sub_q).Updates(map[string]interface{}{"sts_penetapan_penilaian": "0", "tgl_updated": time.Now()})
		db.Table("disinsentif_penilaian_mgmt").Model(&entity.HasilPenilaianMgmt{}).Where("id_penugasan_penilaian_manajemen IN ( ? )", sub_q).Updates(map[string]interface{}{"sts_penetapan_penilaian": "0", "tgl_updated": time.Now()})
		db.Table("penugasan_penilaian_mgmt").Model(&entity.PenugasanPenilaianManajemen{}).Where("id_penugasan_penilaian_manajemen = ?", req.IDPenugasan).Updates(map[string]interface{}{"sts_pelaksanaan_tugas": "0", "tgl_pelaksanaan_tugas": time.Now(), "tgl_updated": time.Now()})
	} else if req.Penilai == "Issue" {
		sub_q := db.Table("penugasan_penilaian_issue")
		sub_q.Select("id_penugasan_penilaian_issue")
		sub_q.Where("id_penugasan_penilaian_issue = ?", req.IDPenugasan)
		sub_q.Where("id_personal = ?", req.IDPersonal)
		db.Table("hasil_penilaian_issue").Model(&entity.HasilPenilaianIssue{}).Where("id_penugasan_penilaian_issue IN ( ? )", sub_q).Updates(map[string]interface{}{"sts_penetapan_penilaian": "0", "tgl_updated": time.Now()})
		db.Table("disinsentif_penilaian_issue").Model(&entity.HasilPenilaianIssue{}).Where("id_penugasan_penilaian_issue IN ( ? )", sub_q).Updates(map[string]interface{}{"sts_penetapan_penilaian": "0", "tgl_updated": time.Now()})
		db.Table("penugasan_penilaian_issue").Model(&entity.PenugasanPenilaianIssue{}).Where("id_penugasan_penilaian_issue = ?", req.IDPenugasan).Updates(map[string]interface{}{"sts_pelaksanaan_tugas": "0", "tgl_pelaksanaan_tugas": time.Now(), "tgl_updated": time.Now()})
	}

	return true, nil
}

func ProgresPenilaianPenyesuaianNilai(req entity.ProgresPenilaianPenyesuaianNilaiDTO) (interface{}, error) {
	var check_penugasan_penilaian int64
	var check_penyesuaian_nilai int64
	var check_penyesuaian_nilai_status int64
	db := config.Con()
	defer config.CloseCon(db)

	if req.Penilai == "Management" {
		db.Table("penugasan_penilaian_mgmt").Where("id_penugasan_penilaian_manajemen = ?", req.IDPenugasan).Count(&check_penugasan_penilaian)
		if check_penugasan_penilaian > 0 {
			db.Table("penyesuaian_nilai_asessor_mgmt").Where("id_penyesuaian_nilai_mgmt = ?", req.IDPenyesuaianNilai).Count(&check_penyesuaian_nilai)
			if check_penyesuaian_nilai > 0 {
				db.Table("penyesuaian_nilai_asessor_mgmt").Model(&entity.PenyesuaianNilaiManagement{}).Where("id_penyesuaian_nilai_mgmt = ?", req.IDPenyesuaianNilai).Updates(map[string]interface{}{"status_penyesuaian": false, "komentar_penyesuaian": req.Komentar, "tgl_updated": time.Now()})
			} else {
				db.Table("penyesuaian_nilai_asessor_mgmt").Where("id_penyesuaian_nilai_mgmt = ?", req.IDPenyesuaianNilai).Where("status_penyesuaian = ?", true).Count(&check_penyesuaian_nilai_status)
				if check_penyesuaian_nilai_status < 1 {
					var save entity.PenyesuaianNilaiManagement
					var last_id int64
					db.Table("penyesuaian_nilai_asessor_mgmt").Select("id_penyesuaian_nilai_mgmt").Order("id_penyesuaian_nilai_mgmt desc").Limit(1).Scan(&last_id)
					save.IDPenugasanPenilaianManajemen = req.IDPenugasan
					save.StatusPenyesuaian = true
					save.KomentarPenyesuaian = req.Komentar
					save.TglCreated = time.Now()
					db.Table("penyesuaian_nilai_asessor_mgmt").Create(&save)
				}
			}
		}
	} else if req.Penilai == "Issue" {
		db.Table("penugasan_penilaian_issue").Where("id_penugasan_penilaian_issue = ?", req.IDPenugasan).Count(&check_penugasan_penilaian)
		if check_penugasan_penilaian > 0 {
			db.Table("penyesuaian_nilai_asessor_issue").Where("id_penyesuaian_nilai_issue = ?", req.IDPenyesuaianNilai).Count(&check_penyesuaian_nilai)
			if check_penyesuaian_nilai > 0 {
				db.Table("penyesuaian_nilai_asessor_issue").Model(&entity.PenyesuaianNilaiIssue{}).Where("id_penugasan_penilaian_issue = ?", req.IDPenyesuaianNilai).Updates(map[string]interface{}{"status_penyesuaian": false, "komentar_penyesuaian": req.Komentar, "tgl_updated": time.Now()})
			} else {
				db.Table("penyesuaian_nilai_asessor_issue").Where("id_penugasan_penilaian_issue = ?", req.IDPenyesuaianNilai).Where("status_penyesuaian = ?", true).Count(&check_penyesuaian_nilai_status)
				if check_penyesuaian_nilai_status < 1 {
					var save entity.PenyesuaianNilaiIssue
					var last_id int64
					db.Table("penyesuaian_nilai_asessor_issue").Select("id_penugasan_penilaian_issue").Order("id_penugasan_penilaian_issue desc").Limit(1).Scan(&last_id)
					save.IDPenugasanPenilaianIssue = req.IDPenugasan
					save.StatusPenyesuaian = true
					save.KomentarPenyesuaian = req.Komentar
					save.TglCreated = time.Now()
					db.Table("penyesuaian_nilai_asessor_issue").Create(&save)
				}
			}
		}
	}

	return true, nil
}

func ProgresPenilaianPenyesuaianNilaiAsessor(req entity.ProgresPenilaianPenyesuaianNilaiAsessorDTO) (entity.ProgresPenilaianPenyesuaianNilaiAsessor, error) {
	db := config.Con()
	defer config.CloseCon(db)
	var progres entity.ProgresPenilaianPenyesuaianNilaiAsessor

	if req.Penilai == "Issue" {
		query := db.Raw("SELECT * FROM arjuna_set_penyesuaian_penilaian_asessor_with_komentar( ( ? ), ( ? ), ( ? ), ( ? ) )", req.IDUsulanAkreditasi, 1, req.Komentar, "id")

		data := db.Table("( ? ) as temp", query).Scan(&progres)

		if data.Error != nil {
			return progres, data.Error
		}
	} else if req.Penilai == "Management" {
		query := db.Raw("SELECT * FROM arjuna_set_penyesuaian_penilaian_asessor_with_komentar( ( ? ), ( ? ), ( ? ), ( ? ) )", req.IDUsulanAkreditasi, 2, req.Komentar, "id")

		data := db.Table("( ? ) as temp", query).Scan(&progres)

		if data.Error != nil {
			return progres, data.Error
		}
	} else {
		return progres, errors.New("Penilai tidak ditemukan")
	}
	return progres, nil
}

func UsulanItemPenolakan() ([]entity.UsulanItemPenolakan, error) {
	db := config.Con()
	defer config.CloseCon(db)
	var item_penolakan []entity.UsulanItemPenolakan

	db.Table("usulan_item_penolakan").Where("status", 1).Order("no_urut asc").Find(&item_penolakan)
	return item_penolakan, nil
}

func KomentarHasilPenilaianAkreditasi(idPersonal, idUsulanAkreditasi string) (entity.KomentarHasilPenilaianAkreditasi, error) {
	db := config.Con()
	defer config.CloseCon(db)

	id_personal, _ := strconv.Atoi(idPersonal)
	id_usulan_akreditasi, _ := strconv.Atoi(idUsulanAkreditasi)

	qParent := db.Raw("SELECT id_usulan_akreditasi, id_identitas_jurnal, id_pic, id_eic, nama_jurnal, eissn, pissn, url_jurnal, url_contact, url_editor, url_statistik_pengunjung, alamat_oai, doi_jurnal, user_penilai, passwd_penilai as password_penilai, tgl_created, tgl_updated FROM arjuna_get_usulan_akreditasi( (?), (?) ) ", id_personal, id_usulan_akreditasi)

	qSub := db.Raw("SELECT id_unsur_penilaian as no_urut_unsur , unsur_penilaian, nilai, komentar FROM arjuna_list_komentar_asessor_all( (?), (?), (?) )", idPersonal, idUsulanAkreditasi, "id")

	var komentar entity.KomentarHasilPenilaianAkreditasi
	dataParent := db.Table("( ? ) as temp", qParent)
	dataParent.Scan(&komentar)
	if dataParent.Error != nil {
		return entity.KomentarHasilPenilaianAkreditasi{}, dataParent.Error
	}

	var komentar_asessor []entity.KomentarAsessor
	dataSub := db.Table("( ? ) as temp", qSub)
	dataSub.Scan(&komentar_asessor)
	if dataSub.Error != nil {
		return entity.KomentarHasilPenilaianAkreditasi{}, dataSub.Error
	}
	komentarHasil := entity.KomentarHasilPenilaianAkreditasi{
		IdUsulanAkreditasi:     int64(id_usulan_akreditasi),
		IdIdentitasJurnal:      komentar.IdIdentitasJurnal,
		IdPic:                  komentar.IdPic,
		NamaJurnal:             komentar.NamaJurnal,
		Eissn:                  komentar.Eissn,
		Pissn:                  komentar.Pissn,
		UrlJurnal:              komentar.UrlJurnal,
		UrlContact:             komentar.UrlContact,
		UrlEditor:              komentar.UrlEditor,
		UrlStatistikPengunjung: komentar.UrlStatistikPengunjung,
		AlamatOai:              komentar.AlamatOai,
		DoiJurnal:              komentar.DoiJurnal,
		UserPenilai:            komentar.UserPenilai,
		PasswordPenilai:        komentar.PasswordPenilai,
		TglCreated:             komentar.TglCreated,
		TglUpdated:             komentar.TglUpdated,
		KomentarAsessor:        komentar_asessor,
	}
	return komentarHasil, nil
}
