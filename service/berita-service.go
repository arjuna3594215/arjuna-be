package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/vcraescu/go-paginator/v2"
	"github.com/vcraescu/go-paginator/v2/adapter"
	"github.com/vcraescu/go-paginator/v2/view"
)

func ListBerita(search string, page string, row string, baseUrl string, status string) (helper.ResponsePagination, error) {
	var berita []entity.Berita

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("berita")
	data.Select("berita.*, berita_lang.title, berita_lang.isi, berita_lang.lang")
	data.Joins("join berita_lang on berita_lang.id = berita.id")
	data.Where("berita.kategori = ?", "berita")
	if status != "" {
		data.Where("berita.status = ?", status)
	}
	if search != "" {
		data.Where("lower(berita_lang) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Order("berita.date_created DESC")
	data.Find(&berita)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(berita)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&berita); err != nil {
		panic(err)
	}

	for i, v := range berita {
		path := baseUrl + "/file/berita/"
		if v.Imagename != "" {
			berita[i].PathImagename = path + v.Imagename
		}
		if v.Lampiran1 != "" {
			berita[i].PathLampiran1 = path + v.Lampiran1
		}
		if v.Lampiran2 != "" {
			berita[i].PathLampiran2 = path + v.Lampiran2
		}
		if v.Lampiran3 != "" {
			berita[i].PathLampiran3 = path + v.Lampiran3
		}
	}

	res := helper.BuildPaginationResponse(berita, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func InsertBerita(req entity.BeritaDTO, c *fiber.Ctx) (entity.Berita, error) {
	var berita entity.Berita
	var berita_lang entity.BeritaLang
	db := config.Con()
	defer config.CloseCon(db)

	berita.DateCreated = time.Now()
	berita.Kategori = "berita"
	if req.Image != nil {
		filename, _ := helper.UploadFile("./files/berita", req.Image, c)
		berita.Imagename = filename
	}

	if req.Lampiran1 != nil {
		filename, _ := helper.UploadFile("./files/berita", req.Lampiran1, c)
		berita.Lampiran1 = filename
	}

	if req.Lampiran2 != nil {
		filename, _ := helper.UploadFile("./files/berita", req.Lampiran2, c)
		berita.Lampiran2 = filename
	}

	if req.Lampiran3 != nil {
		filename, _ := helper.UploadFile("./files/berita", req.Lampiran3, c)
		berita.Lampiran3 = filename
	}

	saveBerita := db.Table("berita").Save(&berita)

	if saveBerita.Error != nil {
		return entity.Berita{}, saveBerita.Error
	}

	berita_lang.ID = berita.ID
	berita_lang.Isi = req.Isi
	berita_lang.Lang = req.Lang
	berita_lang.Snapshot = req.Title
	berita_lang.Title = req.Title

	saveBeritaLang := db.Table("berita_lang").Save(&berita_lang)

	if saveBeritaLang.Error != nil {
		return entity.Berita{}, saveBeritaLang.Error
	}

	berita.Title = berita_lang.Title
	berita.Isi = berita_lang.Isi
	berita.Lang = berita_lang.Lang

	return berita, nil
}

func ShowBerita(id int, baseUrl string) (interface{}, error) {
	var berita entity.Berita
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("berita")
	data.Select("berita.*, berita_lang.title, berita_lang.isi, berita_lang.lang, berita_lang.id_lang")
	data.Joins("join berita_lang on berita_lang.id = berita.id")
	data.Where("berita.id = ?", id)
	data.Take(&berita)
	if data.Error != nil {
		return nil, data.Error
	}

	path := baseUrl + "/file/berita/"
	if berita.Imagename != "" {
		berita.PathImagename = path + berita.Imagename
	}
	if berita.Lampiran1 != "" {
		berita.PathLampiran1 = path + berita.Lampiran1
	}
	if berita.Lampiran2 != "" {
		berita.PathLampiran2 = path + berita.Lampiran2
	}
	if berita.Lampiran3 != "" {
		berita.PathLampiran3 = path + berita.Lampiran3
	}
	return berita, nil
}

func UpdateBerita(req entity.BeritaDTO, old entity.Berita, id int, c *fiber.Ctx) (entity.Berita, error) {
	var berita entity.Berita
	var berita_lang entity.BeritaLang
	db := config.Con()
	defer config.CloseCon(db)

	berita.ID = int64(id)
	berita.DateCreated = time.Now()
	berita.Kategori = "berita"
	if req.Image != nil {
		os.Remove("./files/berita/" + old.Imagename)
		filename, _ := helper.UploadFile("./files/berita", req.Image, c)
		berita.Imagename = filename
	} else {
		berita.Imagename = old.Imagename
	}

	if req.Lampiran1 != nil {
		os.Remove("./files/berita/" + old.Lampiran1)
		filename, _ := helper.UploadFile("./files/berita", req.Lampiran1, c)
		berita.Lampiran1 = filename
	} else {
		berita.Lampiran1 = old.Lampiran1
	}

	if req.Lampiran2 != nil {
		os.Remove("./files/berita/" + old.Lampiran2)
		filename, _ := helper.UploadFile("./files/berita", req.Lampiran2, c)
		berita.Lampiran2 = filename
	} else {
		berita.Lampiran2 = old.Lampiran2
	}

	if req.Lampiran3 != nil {
		os.Remove("./files/berita/" + old.Lampiran3)
		filename, _ := helper.UploadFile("./files/berita", req.Lampiran3, c)
		berita.Lampiran3 = filename
	} else {
		berita.Lampiran3 = old.Lampiran3
	}

	saveBerita := db.Table("berita").Save(&berita)

	if saveBerita.Error != nil {
		return entity.Berita{}, saveBerita.Error
	}

	berita_lang.IDLang = old.IDLang
	berita_lang.ID = berita.ID
	berita_lang.Isi = req.Isi
	berita_lang.Lang = req.Lang
	berita_lang.Snapshot = req.Title
	berita_lang.Title = req.Title

	saveBeritaLang := db.Table("berita_lang").Save(&berita_lang)

	if saveBeritaLang.Error != nil {
		return entity.Berita{}, saveBeritaLang.Error
	}

	berita.Title = berita_lang.Title
	berita.Isi = berita_lang.Isi
	berita.Lang = berita_lang.Lang

	return berita, nil
}

func UpdateStatusBerita(req entity.BeritaStatusDTO, old entity.Berita, id int) (entity.Berita, error) {
	db := config.Con()
	defer config.CloseCon(db)

	updateStatus := db.Table("berita").Where("id = ?", id).Update("status", req.Status)
	if updateStatus.Error != nil {
		return entity.Berita{}, updateStatus.Error
	}
	old.Status = req.Status
	return old, nil
}
