package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"strconv"
	"strings"
	"time"

	"github.com/vcraescu/go-paginator/v2"
	"github.com/vcraescu/go-paginator/v2/adapter"
	"github.com/vcraescu/go-paginator/v2/view"
)

func ListIssueUsulanAkreditasi(search string, page string, row string, id_usulan_akreditasi string) (helper.ResponsePagination, error) {
	var issue []entity.Issue

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	// data := db.Table("issue")
	// data.Select("*")
	// data.Where("id_usulan_akreditasi = ?", id_usulan_akreditasi)

	data := db.Table("issue")
	data.Select("issue.*, identitas_jurnal.frekuensi_terbitan")
	data.Joins("LEFT JOIN identitas_jurnal on issue.id_identitas_jurnal = identitas_jurnal.id_identitas_jurnal")
	data.Where("issue.id_usulan_akreditasi = ?", id_usulan_akreditasi)

	if search != "" {
		data.Where("lower(judul_issue) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Order("volume_issue asc, nomor_issue asc, CAST(tahun_issue as integer) asc")
	data.Find(&issue)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(issue)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&issue); err != nil {
		panic(err)
	}

	res := helper.BuildPaginationResponse(issue, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func ListIssueDiajukanUsulanAkreditasi(page string, row string, id_usulan_akreditasi string) (helper.ResponsePagination, error) {
	var issue []entity.Issue

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	//data := db.Table("issue")
	//data.Select("*")
	//data.Where("id_usulan_akreditasi = ?", id_usulan_akreditasi)
	//data.Order("volume_issue asc, nomor_issue asc, CAST(tahun_issue as integer) asc")
	//data.Find(&issue)
	data := db.Table("issue")
	data.Select("issue.*, identitas_jurnal.frekuensi_terbitan")
	data.Joins("LEFT JOIN identitas_jurnal on issue.id_identitas_jurnal = identitas_jurnal.id_identitas_jurnal")
	data.Where("issue.id_usulan_akreditasi = ?", id_usulan_akreditasi)
	data.Order("volume_issue asc, nomor_issue asc, tahun_issue asc")
	data.Find(&issue)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(issue)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&issue); err != nil {
		panic(err)
	}

	for i, v := range issue {
		issue[i].PenugasanPenilaianIssue = getPenugasanPenilaiIssueByIssue(v.IDIssue, v.IDUsulanAkreditasi)
	}

	res := helper.BuildPaginationResponse(issue, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func getPenugasanPenilaiIssueByIssue(id_issue int64, id_usulan_akreditasi int64) []entity.PenugasanPenilaianIssue {
	var res []entity.PenugasanPenilaianIssue

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penugasan_penilaian_issue as t1")
	data.Select("t1.*, t2.nama as nama_penilai")
	data.Joins("LEFT JOIN personal as t2 on t2.id_personal = t1.id_personal")
	data.Where("id_issue = ?", id_issue)
	data.Where("id_usulan_akreditasi = ?", id_usulan_akreditasi)
	data.Find(&res)

	for i, v := range res {
		if v.StsPenerimaanPenugasan == "1" {
			res[i].StsPenerimaanPenugasan = "Diterima"
		} else if v.StsPenerimaanPenugasan == "0" {
			res[i].StsPenerimaanPenugasan = "Ditolak"
		} else {
			res[i].StsPenerimaanPenugasan = "Blm. ditentukan"
		}

		if v.StsPelaksanaanTugas == "1" {
			res[i].StsPelaksanaanTugas = "Selesai"
		} else {
			res[i].StsPelaksanaanTugas = "Blm. selesai"
		}
	}

	return res
}

func listIssueByUsulanAkreditasi(id_usulan_akreditasi int64) []entity.Issue {
	var issue []entity.Issue

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("issue")
	data.Select("*")
	data.Where("id_usulan_akreditasi = ?", id_usulan_akreditasi)
	//data.Order("nomor_issue, volume_issue, tahun_issue") remark by agus 20230508 (set order data)
	data.Order("tahun_issue, volume_issue, nomor_issue")
	data.Find(&issue)
	return issue
}

func listIssueByUsulanAkreditasiDiajukanSebelumnya(id_usulan_akreditasi int64) []entity.Issue {
	var issue []entity.Issue
	var id_identitas_jurnal int64
	var id_usulan_akreditasi_old int64

	db := config.Con()
	defer config.CloseCon(db)

	get_id_identitas_jurnal := db.Table("usulan_akreditasi as t1")
	get_id_identitas_jurnal.Distinct("t1.id_identitas_jurnal")
	get_id_identitas_jurnal.Where("t1.id_usulan_akreditasi = ?", id_usulan_akreditasi)
	get_id_identitas_jurnal.Scan(&id_identitas_jurnal)

	get_id_usulan_akreditasi_old := db.Table("usulan_akreditasi as t1")
	get_id_usulan_akreditasi_old.Select("t1.id_usulan_akreditasi")
	get_id_usulan_akreditasi_old.Joins("INNER JOIN pengajuan_usulan_akreditasi t2 on t2.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	get_id_usulan_akreditasi_old.Where("t1.id_identitas_jurnal = ?", id_identitas_jurnal)
	get_id_usulan_akreditasi_old.Where("t1.id_usulan_akreditasi != ?", id_usulan_akreditasi)
	get_id_usulan_akreditasi_old.Order("t2.tgl_pengajuan DESC")
	get_id_usulan_akreditasi_old.Limit(1)
	get_id_usulan_akreditasi_old.Scan(&id_usulan_akreditasi_old)

	data := db.Table("issue")
	data.Select("*")
	data.Where("id_usulan_akreditasi = ?", id_usulan_akreditasi_old)

	//add by agus 20230508 (set order data)
	data.Order("tahun_issue, volume_issue, nomor_issue")
	data.Find(&issue)
	return issue
}

func CheckDuplicateUrlIssue(id_usulan_akreditasi int64, url string) bool {
	var issue entity.Issue

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("issue").Where("id_usulan_akreditasi = ?", id_usulan_akreditasi).Where("url_issue = ?", url).Take(&issue)
	return !(data.Error == nil)
}

func CheckCanAddNewIssue(id_usulan_akreditasi int64, id_identitas_jurnal int64) bool {
	var count int64
	var count2 int64
	var status bool

	db := config.Con()
	defer config.CloseCon(db)

	db.Table("issue").Where("id_usulan_akreditasi = ?", id_usulan_akreditasi).Count(&count)

	check := db.Table("usulan_akreditasi as tb1").Select("*")
	check.Joins("inner join public.penetapan_desk_evaluasi tb2 on tb1.id_usulan_akreditasi=tb2.id_usulan_akreditasi")
	check.Joins("inner join public.hasil_akreditasi tb3 on tb3.id_usulan_akreditasi = tb1.id_usulan_akreditasi")
	check.Joins("inner join public.penetapan_akreditasi tb4 on tb4.id_usulan_akreditasi = tb1.id_usulan_akreditasi")
	check.Where("tb1.id_identitas_jurnal = ?", id_identitas_jurnal)
	check.Where("tb1.sts_pengajuan_usulan = ?", "1")
	check.Where("tb4.sts_hasil_akreditasi = ?", "1")
	check.Where("tb2.sts_hasil_desk_evaluasi = ?", "1")
	check.Count(&count2)
	if count2 > 0 {
		status = true
	} else {
		status = false
	}

	return status == false || (status == true && count == 0)
}

func InsertIssueUsulanAkreditasi(req entity.IssueDTO, id_identitas_jurnal int64, id_jurnal int64) (interface{}, error) {
	var issue entity.Issue

	db := config.Con()
	defer config.CloseCon(db)

	issue.IDJurnal = id_jurnal
	issue.IDIdentitasJurnal = id_identitas_jurnal
	issue.IDUsulanAkreditasi = req.IdUsulanAkreditasi
	issue.JudulIssue = req.JudulIssue
	issue.UrlIssue = req.UrlIssue
	issue.VolumeIssue = req.VolumeIssue
	issue.NomorIssue = req.NomorIssue
	issue.TahunIssue = req.TahunIssue
	issue.TglCreated = time.Now()
	save := db.Table("issue").Create(&issue)
	if save.Error != nil {
		return nil, save.Error
	}
	return issue, nil
}

func ShowIssueUsulanAkreditasi(id_issue int) interface{} {
	var issue entity.Issue

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("issue")
	data.Select("issue.*, identitas_jurnal.frekuensi_terbitan")
	data.Joins("LEFT JOIN identitas_jurnal on issue.id_identitas_jurnal = identitas_jurnal.id_identitas_jurnal")
	data.Where("issue.id_issue = ?", id_issue)
	data.Take(&issue)
	//data.Select("*")
	//data.Where("id_issue = ?", id_issue)
	//data.Take(&issue)

	return issue
}

func UpdateIssueUsulanAkreditasi(req entity.IssueDTO, id_issue int64, id_identitas_jurnal int64, id_jurnal int64) (interface{}, error) {

	var issue entity.Issue

	db := config.Con()
	defer config.CloseCon(db)

	issue.IDIssue = id_issue
	issue.IDJurnal = id_jurnal
	issue.IDIdentitasJurnal = id_identitas_jurnal
	issue.IDUsulanAkreditasi = req.IdUsulanAkreditasi
	issue.JudulIssue = req.JudulIssue
	issue.UrlIssue = req.UrlIssue
	issue.VolumeIssue = req.VolumeIssue
	issue.NomorIssue = req.NomorIssue
	issue.TahunIssue = req.TahunIssue
	issue.TglUpdated = time.Now()
	update := db.Table("issue").Where("id_issue = ?", id_issue).Save(&issue)
	if update.Error != nil {
		return nil, update.Error
	}
	return issue, nil
}

func DeleteIssueUsulanAkreditasi(req entity.Issue, id_issue int) (interface{}, error) {
	db := config.Con()
	defer config.CloseCon(db)

	delete := db.Table("issue").Where("id_issue = ?", id_issue).Delete(&entity.Issue{})
	if delete.Error != nil {
		return nil, delete.Error
	}
	return true, nil
}
