package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"bytes"
	"encoding/base64"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/makiuchi-d/gozxing"
	"github.com/makiuchi-d/gozxing/oned"
	"github.com/skip2/go-qrcode"
	"github.com/vcraescu/go-paginator/v2"
	"github.com/vcraescu/go-paginator/v2/adapter"
	"github.com/vcraescu/go-paginator/v2/view"
)

func ListDaftarTerbitanJurnal(search string, page string, row string, status string, personal string, baseUrl string) (helper.ResponsePagination, error) {
	var res helper.ResponsePagination
	var err error
	if personal !="" {
		data, errs := ListDaftarTerbitanJurnal_PIC(search, page, row, status,personal,baseUrl)
		res = data
		err = errs
	}else{
		data, errs := ListDaftarTerbitanJurnal_AllPIC(search, page, row, status,baseUrl)
		res = data
		err = errs
	}
	 
	if err != nil {
		return helper.ResponsePagination{}, err
	}

	return res, nil

}

func ListDaftarTerbitanJurnal_PIC(search string, page string, row string, status string, personal string, baseUrl string) (helper.ResponsePagination, error) {
	var jurnal []entity.DaftarTerbitanJurnal
	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	db := config.Con()
	defer config.CloseCon(db)
	
	query := db.Raw("SELECT * from z_arjuna_list_jurnal_by_pic( ( ? ),( ? ), ( ? ), ( ? ), ( ? ))", personal, status, (pages - 1), rows,search)
	/*
	if search != "" {
		query := db.Raw("SELECT * from z_arjuna_list_jurnal_by_eissn(('1', ( ? ), ( ? ))", personal , (pages - 1), rows)

	}
	*/
	 
	data := db.Table("( ? ) as tb_temp", query)
	 
	data.Find(&jurnal)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(jurnal)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&jurnal); err != nil {
		panic(err)
	}


	for i, v := range jurnal {
		if v.Progres == "" {
			jurnal[i].Progres = "Belum pernah diajukan"
			jurnal[i].TglUsulanAkreditasiTerakhir = "Belum pernah diajukan"
		}
		if v.GradeAkreditasiSa == "A" {
			jurnal[i].GradeAkreditasiSa = "A"
		} else if v.GradeAkreditasiSa == "B" {
			jurnal[i].GradeAkreditasiSa = "B"
		} else if v.GradeAkreditasiSa == "" {
			jurnal[i].GradeAkreditasiSa = "-" //"Belum dilakukan"
		} else {
			jurnal[i].GradeAkreditasiSa = "Tidak terakreditasi"
		}

		// Edit & Add by agus 2023-04-02
		// -----------
		if v.NilaiTotalSa >= 85 {
			jurnal[i].PeringkatGradeAkreditasiSa = "Peringkat 1"
		} else if v.NilaiTotalSa >= 70 {
			jurnal[i].PeringkatGradeAkreditasiSa = "Peringkat 2"
		} else if v.NilaiTotalSa >= 60 {
			jurnal[i].PeringkatGradeAkreditasiSa = "Peringkat 3"
		} else if v.NilaiTotalSa >= 50 {
			jurnal[i].PeringkatGradeAkreditasiSa = "Peringkat 4"
		} else if v.NilaiTotalSa >= 40 {
			jurnal[i].PeringkatGradeAkreditasiSa = "Peringkat 5"
		} else if v.NilaiTotalSa >= 30 {
			jurnal[i].PeringkatGradeAkreditasiSa = "Peringkat 6"
		} else {
			jurnal[i].PeringkatGradeAkreditasiSa = "Belum dilakukan"
		}

		// Untuk menampilakan data peringkat akreditasi
		if v.GradeAkreditasi != "" {
			//jurnal[i].PeringkatGradeAkreditasi  = v.GradeAkreditasi
			jurnal[i].PeringkatGradeAkreditasi = "Terakreditasi (Peringkat: " + v.GradeAkreditasi + ")"
		} else if v.GradeAkreditasi != "" && v.IdProgres != 4 {
			//IdProgres akan 0 jika GradeAkredutasi kosong (belum tersertifikasi)
			jurnal[i].PeringkatGradeAkreditasi = "-"
			jurnal[i].IdProgres= 0  
		 } else {
			//IdProgres akan 0 jika GradeAkredutasi kosong (belum tersertifikasi)
			 jurnal[i].PeringkatGradeAkreditasi = "-"
			//jurnal[i].IdProgres= 0 

		}
		// -----------

		jurnal[i].BidangIlmu = checkBidangIlmuJurnal(v.IdJurnal)
		if v.Image != "" {
			jurnal[i].Image = baseUrl + "/file/logo/" + v.Image
		} else {
			jurnal[i].Image = baseUrl + "/file/logo/no_image.jpg"
		}

		jurnal[i].Issue = listIssueByUsulanAkreditasi(v.IDUsulanAkreditasi)
	}

	res := helper.BuildPaginationResponse(jurnal, view)
	res.PerPage = rows
	res.Total = total
	return res, nil

}

func ListDaftarTerbitanJurnal_AllPIC(search string, page string, row string, status string,  baseUrl string) (helper.ResponsePagination, error) {
	var jurnal []entity.DaftarTerbitanJurnal
	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	db := config.Con()
	defer config.CloseCon(db)
	
	query := db.Raw("SELECT * from z_arjuna_list_jurnal_by_eissn( ( ? ), ( ? ), ( ? ), ( ? ))", status, (pages - 1), rows,search)
	/*
	if search != "" {
		query := db.Raw("SELECT * from z_arjuna_list_jurnal_by_eissn(('1', ( ? ), ( ? ))", personal , (pages - 1), rows)

	}
	*/
	 
	data := db.Table("( ? ) as tb_temp", query)
	 
	data.Find(&jurnal)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(jurnal)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&jurnal); err != nil {
		panic(err)
	}


	for i, v := range jurnal {
		if v.Progres == "" {
			jurnal[i].Progres = "Belum pernah diajukan"
			jurnal[i].TglUsulanAkreditasiTerakhir = "Belum pernah diajukan"
		}
		if v.GradeAkreditasiSa == "A" {
			jurnal[i].GradeAkreditasiSa = "A"
		} else if v.GradeAkreditasiSa == "B" {
			jurnal[i].GradeAkreditasiSa = "B"
		} else if v.GradeAkreditasiSa == "" {
			jurnal[i].GradeAkreditasiSa = "-" //"Belum dilakukan"
		} else {
			jurnal[i].GradeAkreditasiSa = "Tidak terakreditasi"
		}

		// Edit & Add by agus 2023-04-02
		// -----------
		if v.NilaiTotalSa >= 85 {
			jurnal[i].PeringkatGradeAkreditasiSa = "Peringkat 1"
		} else if v.NilaiTotalSa >= 70 {
			jurnal[i].PeringkatGradeAkreditasiSa = "Peringkat 2"
		} else if v.NilaiTotalSa >= 60 {
			jurnal[i].PeringkatGradeAkreditasiSa = "Peringkat 3"
		} else if v.NilaiTotalSa >= 50 {
			jurnal[i].PeringkatGradeAkreditasiSa = "Peringkat 4"
		} else if v.NilaiTotalSa >= 40 {
			jurnal[i].PeringkatGradeAkreditasiSa = "Peringkat 5"
		} else if v.NilaiTotalSa >= 30 {
			jurnal[i].PeringkatGradeAkreditasiSa = "Peringkat 6"
		} else {
			jurnal[i].PeringkatGradeAkreditasiSa = "Belum dilakukan"
		}

		// Untuk menampilakan data peringkat akreditasi
		if v.GradeAkreditasi != "" {
			//jurnal[i].PeringkatGradeAkreditasi  = v.GradeAkreditasi
			jurnal[i].PeringkatGradeAkreditasi = "Terakreditasi (Peringkat: " + v.GradeAkreditasi + ")"
		} else if v.GradeAkreditasi != "" && v.IdProgres != 4 {
			//IdProgres akan 0 jika GradeAkredutasi kosong (belum tersertifikasi)
			jurnal[i].PeringkatGradeAkreditasi = "-"
			jurnal[i].IdProgres= 0  
		 } else {
			//IdProgres akan 0 jika GradeAkredutasi kosong (belum tersertifikasi)
			 jurnal[i].PeringkatGradeAkreditasi = "-"
			//jurnal[i].IdProgres= 0 

		}
		// -----------

		jurnal[i].BidangIlmu = checkBidangIlmuJurnal(v.IdJurnal)
		if v.Image != "" {
			jurnal[i].Image = baseUrl + "/file/logo/" + v.Image
		} else {
			jurnal[i].Image = baseUrl + "/file/logo/no_image.jpg"
		}

		jurnal[i].Issue = listIssueByUsulanAkreditasi(v.IDUsulanAkreditasi)
	}

	res := helper.BuildPaginationResponse(jurnal, view)
	res.PerPage = rows
	res.Total = total
	return res, nil

}

func ListDaftarTerbitanJurnal_ori(search string, page string, row string, status string, personal string, baseUrl string) (helper.ResponsePagination, error) {
	var jurnal []entity.DaftarTerbitanJurnal

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	// add where pada beberapa query usulan_akreditasi  (.Where("ua.sts_pengajuan_usulan = ?", "1")) by agus 2023-04-08 *

	nilai_total_sa := db.Table("usulan_akreditasi as ua")
	nilai_total_sa.Select("hs.nilai_total")
	// nilai_total_sa.Where("ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	// nilai_total_sa.Order("ua.id_usulan_akreditasi DESC")
	// nilai_total_sa.Joins("Left JOIN hasil_sa as hs on hs.id_usulan_akreditasi = ua.id_usulan_akreditasi AND ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	nilai_total_sa.Joins("Left JOIN hasil_sa as hs on hs.id_usulan_akreditasi = ua.id_usulan_akreditasi")
	nilai_total_sa.Where("ua.id_usulan_akreditasi = temp.id_usulan_akreditasi")
	nilai_total_sa.Where("ua.sts_pengajuan_usulan = ?", "1")
	nilai_total_sa.Limit(1)

	grade_akreditasi_sa := db.Table("usulan_akreditasi as ua")
	grade_akreditasi_sa.Select("hs.grade_akreditasi")
	// grade_akreditasi_sa.Where("ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	// grade_akreditasi_sa.Order("ua.id_usulan_akreditasi DESC")
	// grade_akreditasi_sa.Joins("Left JOIN hasil_sa as hs on hs.id_usulan_akreditasi = ua.id_usulan_akreditasi AND ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	grade_akreditasi_sa.Joins("Left JOIN hasil_sa as hs on hs.id_usulan_akreditasi = ua.id_usulan_akreditasi")
	grade_akreditasi_sa.Where("ua.id_usulan_akreditasi = temp.id_usulan_akreditasi")
	grade_akreditasi_sa.Where("ua.sts_pengajuan_usulan = ?", "1")
	grade_akreditasi_sa.Limit(1)

	nilai_total := db.Table("usulan_akreditasi as ua")
	nilai_total.Select("ha.nilai_total")
	// nilai_total.Joins("JOIN hasil_akreditasi as ha on ha.id_usulan_akreditasi = ua.id_usulan_akreditasi AND ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	// nilai_total.Order("ua.id_usulan_akreditasi DESC")
	nilai_total.Joins("JOIN hasil_akreditasi as ha on ha.id_usulan_akreditasi = ua.id_usulan_akreditasi")
	nilai_total.Where("ua.id_usulan_akreditasi = temp.id_usulan_akreditasi")
	nilai_total.Where("ua.sts_pengajuan_usulan = ?", "1")
	nilai_total.Limit(1)

	grade_akreditasi := db.Table("usulan_akreditasi as ua")
	grade_akreditasi.Select("ha.grade_akreditasi")
	// grade_akreditasi.Joins("JOIN hasil_akreditasi as ha on ha.id_usulan_akreditasi = ua.id_usulan_akreditasi AND ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	// grade_akreditasi.Order("ua.id_usulan_akreditasi DESC")
	grade_akreditasi.Joins("JOIN hasil_akreditasi as ha on ha.id_usulan_akreditasi = ua.id_usulan_akreditasi")
	grade_akreditasi.Where("ua.id_usulan_akreditasi = temp.id_usulan_akreditasi")
	grade_akreditasi.Where("ua.sts_pengajuan_usulan = ?", "1")
	grade_akreditasi.Limit(1)

	progres := db.Table("usulan_akreditasi as ua")
	progres.Select("progres.progres")
	// progres.Where("ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	// progres.Order("ua.id_usulan_akreditasi DESC")
	// progres.Joins("Left JOIN progres_usulan_akreditasi as pua ON pua.id_usulan_akreditasi = ua.id_usulan_akreditasi AND ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	progres.Joins("Left JOIN progres_usulan_akreditasi as pua ON pua.id_usulan_akreditasi = ua.id_usulan_akreditasi")
	progres.Where("ua.id_usulan_akreditasi = temp.id_usulan_akreditasi")
	progres.Where("ua.sts_pengajuan_usulan = ?", "1")
	progres.Joins("Left JOIN progres ON progres.id_progres = pua.id_progres")
	progres.Limit(1)

	id_progres := db.Table("usulan_akreditasi as ua")
	id_progres.Select("progres.id_progres")
	id_progres.Joins("Left JOIN progres_usulan_akreditasi as pua ON pua.id_usulan_akreditasi = ua.id_usulan_akreditasi")
	id_progres.Where("ua.id_usulan_akreditasi = temp.id_usulan_akreditasi")
	id_progres.Where("ua.sts_pengajuan_usulan = ?", "1")
	id_progres.Joins("Left JOIN progres ON progres.id_progres = pua.id_progres")
	id_progres.Limit(1)

   	
	usulan_akreditasi := db.Table("usulan_akreditasi as ua")
	usulan_akreditasi.Select("ua.id_usulan_akreditasi")
	usulan_akreditasi.Where("ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	usulan_akreditasi.Where("ua.sts_pengajuan_usulan = ?", "1")
	usulan_akreditasi.Order("ua.id_usulan_akreditasi DESC")
	usulan_akreditasi.Limit(1)
    
  /*
	usulan_akreditasi := db.Table("usulan_akreditasi as ua")
	usulan_akreditasi.Select("ua.id_usulan_akreditasi")
	usulan_akreditasi.Joins("left JOIN hasil_akreditasi as ha on ha.id_usulan_akreditasi = ua.id_usulan_akreditasi")
	usulan_akreditasi.Where("ua.id_identitas_jurnal = ij.id_identitas_jurnal")
	usulan_akreditasi.Where("ua.sts_pengajuan_usulan = ?", "1")
	usulan_akreditasi.Order("ha.id_hasil_akreditasi asc, ua.id_usulan_akreditasi desc")
	//usulan_akreditasi.Order("ha.id_hasil_akreditasi desc, ua.id_usulan_akreditasi desc")
	usulan_akreditasi.Limit(1)
 */

	q := db.Table("pic")
	q.Distinct()
	// q.Select("ij.eissn, ij.pissn, ij.publisher, ij.society, j.nama_awal_jurnal, j.url_contact, j.url_editor, j.url_statistik_pengunjung, j.country, j.city, j.tgl_akhir_terakreditasi, pic.id_pic, personal.email as pic_email, personal.nama as pic_nama, personal.no_hand_phone as pic_no_telepon, j.id_jurnal, ij.id_identitas_jurnal, ij.nama_jurnal, j.tgl_pembuatan, j.tgl_created, pengguna.username, j.alamat, j.alamat_surel, j.no_telepon, j.url_jurnal, ij.sts_aktif_identitas_jurnal, j.tahun_1_terbit as tahun_terbit, ij.frekuensi_terbitan, j.tgl_usulan_akreditasi_terakhir, ij.image, ij.tgl_created, ( ? ) as nilai_total_sa, ( ? ) as grade_akreditasi_sa, ( ? ) as progres, ( ? ) as nilai_total, ( ? ) as grade_akreditasi, ( ? ) as id_usulan_akreditasi", nilai_total_sa, grade_akreditasi_sa, progres, nilai_total, grade_akreditasi, usulan_akreditasi)
	q.Select("ij.eissn, ij.pissn, ij.publisher, ij.society, j.nama_awal_jurnal, j.url_contact, j.url_editor, j.url_statistik_pengunjung, j.country, j.city, j.tgl_akhir_terakreditasi, pic.id_pic, personal.email as pic_email, personal.nama as pic_nama, personal.no_hand_phone as pic_no_telepon, j.id_jurnal, ij.id_identitas_jurnal, ij.nama_jurnal, j.tgl_pembuatan, j.tgl_created, pengguna.username, j.alamat, j.alamat_surel, j.no_telepon, j.url_jurnal, ij.sts_aktif_identitas_jurnal, j.tahun_1_terbit as tahun_terbit, ij.frekuensi_terbitan, to_char(j.tgl_usulan_akreditasi_terakhir,'dd Mon yyyy') as tgl_usulan_akreditasi_terakhir, ij.image, ( ? ) as id_usulan_akreditasi", usulan_akreditasi)
	q.Joins("JOIN identitas_jurnal AS ij ON ij.id_identitas_jurnal = pic.id_identitas_jurnal")
	q.Joins("JOIN jurnal AS j ON j.id_jurnal = ij.id_jurnal")
	q.Joins("JOIN pengguna ON pengguna.id_personal = pic.id_personal ")
	q.Joins("JOIN personal ON personal.id_personal = pic.id_personal ")
	q.Where("pic.sts_aktif_pic = ?", "1")
	
 
	if personal != "" {
		q.Where("pic.id_personal = ?", personal)
	}
	if search != "" {
		//remark & edit by agus 2023-03-24 (tambah param search dgn eissn)
		//q.Where("lower(ij.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
		q.Where("lower(ij.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%").Or("lower(ij.eissn) LIKE ?", "%"+strings.ToLower(search)+"%")
	}

	if status != "" {
		q.Where("ij.sts_aktif_identitas_jurnal = ?", status)
	}

	data := db.Table("( ? ) as temp", q)
	data.Select("temp.*, ( ? ) as id_progres, ( ? ) as nilai_total_sa, ( ? ) as grade_akreditasi_sa, ( ? ) as progres, ( ? ) as nilai_total, ( ? ) as grade_akreditasi", id_progres, nilai_total_sa, grade_akreditasi_sa, progres, nilai_total, grade_akreditasi)
	data.Order("temp.tgl_created desc")
	data.Find(&jurnal)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(jurnal)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&jurnal); err != nil {
		panic(err)
	}

	for i, v := range jurnal {
		if v.Progres == "" {
			jurnal[i].Progres = "Belum pernah diajukan"
			jurnal[i].TglUsulanAkreditasiTerakhir = "Belum pernah diajukan"
		}
		if v.GradeAkreditasiSa == "A" {
			jurnal[i].GradeAkreditasiSa = "A"
		} else if v.GradeAkreditasiSa == "B" {
			jurnal[i].GradeAkreditasiSa = "B"
		} else if v.GradeAkreditasiSa == "" {
			jurnal[i].GradeAkreditasiSa = "-" //"Belum dilakukan"
		} else {
			jurnal[i].GradeAkreditasiSa = "Tidak terakreditasi"
		}

		// Edit & Add by agus 2023-04-02
		// -----------
		if v.NilaiTotalSa >= 85 {
			jurnal[i].PeringkatGradeAkreditasiSa = "Peringkat 1"
		} else if v.NilaiTotalSa >= 70 {
			jurnal[i].PeringkatGradeAkreditasiSa = "Peringkat 2"
		} else if v.NilaiTotalSa >= 60 {
			jurnal[i].PeringkatGradeAkreditasiSa = "Peringkat 3"
		} else if v.NilaiTotalSa >= 50 {
			jurnal[i].PeringkatGradeAkreditasiSa = "Peringkat 4"
		} else if v.NilaiTotalSa >= 40 {
			jurnal[i].PeringkatGradeAkreditasiSa = "Peringkat 5"
		} else if v.NilaiTotalSa >= 30 {
			jurnal[i].PeringkatGradeAkreditasiSa = "Peringkat 6"
		} else {
			jurnal[i].PeringkatGradeAkreditasiSa = "Belum dilakukan"
		}

		// Untuk menampilakan data peringkat akreditasi
		if v.GradeAkreditasi != "" {
			//jurnal[i].PeringkatGradeAkreditasi  = v.GradeAkreditasi
			jurnal[i].PeringkatGradeAkreditasi = "Terakreditasi (Peringkat: " + v.GradeAkreditasi + ")"
		} else if v.GradeAkreditasi != "" && v.IdProgres != 4 {
			//IdProgres akan 0 jika GradeAkredutasi kosong (belum tersertifikasi)
			jurnal[i].PeringkatGradeAkreditasi = "-"
			jurnal[i].IdProgres= 0  
		 } else {
			//IdProgres akan 0 jika GradeAkredutasi kosong (belum tersertifikasi)
			 jurnal[i].PeringkatGradeAkreditasi = "-"
			//jurnal[i].IdProgres= 0 

		}
		// -----------

		jurnal[i].BidangIlmu = checkBidangIlmuJurnal(v.IdJurnal)
		if v.Image != "" {
			jurnal[i].Image = baseUrl + "/file/logo/" + v.Image
		} else {
			jurnal[i].Image = baseUrl + "/file/logo/no_image.jpg"
		}

		jurnal[i].Issue = listIssueByUsulanAkreditasi(v.IDUsulanAkreditasi)
	}

	res := helper.BuildPaginationResponse(jurnal, view)
	res.PerPage = rows
	res.Total = total
	return res, nil

}

func checkBidangIlmuJurnal(id_jurnal int64) bool {
	var bidang_ilmi_jurnal entity.BidangIlmuJurnal
	db := config.Con()
	defer config.CloseCon(db)
	data := db.Table("bidang_ilmu_jurnal")
	data.Where("sts_aktif_bidang_ilmu_jurnal = ?", "1")
	data.Where("id_jurnal = ?", id_jurnal)
	data.Take(&bidang_ilmi_jurnal)
	if data.Error != nil {
		return false
	}
	return true
}

func GetBidangIlmuJurnalByIDBidangIlmuJurnal(id_bidang_ilmu_jurnal int) (interface{}, error) {
	var bidang_ilmu_jurnal entity.BidangIlmuJurnal

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("bidang_ilmu_jurnal")
	data.Where("id_bidang_ilmu_jurnal = ?", id_bidang_ilmu_jurnal)
	data.Take(&bidang_ilmu_jurnal)

	if data.Error != nil {
		return nil, data.Error
	}
	return bidang_ilmu_jurnal, nil
}

func CheckDuplicateEissnJurnal(eissn string) bool {
	var iden_jurnal entity.IdentitasJurnal

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("identitas_jurnal").Where("eissn = ?", eissn).Take(&iden_jurnal)
	return !(data.Error == nil)
}

func CheckAssesmentJurnal(id_jurnal int) bool {
	var id_usulan_akreditasi int64

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("usulan_akreditasi as ua").Select("ua.id_usulan_akreditasi")
	data.Joins("join identitas_jurnal as ij on ij.id_identitas_jurnal = ua.id_identitas_jurnal")
	data.Where("ij.id_jurnal = ?", id_jurnal)
	data.Where("ua.sts_pengajuan_usulan = ?", "1")
	data.Take(&id_usulan_akreditasi)

	if data.Error != nil {
		return false
	}
	return true
}

func InsertJurnal(req entity.JurnalDTO, id_personal int) (interface{}, error) {
	var jurnal entity.Jurnal
	var iden_jurnal entity.IdentitasJurnal
	var eic_jurnal entity.EICJurnal
	var pic_jurnal entity.PICJurnal
	var bidang_ilmi_jurnal entity.BidangIlmuJurnal
	country := getCountryByCode(req.CodeCountry)
	kota := getKotaByCode(req.CodeCity)

	jurnal.NamaAwalJurnal = req.NamaAwalJurnal
	jurnal.Tahun_1Terbit = req.Tahun1Terbit
	jurnal.UrlJurnal = req.UrlJurnal
	jurnal.UrlContact = req.UrlContact
	jurnal.UrlEditor = req.UrlEditor
	jurnal.UrlReviewer = req.UrlReviewer
	jurnal.Country = country
	jurnal.City = kota
	jurnal.Alamat = req.Alamat
	jurnal.NoTelepon = req.NoTelepon
	jurnal.AlamatSurel = req.AlamatSurel
	jurnal.UrlStatistikPengunjung = req.UrlStatistikPengunjung
	jurnal.UrlPengindeks = req.UrlPengindeks
	jurnal.TglCreated = time.Now()
	jurnal.IDBidangFokus = req.IDBidangFokus
	jurnal.NamaPT = req.NamaPT

	db := config.Con()
	defer config.CloseCon(db)
	// begin a transaction
	tx := db.Begin()

	// do some database operations in the transaction (use 'tx' from this point, not 'db')
	saveJurnal := tx.Table("jurnal").Create(&jurnal)
	if saveJurnal.Error != nil {
		// rollback the transaction in case of error
		tx.Rollback()
		return nil, saveJurnal.Error
	}

	iden_jurnal.IdJurnal = jurnal.IdJurnal
	iden_jurnal.NamaJurnal = req.NamaJurnal
	iden_jurnal.Eissn = req.Eissn
	iden_jurnal.Pissn = req.Pissn
	iden_jurnal.Publisher = req.Publisher
	iden_jurnal.Society = req.Society
	iden_jurnal.UrlEtikapublikasi = req.UrlEtikapublikasi
	iden_jurnal.UrlGooglescholar = req.UrlGooglescholar
	iden_jurnal.AlamatOai = req.AlamatOai
	iden_jurnal.DoiJurnal = req.DoiJurnal
	iden_jurnal.FrekuensiTerbitan = 0
	iden_jurnal.TglCreated = time.Now()
	iden_jurnal.IDBidangFokus = jurnal.IDBidangFokus
	iden_jurnal.NamaPT = jurnal.NamaPT
	saveIdenJurnal := tx.Table("identitas_jurnal").Create(&iden_jurnal)
	if saveIdenJurnal.Error != nil {
		// rollback the transaction in case of error
		tx.Rollback()
		return nil, saveIdenJurnal.Error
	}

	pic_jurnal.IdIdentitasJurnal = iden_jurnal.IdIdentitasJurnal
	pic_jurnal.IdPersonal = int64(id_personal)
	savePIC := tx.Table("pic").Create(&pic_jurnal)
	if savePIC.Error != nil {
		// rollback the transaction in case of error
		tx.Rollback()
		return nil, savePIC.Error
	}

	eic_jurnal.IdIdentitasJurnal = iden_jurnal.IdIdentitasJurnal
	eic_jurnal.NamaEic = req.NamaEic
	eic_jurnal.AlamatSurel = req.AlamatSurel
	eic_jurnal.NoTelepon = req.NoTelepon
	eic_jurnal.TglCreated = time.Now()
	saveEIC := tx.Table("eic").Create(&eic_jurnal)
	if saveEIC.Error != nil {
		// rollback the transaction in case of error
		tx.Rollback()
		return nil, saveEIC.Error
	}

	checkBidangIlmu := getBidangIlmuByKode(req.KodeBidangIlmu)
	if checkBidangIlmu != "" {
		bidang_ilmi_jurnal.IdJurnal = jurnal.IdJurnal
		bidang_ilmi_jurnal.IdBidangIlmu = checkBidangIlmu
		saveBidangIlmuJurnal := tx.Table("bidang_ilmu_jurnal").Create(&bidang_ilmi_jurnal)
		if saveBidangIlmuJurnal.Error != nil {
			// rollback the transaction in case of error
			tx.Rollback()
			return nil, saveBidangIlmuJurnal.Error
		}
	}

	// Or commit the transaction
	tx.Commit()
	return jurnal, nil
}

func UpdateJurnal(req entity.JurnalUpdateDTO, id_personal int, id_identitas_jurnal int) (interface{}, error) {
	var jurnal entity.Jurnal
	var iden_jurnal entity.IdentitasJurnal
	var eic_jurnal entity.EICJurnal

	// country := getCountryByCode(req.CodeCountry)
	// kota := getKotaByCode(req.CodeCity)

	jurnal.IdJurnal = req.IdJurnal
	jurnal.Tahun_1Terbit = req.Tahun1Terbit
	jurnal.UrlJurnal = req.UrlJurnal
	jurnal.UrlContact = req.UrlContact
	jurnal.UrlEditor = req.UrlEditor
	jurnal.UrlReviewer = req.UrlReviewer
	jurnal.Country = req.Country
	jurnal.City = req.City
	jurnal.Alamat = req.Alamat
	jurnal.NoTelepon = req.NoTelepon
	jurnal.AlamatSurel = req.AlamatSurel
	jurnal.UrlStatistikPengunjung = req.UrlStatistikPengunjung
	jurnal.UrlPengindeks = req.UrlPengindeks
	jurnal.IDBidangFokus = req.IDBidangFokus
	jurnal.NamaPT = req.NamaPT

	db := config.Con()
	defer config.CloseCon(db)
	// begin a transaction
	tx := db.Begin()

	// do some database operations in the transaction (use 'tx' from this point, not 'db')
	saveJurnal := tx.Table("jurnal").Where("id_jurnal = ?", req.IdJurnal).Save(&jurnal)
	if saveJurnal.Error != nil {
		// rollback the transaction in case of error
		tx.Rollback()
		return nil, saveJurnal.Error
	}

	iden_jurnal.IdIdentitasJurnal = int64(id_identitas_jurnal)
	iden_jurnal.IdJurnal = jurnal.IdJurnal
	iden_jurnal.NamaJurnal = req.NamaJurnal
	iden_jurnal.Eissn = req.Eissn
	iden_jurnal.Pissn = req.Pissn
	iden_jurnal.Publisher = req.Publisher
	iden_jurnal.Society = req.Society
	iden_jurnal.UrlEtikapublikasi = req.UrlEtikapublikasi
	iden_jurnal.UrlGooglescholar = req.UrlGooglescholar
	iden_jurnal.AlamatOai = req.AlamatOai
	iden_jurnal.DoiJurnal = req.DoiJurnal
	iden_jurnal.FrekuensiTerbitan = req.FrekuensiTerbitan
	iden_jurnal.IDBidangFokus = jurnal.IDBidangFokus
	iden_jurnal.NamaPT = jurnal.NamaPT
	// iden_jurnal.TglCreated = time.Now()
	saveIdenJurnal := tx.Table("identitas_jurnal").Where("id_identitas_jurnal = ?", id_identitas_jurnal).Save(&iden_jurnal)
	if saveIdenJurnal.Error != nil {
		// rollback the transaction in case of error
		tx.Rollback()
		return nil, saveIdenJurnal.Error
	}

	// db..Table("eic").Model(&entity.EICJurnal{}).Where("id_identitas_jurnal = ?", id_identitas_jurnal).Update("sts_aktif_eic", "0")
	// var id_eic int64
	// db.Table("eic").Select("id_eic").Where("id_identitas_jurnal = ?", id_identitas_jurnal).Where("sts_aktif_eic = ?", "1").Update("sts_aktif_eic", "0").Take(&id_eic)

	eic_jurnal.IdEic = req.IdPic
	eic_jurnal.IdIdentitasJurnal = iden_jurnal.IdIdentitasJurnal
	eic_jurnal.NamaEic = req.NamaEic
	eic_jurnal.NoTelepon = req.NoTelepon
	eic_jurnal.AlamatSurel = req.AlamatSurel
	saveEIC := tx.Table("eic").Where("id_eic", req.IdPic).Save(&eic_jurnal)
	if saveEIC.Error != nil {
		// rollback the transaction in case of error
		tx.Rollback()
		return nil, saveEIC.Error
	}

	// Or commit the transaction
	tx.Commit()
	return jurnal, nil
}

func InsertBidangIlmuJurnal(req entity.BidangIlmuJurnalDTO) (interface{}, error) {
	var bi_jurnal entity.BidangIlmuJurnal

	db := config.Con()
	defer config.CloseCon(db)

	check := checkBidangIlmuJurnal(req.IdJurnal)
	if check {
		var data_bi_jurnal entity.BidangIlmuJurnal
		var count int64
		db.Table("bidang_ilmu_jurnal").Where("id_jurnal = ?", req.IdJurnal).Count(&count)
		db.Table("bidang_ilmu_jurnal").Where("sts_aktif_bidang_ilmu_jurnal = ?", "1").Where("id_jurnal = ?", req.IdJurnal).Take(&data_bi_jurnal)
		if count > 1 {
			db.Table("bidang_ilmu_jurnal").Where("id_jurnal = ?", req.IdJurnal).Where("id_bidang_ilmu_jurnal != ?", data_bi_jurnal.IdBidangIlmuJurnal).Delete(&entity.BidangIlmuJurnal{})
		}
		db.Table("bidang_ilmu_jurnal").Where("id_bidang_ilmu_jurnal = ?", data_bi_jurnal.IdBidangIlmuJurnal).Updates(map[string]interface{}{"id_bidang_ilmu": req.IdBidangIlmu, "tgl_update": time.Now()})
	} else {
		bi_jurnal.IdJurnal = req.IdJurnal
		bi_jurnal.IdBidangIlmu = req.IdBidangIlmu
		saveBidangIlmuJurnal := db.Table("bidang_ilmu_jurnal").Create(&bi_jurnal)
		if saveBidangIlmuJurnal.Error != nil {
			return nil, saveBidangIlmuJurnal.Error
		}
	}
	return true, nil
}

func DeleteBidangIlmuJurnal(id_bidang_ilmu_jurnal int) (bool, error) {
	db := config.Con()
	defer config.CloseCon(db)

	delete := db.Table("bidang_ilmu_jurnal").Where("id_bidang_ilmu_jurnal = ?", id_bidang_ilmu_jurnal).Delete(&entity.BidangIlmuJurnal{})
	if delete.Error != nil {
		return false, nil
	}
	return true, nil
}

func ShowJurnal(id_identitas_jurnal int, baseUrl string) (interface{}, error) {
	var jurnal entity.DetailJurnal
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("pic")
	data.Select("pic.id_identitas_jurnal, pic.id_pic, j.id_jurnal, ij.doi_jurnal, ij.nama_jurnal, ij.eissn, ij.pissn, ij.publisher, ij.society, ij.id_bidang_fokus, ij.nama_pt, j.nama_awal_jurnal, j.tahun_1_terbit as tahun_terbit, j.url_jurnal, j.url_contact, j.url_editor, j.url_statistik_pengunjung, j.url_pengindeks, j.url_reviewer,ij.url_etikapublikasi, ij.url_googlescholar, j.country, j.city, j.alamat, j.no_telepon, j.alamat_surel, ij.alamat_oai, j.tgl_akhir_terakreditasi, j.tgl_usulan_akreditasi_terakhir,ij.doi_jurnal, ij.frekuensi_terbitan, ij.image")
	data.Joins("JOIN identitas_jurnal AS ij ON ij.id_identitas_jurnal = pic.id_identitas_jurnal")
	data.Joins("JOIN jurnal AS j ON j.id_jurnal = ij.id_jurnal")
	data.Where("ij.id_identitas_jurnal = ?", id_identitas_jurnal)
	data.Take(&jurnal)
	jurnal.EICJurnal = getEICJurnal(id_identitas_jurnal)
	jurnal.BidangIlmuJurnal = getBidangJurnalByIdentitasJurnal(id_identitas_jurnal)
	if data.Error != nil {
		return nil, data.Error
	}
	if jurnal.Image != "" {
		jurnal.Image = baseUrl + "/file/logo/" + jurnal.Image
	} else {
		jurnal.Image = baseUrl + "/file/logo/no_image.jpg"
	}
	return jurnal, nil
}

func getEICJurnal(id_identitas_jurnal int) entity.EICJurnal {
	var res entity.EICJurnal
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("eic")
	data.Where("eic.id_identitas_jurnal = ?", id_identitas_jurnal)
	//data.Where("eic.sts_aktif_eic = ?", "1") remark by agus 20230510 (ambil data eic yg terakhir jika ada lebih dari 1)
	data.Order("id_eic asc")
	data.Take(&res)
	if data.Error != nil {
		return entity.EICJurnal{}
	}
	return res
}

func getBidangJurnalByIdentitasJurnal(id_identitas_jurnal int) []entity.BidangIlmuJurnal {
	var res []entity.BidangIlmuJurnal
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("bidang_ilmu_jurnal as t1")
	data.Select("t1.id_jurnal, t1.id_bidang_ilmu_jurnal, t3.id_bidang_ilmu, t3.kode_bidang_ilmu, t3.bidang_ilmu, t3.level_taksonomi, t3.id_bidang_ilmu_parent, t2.id_identitas_jurnal")
	data.Joins("JOIN identitas_jurnal t2 ON t1.id_jurnal=t2.id_jurnal ")
	data.Joins("JOIN bidang_ilmu t3 ON t3.id_bidang_ilmu = t1.id_bidang_ilmu ")
	data.Where("t2.id_identitas_jurnal = ?", id_identitas_jurnal)
	data.Where("t1.sts_aktif_bidang_ilmu_jurnal = ?", "1")
	data.Where("t3.sts_aktif_bidang_ilmu = ?", "1")
	data.Find(&res)
	if data.Error != nil {
		return nil
	}
	return res
}

func DeleteJurnal(req entity.DetailJurnal) (bool, error) {
	var id_usulan_akreditasi []int64
	var id_identitas_jurnal []int64
	db := config.Con()
	defer config.CloseCon(db)
	// begin a transaction
	tx := db.Begin()
	tx.Table("identitas_jurnal as ij").Select("ij.id_identitas_jurnal").Where("ij.id_jurnal = ?", req.IdJurnal).Find(&id_identitas_jurnal)
	tx.Table("usulan_akreditasi as ua").Select("ua.id_usulan_akreditasi").Where("ua.id_identitas_jurnal IN ?", id_identitas_jurnal).Find(&id_usulan_akreditasi)

	// do some database operations in the transaction (use 'tx' from this point, not 'db')
	tx.Table("hasil_sa").Where("id_usulan_akreditasi IN ?", id_usulan_akreditasi).Delete(&entity.HasilSa{})
	tx.Table("hasil_penilaian_sa").Where("id_usulan_akreditasi IN ?", id_usulan_akreditasi).Delete(&entity.HasilPenilaianSa{})
	tx.Table("usulan_akreditasi").Where("id_identitas_jurnal IN ?", id_identitas_jurnal).Delete(&entity.UsulanAkreditasi{})
	tx.Table("identitas_jurnal as ij").Where("ij.id_jurnal = ?", req.IdJurnal).Delete(&entity.IdentitasJurnal{})
	tx.Table("jurnal").Where("id_jurnal = ?", req.IdJurnal).Delete(&entity.Jurnal{})
	tx.Table("bidang_ilmu_jurnal").Where("id_jurnal = ?", req.IdJurnal).Delete(&entity.BidangIlmuJurnal{})

	// rollback the transaction in case of error
	if err := tx.Error; err != nil {
		tx.Rollback()
		return false, err
	}

	// Or commit the transaction
	tx.Commit()

	return true, nil
}

func UpdatePICJurnal(req entity.UpdatePICDTO) error {
	db := config.Con()
	defer config.CloseCon(db)

	update := db.Table("pic").Model(&entity.PICJurnal{}).Where("id_pic = ?", req.IdPic).Updates(map[string]interface{}{"id_personal": req.IdPersonal, "tgl_updated": time.Now()})
	if update.Error != nil {
		return update.Error
	}
	return nil
}

func UpdateLogoJurnal(file []string, id_identitas_jurnal int) error {
	db := config.Con()
	defer config.CloseCon(db)
	update := db.Table("identitas_jurnal").Where("id_identitas_jurnal = ?", id_identitas_jurnal).Update("image", file[0])
	if update.Error != nil {
		return update.Error
	}
	return nil
}

func ListDaftarUsulanJurnalTerdaftar(search string, page string, row string) (helper.ResponsePagination, error) {
	var jurnal []entity.DaftarUsulanJurnal

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	usulan_akreditasi := db.Table("usulan_akreditasi as ua")
	usulan_akreditasi.Select("ua.id_identitas_jurnal,MAX(ua.id_usulan_akreditasi) id_usulan_akreditasi")
	usulan_akreditasi.Where("sts_pengajuan_usulan = ?", "1")
	usulan_akreditasi.Group("ua.id_identitas_jurnal")

	penetapan_desk_evaluasi := db.Table("penetapan_desk_evaluasi as r1")
	penetapan_desk_evaluasi.Select("r1.id_usulan_akreditasi,MAX(r1.id_penetapan_desk_evaluasi) as id_penetapan_desk_evaluasi")
	penetapan_desk_evaluasi.Group("r1.id_usulan_akreditasi")

	penugasan_penilaian_issue := db.Table("penugasan_penilaian_issue as r1")
	penugasan_penilaian_issue.Select("r1.id_usulan_akreditasi, max(r1.id_penugasan_penilaian_issue) as id_penugasan_penilaian_issue, array_agg(DISTINCT r2.nama) as nama")
	penugasan_penilaian_issue.Joins("LEFT JOIN personal as r2 ON r2.id_personal = r1.id_personal")
	penugasan_penilaian_issue.Group("r1.id_usulan_akreditasi")

	penugasan_penilaian_mgmt := db.Table("penugasan_penilaian_mgmt as r1")
	penugasan_penilaian_mgmt.Select("r1.id_usulan_akreditasi, max(r1.id_penugasan_penilaian_manajemen) as id_penugasan_penilaian_manajemen, array_agg(DISTINCT r2.nama) as nama")
	penugasan_penilaian_mgmt.Joins(" JOIN personal r2 ON r2.id_personal = r1.id_personal")
	penugasan_penilaian_mgmt.Group("r1.id_usulan_akreditasi")

	hasil_penilaian_issue := db.Table("hasil_penilaian_issue as r1")
	hasil_penilaian_issue.Select("r2.id_usulan_akreditasi, SUM(r1.nilai) nilai")
	hasil_penilaian_issue.Joins("LEFT JOIN penugasan_penilaian_issue as r2 ON r2.id_penugasan_penilaian_issue = r1.id_penugasan_penilaian_issue")
	hasil_penilaian_issue.Group("r2.id_usulan_akreditasi")

	data := db.Table("identitas_jurnal as t1")
	data.Select("t1.id_identitas_jurnal, t3.id_usulan_akreditasi, t1.nama_jurnal, t1.publisher, t1b.url_jurnal, t17.bidang_ilmu, t1b.tgl_usulan_akreditasi_terakhir, t2.id_pic as id_pic1, t2.id_personal, t3.id_pic as id_pic2, t3.tgl_created as tgl_created_usulan, t3.tgl_updated as tgl_updated_usulan, t4.tgl_created as tgl_evaluasidiri, t4.nilai_total as nilai_evaluasi, t7.id_penetapan_desk_evaluasi, t7.tgl_created as desk_created, t7.tgl_updated as desk_updated, t7.sts_hasil_desk_evaluasi, t5.tgl_created as penugasan_issue_created, tb.nama as penilai_issue, t5.tgl_pelaksanaan_tugas as penugasan_issue_terlaksana, t6.tgl_created AS penugasan_mgmt_created, tc.nama as penilai_mgmt, t6.tgl_pelaksanaan_tugas as penugasan_mgmt_terlaksana, t8.tgl_updated as penetapan_updated, t8.sts_hasil_akreditasi, t9.tgl_updated as akreditasi_updated, t9.nilai_total as nilai_akreditasi, t9.grade_akreditasi")
	data.Joins("left join jurnal as t1b on t1b.id_jurnal = t1.id_jurnal")
	data.Joins("LEFT JOIN pic as t2 ON t2.id_identitas_jurnal = t1.id_identitas_jurnal")
	data.Joins("INNER JOIN bidang_ilmu_jurnal as t16 ON t16.id_jurnal = t1.id_jurnal")
	data.Joins("INNER JOIN bidang_ilmu t17 ON t17.id_bidang_ilmu = t16.id_bidang_ilmu AND t16.sts_aktif_bidang_ilmu_jurnal = '1' AND t17.sts_aktif_bidang_ilmu = '1' ")
	data.Joins("LEFT JOIN ( ? ) as ta ON t1.id_identitas_jurnal = ta.id_identitas_jurnal", usulan_akreditasi)
	data.Joins("LEFT JOIN usulan_akreditasi t3 ON t3.id_usulan_akreditasi = ta.id_usulan_akreditasi")
	data.Joins("LEFT JOIN hasil_sa t4 ON t4.id_usulan_akreditasi = t3.id_usulan_akreditasi")
	data.Joins("LEFT JOIN ( ? ) as te on te.id_usulan_akreditasi = t3.id_usulan_akreditasi", penetapan_desk_evaluasi)
	data.Joins("LEFT JOIN penetapan_desk_evaluasi as t7 ON t7.id_penetapan_desk_evaluasi = te.id_penetapan_desk_evaluasi")
	data.Joins("LEFT JOIN ( ? ) as tb  ON tb.id_usulan_akreditasi = t3.id_usulan_akreditasi", penugasan_penilaian_issue)
	data.Joins("LEFT JOIN penugasan_penilaian_issue as t5 ON t5.id_penugasan_penilaian_issue = tb.id_penugasan_penilaian_issue")
	data.Joins("LEFT JOIN ( ? ) as tc ON tc.id_usulan_akreditasi = t3.id_usulan_akreditasi", penugasan_penilaian_mgmt)
	data.Joins("LEFT JOIN penugasan_penilaian_mgmt as t6 ON t6.id_penugasan_penilaian_manajemen = tc.id_penugasan_penilaian_manajemen")
	data.Joins("LEFT JOIN ( ? ) as td ON td.id_usulan_akreditasi = t3.id_usulan_akreditasi", hasil_penilaian_issue)
	data.Joins("left join penetapan_akreditasi as t8 on t8.id_usulan_akreditasi = t3.id_usulan_akreditasi")
	data.Joins("left join hasil_akreditasi t9 on t9.id_usulan_akreditasi = t3.id_usulan_akreditasi")
	data.Where("t3.sts_pengajuan_usulan = ?", "1")
	if search != "" {
		data.Where("lower(t1.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Find(&jurnal)

	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(jurnal)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&jurnal); err != nil {
		panic(err)
	}

	res := helper.BuildPaginationResponse(jurnal, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func ListJurnalBySK(id_sk int, search string, page string, row string) (helper.ResponsePagination, error) {
	var jurnal []entity.ListJurnalSk

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	sub_query_select := db.Table("penetapan_akreditasi as t0 ")
	sub_query_select.Select("t0.id_penetapan_akreditasi")
	sub_query_select.Where("id_usulan_akreditasi = t2.id_usulan_akreditasi")
	sub_query_select.Order("t0.tgl_created DESC")
	sub_query_select.Limit(1)

	sub_query_join := db.Table("pengajuan_usulan_akreditasi as ta")
	sub_query_join.Select("ta.id_usulan_akreditasi, MAX ( ta.tgl_pengajuan ) AS tgl_created")
	sub_query_join.Group("ta.id_usulan_akreditasi")

	data := db.Table("penetapan_akreditasi AS t0")
	data.Select("( ? ) as id_penetapan_akreditasi,  t2.id_usulan_akreditasi, t2.id_identitas_jurnal, t2.id_pic, t2.id_eic, t3.nama_jurnal, t5.url_jurnal, t3.publisher, t5.alamat, t5.city, t5.country, t5.no_telepon, t5.alamat_surel, t3.eissn, t3.pissn, t5.url_statistik_pengunjung, t5.url_contact, t5.url_editor, t3.alamat_oai, t3.doi_jurnal, t0a.tgl_created :: DATE, t0b.tgl_sk :: DATE, t4.nilai_total, t4.grade_akreditasi, t0.url_sinta, t_issue.id_issue_penetapan_akreditasi, t0.issue_terakreditasi, t_issue.volume_awal, t_issue.volume_akhir, t_issue.nomor_awal,t_issue.nomor_akhir,t_issue.tahun_awal,t_issue.tahun_akhir", sub_query_select)
	data.Joins("INNER JOIN ( ? ) as t0a ON t0.id_usulan_akreditasi = t0a.id_usulan_akreditasi", sub_query_join)
	data.Joins("LEFT JOIN sk_akreditasi t0b ON t0.id_sk_akreditasi = t0b.id_sk_akreditasi")
	data.Joins("INNER JOIN usulan_akreditasi t2 ON t0.id_usulan_akreditasi = t2.id_usulan_akreditasi")
	data.Joins("INNER JOIN pic t1 ON t1.id_identitas_jurnal = t0.id_identitas_jurnal")
	data.Joins("INNER JOIN identitas_jurnal t3 ON t3.id_identitas_jurnal = t0.id_identitas_jurnal")
	data.Joins("INNER JOIN jurnal t5 ON t5.id_jurnal = t3.id_jurnal")
	data.Joins("INNER JOIN hasil_akreditasi t4 ON t4.id_hasil_akreditasi = t0.id_hasil_akreditasi")
	data.Joins("LEFT JOIN issue_penetapan_akreditasi t_issue ON t_issue.id_issue_penetapan_akreditasi = t0.id_issue_penetapan_akreditasi")
	data.Where("t0.id_sk_akreditasi = ?", id_sk)
	data.Where("t3.sts_aktif_identitas_jurnal = ?", "1")
	data.Where("t4.nilai_total >= ?", 30.00)
	if search != "" {
		data.Where("lower(t3.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Group("t2.id_usulan_akreditasi, t2.id_identitas_jurnal, t2.id_pic, t2.id_eic, t3.nama_jurnal, t5.url_jurnal, t3.publisher, t5.alamat, t5.city, t5.country, t5.no_telepon, t5.alamat_surel, t3.eissn, t3.pissn, t5.url_statistik_pengunjung, t5.url_contact, t5.url_editor, t3.alamat_oai, t3.doi_jurnal, t0a.tgl_created :: DATE, t0b.tgl_sk :: DATE, t4.nilai_total, t4.grade_akreditasi, t0.url_sinta, t0.issue_terakreditasi, t0.tgl_created, t_issue.id_issue_penetapan_akreditasi")
	data.Order("t0.tgl_created")
	data.Find(&jurnal)

	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(jurnal)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&jurnal); err != nil {
		panic(err)
	}

	res := helper.BuildPaginationResponse(jurnal, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func countJurnalBySK(id_sk int64) int64 {
	var res int64

	db := config.Con()
	defer config.CloseCon(db)

	sub_query_select := db.Table("penetapan_akreditasi as t0 ")
	sub_query_select.Select("t0.id_penetapan_akreditasi")
	sub_query_select.Where("id_usulan_akreditasi = t2.id_usulan_akreditasi")
	sub_query_select.Order("t0.tgl_created DESC")
	sub_query_select.Limit(1)

	sub_query_join := db.Table("pengajuan_usulan_akreditasi as ta")
	sub_query_join.Select("ta.id_usulan_akreditasi, MAX ( ta.tgl_pengajuan ) AS tgl_created")
	sub_query_join.Group("ta.id_usulan_akreditasi")

	data := db.Table("penetapan_akreditasi AS t0")
	data.Select("( ? ) as id_penetapan_akreditasi,  t2.id_usulan_akreditasi, t2.id_identitas_jurnal, t2.id_pic, t2.id_eic, t3.nama_jurnal, t5.url_jurnal, t3.publisher, t5.alamat, t5.city, t5.country, t5.no_telepon, t5.alamat_surel, t3.eissn, t3.pissn, t5.url_statistik_pengunjung, t5.url_contact, t5.url_editor, t3.alamat_oai, t3.doi_jurnal, t0a.tgl_created :: DATE, t0b.tgl_sk :: DATE, t4.nilai_total, t4.grade_akreditasi, t0.url_sinta, t_issue.id_issue_penetapan_akreditasi, t0.issue_terakreditasi, t_issue.volume_awal, t_issue.volume_akhir, t_issue.nomor_awal,t_issue.nomor_akhir,t_issue.tahun_awal,t_issue.tahun_akhir", sub_query_select)
	data.Joins("INNER JOIN ( ? ) as t0a ON t0.id_usulan_akreditasi = t0a.id_usulan_akreditasi", sub_query_join)
	data.Joins("LEFT JOIN sk_akreditasi t0b ON t0.id_sk_akreditasi = t0b.id_sk_akreditasi")
	data.Joins("INNER JOIN usulan_akreditasi t2 ON t0.id_usulan_akreditasi = t2.id_usulan_akreditasi")
	data.Joins("INNER JOIN pic t1 ON t1.id_identitas_jurnal = t0.id_identitas_jurnal")
	data.Joins("INNER JOIN identitas_jurnal t3 ON t3.id_identitas_jurnal = t0.id_identitas_jurnal")
	data.Joins("INNER JOIN jurnal t5 ON t5.id_jurnal = t3.id_jurnal")
	data.Joins("INNER JOIN hasil_akreditasi t4 ON t4.id_hasil_akreditasi = t0.id_hasil_akreditasi")
	data.Joins("LEFT JOIN issue_penetapan_akreditasi t_issue ON t_issue.id_issue_penetapan_akreditasi = t0.id_issue_penetapan_akreditasi")
	data.Where("t0.id_sk_akreditasi = ?", id_sk)
	data.Where("t3.sts_aktif_identitas_jurnal = ?", "1")
	data.Where("t4.nilai_total >= ?", 30.00)
	data.Group("t2.id_usulan_akreditasi, t2.id_identitas_jurnal, t2.id_pic, t2.id_eic, t3.nama_jurnal, t5.url_jurnal, t3.publisher, t5.alamat, t5.city, t5.country, t5.no_telepon, t5.alamat_surel, t3.eissn, t3.pissn, t5.url_statistik_pengunjung, t5.url_contact, t5.url_editor, t3.alamat_oai, t3.doi_jurnal, t0a.tgl_created :: DATE, t0b.tgl_sk :: DATE, t4.nilai_total, t4.grade_akreditasi, t0.url_sinta, t0.issue_terakreditasi, t0.tgl_created, t_issue.id_issue_penetapan_akreditasi")
	data.Order("t0.tgl_created")
	data.Count(&res)
	return res
}

func UpdateSKJurnal(req entity.UpdateSKJurnalDTO) (interface{}, error) {
	var issue_penetapan_akreditasi entity.IssuePenetapanAkreditasi
	var count_penetapan_akreditasi int64
	var count_issue_penetapan_akreditasi int64
	var id_issue_penetapan_akreditasi string
	var issue_akreditasi string

	db := config.Con()
	defer config.CloseCon(db)

	db.Table("penetapan_akreditasi").Where("id_penetapan_akreditasi", req.IDPenetapan).Where("id_sk_akreditasi IS NOT NULL").Count(&count_penetapan_akreditasi)
	if count_penetapan_akreditasi > 0 {
		db.Table("issue_penetapan_akreditasi as t1").Where("t1.id_penetapan_akreditasi = ?", req.IDPenetapan).Count(&count_issue_penetapan_akreditasi)
		if count_issue_penetapan_akreditasi > 0 {
			db.Table("issue_penetapan_akreditasi as t_get").Select("t_get.id_issue_penetapan_akreditasi").Joins("INNER JOIN penetapan_akreditasi as tp ON t_get.id_issue_penetapan_akreditasi = tp.id_issue_penetapan_akreditasi").Where("tp.id_penetapan_akreditasi = ?", req.IDPenetapan).Scan(&id_issue_penetapan_akreditasi)
			db.Table("issue_penetapan_akreditasi").Where("id_issue_penetapan_akreditasi = ?", id_issue_penetapan_akreditasi).Updates(map[string]interface{}{"volume_awal": req.VolumeAwal, "volume_akhir": req.VolumeAkhir, "nomor_awal": req.NomorAwal, "nomor_akhir": req.NomorAkhir, "tahun_awal": req.TahunAwal, "tahun_akhir": req.TahunAkhir, "tgl_updated": time.Now()})
			db.Table("issue_penetapan_akreditasi as t_issue").Select("CONCAT('Volume ', t_issue.volume_awal, ' Nomor ', t_issue.nomor_awal, ' Tahun ', t_issue.tahun_awal,' sampai ','Volume ', t_issue.volume_akhir, ' Nomor ', t_issue.nomor_akhir, ' Tahun ', t_issue.tahun_akhir) as issue_akreditasi").Where("t_issue.id_issue_penetapan_akreditasi = ?", id_issue_penetapan_akreditasi).Scan(&issue_akreditasi)
			db.Table("penetapan_akreditasi").Where("id_penetapan_akreditasi = ?", req.IDPenetapan).Where("id_sk_akreditasi IS NOT NULL").Updates(map[string]interface{}{"url_sinta": req.UrlSinta, "id_issue_penetapan_akreditasi": id_issue_penetapan_akreditasi, "issue_terakreditasi": issue_akreditasi, "tgl_updated": time.Now()})
		} else {
			uuid := uuid.New()
			issue_penetapan_akreditasi.IDIssuePenetapanAkreditasi = uuid.String()
			issue_penetapan_akreditasi.VolumeAwal = req.VolumeAwal
			issue_penetapan_akreditasi.VolumeAkhir = req.VolumeAkhir
			issue_penetapan_akreditasi.NomorAwal = req.NomorAwal
			issue_penetapan_akreditasi.NomorAkhir = req.NomorAkhir
			issue_penetapan_akreditasi.TahunAwal = req.TahunAwal
			issue_penetapan_akreditasi.TahunAkhir = req.TahunAkhir
			issue_penetapan_akreditasi.IDPenetapanAkreditasi = req.IDPenetapan
			issue_penetapan_akreditasi.TglCreated = time.Now()
			db.Table("issue_penetapan_akreditasi").Create(&issue_penetapan_akreditasi)
			db.Table("issue_penetapan_akreditasi as t_issue").Select("CONCAT('Volume ', t_issue.volume_awal, ' Nomor ', t_issue.nomor_awal, ' Tahun ', t_issue.tahun_awal,' sampai ','Volume ', t_issue.volume_akhir, ' Nomor ', t_issue.nomor_akhir, ' Tahun ', t_issue.tahun_akhir) as issue_akreditasi").Where("t_issue.id_issue_penetapan_akreditasi = ?", uuid.String()).Scan(&issue_akreditasi)
			db.Table("penetapan_akreditasi").Where("id_penetapan_akreditasi = ?", req.IDPenetapan).Where("id_sk_akreditasi IS NOT NULL").Updates(map[string]interface{}{"url_sinta": req.UrlSinta, "id_issue_penetapan_akreditasi": uuid.String(), "issue_terakreditasi": issue_akreditasi, "tgl_updated": time.Now()})
		}
	}
	return count_issue_penetapan_akreditasi, nil
}

func ListJurnalTerSK(search string, page string, row string, id_sk string, id_bidang_ilmu string, grade string) (helper.ResponsePagination, error) {
	var hasil_akreditasi []entity.HasilAkreditasi

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("hasil_akreditasi as t1")
	data.Distinct("t1.id_usulan_akreditasi, t1.tgl_updated :: date AS tgl_penetapan_akreditasi, t5.url_statistik_pengunjung, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society, t5.nama_awal_jurnal,t5.tgl_pembuatan, t5.url_jurnal, t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat, t5.no_telepon, t5.alamat_surel, t4.alamat_oai, t4.doi_jurnal, t5.tgl_akhir_terakreditasi, t7.nama AS nama_pic, t7.nama_institusi, t8.nama_eic, t8.alamat_surel AS alamat_surel_eic, t1.nilai_total, t1.grade_akreditasi, t10.id_sk_akreditasi, t10.sts_published, t11.id_bidang_ilmu as id_bidang_ilmu_jurnal, t12.bidang_ilmu as bidang_ilmu_jurnal")
	data.Joins("INNER JOIN usulan_akreditasi AS t3 ON t3.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("INNER JOIN identitas_jurnal AS t4 ON t4.id_identitas_jurnal = t3.id_identitas_jurnal")
	data.Joins("INNER JOIN jurnal AS t5 ON t5.id_jurnal = t4.id_jurnal")
	data.Joins("INNER JOIN pic AS t6 ON t6.id_pic = t3.id_pic")
	data.Joins("INNER JOIN personal AS t7 ON t7.id_personal = t6.id_personal")
	data.Joins("INNER JOIN eic AS t8 ON t8.id_eic = t3.id_eic")
	data.Joins("LEFT JOIN penetapan_akreditasi AS t9 ON t9.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("LEFT JOIN sk_akreditasi AS t10 ON t10.id_sk_akreditasi = t9.id_sk_akreditasi")
	data.Joins("LEFT JOIN bidang_ilmu_jurnal as t11 on t11.id_jurnal = t5.id_jurnal")
	data.Joins("LEFT JOIN bidang_ilmu as t12 on t12.id_bidang_ilmu = t11.id_bidang_ilmu")
	data.Where("t4.nama_jurnal IS NOT NULL")
	if id_sk != "" {
		data.Where("t10.id_sk_akreditasi = ?", id_sk)
	}
	if grade != "" {
		data.Where("t1.grade_akreditasi = ?", grade)
	}
	if id_bidang_ilmu != "" {
		data.Where("t11.id_bidang_ilmu = ?", id_bidang_ilmu)
	}
	if search != "" {
		data.Where("lower(t4.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Order("tgl_penetapan_akreditasi DESC")
	data.Find(&hasil_akreditasi)

	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(hasil_akreditasi)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&hasil_akreditasi); err != nil {
		panic(err)
	}

	res := helper.BuildPaginationResponse(hasil_akreditasi, view)
	res.PerPage = rows
	res.Total = total

	return res, nil
}

func PrintSKPengelolaJurnals(id_identitas_jurnal int) (entity.PrintSKJurnal, error) {
	var print_sk_jurnal entity.PrintSKJurnal

	db := config.Con()
	defer config.CloseCon(db)

	query := db.Raw("SELECT * FROM public.z_arjuna_print_sk_pengelola_jurnal( ( ? ) )", id_identitas_jurnal)

	data := db.Table("( ? ) as temp", query)
	data.Scan(&print_sk_jurnal)
	if data.Error != nil {
		return entity.PrintSKJurnal{}, data.Error
	}
	return print_sk_jurnal, nil
}

func PrintSKPengelolaJurnal(id_identitas_jurnal int) (entity.PrintSKJurnal, error) {
	db := config.Con()
	defer config.CloseCon(db)

	query := db.Raw("SELECT * FROM public.z_arjuna_print_sk_pengelola_jurnal( ( ? ) )", id_identitas_jurnal)
	if query.Error != nil {
		return entity.PrintSKJurnal{}, query.Error
	}

	var data entity.PrintSKJurnal
	dataQuery := db.Table("( ? ) as temp", query)
	dataQuery.Scan(&data)
	if dataQuery.Error != nil {
		return entity.PrintSKJurnal{}, query.Error
	}

	base64Encoding := "data:image/png;base64,"

	// generate qr code
	qrData := strconv.Itoa(id_identitas_jurnal)
	qrCode, err := qrcode.New(data.PQRCode, qrcode.Medium)
	if err != nil {
		return entity.PrintSKJurnal{}, err
	}

	imgQr, err := qrCode.PNG(256)
	if err != nil {
		return entity.PrintSKJurnal{}, err
	}
	err = ioutil.WriteFile("files/qrcode/"+qrData+".png", imgQr, 0644)
	if err != nil {
		return entity.PrintSKJurnal{}, err
	}

	encodedQRCode := base64Encoding + base64.StdEncoding.EncodeToString(imgQr)

	// generate barcode
	writer := oned.NewCode128Writer()

	bm, _ := writer.Encode(data.PBarcode, gozxing.BarcodeFormat_CODE_128, 140, 50, nil)

	img := image.NewNRGBA(image.Rect(0, 0, bm.GetWidth(), bm.GetHeight()))

	draw.Draw(img, img.Bounds(), &image.Uniform{color.Transparent}, image.Point{}, draw.Src)

	for y := 0; y < bm.GetHeight(); y++ {
		for x := 0; x < bm.GetWidth(); x++ {
			if bm.Get(x, y) {
				img.Set(x, y, color.Black)
			}
		}
	}

	var buf bytes.Buffer
	png.Encode(&buf, img)

	encodedBarcode := base64Encoding + base64.StdEncoding.EncodeToString(buf.Bytes())
	file, _ := os.Create("files/barcode/" + qrData + "-barcode" + ".png")
	defer file.Close()
	png.Encode(file, img)

	// get ttd img
	var ttd string

	if data.TtdImg == "" {
		ttd = "-"

		data = entity.PrintSKJurnal{
			PQRCode:             encodedQRCode,
			PBarcode:            encodedBarcode,
			NOSk:                data.NOSk,
			JudulSK:             data.JudulSK,
			NamaJurnal:          data.NamaJurnal,
			Eissn:               data.Eissn,
			Publisher:           data.Publisher,
			GradeAkreditasi:     data.GradeAkreditasi,
			IssueTerakreditasi:  data.IssueTerakreditasi,
			TanggalPenetapan:    data.TanggalPenetapan,
			NamaPenandaTanganSK: data.NamaPenandaTanganSK,
			NipPenandaTanganSK:  data.NipPenandaTanganSK,
			TglSK:               data.TglSK,
			Periode:             data.Periode,
			Tahun:               data.Tahun,
			TtdImg:              ttd,
			StatusSK:            data.StatusSK,
		}
	} else {
		ttd = data.TtdImg

		path, err := filepath.Abs(filepath.Join("files/certificate/signatures/" + ttd))
		if err != nil {
			return entity.PrintSKJurnal{}, err
		}

		ttdImg, err := ioutil.ReadFile(path)
		if err != nil {
			log.Fatal(err)
		}
		encodedTtdImg := base64Encoding + base64.StdEncoding.EncodeToString(ttdImg)

		data = entity.PrintSKJurnal{
			PQRCode:             encodedQRCode,
			PBarcode:            encodedBarcode,
			NOSk:                data.NOSk,
			JudulSK:             data.JudulSK,
			NamaJurnal:          data.NamaJurnal,
			Eissn:               data.Eissn,
			Publisher:           data.Publisher,
			GradeAkreditasi:     data.GradeAkreditasi,
			IssueTerakreditasi:  data.IssueTerakreditasi,
			TanggalPenetapan:    data.TanggalPenetapan,
			NamaPenandaTanganSK: data.NamaPenandaTanganSK,
			NipPenandaTanganSK:  data.NipPenandaTanganSK,
			TglSK:               data.TglSK,
			Periode:             data.Periode,
			Tahun:               data.Tahun,
			TtdImg:              encodedTtdImg,
			StatusSK:            data.StatusSK,
		}
	}
	return data, nil
}

func ListHasilJurnalAkreditasiByPic(idPersonal, page, row string) (helper.ResponsePagination, error) {
	db := config.Con()
	defer config.CloseCon(db)

	id_personal, _ := strconv.Atoi(idPersonal)
	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	query := db.Raw("SELECT id_usulan_akreditasi, id_identitas_jurnal, id_pic, id_eic, nama_jurnal, eissn, pissn, url_statistik_pengunjung, url_jurnal, url_contact, editor, alamat_oai, doi_jurnal, tgl_diusulkan, nilai_total_sa, grade_akreditasi_sa, nilai_total_akreditasi, grade_akreditasi, tgl_pencatatan_sk, sts_hasil_akreditasi, hasil_desk_evaluasi, tgl_penetapan_deskevaluasi, komentar_penolakan, jml_record  FROM public.arjuna_list_hasil_akreditasi_by_pic( ( ? ), ( ? ), ( ? ), ( ? ) )", id_personal, "1", pages, rows)

	var jurnalPic []entity.JurnalPic
	data := db.Table("( ? ) as temp", query)
	data.Scan(&jurnalPic)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	var total int
	if len(jurnalPic) > 0 {
		total = jurnalPic[0].JmlRecord
	} else {
		total = 0
	}
	res := helper.ResponsePagination{
		Data:        jurnalPic,
		CurrentPage: pages,
		LastPage:    ((total / rows) + 1),
		PerPage:     rows,
		Total:       total,
	}
	return res, nil
}
