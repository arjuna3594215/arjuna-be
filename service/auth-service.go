package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"math/rand"
	"time"
)

func Login(logindto entity.LoginDTO, baseURL string) interface{} {
	var datalogin entity.Login
	db := config.Con()
	defer config.CloseCon(db)
	publicpass := config.EncryptPassword("sahabatarjuna")
	encryptpass := config.EncryptPassword(logindto.Password)
	data := db.Table("pengguna").Select("pengguna.*, p.nama, p.image")
	data.Joins("inner join kewenangan_pengguna as kp on kp.id_pengguna = pengguna.id_pengguna")
	data.Joins("inner join personal as p on p.id_personal = pengguna.id_personal")
	data.Where("pengguna.username = ?", logindto.Username)
	if publicpass != encryptpass {
		data.Where("pengguna.password = md5(?)", logindto.Password)
	}
	data.Where("kp.sts_aktif_kewenangan_pengguna = ?", "1")
	data.Where("pengguna.sts_aktif_pengguna = ?", "1")
	data.Where("p.sts_aktif_personal = ?", "1")
	data.Take(&datalogin)

	datalogin.Kewenangan = getKewenanganByPengguna(datalogin.IDPengguna)
	if datalogin.Image != "" {
		datalogin.Image = baseURL + "/file/photos/" + datalogin.Image
	}

	if data.Error != nil {
		return nil
	}

	return datalogin
}

func getKewenanganByPengguna(id_pengguna int64) []entity.ListKewenangan {
	var kewenangan []entity.ListKewenangan
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("kewenangan_pengguna as kp").Select("k.kewenangan, k.id_kewenangan")
	data.Joins("inner join kewenangan as k on k.id_kewenangan = kp.id_kewenangan")
	data.Where("kp.id_pengguna = ?", id_pengguna)
	data.Find(&kewenangan)

	for i, v := range kewenangan {
		kewenangan[i].Permission = getPermission(int(v.IDKewenangan))
	}

	return kewenangan
}

func getPermission(id_kewenangan int) []string {
	var data []string
	var permission []entity.Permission
	db := config.Con()
	defer config.CloseCon(db)

	query := db.Table("kewenangan_has_permissions").Select("p.name")
	query.Joins("join permissions as p on p.id = kewenangan_has_permissions.id_permission")
	query.Where("id_kewenangan = ?", id_kewenangan)
	query.Find(&permission)

	for _, v := range permission {
		data = append(data, v.Name)
	}
	return data
}

func ForgotPassword(personal entity.Personal, pengguna entity.Pengguna) (status bool, err error) {
	var DataMailForgotPass entity.DataMailForgotPass
	db := config.Con()
	defer config.CloseCon(db)
	NewPass := String(10)

	DataMailForgotPass.Name = personal.Nama
	DataMailForgotPass.Password = NewPass
	DataMailForgotPass.Username = pengguna.Username
	DataMailForgotPass.Email = personal.Email

	sendmailError := helper.SendForgotPasswordMail(DataMailForgotPass)
	if sendmailError != nil {
		return false, sendmailError
	}

	pengguna.Password = config.EncryptPassword(NewPass)
	_, errUpdatePengguna := UpdatePengguna(pengguna)
	if errUpdatePengguna != nil {
		return false, errUpdatePengguna
	}

	return true, nil
}

const charset = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var seededRand *rand.Rand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

func StringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func String(length int) string {
	return StringWithCharset(length, charset)
}
