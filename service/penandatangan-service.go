package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"os"
	"strconv"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/vcraescu/go-paginator/v2"
	"github.com/vcraescu/go-paginator/v2/adapter"
	"github.com/vcraescu/go-paginator/v2/view"
)

func ListPenandatangan(search string, page string, row string, baseUrl string) (helper.ResponsePagination, error) {
	var penandatangan []entity.Penandatangan

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penanda_tangan_sk")
	data.Select("*")
	if search != "" {
		data.Where("lower(nama_penanda_tangan_sk) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Order("id_penanda_tangan_sk asc")
	data.Find(&penandatangan)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(penandatangan)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&penandatangan); err != nil {
		panic(err)
	}

	for i, v := range penandatangan {
		path := baseUrl + "/file/certificate/signatures/"
		if v.File != "" {
			penandatangan[i].ImagePath = path + v.File
		}
	}

	res := helper.BuildPaginationResponse(penandatangan, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func InsertPenandatangan(req entity.PenandatanganDTO, c *fiber.Ctx) (entity.Penandatangan, error) {
	var penandatangan entity.Penandatangan
	db := config.Con()
	defer config.CloseCon(db)

	penandatangan.NamaPenandaTanganSK = req.Nama
	penandatangan.NipPenandaTanganSK = req.Nip
	penandatangan.StatusAktifPenandaTangan = "0"
	if req.File != nil {
		filename, _ := helper.UploadFile("./files/certificate/signatures", req.File, c)
		penandatangan.File = filename
	}

	SavePenandatangan := db.Table("penanda_tangan_sk").Create(&penandatangan)

	if SavePenandatangan.Error != nil {
		return entity.Penandatangan{}, SavePenandatangan.Error
	}

	return penandatangan, nil
}

func ShowPenandatangan(id string, baseUrl string) (interface{}, error) {
	var penandatangan entity.Penandatangan
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penanda_tangan_sk")
	data.Select("*")
	data.Where("id_penanda_tangan_sk = ?", id)
	data.Take(&penandatangan)
	if data.Error != nil {
		return nil, data.Error
	}

	path := baseUrl + "/file/certificate/signatures/"
	if penandatangan.File != "" {
		penandatangan.ImagePath = path + penandatangan.File
	}
	return penandatangan, nil
}

func UpdatePenandatangan(req entity.PenandatanganDTO, old entity.Penandatangan, id string, c *fiber.Ctx) (entity.Penandatangan, error) {
	var penandatangan entity.Penandatangan
	db := config.Con()
	defer config.CloseCon(db)

	penandatangan.IDPenandaTanganSK = id
	penandatangan.NamaPenandaTanganSK = req.Nama
	penandatangan.NipPenandaTanganSK = req.Nip
	penandatangan.StatusAktifPenandaTangan = old.StatusAktifPenandaTangan
	if req.File != nil {
		os.Remove("./files/certificate/signatures/" + old.File)
		filename, _ := helper.UploadFile("./files/certificate/signatures", req.File, c)
		penandatangan.File = filename
	} else {
		penandatangan.File = old.File
	}

	savePenandatangan := db.Table("penanda_tangan_sk").Where("id_penanda_tangan_sk = ?", id).Save(&penandatangan)

	if savePenandatangan.Error != nil {
		return entity.Penandatangan{}, savePenandatangan.Error
	}

	return penandatangan, nil
}

func UpdateStatusPenandatangan(req entity.PenandatanganStatusDTO, old entity.Penandatangan, id string) (entity.Penandatangan, error) {
	db := config.Con()
	defer config.CloseCon(db)

	if req.Status == "0" {
		updateStatus := db.Table("penanda_tangan_sk").Where("id_penanda_tangan_sk = ?", id).Update("status_aktif_penanda_tangan", req.Status)
		if updateStatus.Error != nil {
			return entity.Penandatangan{}, updateStatus.Error
		}
	} else {
		db.Table("penanda_tangan_sk").Where("status_aktif_penanda_tangan = ?", "1").Update("status_aktif_penanda_tangan", "0")
		updateStatus := db.Table("penanda_tangan_sk").Where("id_penanda_tangan_sk = ?", id).Update("status_aktif_penanda_tangan", req.Status)
		if updateStatus.Error != nil {
			return entity.Penandatangan{}, updateStatus.Error
		}
	}

	old.StatusAktifPenandaTangan = req.Status
	return old, nil
}
