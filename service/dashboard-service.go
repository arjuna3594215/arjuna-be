package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"errors"
	"fmt"
	"log"
	"strconv"
)

func DashboardPengusulCount(id_personal int) (interface{}, error) {
	var data entity.DasboardPengusulCount

	jurnal_aktif, err := getTotalJurnalAktifPengusul(int64(id_personal))
	jurnal_terakreditasi, err := getTotalJurnalTerakreditasiPengusul(int64(id_personal))
	usulan_jurnal, err := getTotalUsulanJurnalPengusul(int64(id_personal))
	if err != nil {
		return nil, err
	}
	data.TotalJurnalAktif = jurnal_aktif
	data.TotalJurnalTerakreditasi = jurnal_terakreditasi
	data.TotalUsulanJurnal = usulan_jurnal
	return data, nil
}

func getTotalJurnalAktifPengusul(id_personal int64) (int64, error) {
	var res int64
	db := config.Con()
	defer config.CloseCon(db)

	jurnal_aktif := db.Table("jurnal AS j")
	jurnal_aktif.Select("j.id_jurnal")
	jurnal_aktif.Joins("JOIN identitas_jurnal AS ij ON ij.id_jurnal = j.id_jurnal")
	jurnal_aktif.Joins("JOIN pic ON pic.id_identitas_jurnal = ij.id_identitas_jurnal ")
	jurnal_aktif.Where("pic.sts_aktif_pic = ?", "1")
	jurnal_aktif.Where("ij.sts_aktif_identitas_jurnal = ?", "1")
	jurnal_aktif.Where("pic.id_personal = ?", id_personal)
	jurnal_aktif.Count(&res)

	if jurnal_aktif.Error != nil {
		return 0, jurnal_aktif.Error
	}
	return res, nil
}

func getTotalJurnalTerakreditasiPengusul(id_personal int64) (int64, error) {
	var res int64
	db := config.Con()
	defer config.CloseCon(db)

	jurnal_terakreditasi := db.Table("penetapan_akreditasi AS pa")
	jurnal_terakreditasi.Select("pa.id_penetapan_akreditasi")
	jurnal_terakreditasi.Joins("JOIN usulan_akreditasi AS ua ON ua.id_identitas_jurnal = pa.id_identitas_jurnal")
	jurnal_terakreditasi.Joins("JOIN hasil_akreditasi AS ha ON ha.id_usulan_akreditasi = ua.id_usulan_akreditasi")
	jurnal_terakreditasi.Joins("JOIN identitas_jurnal AS ij ON ij.id_identitas_jurnal = pa.id_identitas_jurnal")
	jurnal_terakreditasi.Joins("JOIN pic ON pic.id_identitas_jurnal = ij.id_identitas_jurnal")
	jurnal_terakreditasi.Joins("JOIN jurnal AS j ON j.id_jurnal = ij.id_jurnal")
	jurnal_terakreditasi.Where("pic.sts_aktif_pic = ?", "1")
	jurnal_terakreditasi.Where("ij.sts_aktif_identitas_jurnal = ?", "1")
	jurnal_terakreditasi.Where("pa.sts_hasil_akreditasi = ?", "1")
	jurnal_terakreditasi.Where("ha.grade_akreditasi <> ?", "T")
	jurnal_terakreditasi.Where("pic.id_personal = ?", id_personal)
	jurnal_terakreditasi.Count(&res)

	if jurnal_terakreditasi.Error != nil {
		return 0, jurnal_terakreditasi.Error
	}
	return res, nil
}

func getTotalUsulanJurnalPengusul(id_personal int64) (int64, error) {
	var res int64
	db := config.Con()
	defer config.CloseCon(db)

	usulan_jurnal := db.Table("progres_usulan_akreditasi as pua")
	usulan_jurnal.Joins("join progres on progres.id_progres = pua.id_progres")
	usulan_jurnal.Joins("join usulan_akreditasi as ua on ua.id_usulan_akreditasi = pua.id_usulan_akreditasi")
	usulan_jurnal.Joins("join identitas_jurnal as ij on ij.id_identitas_jurnal = ua.id_identitas_jurnal")
	usulan_jurnal.Joins("JOIN pic ON pic.id_identitas_jurnal = ij.id_identitas_jurnal")
	usulan_jurnal.Joins("JOIN jurnal AS j ON j.id_jurnal = ij.id_jurnal")
	usulan_jurnal.Where("pic.sts_aktif_pic = ?", "1")
	usulan_jurnal.Where("ij.sts_aktif_identitas_jurnal = ?", "1")
	usulan_jurnal.Where("pic.id_personal = ?", id_personal)
	usulan_jurnal.Count(&res)

	if usulan_jurnal.Error != nil {
		return 0, usulan_jurnal.Error
	}
	return res, nil
}

func DashboardPengusulChart(types string, id_personal int64) (interface{}, error) {
	var res interface{}
	if types == "1" {
		res = barChart(getRekapJurnalAktifPertahun(id_personal), "Rekap Jurnal Aktif Pertahun")
	} else if types == "2" {
		res = barChart(getRekapJurnalTerakreditasiPertahun(id_personal), "Rekap Jurnal Terakreditasi Pertahun")
	}
	return res, nil
}

func DashboardPenilaiCount(id_personal int, penilai string) (interface{}, error) {
	var data entity.DashboardPenilaiCount

	db := config.Con()
	defer config.CloseCon(db)

	if penilai == "management" {
		query := db.Raw("SELECT * from arjuna_rekap_dashboard_penilai_manajemen1(?)", id_personal).Scan(&data)
		if query.Error != nil {
			return nil, query.Error
		}
	} else if penilai == "evaluator" {
		query := db.Raw("SELECT * from arjuna_rekap_dashboard_penilai_evaluator1(?)", id_personal).Scan(&data)
		if query.Error != nil {
			return nil, query.Error
		}
	} else if penilai == "issue" {
		query := db.Raw("SELECT * from arjuna_rekap_dashboard_penilai_konten1(?)", id_personal).Scan(&data)
		if query.Error != nil {
			return nil, query.Error
		}
	}
	return data, nil
}

func DashboardPenilaiLineChart(id_personal int, penilai string) (interface{}, error) {
	var res interface{}
	var data_chart []entity.DataChart

	db := config.Con()
	defer config.CloseCon(db)

	if penilai == "management" {
		query := db.Raw("SELECT thn_usulan_penugasan as label, total as count FROM arjuna_rekap_dashboard_penilai_manajemen3(?)", id_personal).Scan(&data_chart)
		if query.Error != nil {
			return nil, query.Error
		}
		res = barChart(data_chart, "Rekapitulasi Penilaian Tahunan")
	} else if penilai == "evaluator" {
		query := db.Raw("SELECT thn_usulan_penugasan as label, total as count FROM arjuna_rekap_dashboard_penilai_evaluator3(?)", id_personal).Scan(&data_chart)
		if query.Error != nil {
			return nil, query.Error
		}
		res = barChart(data_chart, "Rekapitulasi Penilaian Tahunan")
	} else if penilai == "issue" {
		query := db.Raw("SELECT thn_usulan_penugasan as label, total as count FROM arjuna_rekap_dashboard_penilai_konten3(?)", id_personal).Scan(&data_chart)
		if query.Error != nil {
			return nil, query.Error
		}
		res = barChart(data_chart, "Rekapitulasi Penilaian Tahunan")
	}
	return res, nil
}

func DashboardPenilaiBarChart(id_personal int, penilai string) (interface{}, error) {
	var res interface{}
	var data_chart []entity.DataChart

	db := config.Con()
	defer config.CloseCon(db)

	if penilai == "management" {
		query := db.Raw("SELECT bidang_ilmu as label, total as count FROM public.arjuna_rekap_dashboard_penilai_manajemen2(( ? ), 'id')", id_personal).Scan(&data_chart)
		if query.Error != nil {
			return nil, query.Error
		}
		res = barChart(data_chart, "Rekapitulasi Penilaian Tahunan")
	} else if penilai == "evaluator" {
		query := db.Raw("SELECT bidang_ilmu as label, total as count FROM public.arjuna_rekap_dashboard_penilai_evaluator2(( ? ), 'id')", id_personal).Scan(&data_chart)
		if query.Error != nil {
			return nil, query.Error
		}
		res = barChart(data_chart, "Rekapitulasi Penilaian Tahunan")
	} else if penilai == "issue" {
		query := db.Raw("SELECT bidang_ilmu as label, total as count FROM public.arjuna_rekap_dashboard_penilai_konten2(( ? ), 'id')", id_personal).Scan(&data_chart)
		if query.Error != nil {
			return nil, query.Error
		}
		res = barChart(data_chart, "Rekapitulasi Penilaian Tahunan")
	}
	return res, nil
}

func DashboardDistributorCount() (interface{}, error) {
	var data entity.DashboardDistributorCount

	db := config.Con()
	defer config.CloseCon(db)

	query := db.Raw("SELECT * FROM arjuna_rekap_all_proses_akreditasi_distributor()").Scan(&data)
	if query.Error != nil {
		return nil, query.Error
	}

	return data, nil
}

func DashboardDistributorBarChartSkTerakhir() (interface{}, error) {
	var res interface{}
	var data_chart []entity.DataChart

	db := config.Con()
	defer config.CloseCon(db)

	query := db.Raw("SELECT sumbux as label, sumbuy as count FROM public.arjuna_rekap_grafik_sk_distributor()").Scan(&data_chart)
	if query.Error != nil {
		return nil, query.Error
	}
	res = barChart(data_chart, "10 SK Terakhir")

	return res, nil
}

func DashboardDashboardDonutChartSebaranJurnal(types string) (interface{}, error) {
	var res interface{}
	var data_chart []entity.DataChart

	db := config.Con()
	defer config.CloseCon(db)

	if types == "1" {
		query := db.Raw("select INITCAP(lower(publisher)) as label, count(*) as count from public.identitas_jurnal where id_identitas_jurnal in (select tb1.id_identitas_jurnal from public.usulan_akreditasi tb1 inner join public.identitas_jurnal tb2 on (tb1.id_identitas_jurnal=tb2.id_identitas_jurnal) where tb2.sts_aktif_identitas_jurnal = '1' and tb2.publisher <> '' group by tb1.id_identitas_jurnal) group by INITCAP(lower(publisher)) order by count(*) desc limit 5").Scan(&data_chart)
		if query.Error != nil {
			return nil, query.Error
		}
		res = barChart(data_chart, "Usulan Jurnal")
	} else if types == "2" {
		query := db.Raw("select initcap(lower(tb2.publisher)) as label, count(*) as count from (select ij.id_identitas_jurnal,pa.sts_hasil_akreditasi from public.penetapan_akreditasi pa inner join public.identitas_jurnal ij on (pa.id_identitas_jurnal=ij.id_identitas_jurnal) where pa.sts_hasil_akreditasi = '1'	group by ij.id_identitas_jurnal, pa.sts_hasil_akreditasi order by ij.id_identitas_jurnal asc) tb1 inner join public.identitas_jurnal tb2 on tb1.id_identitas_jurnal = tb2.id_identitas_jurnal where tb1.sts_hasil_akreditasi = '1' and tb2.sts_aktif_identitas_jurnal = '1' group by lower(tb2.publisher) order by count(*) desc limit 5").Scan(&data_chart)
		if query.Error != nil {
			return nil, query.Error
		}
		res = barChart(data_chart, "Jurnal Terakreditasi")
	} else if types == "3" {
		query := db.Raw("select initcap(lower(tb2.publisher)) as label, count(*) as count from (select ij.id_identitas_jurnal,pa.sts_hasil_akreditasi from public.penetapan_akreditasi pa inner join public.identitas_jurnal ij on (pa.id_identitas_jurnal=ij.id_identitas_jurnal) where pa.sts_hasil_akreditasi = '0'	group by ij.id_identitas_jurnal, pa.sts_hasil_akreditasi order by ij.id_identitas_jurnal asc) tb1 inner join public.identitas_jurnal tb2 on tb1.id_identitas_jurnal = tb2.id_identitas_jurnal where tb1.sts_hasil_akreditasi = '0' and tb2.sts_aktif_identitas_jurnal = '1' group by lower(tb2.publisher) order by count(*) desc limit 5").Scan(&data_chart)
		if query.Error != nil {
			return nil, query.Error
		}
		res = barChart(data_chart, "Usulan Ditolak")
	}

	return res, nil
}

func DashboardDashboardBarChartBidangIlmu(types string) (interface{}, error) {
	var res interface{}
	var data_chart []entity.DataChart

	db := config.Con()
	defer config.CloseCon(db)

	if types == "1" {
		query := db.Raw("select tb4.bidang_ilmu as label, count(*) as count from public.usulan_akreditasi tb1 inner  join public.identitas_jurnal tb2 on tb1.id_identitas_jurnal=tb2.id_identitas_jurnal  inner join public.bidang_ilmu_jurnal tb3 on tb2.id_jurnal=tb3.id_jurnal inner join  public.bidang_ilmu tb4 on tb4.id_bidang_ilmu=tb3.id_bidang_ilmu  group by tb3.id_bidang_ilmu,tb4.bidang_ilmu order by count(*) desc limit 10").Scan(&data_chart)
		if query.Error != nil {
			return nil, query.Error
		}
		res = barChart(data_chart, "10 Bidang Ilmu dengan Jurnal Paling Banyak Diusulkan")
	} else if types == "2" {
		query := db.Raw("select bi.bidang_ilmu as label, count(*) as count from public.bidang_ilmu_jurnal bij inner join public.bidang_ilmu bi on (bij.id_bidang_ilmu=bi.id_bidang_ilmu) inner join (select j.id_jurnal from public.identitas_jurnal ij inner join public.jurnal j on (ij.id_jurnal=j.id_jurnal) inner join public.bidang_ilmu_jurnal bij on (j.id_jurnal=bij.id_jurnal) where ij.id_identitas_jurnal in (select ij.id_identitas_jurnal from public.penetapan_akreditasi pa inner join public.identitas_jurnal ij on (ij.id_identitas_jurnal=pa.id_identitas_jurnal) where pa.sts_hasil_akreditasi = '1' and ij.sts_aktif_identitas_jurnal = '1' group by ij.id_identitas_jurnal order by ij.id_identitas_jurnal asc) group by j.id_jurnal order by j.id_jurnal asc) tempo on (bij.id_jurnal=tempo.id_jurnal) group by  bi.id_bidang_ilmu, bi.bidang_ilmu order by count(*) desc limit 10").Scan(&data_chart)
		if query.Error != nil {
			return nil, query.Error
		}
		res = barChart(data_chart, "10 Bidang Ilmu dengan Jurnal Paling Banyak Terakreditasi")
	} else if types == "3" {
		query := db.Raw("select bi.bidang_ilmu as label, count(*) as count from public.bidang_ilmu_jurnal bij inner join public.bidang_ilmu bi on (bij.id_bidang_ilmu=bi.id_bidang_ilmu) inner join (select j.id_jurnal from public.identitas_jurnal ij inner join public.jurnal j on (ij.id_jurnal=j.id_jurnal) inner join public.bidang_ilmu_jurnal bij on (j.id_jurnal=bij.id_jurnal) where ij.id_identitas_jurnal in (select ij.id_identitas_jurnal from public.penetapan_akreditasi pa inner join public.identitas_jurnal ij on (ij.id_identitas_jurnal=pa.id_identitas_jurnal) where pa.sts_hasil_akreditasi = '0' and ij.sts_aktif_identitas_jurnal = '1' group by ij.id_identitas_jurnal order by ij.id_identitas_jurnal asc) group by j.id_jurnal order by j.id_jurnal asc) tempo on (bij.id_jurnal=tempo.id_jurnal) group by  bi.id_bidang_ilmu, bi.bidang_ilmu order by count(*) desc limit 10").Scan(&data_chart)
		if query.Error != nil {
			return nil, query.Error
		}
		res = barChart(data_chart, "10 Bidang Ilmu dengan Jurnal Paling Banyak Ditolak")
	}

	return res, nil
}

func DashboardDistributorRekapAkreditasi() (interface{}, error) {
	var data entity.DashboardDistributorRekapAkreditasi

	db := config.Con()
	defer config.CloseCon(db)

	query := db.Raw("SELECT * FROM public.arjuna_rekap_dashboard_distributor2()").Scan(&data)
	if query.Error != nil {
		return nil, query.Error
	}

	return data, nil
}

func DashboardDistributorDetailCount(types string, page string, row string) (interface{}, error) {
	var hasil_usulan []entity.DashboardDistributorDetail

	db := config.Con()
	defer config.CloseCon(db)

	// rows, _ := strconv.Atoi(row)
	// pages, _ := strconv.Atoi(page)

	query := db.Raw("SELECT eissn, nama_jurnal, url_jurnal, publisher as penerbit, jml_record FROM public.arjuna_detail_informasi_rekap_on_distributor(( ? ), ( ? ), ( ? ))", types, page, row)

	data := db.Table("( ? ) as temp", query)
	data.Scan(&hasil_usulan)

	if data.Error != nil {
		return nil, data.Error
	}
	return hasil_usulan, nil
}

func DashboardDistributorRekapPeta(par_kota string) (interface{}, error) {
	var data_peta entity.DashboardDistributorPeta

	db := config.Con()
	defer config.CloseCon(db)

	query := db.Raw("SELECT * FROM public.arjuna_rekap_peta_dashboard_distributor(( ? ))", par_kota).Scan(&data_peta)

	if query.Error != nil {
		return nil, query.Error
	}

	return data_peta, nil
}

func DashboardDistributorDetailSK(id_sk_akreditasi, row, page string) (interface{}, error) {
	var data_sk []entity.DashboardDistributorDetailSK

	db := config.Con()
	defer config.CloseCon(db)

	query := db.Raw("SELECT eissn, nama_jurnal, url_jurnal, publisher as penerbit, grade_akreditasi, jml_record FROM arjuna_detail_chart_sk_distributor( ( ? ), (? ), ( ? ) )", id_sk_akreditasi, row, page).Scan(&data_sk)

	fmt.Println(query)

	if query.Error != nil {
		return nil, query.Error
	}
	return data_sk, nil
}

func DashboardDistributorTabelRekapitulasi(request, method, row, page string) ([]entity.DashboardDistributorDetail, error) {

	db := config.Con()
	defer config.CloseCon(db)

	ch := make(chan []entity.DashboardDistributorDetail)
	go func() {
		var data_tabel []entity.DashboardDistributorDetail
		query := db.Raw("SELECT * FROM arjuna_detail_tabel_rekapitulasi_usulan_on_distributor(( ? ), ( ? ), ( ? ), ( ? ))", request, method, row, page).Scan(&data_tabel).Limit(100)
		if query.Error != nil {
			ch <- nil
		}
		ch <- data_tabel
	}()

	data_tabel := <-ch
	return data_tabel, nil
}

func DashboardDistributorBidangIlmu(request, bidang_ilmu, method, institusi, page, row string) ([]entity.DashboardDistributorDetail, error) {

	db := config.Con()
	defer config.CloseCon(db)

	if request == "bidang_ilmu" {
		data, err := getDashboardDetailBidangIlmu(method, bidang_ilmu, institusi, page, row)
		if err != nil {
			return nil, err
		}
		return data, nil
	} else if request == "institusi" {
		data, err := getDashboardDetailInstitusi(method, bidang_ilmu, institusi, page, row)
		if err != nil {
			return nil, err
		}
		return data, nil
	}
	return nil, errors.New("request not found")
}

func getDashboardDetailBidangIlmu(method, bidang_ilmu, institusi, page, row string) ([]entity.DashboardDistributorDetail, error) {
	db := config.Con()
	defer config.CloseCon(db)

	ch := make(chan []entity.DashboardDistributorDetail)
	var data_tabel []entity.DashboardDistributorDetail

	if method == "usulan_bidang_ilmu" {
		go func() {
			query := db.Raw("SELECT eissn, nama_jurnal, url_jurnal, publisher as penerbit, jml_record FROM arjuna_detail_chart_frontpage_2(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 1, bidang_ilmu, "usulan_bidang_ilmu", institusi, page, row).Scan(&data_tabel).Limit(100)
			if query.Error != nil {
				ch <- nil
			}
			ch <- data_tabel
		}()
	} else if method == "akreditasi_bidang_ilmu" {
		go func() {
			query := db.Raw("SELECT eissn, nama_jurnal, url_jurnal, publisher as penerbit, jml_record FROM arjuna_detail_chart_frontpage_2(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 1, bidang_ilmu, "akreditasi_bidang_ilmu", institusi, page, row).Scan(&data_tabel).Limit(100)
			if query.Error != nil {
				ch <- nil
			}
			ch <- data_tabel
		}()

	} else if method == "tolak_bidang_ilmu" {
		go func() {
			query := db.Raw("SELECT eissn, nama_jurnal, url_jurnal, publisher as penerbit, jml_record FROM arjuna_detail_chart_frontpage_2(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 1, bidang_ilmu, "tolak_bidang_ilmu", institusi, page, row).Scan(&data_tabel).Limit(100)
			if query.Error != nil {
				ch <- nil
			}
			ch <- data_tabel
		}()
	} else {
		return nil, errors.New("method not found")
	}
	data_tabel = <-ch
	return data_tabel, nil
}

func getDashboardDetailInstitusi(method, bidang_ilmu, institusi, page, row string) ([]entity.DashboardDistributorDetail, error) {
	db := config.Con()
	defer config.CloseCon(db)

	ch := make(chan []entity.DashboardDistributorDetail)
	var data_tabel []entity.DashboardDistributorDetail

	if method == "institusi_usulan" {
		go func() {
			query := db.Raw("SELECT eissn, nama_jurnal, url_jurnal, publisher as penerbit, jml_record FROM arjuna_detail_chart_frontpage_2(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 2, bidang_ilmu, "institusi_usulan", institusi, page, row).Scan(&data_tabel).Limit(100)
			if query.Error != nil {
				ch <- nil
			}
			ch <- data_tabel
			log.Println(ch)
		}()
	} else if method == "institusi_akreditasi" {
		go func() {
			query := db.Raw("SELECT eissn, nama_jurnal, url_jurnal, publisher as penerbit, jml_record FROM arjuna_detail_chart_frontpage_2(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 2, bidang_ilmu, "institusi_akreditasi", institusi, page, row).Scan(&data_tabel).Limit(100)
			if query.Error != nil {
				ch <- nil
			}
			ch <- data_tabel
			log.Println(ch)
		}()

	} else if method == "institusi_ditolak" {
		go func() {
			query := db.Raw("SELECT eissn, nama_jurnal, url_jurnal, publisher as penerbit, jml_record FROM arjuna_detail_chart_frontpage_2(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 2, bidang_ilmu, "institusi_ditolak", institusi, page, row).Scan(&data_tabel).Limit(100)
			if query.Error != nil {
				ch <- nil
			}
			ch <- data_tabel
			log.Println(ch)
		}()
	} else {
		return nil, errors.New("method not found")
	}
	data_tabel = <-ch
	return data_tabel, nil
}

func DashboardDetailChartPenilai(request, method, bidangIlmu, tahun, idPersonal, page, row string) (helper.ResponsePagination, error) {
	db := config.Con()
	defer config.CloseCon(db)

	if request == "Penilai Konten" || request == "penilai konten" {
		data, err := getPenilaiKonten(method, bidangIlmu, tahun, idPersonal, page, row)
		if err != nil {
			return helper.ResponsePagination{}, err
		}
		return data, nil
	} else if request == "Penilai Manajemen" || request == "penilai manajemen" {
		data, err := getPenilaiManajemen(method, bidangIlmu, tahun, idPersonal, page, row)
		if err != nil {
			return helper.ResponsePagination{}, err
		}
		return data, nil
	} else if request == "evaluator" {
		data, err := getPenilaiEvaluator(method, bidangIlmu, tahun, idPersonal, page, row)
		if err != nil {
			return helper.ResponsePagination{}, err
		}
		return data, nil
	}
	return helper.ResponsePagination{}, errors.New("request not found")
}

func getPenilaiKonten(method, bidangIlmu, tahun, idPersonal, page, row string) (helper.ResponsePagination, error) {
	db := config.Con()
	defer config.CloseCon(db)

	pages, _ := strconv.Atoi(page)
	rows, _ := strconv.Atoi(row)

	var detail []entity.DashboardDetailAsessor
	var response helper.ResponsePagination
	ch := make(chan helper.ResponsePagination)

	if method == "Total Penugasan" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 1, 1, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Penugasan Diterima" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 1, 2, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Penugasan Ditolak" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 1, 3, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Penugasan Belum Diputuskan" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 1, 4, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Dalam Proses Penilaian" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 1, 5, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Penilaian Selesai" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 1, 6, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Rekapitulasi Penilaian Tahunan" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 1, 7, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Rekapitulasi Penilaian berdasarkan Bidang Keilmuan" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 1, 8, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else {
		return helper.ResponsePagination{}, errors.New("method not found")
	}
	response = <-ch
	return response, nil
}

func getPenilaiManajemen(method, bidangIlmu, tahun, idPersonal, page, row string) (helper.ResponsePagination, error) {
	db := config.Con()
	defer config.CloseCon(db)

	pages, _ := strconv.Atoi(page)
	rows, _ := strconv.Atoi(row)

	var detail []entity.DashboardDetailAsessor
	var response helper.ResponsePagination
	ch := make(chan helper.ResponsePagination)

	if method == "Total Penugasan" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 2, 1, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Penugasan Diterima" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 2, 2, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Penugasan Ditolak" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 2, 3, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Penugasan Belum Diputuskan" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 2, 4, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Dalam Proses Penilaian" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 2, 5, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Penilaian Selesai" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 2, 6, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Rekapitulasi Penilaian Tahunan" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 2, 7, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Rekapitulasi Penilaian berdasarkan Bidang Keilmuan" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 2, 8, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else {
		return helper.ResponsePagination{}, errors.New("method not found")
	}
	response = <-ch
	return response, nil
}

func getPenilaiEvaluator(method, bidangIlmu, tahun, idPersonal, page, row string) (helper.ResponsePagination, error) {
	db := config.Con()
	defer config.CloseCon(db)

	pages, _ := strconv.Atoi(page)
	rows, _ := strconv.Atoi(row)

	var detail []entity.DashboardDetailAsessor
	var response helper.ResponsePagination
	ch := make(chan helper.ResponsePagination)

	if method == "Total Penugasan" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 3, 1, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Penugasan Diterima" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 3, 2, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Penugasan Ditolak" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 3, 3, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Penugasan Belum Diputuskan" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 3, 4, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Dalam Proses Penilaian" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 3, 5, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Penilaian Selesai" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 3, 6, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Rekapitulasi Penilaian Tahunan" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 3, 7, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if method == "Rekapitulasi Penilaian berdasarkan Bidang Keilmuan" {
		go func() {
			query := db.Raw("SELECT * FROM arjuna_detail_chart_asessor(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 3, 8, bidangIlmu, tahun, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else {
		return helper.ResponsePagination{}, errors.New("method not found")
	}
	response = <-ch
	return response, nil
}

func DashboardDetailPengusul(dashboard, method, tahun, idPersonal, page, row string) (helper.ResponsePagination, error) {

	db := config.Con()
	defer config.CloseCon(db)

	pages, _ := strconv.Atoi(page)
	rows, _ := strconv.Atoi(row)
	id_personal, _ := strconv.Atoi(idPersonal)
	tahunConv, _ := strconv.Atoi(tahun)

	var detail []entity.DashboardDetailPengusul
	var response helper.ResponsePagination
	ch := make(chan helper.ResponsePagination)

	if dashboard == "dashboard header" {
		go func() {
			query := db.Raw("SELECT * FROM z_arjuna_detail_dashboard(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 1, method, tahunConv, id_personal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if dashboard == "dashboard chart rekap jurnal pertahun" {
		go func() {
			query := db.Raw("SELECT * FROM z_arjuna_detail_dashboard(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 2, "", tahunConv, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else if dashboard == "dashboard rekap akreditasi tahunan" {
		go func() {
			query := db.Raw("SELECT * FROM z_arjuna_detail_dashboard(( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ))", 3, "", tahunConv, idPersonal, (pages - 1), rows).Scan(&detail)
			if query.Error != nil {
				ch <- helper.ResponsePagination{}
			}
			var total int
			if len(detail) > 0 {
				total = detail[0].JmlRecord
			} else {
				total = 0
			}
			ch <- helper.ResponsePagination{
				Data:        detail,
				CurrentPage: pages,
				LastPage:    (total / rows) + 1,
				PerPage:     rows,
				Total:       total,
			}
		}()
	} else {
		return helper.ResponsePagination{}, errors.New("dashboard not found")
	}
	response = <-ch
	return response, nil
}
