package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/vcraescu/go-paginator/v2"
	"github.com/vcraescu/go-paginator/v2/adapter"
	"github.com/vcraescu/go-paginator/v2/view"
)

func ListSKAkreditasi(page string, row string, status string, search string) (helper.ResponsePagination, error) {
	var sk_akreditasi []entity.SKAkreditasi

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("sk_akreditasi")
	data.Select("*")
	if status != "" {
		data.Where("sts_published = ?", status)
	}
	if search != "" {
		data.Where("lower(judul_sk) LIKE ?", "%"+strings.ToLower(search)+"%").Or("lower(no_sk) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Order("tgl_sk DESC")
	data.Find(&sk_akreditasi)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(sk_akreditasi)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&sk_akreditasi); err != nil {
		panic(err)
	}

	for i, v := range sk_akreditasi {
		sk_akreditasi[i].JumlahJurnal = countJurnalBySK(v.ID_SK_Akreditasi)
	}

	res := helper.BuildPaginationResponse(sk_akreditasi, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func GetAllListSKAkreditasi(status string) ([]entity.SKAkreditasi, error) {
	var sk_akreditasi []entity.SKAkreditasi

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("sk_akreditasi")
	data.Select("*")
	data.Where("sts_published = ?", status)
	data.Order("tgl_sk DESC")
	data.Find(&sk_akreditasi)
	if data.Error != nil {
		return nil, data.Error
	}
	return sk_akreditasi, nil
}

func CheckDuplicateNoSK(no_sk string) bool {
	var sk_akreditasi entity.SKAkreditasi

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("sk_akreditasi").Where("trim(no_sk) = trim( ? )", no_sk).Take(&sk_akreditasi)
	return !(data.Error == nil)
}

func CreateSKAkreditasi(req entity.SKAkreditasiDTO) (interface{}, error) {
	var sk_akreditasi entity.SKAkreditasi
	var id_penanda_tangan_sk *string
	var id_periode_akreditasi int64

	db := config.Con()
	defer config.CloseCon(db)

	db.Table("penanda_tangan_sk").Select("id_penanda_tangan_sk").Where("status_aktif_penanda_tangan = ?", "1").Scan(&id_penanda_tangan_sk)
	db.Table("periode_akreditasi").Select("id_periode_akreditasi").Where("sts_aktif_periode_akreditasi = ?", "1").Scan(&id_periode_akreditasi)

	sk_akreditasi.NO_SK = req.NO_SK
	sk_akreditasi.JudulSK = req.JudulSk
	sk_akreditasi.TglSK = req.TglSk
	sk_akreditasi.StsPublished = "0"
	sk_akreditasi.TglCreated = time.Now()
	sk_akreditasi.IDPenandaTanganSK = id_penanda_tangan_sk
	sk_akreditasi.IDPeriodeAkreditasi = id_periode_akreditasi

	save := db.Table("sk_akreditasi").Create(&sk_akreditasi)
	if save.Error != nil {
		return false, save.Error
	}
	return sk_akreditasi, nil
}

func ShowSKAkreditasi(id_sk_akreditasi int) (interface{}, error) {
	var sk_akreditasi entity.SKAkreditasi
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("sk_akreditasi")
	data.Select("*")
	data.Where("id_sk_akreditasi = ?", id_sk_akreditasi)
	data.Take(&sk_akreditasi)
	if data.Error != nil {
		return nil, data.Error
	}
	return sk_akreditasi, nil
}

func UpdateSKAkreditasi(req entity.SKAkreditasiDTO, id_sk int) (interface{}, error) {
	db := config.Con()
	defer config.CloseCon(db)
	update := db.Table("sk_akreditasi").Where("id_sk_akreditasi = ?", id_sk).Updates(map[string]interface{}{"no_sk": req.NO_SK, "judul_sk": req.JudulSk, "tgl_sk": req.TglSk, "tgl_updated": time.Now()})
	if update.Error != nil {
		return false, update.Error
	}
	return true, nil
}

func PublishOrUnPublishSKAkreditasi(id_sk_akreditasi int64, sts_published string) (interface{}, error) {
	var sk_akreditasi []entity.SKAkreditasi
	var id_periode_akreditasi int64

	db := config.Con()
	defer config.CloseCon(db)

	db.Table("periode_akreditasi").Select("id_periode_akreditasi").Where("sts_aktif_periode_akreditasi = ?", "1").Scan(&id_periode_akreditasi)

	data_sk := db.Table("sk_akreditasi as ta")
	data_sk.Select("td.id_jurnal, ta.tgl_sk")
	data_sk.Joins("INNER JOIN penetapan_akreditasi as tb ON ta.id_sk_akreditasi = tb.id_sk_akreditasi AND ta.id_sk_akreditasi = ?", id_sk_akreditasi)
	data_sk.Joins("INNER JOIN identitas_jurnal as td ON tb.id_identitas_jurnal = td.id_identitas_jurnal")
	data_sk.Find(&sk_akreditasi)

	db.Table("sk_akreditasi").Where("id_sk_akreditasi = ?", id_sk_akreditasi).Updates(map[string]interface{}{"sts_published": sts_published, "tgl_updated": time.Now()})

	for _, v := range sk_akreditasi {
		db.Table("jurnal").Where("id_jurnal = ?", v.IDJurnal).Updates(map[string]interface{}{"tgl_akhir_terakreditasi": v.TglSK, "tgl_updated": time.Now()})
	}

	if sts_published == "1" {
		sub_q_where := db.Table("penetapan_akreditasi").Select("id_usulan_akreditasi").Where("id_sk_akreditasi = ?", id_sk_akreditasi)
		db.Table("progres_usulan_akreditasi").Where("id_usulan_akreditasi IN ? ", sub_q_where).Updates(map[string]interface{}{"id_progres": "4", "tgl_updated": time.Now()})
	} else {
		sub_q_where := db.Table("penetapan_akreditasi").Select("id_usulan_akreditasi").Where("id_sk_akreditasi = ?", id_sk_akreditasi)
		db.Table("progres_usulan_akreditasi").Where("id_usulan_akreditasi IN ? ", sub_q_where).Updates(map[string]interface{}{"id_progres": "3", "tgl_updated": time.Now()})
		db.Table("sk_akreditasi").Where("id_sk_akreditasi = ? ", id_sk_akreditasi).Updates(map[string]interface{}{"id_periode_akreditasi": id_periode_akreditasi})
	}
	return true, nil
}

func DeleteSKAkreditasi(id_sk_akreditasi int64) (interface{}, error) {
	var count int64
	var array_id_penetapan_akreditasi []int64

	db := config.Con()
	defer config.CloseCon(db)

	db.Table("penetapan_akreditasi").Where("id_sk_akreditasi = ?", id_sk_akreditasi).Count(&count)
	if count == 0 {
		db.Table("sk_akreditasi").Where("id_sk_akreditasi = ?", id_sk_akreditasi).Delete(&entity.SKAkreditasi{})
	} else {
		db.Table("penetapan_akreditasi").Select("id_penetapan_akreditasi").Where("id_sk_akreditasi = ?", id_sk_akreditasi).Find(&array_id_penetapan_akreditasi)
		for _, v := range array_id_penetapan_akreditasi {
			var check_penetapan_akreditasi int64
			db.Table("penetapan_akreditasi").Where("id_penetapan_akreditasi = ?", v).Count(&check_penetapan_akreditasi)
			if check_penetapan_akreditasi > 0 {
				var check_issue_penetapan_akreditasi int64
				db.Table("issue_penetapan_akreditasi").Where("id_penetapan_akreditasi = ?", v).Count(&check_issue_penetapan_akreditasi)
				if check_issue_penetapan_akreditasi > 0 {
					db.Table("penetapan_akreditasi").Where("id_penetapan_akreditasi = ?", v).Updates(map[string]interface{}{"id_issue_penetapan_akreditasi": nil})
				}
			}
			db.Table("penetapan_akreditasi").Where("id_penetapan_akreditasi = ?", v).Where("id_sk_akreditasi = ?", id_sk_akreditasi).Updates(map[string]interface{}{"id_sk_akreditasi": nil, "tgl_updated": time.Now()})
		}
		db.Table("sk_akreditasi").Where("id_sk_akreditasi = ?", id_sk_akreditasi).Delete(&entity.SKAkreditasi{})
	}
	return true, nil
}

func GetTerakdetasiBelumSK(page, row, jurnal string) (helper.ResponsePagination, error) {
	var terakreditasi_belum_sk []entity.TerakreditasiBelumSK

	db := config.Con()
	defer config.CloseCon(db)

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	query := db.Raw("SELECT * FROM arjuna_list_terakreditasi_blm_sk( ( ? ), ( ? ), ( ? ) )", (pages - 1), row, jurnal)

	data := db.Table("( ? ) as temp", query)
	data.Scan(&terakreditasi_belum_sk)

	if data.Error != nil {
		return helper.ResponsePagination{}, query.Error
	}

	var count int
	if len(terakreditasi_belum_sk) > 0 {
		count = terakreditasi_belum_sk[0].JmlRecord
	} else {
		count = 0
	}

	res := helper.ResponsePagination{}
	res.Data = terakreditasi_belum_sk
	res.CurrentPage = pages
	res.LastPage = ((count / rows) + 1)
	res.PerPage = rows
	res.Total = count

	return res, nil
}

func CheckIDPenetapanAkreditasi(id_penetapan_akreditasi int64) bool {
	var res int64

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Select("id_penetapan_akreditasi").Table("penetapan_akreditasi").Where("id_penetapan_akreditasi = ?", id_penetapan_akreditasi).Take(&res)
	data.Find(&res)

	if data.Error != nil {
		return false
	}

	return true
}

func CheckIDSKAkreditasi(id_sk_akreditasi int) bool {
	var res int

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Select("id_sk_akreditasi").Table("sk_akreditasi").Where("id_sk_akreditasi = ?", id_sk_akreditasi).Take(&res)
	data.Find(&res)

	if data.Error != nil {
		return false
	}
	return true
}

func SetSKAkreditasi(req entity.SetAkreditasiDTO) (entity.SetAkreditasi, error) {
	db := config.Con()
	defer config.CloseCon(db)

	var sk_akreditasi entity.SetAkreditasi

	sk_akreditasi.IDPenetapanAkreditasi = req.IDPenetapanAkreditasi
	sk_akreditasi.IDSKAkreditasi = req.IDSKAkreditasi
	sk_akreditasi.URLSinta = req.URLSinta
	sk_akreditasi.VolumeAwal = req.VolumeAwal
	sk_akreditasi.VolumeAkhir = req.VolumeAkhir
	sk_akreditasi.NomerAwal = req.NomerAwal
	sk_akreditasi.NomerAkhir = req.NomerAkhir
	sk_akreditasi.TahunAwal = req.TahunAwal
	sk_akreditasi.TahunAkhir = req.TahunAkhir

	query := db.Raw("SELECT arjuna_set_sk_akreditasi_rev2 as status FROM arjuna_set_sk_akreditasi_rev2( ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ), ( ? ) )", req.IDPenetapanAkreditasi, req.IDSKAkreditasi, req.URLSinta, req.VolumeAwal, req.VolumeAkhir, req.NomerAwal, req.NomerAkhir, req.TahunAwal, req.TahunAkhir)

	data := db.Table("( ? ) as temp", query)
	data.Scan(&sk_akreditasi)

	tx := db.Begin()

	if data.Error != nil {
		if e := tx.Rollback().Error; e != nil {
			return sk_akreditasi, fmt.Errorf("Rolling back transaction: %v", e)
		}
		return sk_akreditasi, query.Error
	}
	tx.Commit()
	return sk_akreditasi, nil
}

func UnsetSKAkreditasi(req entity.UnsetAkreditasiDTO) (entity.UnsetAkreditasi, error) {
	db := config.Con()
	defer config.CloseCon(db)

	var unsetSK entity.UnsetAkreditasi

	unsetSK.IDPenetapanAkreditasi = req.IDPenetapanAkreditasi
	unsetSK.IDSKAkreditasi = req.IDSKAkreditasi

	query := db.Raw("SELECT arjuna_unset_sk_akreditasi as status FROM arjuna_unset_sk_akreditasi( ( ? ), ( ? ) )", req.IDPenetapanAkreditasi, req.IDSKAkreditasi)

	data := db.Table("( ? ) as temp", query)
	data.Scan(&unsetSK)

	tx := db.Begin()

	if data.Error != nil {
		if e := tx.Rollback().Error; e != nil {
			return unsetSK, fmt.Errorf("Rolling back transaction: %v", e)
		}
		return unsetSK, query.Error
	}
	tx.Commit()
	return unsetSK, nil
}
