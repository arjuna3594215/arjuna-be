package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"math"
	"strconv"
	"time"
)

// ========================================== Dashboard Pengusul ============================================
func getRekapJurnalAktifPertahun(id_personal int64) []entity.DataChart {
	var dataChart []entity.DataChart

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("jurnal as j")
	data.Select("EXTRACT( 'year' FROM ij.tgl_created ) AS label, COUNT ( * ) AS count ")
	data.Joins("INNER JOIN identitas_jurnal as ij ON j.id_jurnal = ij.id_jurnal")
	data.Joins("INNER JOIN pic ON pic.id_identitas_jurnal = ij.id_identitas_jurnal")
	data.Where("ij.sts_aktif_identitas_jurnal = ?", "1")
	data.Where("pic.sts_aktif_pic = ?", "1")
	data.Where("pic.id_personal = ?", id_personal)
	data.Group("EXTRACT( 'year' FROM ij.tgl_created )")
	data.Find(&dataChart)
	if data.Error != nil {
		return nil
	}
	return dataChart
}

func getRekapJurnalTerakreditasiPertahun(id_personal int64) []entity.DataChart {
	var dataChart []entity.DataChart

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penetapan_akreditasi as pa")
	data.Select("EXTRACT( 'year' FROM pa.tgl_created ) AS label, COUNT ( * ) AS count ")
	data.Joins("INNER JOIN identitas_jurnal ij ON ij.id_identitas_jurnal = pa.id_identitas_jurnal")
	data.Joins("INNER JOIN pic ON pic.id_identitas_jurnal = ij.id_identitas_jurnal")
	data.Joins("INNER JOIN jurnal as j ON j.id_jurnal = ij.id_jurnal")
	data.Where("ij.sts_aktif_identitas_jurnal = ?", "1")
	data.Where("pic.sts_aktif_pic = ?", "1")
	data.Where("pic.id_personal = ?", id_personal)
	data.Group("EXTRACT( 'year' FROM pa.tgl_created )")
	data.Find(&dataChart)
	if data.Error != nil {
		return nil
	}
	return dataChart
}

// ========================================== Statistik Jurnal Frontpage ============================================
func getStatistikBidangIlmuUsulanTerbanyak(fromyear string, toyear string) []entity.DataChart {
	var dataChart []entity.DataChart

	db := config.Con()
	defer config.CloseCon(db)

	var start string
	var end string
	if fromyear != "" && toyear != "" {
		start = fromyear + "-01-01 00:00:00"
		end = toyear + "-01-01 00:00:00"
	} else {
		t := time.Now()
		start = strconv.Itoa(t.Year()) + "-01-01 00:00:00"
		end = strconv.Itoa(t.Year()+1) + "-01-01 00:00:00"
	}

	data := db.Table("usulan_akreditasi as ua")
	data.Select("bi.bidang_ilmu as label, count(distinct(ij.id_identitas_jurnal)) as count")
	data.Joins("join identitas_jurnal as ij on ij.id_identitas_jurnal = ua.id_identitas_jurnal")
	data.Joins("join bidang_ilmu_jurnal as bij on bij.id_jurnal = ij.id_jurnal")
	data.Joins("join bidang_ilmu as bi on bi.id_bidang_ilmu = bij.id_bidang_ilmu")
	data.Where("ij.sts_aktif_identitas_jurnal = ?", "1")
	data.Where("bij.sts_aktif_bidang_ilmu_jurnal = ?", "1")
	data.Where("bi.sts_aktif_bidang_ilmu = ?", "1")
	data.Where("ua.tgl_created >= ?", start)
	data.Where("ua.tgl_created < ?", end)
	data.Group("bi.bidang_ilmu")
	data.Order("count DESC")
	data.Limit(10)
	data.Find(&dataChart)

	if data.Error != nil {
		return nil
	}
	return dataChart
}

func getStatistikBidangIlmuTerakreditasiTerbanyak(fromyear string, toyear string) []entity.DataChart {
	var dataChart []entity.DataChart

	db := config.Con()
	defer config.CloseCon(db)

	var start string
	var end string
	if fromyear != "" && toyear != "" {
		start = fromyear + "-01-01 00:00:00"
		end = toyear + "-01-01 00:00:00"
	} else {
		t := time.Now()
		start = strconv.Itoa(t.Year()) + "-01-01 00:00:00"
		end = strconv.Itoa(t.Year()+1) + "-01-01 00:00:00"
	}

	data := db.Table("penetapan_akreditasi as pa")
	data.Select("bi.bidang_ilmu as label, count(distinct(ij.id_identitas_jurnal)) as count")
	data.Joins("join identitas_jurnal as ij on ij.id_identitas_jurnal = pa.id_identitas_jurnal")
	data.Joins("join bidang_ilmu_jurnal as bij on bij.id_jurnal = ij.id_jurnal")
	data.Joins("join bidang_ilmu as bi on bi.id_bidang_ilmu = bij.id_bidang_ilmu")
	data.Where("pa.sts_hasil_akreditasi = ?", "1")
	data.Where("ij.sts_aktif_identitas_jurnal = ?", "1")
	data.Where("bij.sts_aktif_bidang_ilmu_jurnal = ?", "1")
	data.Where("bi.sts_aktif_bidang_ilmu = ?", "1")
	data.Where("pa.tgl_created >= ?", start)
	data.Where("pa.tgl_created < ?", end)
	data.Group("bi.bidang_ilmu")
	data.Order("count DESC")
	data.Limit(10)
	data.Find(&dataChart)

	if data.Error != nil {
		return nil
	}
	return dataChart
}

func getStatistikInstitusiUsulanJurnalTerbanyak(fromyear string, toyear string) []entity.DataChart {
	var dataChart []entity.DataChart

	db := config.Con()
	defer config.CloseCon(db)

	var start string
	var end string
	if fromyear != "" && toyear != "" {
		start = fromyear + "-01-01 00:00:00"
		end = toyear + "-01-01 00:00:00"
	} else {
		t := time.Now()
		start = strconv.Itoa(t.Year()) + "-01-01 00:00:00"
		end = strconv.Itoa(t.Year()+1) + "-01-01 00:00:00"
	}

	data := db.Table("usulan_akreditasi as ua")
	data.Select("ij.publisher as label, count( DISTINCT(ij.id_identitas_jurnal) ) AS count")
	data.Joins("JOIN identitas_jurnal AS ij ON ij.id_identitas_jurnal = ua.id_identitas_jurnal ")
	data.Where("ij.sts_aktif_identitas_jurnal = ?", "1")
	data.Where("publisher != ?", "")
	data.Where("ua.tgl_created >= ?", start)
	data.Where("ua.tgl_created < ?", end)
	data.Group("ij.publisher")
	data.Order("count DESC")
	data.Limit(10)
	data.Find(&dataChart)

	if data.Error != nil {
		return nil
	}
	return dataChart
}

func getStatistikInstitusiUsulanTerakreditasiTerbanyak(fromyear string, toyear string) []entity.DataChart {
	var dataChart []entity.DataChart

	db := config.Con()
	defer config.CloseCon(db)

	var start string
	var end string
	if fromyear != "" && toyear != "" {
		start = fromyear + "-01-01 00:00:00"
		end = toyear + "-01-01 00:00:00"
	} else {
		t := time.Now()
		start = strconv.Itoa(t.Year()) + "-01-01 00:00:00"
		end = strconv.Itoa(t.Year()+1) + "-01-01 00:00:00"
	}

	data := db.Table("penetapan_akreditasi AS pa")
	data.Select("ij.publisher as label, count( DISTINCT(ij.id_identitas_jurnal) ) AS count")
	data.Joins("JOIN identitas_jurnal AS ij ON ij.id_identitas_jurnal = pa.id_identitas_jurnal")
	data.Where("ij.sts_aktif_identitas_jurnal = ?", "1")
	data.Where("pa.sts_hasil_akreditasi = ?", "1")
	data.Where("pa.tgl_created >= ?", start)
	data.Where("pa.tgl_created < ?", end)
	data.Group("ij.publisher")
	data.Order("count DESC")
	data.Limit(10)
	data.Find(&dataChart)

	if data.Error != nil {
		return nil
	}
	return dataChart
}

// ========================================== Kinerja Penilai ============================================

func getDetailKinerjaDataBarChartIssue(personal string) []entity.DataChart {
	var dataChart []entity.DataChart

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penugasan_penilaian_issue as t1")
	data.Select("t6.bidang_ilmu as label, count(*) as count")
	data.Joins("inner join usulan_akreditasi as t2 on t2.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("inner join identitas_jurnal as t3 on t3.id_identitas_jurnal = t2.id_identitas_jurnal")
	data.Joins("inner join jurnal as t4 on t4.id_jurnal = t3.id_jurnal")
	data.Joins("inner join bidang_ilmu_jurnal as t5 on t5.id_jurnal = t4.id_jurnal")
	data.Joins("inner join bidang_ilmu as t6 on t6.id_bidang_ilmu = t5.id_bidang_ilmu")
	data.Where("t1.id_personal = ?", personal)
	data.Where("t3.sts_aktif_identitas_jurnal = ?", "1")
	data.Where("((t1.sts_penerimaan_penugasan = '1' and t1.sts_pelaksanaan_tugas = '1') or (t1.sts_penerimaan_penugasan = '1' and t1.sts_pelaksanaan_tugas = '0'))")
	data.Group("t6.id_bidang_ilmu")
	data.Find(&dataChart)

	if data.Error != nil {
		return nil
	}
	return dataChart
}

func getDetailKinerjaDataBarChartManagement(personal string) []entity.DataChart {
	var dataChart []entity.DataChart

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penugasan_penilaian_mgmt as t1")
	data.Select("t6.bidang_ilmu as label, count(*) as count")
	data.Joins("inner join usulan_akreditasi as t2 on t2.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("inner join identitas_jurnal as t3 on t3.id_identitas_jurnal = t2.id_identitas_jurnal")
	data.Joins("inner join jurnal as t4 on t4.id_jurnal = t3.id_jurnal")
	data.Joins("inner join bidang_ilmu_jurnal as t5 on t5.id_jurnal = t4.id_jurnal")
	data.Joins("inner join bidang_ilmu as t6 on t6.id_bidang_ilmu = t5.id_bidang_ilmu")
	data.Where("t1.id_personal = ?", personal)
	data.Where("t3.sts_aktif_identitas_jurnal = ?", "1")
	data.Where("((t1.sts_penerimaan_penugasan = '1' and t1.sts_pelaksanaan_tugas = '1') or (t1.sts_penerimaan_penugasan = '1' and t1.sts_pelaksanaan_tugas = '0'))")
	data.Group("t6.id_bidang_ilmu")
	data.Find(&dataChart)

	if data.Error != nil {
		return nil
	}
	return dataChart
}

func getDetailKinerjaDataBarChartEvaluator(personal string) []entity.DataChart {
	var dataChart []entity.DataChart

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penugasan_penilaian_evaluator as t1")
	data.Select("t6.bidang_ilmu as label, count(*) as count")
	data.Joins("inner join usulan_akreditasi as t2 on t2.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("inner join identitas_jurnal as t3 on t3.id_identitas_jurnal = t2.id_identitas_jurnal")
	data.Joins("inner join jurnal as t4 on t4.id_jurnal = t3.id_jurnal")
	data.Joins("inner join bidang_ilmu_jurnal as t5 on t5.id_jurnal = t4.id_jurnal")
	data.Joins("inner join bidang_ilmu as t6 on t6.id_bidang_ilmu = t5.id_bidang_ilmu")
	data.Where("t1.id_personal = ?", personal)
	data.Where("t3.sts_aktif_identitas_jurnal = ?", "1")
	data.Where("((t1.sts_penerimaan_penugasan = '1' and t1.sts_pelaksanaan_tugas = '1') or (t1.sts_penerimaan_penugasan = '1' and t1.sts_pelaksanaan_tugas = '0'))")
	data.Group("t6.id_bidang_ilmu")
	data.Find(&dataChart)

	if data.Error != nil {
		return nil
	}
	return dataChart
}

func getDetailKinerjaDataLineChartIssue(personal string) []entity.DataChart {
	var dataChart []entity.DataChart

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penugasan_penilaian_issue as t1")
	data.Select("extract( 'year' FROM t2.tgl_created ) AS label, count(*) as count")
	data.Joins("inner join usulan_akreditasi as t2 on t2.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("inner join identitas_jurnal as t3 on t3.id_identitas_jurnal = t2.id_identitas_jurnal")
	data.Where("t1.id_personal = ?", personal)
	data.Where("t3.sts_aktif_identitas_jurnal = ?", "1")
	data.Where("((t1.sts_penerimaan_penugasan = '1' and t1.sts_pelaksanaan_tugas = '1') or (t1.sts_penerimaan_penugasan = '1' and t1.sts_pelaksanaan_tugas = '0'))")
	data.Group("extract( 'year' FROM t2.tgl_created )")
	data.Order("extract( 'year' FROM t2.tgl_created ) ASC")
	data.Find(&dataChart)

	if data.Error != nil {
		return nil
	}
	return dataChart
}

func getDetailKinerjaDataLineChartManagement(personal string) []entity.DataChart {
	var dataChart []entity.DataChart

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penugasan_penilaian_mgmt as t1")
	data.Select("extract( 'year' FROM t2.tgl_created ) AS label, count(*) as count")
	data.Joins("inner join usulan_akreditasi as t2 on t2.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("inner join identitas_jurnal as t3 on t3.id_identitas_jurnal = t2.id_identitas_jurnal")
	data.Where("t1.id_personal = ?", personal)
	data.Where("t3.sts_aktif_identitas_jurnal = ?", "1")
	data.Where("((t1.sts_penerimaan_penugasan = '1' and t1.sts_pelaksanaan_tugas = '1') or (t1.sts_penerimaan_penugasan = '1' and t1.sts_pelaksanaan_tugas = '0'))")
	data.Group("extract( 'year' FROM t2.tgl_created )")
	data.Order("extract( 'year' FROM t2.tgl_created ) ASC")
	data.Find(&dataChart)

	if data.Error != nil {
		return nil
	}
	return dataChart
}

func getDetailKinerjaDataLineChartEvaluator(personal string) []entity.DataChart {
	var dataChart []entity.DataChart

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penugasan_penilaian_evaluator as t1")
	data.Select("extract( 'year' FROM t2.tgl_created ) AS label, count(*) as count")
	data.Joins("inner join usulan_akreditasi as t2 on t2.id_usulan_akreditasi = t1.id_usulan_akreditasi")
	data.Joins("inner join identitas_jurnal as t3 on t3.id_identitas_jurnal = t2.id_identitas_jurnal")
	data.Where("t1.id_personal = ?", personal)
	data.Where("t3.sts_aktif_identitas_jurnal = ?", "1")
	data.Where("((t1.sts_penerimaan_penugasan = '1' and t1.sts_pelaksanaan_tugas = '1') or (t1.sts_penerimaan_penugasan = '1' and t1.sts_pelaksanaan_tugas = '0'))")
	data.Group("extract( 'year' FROM t2.tgl_created )")
	data.Order("extract( 'year' FROM t2.tgl_created ) ASC")
	data.Find(&dataChart)

	if data.Error != nil {
		return nil
	}
	return dataChart
}

// ========================================================================================================
func barChart(data []entity.DataChart, title string) entity.BarChart {
	var barchart entity.BarChart
	var bardataset []entity.BarDatasets
	var xlabels []string
	var dataChart []int
	for _, v := range data {
		xlabels = append(xlabels, v.Label)
		dataChart = append(dataChart, int(v.Count))
	}
	bardataset = append(bardataset, entity.BarDatasets{
		Data: dataChart,
		// Label: v.KampusJurnalAkreditasi,
		Label: title,
	})
	barchart.Title = title
	barchart.Datasets = bardataset
	barchart.Xlabels = xlabels
	return barchart
}

func lineChart(data []entity.DataChart, title string) entity.LineChart {
	var barchart entity.LineChart
	var bardataset []entity.LineDatasets
	var xlabels []string
	var dataChart []int
	for _, v := range data {
		xlabels = append(xlabels, v.Label)
		dataChart = append(dataChart, int(v.Count))
	}
	bardataset = append(bardataset, entity.LineDatasets{
		Data: dataChart,
		// Label: v.KampusJurnalAkreditasi,
		Label: title,
	})
	barchart.Title = title
	barchart.Datasets = bardataset
	barchart.Xlabels = xlabels
	return barchart
}

func polarChart(data []entity.DataChart, title string) entity.PolarChart {
	var polarchart entity.PolarChart
	var labels []string
	var polardataset []entity.PolarDataset
	var dataChart []float64
	total_jurnal := sumCountChart(data)
	for _, v := range data {
		labels = append(labels, v.Label)
		persen := float64(v.Count) / float64(total_jurnal) * 100
		dataChart = append(dataChart, math.Round(persen*100)/100)
	}
	polardataset = append(polardataset, entity.PolarDataset{
		Data: dataChart,
		// Label: v.KampusJurnalAkreditasi,
	})
	polarchart.Title = title
	polarchart.Datasets = polardataset
	polarchart.Labels = labels
	return polarchart
}

func sumCountChart(data []entity.DataChart) int64 {
	var res int64
	for _, v := range data {
		res += v.Count
	}
	return res
}
