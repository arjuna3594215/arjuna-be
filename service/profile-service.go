package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
)

func UpdateProfile(req entity.ProfileDTO, id_personal int) (interface{}, error) {
	var personal entity.Personal
	var bidang_ilmu_interest []entity.BidangIlmuInterest
	db := config.Con()
	defer config.CloseCon(db)
	personal.IDPersonal = int64(id_personal)
	personal.Nama = req.Nama
	personal.Email = req.Email
	personal.NOHandPhone = req.NOHandPhone
	personal.NamaInstitusi = req.NamaInstitusi
	updatePersonal, err := UpdatePersonal(personal)

	if err != nil {
		return nil, err
	}

	if len(req.BidangIlmu) > 0 {
		deleteBidangIlmu := db.Table("bidang_ilmu_interest")
		deleteBidangIlmu.Where("id_personal = ?", id_personal)
		deleteBidangIlmu.Delete(&entity.BidangIlmuInterest{})

		if deleteBidangIlmu.Error != nil {
			return nil, deleteBidangIlmu.Error
		}

		for _, v := range req.BidangIlmu {
			bidang_ilmu_interest = append(bidang_ilmu_interest, entity.BidangIlmuInterest{
				IDPersonal:   int64(id_personal),
				IDBidangIlmu: v,
			})
		}

		save := db.Table("bidang_ilmu_interest").Create(&bidang_ilmu_interest)

		if save.Error != nil {
			return nil, save.Error
		}
	}

	return updatePersonal, nil
}

func UpdateFotoProfile(file []string, id_personal int) error {
	db := config.Con()
	defer config.CloseCon(db)
	update := db.Table("personal").Where("id_personal = ?", id_personal).Update("image", file[0])
	if update.Error != nil {
		return update.Error
	}
	return nil
}

func ChangePassword(id_pengguna int, new_password string) error {
	db := config.Con()
	defer config.CloseCon(db)

	save := db.Table("pengguna").Where("id_pengguna = ?", id_pengguna).Updates(map[string]interface{}{"password": config.EncryptPassword(new_password)})
	if save.Error != nil {
		return save.Error
	}
	return nil
}
