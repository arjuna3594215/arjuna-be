package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"strconv"
	"strings"
	"time"

	"github.com/vcraescu/go-paginator/v2"
	"github.com/vcraescu/go-paginator/v2/adapter"
	"github.com/vcraescu/go-paginator/v2/view"
	"gorm.io/gorm"
)

func ShowPenugasanPenilaian(id string, penilai string) (interface{}, error) {
	var penilaian_penugasan entity.PenugasanPenilaian
	db := config.Con()
	defer config.CloseCon(db)

	var data *gorm.DB

	if penilai == "management" {
		data = db.Table("penugasan_penilaian_mgmt")
		data.Select("id_penugasan_penilaian_manajemen as id_penugasan_penilaian, id_usulan_akreditasi, id_personal, sts_penerimaan_penugasan")
		data.Where("id_penugasan_penilaian_manajemen = ?", id)
		data.Take(&penilaian_penugasan)
	} else if penilai == "evaluator" {
		data = db.Table("penugasan_penilaian_evaluator")
		data.Select("id_penugasan_penilaian_evaluator as id_penugasan_penilaian, id_usulan_akreditasi, id_personal, sts_penerimaan_penugasan")
		data.Where("id_penugasan_penilaian_evaluator = ?", id)
		data.Take(&penilaian_penugasan)
	} else if penilai == "issue" {
		data = db.Table("penugasan_penilaian_issue")
		data.Select("id_penugasan_penilaian_issue as id_penugasan_penilaian, id_usulan_akreditasi, id_personal, sts_penerimaan_penugasan")
		data.Where("id_penugasan_penilaian_issue = ?", id)
		data.Take(&penilaian_penugasan)
	}
	if data.Error != nil {
		return nil, data.Error
	}
	return penilaian_penugasan, nil
}

func UpdateStatusPenugasanPenilaian(req entity.UpdateStatusPenugasanPenilaianDTO, id int64) (interface{}, error) {
	db := config.Con()
	defer config.CloseCon(db)

	update := db

	if req.Penilai == "management" {
		if req.KomentarPenolakan != "" {
			update.Model(&entity.PenugasanPenilaian{}).Table("penugasan_penilaian_mgmt").Where("id_personal = ?", req.IDPersonal).Where("id_penugasan_penilaian_manajemen = ?", id).Updates(map[string]interface{}{"sts_penerimaan_penugasan": req.StsPenerimaanPenugasan, "tgl_penerimaan_penugasan": time.Now(), "tgl_updated": time.Now(), "alasan_tolak": req.KomentarPenolakan})
		} else {
			update.Model(&entity.PenugasanPenilaian{}).Table("penugasan_penilaian_mgmt").Where("id_personal = ?", req.IDPersonal).Where("id_penugasan_penilaian_manajemen = ?", id).Updates(map[string]interface{}{"sts_penerimaan_penugasan": req.StsPenerimaanPenugasan, "tgl_penerimaan_penugasan": time.Now(), "tgl_updated": time.Now()})
		}
	} else if req.Penilai == "evaluator" {
		if req.KomentarPenolakan != "" {
			update.Model(&entity.PenugasanPenilaian{}).Table("penugasan_penilaian_evaluator").Where("id_personal = ?", req.IDPersonal).Where("id_penugasan_penilaian_evaluator = ?", id).Updates(map[string]interface{}{"sts_penerimaan_penugasan": req.StsPenerimaanPenugasan, "tgl_penerimaan_penugasan": time.Now(), "tgl_updated": time.Now(), "alasan_tolak": req.KomentarPenolakan})
		} else {
			update.Model(&entity.PenugasanPenilaian{}).Table("penugasan_penilaian_evaluator").Where("id_personal = ?", req.IDPersonal).Where("id_penugasan_penilaian_evaluator = ?", id).Updates(map[string]interface{}{"sts_penerimaan_penugasan": req.StsPenerimaanPenugasan, "tgl_penerimaan_penugasan": time.Now(), "tgl_updated": time.Now()})
		}
	} else if req.Penilai == "issue" {
		if req.KomentarPenolakan != "" {
			update.Model(&entity.PenugasanPenilaian{}).Table("penugasan_penilaian_issue").Where("id_personal = ?", req.IDPersonal).Where("id_penugasan_penilaian_issue = ?", id).Updates(map[string]interface{}{"sts_penerimaan_penugasan": req.StsPenerimaanPenugasan, "tgl_penerimaan_penugasan": time.Now(), "tgl_updated": time.Now(), "komentar_penolakan": req.KomentarPenolakan})
		} else {
			update.Model(&entity.PenugasanPenilaian{}).Table("penugasan_penilaian_issue").Where("id_personal = ?", req.IDPersonal).Where("id_penugasan_penilaian_issue = ?", id).Updates(map[string]interface{}{"sts_penerimaan_penugasan": req.StsPenerimaanPenugasan, "tgl_penerimaan_penugasan": time.Now(), "tgl_updated": time.Now()})
		}
	}

	if update.Error != nil {
		return nil, update.Error
	}

	return true, nil
}

func ListPenugasanPenilaian(search string, page string, row string, penilai string, id_personal string) (interface{}, error) {
	var res interface{}
	var err error

	if penilai == "management" {
		data, errs := penugasanPenilaianManagement(search, page, row, id_personal)
		res = data
		err = errs
	} else if penilai == "evaluator" {
		data, errs := penugasanPenilaianEvaluator(search, page, row, id_personal)
		res = data
		err = errs
	} else if penilai == "issue" {
		data, errs := penugasanPenilaianIssue(search, page, row, id_personal)
		res = data
		err = errs
	}
	return res, err
}

func penugasanPenilaianManagement(search string, page string, row string, id_personal string) (helper.ResponsePagination, error) {
	var penugasan_penilaian []entity.KSPenugasanPenilaianManagement

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("penugasan_penilaian_mgmt as t1")
	data.Select("t1.id_penugasan_penilaian_manajemen, t1.id_usulan_akreditasi, t1.tgl_created::date as tgl_penugasan, t1.tgl_penerimaan_penugasan::date as tgl_penerimaan, t5.url_statistik_pengunjung, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society, t5.nama_awal_jurnal, t5.tgl_pembuatan::date, t5.url_jurnal, t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat, t5.no_telepon, t5.alamat_surel, t5.tgl_akhir_terakreditasi, t7.nama as nama_pic, t7.nama_institusi, t8.nama_eic, t8.alamat_surel as alamat_surel_eic, t4.alamat_oai, t4.doi_jurnal, t3.user_penilai, t3.passwd_penilai as password_penilai")
	data.Joins("INNER JOIN usulan_akreditasi t3 ON t1.id_usulan_akreditasi = t3.id_usulan_akreditasi")
	data.Joins("INNER JOIN identitas_jurnal t4 ON t3.id_identitas_jurnal = t4.id_identitas_jurnal")
	data.Joins("INNER JOIN jurnal t5 ON t4.id_jurnal = t5.id_jurnal")
	data.Joins("INNER JOIN pic t6 ON t3.id_pic = t6.id_pic")
	data.Joins("INNER JOIN personal t7 ON t6.id_personal = t7.id_personal")
	data.Joins("INNER JOIN eic t8 ON t3.id_eic = t8.id_eic")
	data.Where("t1.id_personal = ?", id_personal)
	data.Where("t1.sts_penerimaan_penugasan is null")
	if search != "" {
		data.Where("lower(t4.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
		// lower(t4.nama_jurnal) ~* lower(p_param_search) or lower(t4.eissn) ~* lower(p_param_search)
	}
	data.Order("t1.tgl_updated desc")
	data.Order("t4.nama_jurnal asc")
	data.Find(&penugasan_penilaian)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(penugasan_penilaian)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&penugasan_penilaian); err != nil {
		panic(err)
	}

	for i, v := range penugasan_penilaian {
		penugasan_penilaian[i].Issue = listIssueByUsulanAkreditasi(v.IDUsulanAkreditasi)
	}

	res := helper.BuildPaginationResponse(penugasan_penilaian, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func penugasanPenilaianEvaluator(search string, page string, row string, id_personal string) (helper.ResponsePagination, error) {
	var penugasan_penilaian []entity.KSPenugasanPenilaianEvaluator

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	sub_query := db.Table("hasil_sa")
	sub_query.Select("id_usulan_akreditasi, nilai_total")
	sub_query.Where("nilai_total < 70")

	data := db.Table("penugasan_penilaian_evaluator as t1")
	data.Select("t1.id_penugasan_penilaian_evaluator, t1.id_usulan_akreditasi, t1.tgl_created::date as tgl_penugasan, t1.tgl_penerimaan_penugasan::date as tgl_penerimaan, t5.url_statistik_pengunjung, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society, t5.nama_awal_jurnal, t5.tgl_pembuatan, t5.url_jurnal, t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat, t5.no_telepon, t5.alamat_surel, t5.tgl_akhir_terakreditasi, t7.nama as nama_pic, t7.nama_institusi, t8.nama_eic, t8.alamat_surel as alamat_surel_eic, t4.alamat_oai, t4.doi_jurnal, t3.user_penilai, t3.passwd_penilai as password_penilai,	t2.nilai_total")
	data.Joins("INNER JOIN ( ? ) t2 ON t1.id_usulan_akreditasi = t2.id_usulan_akreditasi", sub_query)
	data.Joins("INNER JOIN usulan_akreditasi t3 ON t1.id_usulan_akreditasi = t3.id_usulan_akreditasi")
	data.Joins("INNER JOIN identitas_jurnal t4 ON t3.id_identitas_jurnal = t4.id_identitas_jurnal")
	data.Joins("INNER JOIN jurnal t5 ON t4.id_jurnal = t5.id_jurnal")
	data.Joins("INNER JOIN pic t6 ON t3.id_pic = t6.id_pic")
	data.Joins("INNER JOIN personal t7 ON t6.id_personal = t7.id_personal")
	data.Joins("INNER JOIN eic t8 ON t3.id_eic = t8.id_eic")
	data.Where("t1.id_personal = ?", id_personal)
	data.Where("t1.sts_penerimaan_penugasan is null")
	if search != "" {
		data.Where("lower(t4.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
		// lower(t4.nama_jurnal) ~* lower(p_param_search) or lower(t4.eissn) ~* lower(p_param_search)
	}
	data.Order("t1.tgl_created desc")
	data.Order("t4.nama_jurnal asc")
	data.Find(&penugasan_penilaian)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(penugasan_penilaian)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&penugasan_penilaian); err != nil {
		panic(err)
	}

	for i, v := range penugasan_penilaian {
		penugasan_penilaian[i].Issue = listIssueByUsulanAkreditasi(v.IDUsulanAkreditasi)
	}

	res := helper.BuildPaginationResponse(penugasan_penilaian, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func penugasanPenilaianIssue(search string, page string, row string, id_personal string) (helper.ResponsePagination, error) {
	var penugasan_penilaian []entity.KSPenugasanPenilaianIssue

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("identitas_jurnal as t4")
	data.Select("t1.id_penugasan_penilaian_issue, t4.id_jurnal, t1.id_issue, t1.id_usulan_akreditasi, t1.tgl_created::date as tgl_penugasan, t1.tgl_penerimaan_penugasan::date as tgl_penerimaan, t5.url_statistik_pengunjung, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society, t5.nama_awal_jurnal, t5.tgl_pembuatan, t5.url_jurnal,  t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat, t5.no_telepon, t5.alamat_surel, t5.tgl_akhir_terakreditasi, t7.nama as nama_pic, t7.nama_institusi, t8.nama_eic, t8.alamat_surel as alamat_surel_eic, t2.url_issue, t2.judul_issue, t2.volume_issue, t2.nomor_issue, t2.tahun_issue, t4.alamat_oai, t4.doi_jurnal, t9.user_penilai, t9.passwd_penilai as password_penilai")
	data.Joins("LEFT JOIN usulan_akreditasi t9 ON t9.id_identitas_jurnal = t4.id_identitas_jurnal")
	data.Joins("INNER JOIN jurnal t5 ON t4.id_jurnal = t5.id_jurnal")
	data.Joins("INNER JOIN pic t6 ON t4.id_identitas_jurnal = t6.id_identitas_jurnal AND t6.sts_aktif_pic = ?", "1")
	data.Joins("INNER JOIN personal t7 ON t6.id_personal = t7.id_personal")
	data.Joins("INNER JOIN eic t8 ON t4.id_identitas_jurnal = t8.id_identitas_jurnal AND t8.sts_aktif_eic = ?", "1")
	data.Joins("INNER JOIN penugasan_penilaian_issue t1 ON t1.id_usulan_akreditasi = t9.id_usulan_akreditasi")
	data.Joins("INNER JOIN issue t2 ON t1.id_issue = t2.id_issue")
	data.Joins("LEFT JOIN hasil_penilaian_issue ta ON ta.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue AND ta.sts_penetapan_penilaian = ?", "1")
	data.Where("t1.id_personal = ?", id_personal)
	data.Where("t1.sts_pelaksanaan_tugas = ?", "0")
	data.Where("t1.sts_penerimaan_penugasan is null")
	data.Where("ta.id_penugasan_penilaian_issue is null")
	if search != "" {
		data.Where("lower(t4.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
		// lower(t4.nama_jurnal) ~* lower(p_param_search) or lower(t4.eissn) ~* lower(p_param_search)
	}
	data.Order("t1.tgl_created desc")
	data.Order("t1.tgl_penerimaan_penugasan desc")
	data.Find(&penugasan_penilaian)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(penugasan_penilaian)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&penugasan_penilaian); err != nil {
		panic(err)
	}

	for i, v := range penugasan_penilaian {
		penugasan_penilaian[i].BidangIlmuJurnal = getBidangIlmuByJurnal(v.IDJurnal)
	}

	res := helper.BuildPaginationResponse(penugasan_penilaian, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func ListProsesPenilaian(search string, page string, row string, penilai string, id_personal string, proses string) (interface{}, error) {
	var res interface{}
	var err error

	if penilai == "management" {
		data, errs := prosesPenilaianManagement(search, page, row, id_personal, proses)
		res = data
		err = errs
	} else if penilai == "evaluator" {
		data, errs := prosesPenilaianEvaluator(search, page, row, id_personal, proses)
		res = data
		err = errs
	} else if penilai == "issue" {
		data, errs := prosesPenilaianIssue(search, page, row, id_personal, proses)
		res = data
		err = errs
	}
	return res, err
}

func prosesPenilaianManagement(search string, page string, row string, id_personal string, proses string) (helper.ResponsePagination, error) {
	var penugasan_penilaian []entity.KSPenugasanPenilaianManagement

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	sub_q := db.Table("hasil_penilaian_mgmt")
	sub_q.Select("id_penugasan_penilaian_manajemen")
	sub_q.Where("sts_penetapan_penilaian = ?", "1")
	sub_q.Group("id_penugasan_penilaian_manajemen")

	nilai_disinsentif := db.Table("disinsentif_penilaian_mgmt as nilai_disinsentif")
	nilai_disinsentif.Select("nilai_disinsentif.nilai")
	nilai_disinsentif.Where("nilai_disinsentif.id_penugasan_penilaian_manajemen = t1.id_penugasan_penilaian_manajemen")

	nilai_mgmt := db.Table("hasil_penilaian_mgmt as nilai_mgmt")
	nilai_mgmt.Select("SUM(nilai_mgmt.nilai) as nilai_management")
	nilai_mgmt.Joins("INNER JOIN sub_unsur_penilaian as sub on nilai_mgmt.id_sub_unsur_penilaian = sub.id_sub_unsur_penilaian")
	nilai_mgmt.Where("nilai_mgmt.id_penugasan_penilaian_manajemen = t1.id_penugasan_penilaian_manajemen")
	nilai_mgmt.Where("sub.id_kelompok_unsur_penilaian = ?", 2)

	selisih_penilaian := db.Raw("select array_to_json(array_agg(tc.*)) from public.arjuna_check_status_selisih_nilai_mgmt_by_asessor(t1.id_usulan_akreditasi::integer) as tc")
	pasangan_asessor := db.Raw("select tc2.* from public.arjuna_get_pasangan_asessor(t1.id_usulan_akreditasi::integer,t1.id_personal::bigint) as tc2")
	komentar_penyesuaian_nilai := db.Raw("select tk.komentar_penyesuaian from public.penyesuaian_nilai_asessor_mgmt tk where tk.id_penugasan_penilaian_manajemen = t1.id_penugasan_penilaian_manajemen AND tk.status_penyesuaian = true limit 1")

	// arjuna_list_tawaran_manajemen_rev_maintenance2
	q := db.Table("penugasan_penilaian_mgmt as t1")
	q.Select("t1.id_penugasan_penilaian_manajemen, t1.id_personal, t1.id_usulan_akreditasi, t1.tgl_created::date as tgl_penugasan, t1.tgl_penerimaan_penugasan::date as tgl_penerimaan, t5.url_statistik_pengunjung, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society, t5.nama_awal_jurnal, t5.tgl_pembuatan::date, t5.url_jurnal, t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat, t5.no_telepon, t5.alamat_surel, t5.tgl_akhir_terakreditasi, t7.nama as nama_pic, t7.nama_institusi, t8.nama_eic, t8.alamat_surel as alamat_surel_eic, t4.alamat_oai, t4.doi_jurnal, t3.user_penilai, t3.passwd_penilai as password_penilai, ( ? ) as nilai_mgmt, ( ? ) as nilai_disinsentif, ( ? ) as selisih_penilaian, ( ? ) as pasangan_asessor, ( ? ) as komentar_penyesuaian_nilai", nilai_mgmt, nilai_disinsentif, selisih_penilaian, pasangan_asessor, komentar_penyesuaian_nilai)
	q.Joins("INNER JOIN usulan_akreditasi t3 ON t1.id_usulan_akreditasi = t3.id_usulan_akreditasi")
	q.Joins("INNER JOIN identitas_jurnal t4 ON t3.id_identitas_jurnal = t4.id_identitas_jurnal")
	q.Joins("INNER JOIN jurnal t5 ON t4.id_jurnal = t5.id_jurnal")
	q.Joins("INNER JOIN pic t6 ON t3.id_pic = t6.id_pic")
	q.Joins("INNER JOIN personal t7 ON t6.id_personal = t7.id_personal")
	q.Joins("INNER JOIN eic t8 ON t3.id_eic = t8.id_eic")
	if proses == "Penyesuaian Penilaian" {
		q.Joins("INNER JOIN penyesuaian_nilai_asessor_mgmt t_penyesuaian_mgmt ON t1.id_penugasan_penilaian_manajemen = t_penyesuaian_mgmt.id_penugasan_penilaian_manajemen AND t_penyesuaian_mgmt.status_penyesuaian = true")
	}
	q.Where("t1.id_personal = ?", id_personal)
	q.Where("t1.sts_penerimaan_penugasan = ?", "1")
	q.Where("t1.id_penugasan_penilaian_manajemen NOT IN ( ? )", sub_q)
	if search != "" {
		q.Where("lower(t4.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
		// lower(t4.nama_jurnal) ~* lower(p_param_search) or lower(t4.eissn) ~* lower(p_param_search)
	}
	q.Order("t1.tgl_created desc")
	q.Order("t4.nama_jurnal asc")
	data := db.Table("( ? ) as temp", q)
	if proses == "Belum Dinilai" {
		data.Where("temp.nilai_mgmt is null")
		data.Where("temp.nilai_disinsentif is null")
	} else if proses == "Proses Penilaian" {
		data.Where("temp.nilai_mgmt is not null").Or("temp.nilai_disinsentif is not null")
	}
	data.Find(&penugasan_penilaian)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(penugasan_penilaian)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&penugasan_penilaian); err != nil {
		panic(err)
	}

	for i, v := range penugasan_penilaian {
		penugasan_penilaian[i].Issue = listIssueByUsulanAkreditasi(v.IDUsulanAkreditasi)
		penugasan_penilaian[i].NilaiManagement = v.NilaiDisinsentif + v.NilaiMgmt
	}

	res := helper.BuildPaginationResponse(penugasan_penilaian, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func prosesPenilaianEvaluator(search string, page string, row string, id_personal string, proses string) (helper.ResponsePagination, error) {
	var penugasan_penilaian []entity.KSPenugasanPenilaianEvaluator

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	sub_q := db.Table("hasil_penilaian_evaluator")
	sub_q.Select("id_penugasan_penilaian_evaluator")
	sub_q.Where("sts_penetapan_penilaian = ?", "1")
	sub_q.Group("id_penugasan_penilaian_evaluator")

	nilai_disinsentif := db.Table("disinsentif_penilaian_evaluator as nilai_disinsentif")
	nilai_disinsentif.Select("nilai_disinsentif.nilai")
	nilai_disinsentif.Where("nilai_disinsentif.id_penugasan_penilaian_evaluator = t1.id_penugasan_penilaian_evaluator")

	nilai_evaluator := db.Table("hasil_penilaian_evaluator as nilai_evaluator")
	nilai_evaluator.Select("SUM(nilai_evaluator.nilai) as nilai_evaluasi")
	nilai_evaluator.Joins("INNER JOIN sub_unsur_penilaian as sub on nilai_evaluator.id_sub_unsur_penilaian = sub.id_sub_unsur_penilaian")
	nilai_evaluator.Where("nilai_evaluator.id_penugasan_penilaian_evaluator = t1.id_penugasan_penilaian_evaluator")
	nilai_evaluator.Where("sub.id_kelompok_unsur_penilaian = ?", 2)

	q := db.Table("penugasan_penilaian_evaluator as t1")
	q.Select("t1.id_penugasan_penilaian_evaluator, t1.id_usulan_akreditasi, t1.tgl_created::date as tgl_penugasan, t1.tgl_penerimaan_penugasan::date as tgl_penerimaan, t5.url_statistik_pengunjung, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society, t5.nama_awal_jurnal, t5.tgl_pembuatan::date, t5.url_jurnal, t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat, t5.no_telepon, t5.alamat_surel, t5.tgl_akhir_terakreditasi, t7.nama as nama_pic, t7.nama_institusi, t8.nama_eic, t8.alamat_surel as alamat_surel_eic, t4.alamat_oai, t4.doi_jurnal, t3.user_penilai, t3.passwd_penilai as password_penilai, ( ? ) as nilai_evaluasi, ( ? ) as nilai_disinsentif", nilai_evaluator, nilai_disinsentif)
	q.Joins("INNER JOIN usulan_akreditasi t3 ON t1.id_usulan_akreditasi = t3.id_usulan_akreditasi")
	q.Joins("INNER JOIN identitas_jurnal t4 ON t3.id_identitas_jurnal = t4.id_identitas_jurnal")
	q.Joins("INNER JOIN jurnal t5 ON t4.id_jurnal = t5.id_jurnal")
	q.Joins("INNER JOIN pic t6 ON t3.id_pic = t6.id_pic")
	q.Joins("INNER JOIN personal t7 ON t6.id_personal = t7.id_personal")
	q.Joins("INNER JOIN eic t8 ON t3.id_eic = t8.id_eic")
	if proses == "Penyesuaian Penilaian" {
		q.Joins("INNER JOIN penyesuaian_nilai_asessor_evaluator t_eval ON t_eval.id_penugasan_penilaian_evaluator = t1.id_penugasan_penilaian_evaluator")
	}
	q.Where("t1.id_personal = ?", id_personal)
	q.Where("t1.sts_penerimaan_penugasan = ?", "1")
	q.Where("t1.id_penugasan_penilaian_evaluator NOT IN ( ? )", sub_q)
	if search != "" {
		q.Where("lower(t4.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
		// lower(t4.nama_jurnal) ~* lower(p_param_search) or lower(t4.eissn) ~* lower(p_param_search)
	}
	q.Order("t1.tgl_created desc")
	q.Order("t4.nama_jurnal asc")
	data := db.Table("( ? ) as temp", q)
	if proses == "Belum Dinilai" {
		data.Where("temp.nilai_evaluasi is null")
		data.Where("temp.nilai_disinsentif is null")
	} else if proses == "Proses Penilaian" {
		data.Where("temp.nilai_evaluasi is not null").Or("temp.nilai_disinsentif is not null")
	}
	data.Find(&penugasan_penilaian)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(penugasan_penilaian)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&penugasan_penilaian); err != nil {
		panic(err)
	}

	for i, v := range penugasan_penilaian {
		penugasan_penilaian[i].Issue = listIssueByUsulanAkreditasi(v.IDUsulanAkreditasi)
		penugasan_penilaian[i].NilaiEvaluator = v.NilaiDisinsentif + v.NilaiEvaluasi
	}

	res := helper.BuildPaginationResponse(penugasan_penilaian, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func prosesPenilaianIssue(search string, page string, row string, id_personal string, proses string) (helper.ResponsePagination, error) {
	var penugasan_penilaian []entity.KSPenugasanPenilaianIssue

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	nilai_disinsentif := db.Table("disinsentif_penilaian_issue as nilai_disinsentif")
	nilai_disinsentif.Select("nilai_disinsentif.nilai")
	nilai_disinsentif.Where("nilai_disinsentif.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue")

	nilai_issue := db.Table("hasil_penilaian_issue as nilai_issue")
	nilai_issue.Select("SUM(nilai_issue.nilai) as nilai_issue")
	nilai_issue.Joins("INNER JOIN sub_unsur_penilaian as sub on nilai_issue.id_sub_unsur_penilaian = sub.id_sub_unsur_penilaian")
	nilai_issue.Where("nilai_issue.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue")
	nilai_issue.Where("sub.id_kelompok_unsur_penilaian = ?", 1)

	selisih_penilaian := db.Raw("select array_to_json(array_agg(tc.*)) from public.arjuna_check_status_selisih_nilai_by_id_issue(t1.id_issue::integer) as tc")
	pasangan_asessor := db.Raw("select tc2.* from public.arjuna_get_pasangan_asessor(t1.id_usulan_akreditasi::integer,( ? )::bigint) as tc2", id_personal)
	komentar_penyesuaian_nilai := db.Raw("select tk.komentar_penyesuaian from public.penyesuaian_nilai_asessor_issue tk where tk.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue AND tk.status_penyesuaian = true limit 1")

	q := db.Table("penugasan_penilaian_issue as t1")
	q.Select("t1.id_penugasan_penilaian_issue, t1.id_personal, t4.id_jurnal, t1.id_issue, t1.id_usulan_akreditasi, t1.tgl_created::date as tgl_penugasan, t1.tgl_penerimaan_penugasan::date as tgl_penerimaan, t5.url_statistik_pengunjung, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society, t5.nama_awal_jurnal, t5.tgl_pembuatan, t5.url_jurnal,  t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat, t5.no_telepon, t5.alamat_surel, t5.tgl_akhir_terakreditasi, t7.nama as nama_pic, t7.nama_institusi, t8.nama_eic, t8.alamat_surel as alamat_surel_eic, t2.url_issue, t2.judul_issue, t2.volume_issue, t2.nomor_issue, t2.tahun_issue, t4.alamat_oai, t4.doi_jurnal, t9.user_penilai, t9.passwd_penilai as password_penilai, ( ? ) as nilai_konten, ( ? ) as nilai_disinsentif, ( ? ) as selisih_penilaian, ( ? ) as pasangan_asessor, ( ? ) as komentar_penyesuaian_nilai", nilai_issue, nilai_disinsentif, selisih_penilaian, pasangan_asessor, komentar_penyesuaian_nilai)
	q.Joins("INNER JOIN issue t2 ON t1.id_issue = t2.id_issue")
	q.Joins("INNER JOIN identitas_jurnal t4 ON t2.id_identitas_jurnal = t4.id_identitas_jurnal")
	q.Joins("INNER JOIN jurnal t5 ON t4.id_jurnal = t5.id_jurnal")
	q.Joins("INNER JOIN pic t6 ON t4.id_identitas_jurnal = t6.id_identitas_jurnal AND t6.sts_aktif_pic = ?", "1")
	q.Joins("INNER JOIN personal t7 ON t6.id_personal = t7.id_personal")
	q.Joins("INNER JOIN eic t8 ON t4.id_identitas_jurnal = t8.id_identitas_jurnal AND t8.sts_aktif_eic = ?", "1")
	q.Joins("INNER JOIN usulan_akreditasi t9 ON t1.id_usulan_akreditasi = t9.id_usulan_akreditasi")
	q.Joins("LEFT JOIN hasil_penilaian_issue ta ON ta.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue AND ta.sts_penetapan_penilaian = ?", "1")
	if proses == "Penyesuaian Penilaian" {
		q.Joins("INNER JOIN penyesuaian_nilai_asessor_issue t_penyesuaian_nilai ON t_penyesuaian_nilai.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue AND t_penyesuaian_nilai.status_penyesuaian = true")
	}
	q.Where("t1.id_personal = ?", id_personal)
	q.Where("t1.sts_pelaksanaan_tugas = ?", "0")
	q.Where("t1.sts_penerimaan_penugasan = ?", "1")
	q.Where("ta.id_penugasan_penilaian_issue IS NULL")
	if search != "" {
		q.Where("lower(t4.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
		// lower(t4.nama_jurnal) ~* lower(p_param_search) or lower(t4.eissn) ~* lower(p_param_search)
	}
	q.Order("t1.tgl_created desc")
	q.Order("t4.nama_jurnal asc")
	data := db.Table("( ? ) as temp", q)
	if proses == "Belum Dinilai" {
		data.Where("temp.nilai_konten is null")
		data.Where("temp.nilai_disinsentif is null")
	} else if proses == "Proses Penilaian" {
		data.Where("temp.nilai_konten is not null").Or("temp.nilai_disinsentif is not null")
	}
	data.Find(&penugasan_penilaian)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(penugasan_penilaian)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&penugasan_penilaian); err != nil {
		panic(err)
	}

	for i, v := range penugasan_penilaian {
		penugasan_penilaian[i].NilaiIssue = v.NilaiDisinsentif + v.NilaiKonten
	}

	res := helper.BuildPaginationResponse(penugasan_penilaian, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func ListPenilaianSelesai(search string, page string, row string, penilai string, id_personal string) (interface{}, error) {
	var res interface{}
	var err error

	if penilai == "management" {
		data, errs := penilaianSelesaiManagement(search, page, row, id_personal)
		res = data
		err = errs
	} else if penilai == "evaluator" {
		data, errs := penilaianSelesaiEvaluator(search, page, row, id_personal)
		res = data
		err = errs
	} else if penilai == "issue" {
		data, errs := penilaianSelesaiIssue(search, page, row, id_personal)
		res = data
		err = errs
	}
	return res, err
}

func penilaianSelesaiManagement(search string, page string, row string, id_personal string) (helper.ResponsePagination, error) {
	var penilaian_selesai []entity.PenilaianSelesaiManagement

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	hasil_penilaian_mgmt := db.Table("hasil_penilaian_mgmt as ta")
	hasil_penilaian_mgmt.Distinct("ta.id_penugasan_penilaian_manajemen, ta.tgl_updated::date as tgl_penetapan_penilaian")
	hasil_penilaian_mgmt.Where("ta.sts_penetapan_penilaian = ?", "1")

	sub_query := db.Table("hasil_penilaian_mgmt as hpm")
	sub_query.Select("SUM(hpm.nilai)")
	sub_query.Where("hpm.id_penugasan_penilaian_manajemen = t1.id_penugasan_penilaian_manajemen")
	sub_query.Group("hpm.id_penugasan_penilaian_manajemen")

	data := db.Table("penugasan_penilaian_mgmt as t1")
	data.Select("t1.id_penugasan_penilaian_manajemen, t1.id_usulan_akreditasi, t1.tgl_created::date as tgl_penugasan, t1.tgl_penerimaan_penugasan::date as tgl_penerimaan_penugasan, t2.tgl_penetapan_penilaian, t5.url_statistik_pengunjung, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society, t5.nama_awal_jurnal, t5.tgl_pembuatan, t5.url_jurnal, t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat, t5.no_telepon, t5.alamat_surel, t5.tgl_akhir_terakreditasi, t7.nama as nama_pic, t7.nama_institusi, t8.nama_eic, t8.alamat_surel as alamat_surel_eic, t4.alamat_oai, t4.doi_jurnal, t9.nilai as nilai_disinsentif, ( ? ) as hasil_penilaian_mgmt ", sub_query)
	data.Joins("INNER JOIN ( ? ) as t2 ON t1.id_penugasan_penilaian_manajemen = t2.id_penugasan_penilaian_manajemen", hasil_penilaian_mgmt)
	data.Joins("INNER JOIN usulan_akreditasi t3 ON t1.id_usulan_akreditasi = t3.id_usulan_akreditasi")
	data.Joins("INNER JOIN identitas_jurnal t4 ON t3.id_identitas_jurnal = t4.id_identitas_jurnal")
	data.Joins("INNER JOIN jurnal t5 ON t4.id_jurnal = t5.id_jurnal")
	data.Joins("INNER JOIN pic t6 ON t3.id_pic = t6.id_pic")
	data.Joins("INNER JOIN personal t7 ON t6.id_personal = t7.id_personal")
	data.Joins("INNER JOIN eic t8 ON t3.id_eic = t8.id_eic")
	data.Joins("LEFT JOIN disinsentif_penilaian_mgmt as t9 ON t9.id_penugasan_penilaian_manajemen = t1.id_penugasan_penilaian_manajemen")
	data.Where("t1.id_personal = ?", id_personal)
	data.Where("t1.sts_penerimaan_penugasan = ?", "1")
	if search != "" {
		data.Where("lower(t4.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Find(&penilaian_selesai)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(penilaian_selesai)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&penilaian_selesai); err != nil {
		panic(err)
	}

	res := helper.BuildPaginationResponse(penilaian_selesai, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func penilaianSelesaiEvaluator(search string, page string, row string, id_personal string) (helper.ResponsePagination, error) {
	var penilaian_selesai []entity.PenilaianSelesaiEvaluator

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	hasil_penilaian_evaluator := db.Table("hasil_penilaian_evaluator as ta")
	hasil_penilaian_evaluator.Distinct("ta.id_penugasan_penilaian_evaluator, ta.tgl_updated::date as tgl_penetapan_penilaian")
	hasil_penilaian_evaluator.Where("ta.sts_penetapan_penilaian = ?", "1")

	sub_query := db.Table("hasil_penilaian_evaluator as hpm")
	sub_query.Select("SUM(hpm.nilai)")
	sub_query.Where("hpm.id_penugasan_penilaian_evaluator = t1.id_penugasan_penilaian_evaluator")
	sub_query.Group("hpm.id_penugasan_penilaian_evaluator")

	data := db.Table("penugasan_penilaian_evaluator as t1")
	data.Select("t1.id_penugasan_penilaian_evaluator, t1.id_usulan_akreditasi, t1.tgl_created::date as tgl_penugasan, t1.tgl_penerimaan_penugasan::date as tgl_penerimaan_penugasan, t2.tgl_penetapan_penilaian, t5.url_statistik_pengunjung, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society, t5.nama_awal_jurnal, t5.tgl_pembuatan, t5.url_jurnal, t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat, t5.no_telepon, t5.alamat_surel, t5.tgl_akhir_terakreditasi, t7.nama as nama_pic, t7.nama_institusi, t8.nama_eic, t8.alamat_surel as alamat_surel_eic, t4.alamat_oai, t4.doi_jurnal, t9.nilai as nilai_disinsentif, ( ? ) as hasil_penilaian_evaluator ", sub_query)
	data.Joins("INNER JOIN ( ? ) as t2 ON t1.id_penugasan_penilaian_evaluator = t2.id_penugasan_penilaian_evaluator", hasil_penilaian_evaluator)
	data.Joins("INNER JOIN usulan_akreditasi t3 ON t1.id_usulan_akreditasi = t3.id_usulan_akreditasi")
	data.Joins("INNER JOIN identitas_jurnal t4 ON t3.id_identitas_jurnal = t4.id_identitas_jurnal")
	data.Joins("INNER JOIN jurnal t5 ON t4.id_jurnal = t5.id_jurnal")
	data.Joins("INNER JOIN pic t6 ON t3.id_pic = t6.id_pic")
	data.Joins("INNER JOIN personal t7 ON t6.id_personal = t7.id_personal")
	data.Joins("INNER JOIN eic t8 ON t3.id_eic = t8.id_eic")
	data.Joins("LEFT JOIN disinsentif_penilaian_evaluator as t9 ON t9.id_penugasan_penilaian_evaluator = t1.id_penugasan_penilaian_evaluator")
	data.Where("t1.id_personal = ?", id_personal)
	data.Where("t1.sts_penerimaan_penugasan = ?", "1")
	if search != "" {
		data.Where("lower(t4.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Find(&penilaian_selesai)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(penilaian_selesai)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&penilaian_selesai); err != nil {
		panic(err)
	}

	res := helper.BuildPaginationResponse(penilaian_selesai, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func penilaianSelesaiIssue(search string, page string, row string, id_personal string) (helper.ResponsePagination, error) {
	var penilaian_selesai []entity.PenilaianSelesaiIssue

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	hasil_penilaian_issue := db.Table("hasil_penilaian_issue as ta")
	hasil_penilaian_issue.Distinct("ta.id_penugasan_penilaian_issue, ta.tgl_updated::date as tgl_penetapan_penilaian")
	hasil_penilaian_issue.Where("ta.sts_penetapan_penilaian = ?", "1")

	sub_query := db.Table("hasil_penilaian_issue as hpm")
	sub_query.Select("SUM(hpm.nilai)")
	sub_query.Where("hpm.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue")
	sub_query.Group("hpm.id_penugasan_penilaian_issue")

	data := db.Table("penugasan_penilaian_issue as t1")
	data.Select("t1.id_penugasan_penilaian_issue, t1.id_usulan_akreditasi, t1.tgl_created::date as tgl_penugasan, t1.tgl_penerimaan_penugasan::date as tgl_penerimaan_penugasan, t2.tgl_penetapan_penilaian, t5.url_statistik_pengunjung, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t4.society, t5.nama_awal_jurnal, t5.tgl_pembuatan, t5.url_jurnal, t5.url_contact, t5.url_editor, t5.country, t5.city, t5.alamat, t5.no_telepon, t5.alamat_surel, t5.tgl_akhir_terakreditasi, t7.nama as nama_pic, t7.nama_institusi, t8.nama_eic, t8.alamat_surel as alamat_surel_eic, t4.alamat_oai, t4.doi_jurnal, t9.nilai as nilai_disinsentif, ( ? ) as hasil_penilaian_issue ", sub_query)
	data.Joins("INNER JOIN ( ? ) as t2 ON t1.id_penugasan_penilaian_issue = t2.id_penugasan_penilaian_issue", hasil_penilaian_issue)
	data.Joins("INNER JOIN usulan_akreditasi t3 ON t1.id_usulan_akreditasi = t3.id_usulan_akreditasi")
	data.Joins("INNER JOIN identitas_jurnal t4 ON t3.id_identitas_jurnal = t4.id_identitas_jurnal")
	data.Joins("INNER JOIN jurnal t5 ON t4.id_jurnal = t5.id_jurnal")
	data.Joins("INNER JOIN pic t6 ON t3.id_pic = t6.id_pic")
	data.Joins("INNER JOIN personal t7 ON t6.id_personal = t7.id_personal")
	data.Joins("INNER JOIN eic t8 ON t3.id_eic = t8.id_eic")
	data.Joins("LEFT JOIN disinsentif_penilaian_issue as t9 ON t9.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue")
	data.Where("t1.id_personal = ?", id_personal)
	data.Where("t1.sts_penerimaan_penugasan = ?", "1")
	if search != "" {
		data.Where("lower(t4.nama_jurnal) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Find(&penilaian_selesai)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(penilaian_selesai)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&penilaian_selesai); err != nil {
		panic(err)
	}

	res := helper.BuildPaginationResponse(penilaian_selesai, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func DetailPenilaian(id string, penilai string) (interface{}, error) {
	var res interface{}
	var err error

	if penilai == "management" {
		data, errs := detailPenilaianManagement(id)
		res = data
		err = errs
	} else if penilai == "evaluator" {
		data, errs := detailPenilaianEvaluator(id)
		res = data
		err = errs
	} else if penilai == "issue" {
		data, errs := detailPenilaianIssue(id)
		res = data
		err = errs
	}
	return res, err
}

func detailPenilaianManagement(id_penugasan_penilaian string) (interface{}, error) {
	var penilaian entity.DetailPenilaian
	var check_status int64
	db := config.Con()
	defer config.CloseCon(db)

	nilai_disinsentif := db.Table("disinsentif_penilaian_mgmt as nilai_disinsentif")
	nilai_disinsentif.Select("nilai_disinsentif.nilai")
	nilai_disinsentif.Where("nilai_disinsentif.id_penugasan_penilaian_manajemen = t1.id_penugasan_penilaian_manajemen")

	nilai_mgmt := db.Table("hasil_penilaian_mgmt as nilai_mgmt")
	nilai_mgmt.Select("SUM(nilai_mgmt.nilai) as nilai_management")
	nilai_mgmt.Joins("INNER JOIN sub_unsur_penilaian as sub on nilai_mgmt.id_sub_unsur_penilaian = sub.id_sub_unsur_penilaian")
	nilai_mgmt.Where("nilai_mgmt.id_penugasan_penilaian_manajemen = t1.id_penugasan_penilaian_manajemen")
	nilai_mgmt.Where("sub.id_kelompok_unsur_penilaian = ?", 2)

	data := db.Table("penugasan_penilaian_mgmt as t1")
	data.Select("t3.id_identitas_jurnal, t2.id_usulan_akreditasi, t3.nama_jurnal, t3.eissn, t3.pissn, t4.url_jurnal, t2.user_penilai, t2.passwd_penilai as password_penilai, ( ? ) as nilai, ( ? ) as nilai_disinsentif", nilai_mgmt, nilai_disinsentif)
	data.Joins("INNER JOIN usulan_akreditasi t2 ON t1.id_usulan_akreditasi = t2.id_usulan_akreditasi")
	data.Joins("INNER JOIN identitas_jurnal t3 ON t3.id_identitas_jurnal = t2.id_identitas_jurnal")
	data.Joins("INNER JOIN jurnal t4 ON t4.id_jurnal = t3.id_jurnal")
	data.Where("t1.id_penugasan_penilaian_manajemen = ?", id_penugasan_penilaian)
	data.Take(&penilaian)

	db.Table("usulan_akreditasi").Where("id_usulan_akreditasi != ?", penilaian.IDUsulanAkreditasi).Where("id_identitas_jurnal = ?", penilaian.IDIdentitasJurnal).Where("sts_pengajuan_usulan = ?", "1").Count(&check_status)
	if check_status == 0 {
		penilaian.Status = "Usulan Baru"
	} else {
		penilaian.Status = "Re-akreditasi"
	}
	penilaian.Issue = listIssueByUsulanAkreditasi(penilaian.IDUsulanAkreditasi)
	penilaian.NilaiTotal = penilaian.NilaiDisinsentif + penilaian.Nilai
	if data.Error != nil {
		return entity.DetailPenilaian{}, data.Error
	}

	return penilaian, nil
}

func detailPenilaianEvaluator(id_penugasan_penilaian string) (interface{}, error) {
	var penilaian entity.DetailPenilaian
	var check_status int64
	db := config.Con()
	defer config.CloseCon(db)

	nilai_disinsentif := db.Table("disinsentif_penilaian_evaluator as nilai_disinsentif")
	nilai_disinsentif.Select("nilai_disinsentif.nilai")
	nilai_disinsentif.Where("nilai_disinsentif.id_penugasan_penilaian_evaluator = t1.id_penugasan_penilaian_evaluator")

	nilai_evaluator := db.Table("hasil_penilaian_evaluator as nilai_evaluator")
	nilai_evaluator.Select("SUM(nilai_evaluator.nilai) as nilai_evaluasi")
	nilai_evaluator.Joins("INNER JOIN sub_unsur_penilaian as sub on nilai_evaluator.id_sub_unsur_penilaian = sub.id_sub_unsur_penilaian")
	nilai_evaluator.Where("nilai_evaluator.id_penugasan_penilaian_evaluator = t1.id_penugasan_penilaian_evaluator")
	nilai_evaluator.Where("sub.id_kelompok_unsur_penilaian = ?", 2)

	data := db.Table("penugasan_penilaian_evaluator as t1")
	data.Select("t3.id_identitas_jurnal, t2.id_usulan_akreditasi, t3.nama_jurnal, t3.eissn, t3.pissn, t4.url_jurnal, t2.user_penilai, t2.passwd_penilai as password_penilai, ( ? ) as nilai, ( ? ) as nilai_disinsentif", nilai_evaluator, nilai_disinsentif)
	data.Joins("INNER JOIN usulan_akreditasi t2 ON t1.id_usulan_akreditasi = t2.id_usulan_akreditasi")
	data.Joins("INNER JOIN identitas_jurnal t3 ON t3.id_identitas_jurnal = t2.id_identitas_jurnal")
	data.Joins("INNER JOIN jurnal t4 ON t4.id_jurnal = t3.id_jurnal")
	data.Where("t1.id_penugasan_penilaian_evaluator = ?", id_penugasan_penilaian)
	data.Take(&penilaian)

	db.Table("usulan_akreditasi").Where("id_usulan_akreditasi != ?", penilaian.IDUsulanAkreditasi).Where("id_identitas_jurnal = ?", penilaian.IDIdentitasJurnal).Where("sts_pengajuan_usulan = ?", "1").Count(&check_status)
	if check_status == 0 {
		penilaian.Status = "Usulan Baru"
	} else {
		penilaian.Status = "Re-akreditasi"
	}
	penilaian.Issue = listIssueByUsulanAkreditasi(penilaian.IDUsulanAkreditasi)
	penilaian.NilaiTotal = penilaian.NilaiDisinsentif + penilaian.Nilai
	if data.Error != nil {
		return entity.DetailPenilaian{}, data.Error
	}

	return penilaian, nil
}

func detailPenilaianIssue(id_penugasan_penilaian string) (interface{}, error) {
	var penilaian entity.DetailPenilaianIssue
	var check_status int64
	db := config.Con()
	defer config.CloseCon(db)

	nilai_issue := db.Table("hasil_penilaian_issue as nilai_issue")
	nilai_issue.Select("SUM(nilai_issue.nilai) as nilai_subs")
	nilai_issue.Joins("INNER JOIN sub_unsur_penilaian as sub on nilai_issue.id_sub_unsur_penilaian = sub.id_sub_unsur_penilaian")
	nilai_issue.Where("nilai_issue.id_penugasan_penilaian_issue = t1.id_penugasan_penilaian_issue")
	nilai_issue.Where("sub.id_kelompok_unsur_penilaian = ?", 1)

	data := db.Table("penugasan_penilaian_issue as t1")
	data.Select("t1.id_penugasan_penilaian_issue,t1.id_issue, t2.id_jurnal, t4.nama_jurnal, t4.eissn, t4.pissn, t4.publisher, t2.id_identitas_jurnal, t2.judul_issue, t2.volume_issue, t2.nomor_issue, t2.tahun_issue, t2.url_issue, t1.id_usulan_akreditasi, t5.user_penilai, t5.passwd_penilai as password_penilai, ( ? ) as nilai_issue", nilai_issue)
	data.Joins("INNER JOIN issue t2 ON t1.id_issue = t2.id_issue")
	data.Joins("INNER JOIN identitas_jurnal t4 ON t2.id_identitas_jurnal = t4.id_identitas_jurnal")
	data.Joins("INNER JOIN usulan_akreditasi t5 ON t1.id_usulan_akreditasi = t5.id_usulan_akreditasi")
	data.Where("t1.id_penugasan_penilaian_issue = ?", id_penugasan_penilaian)
	data.Take(&penilaian)

	db.Table("usulan_akreditasi").Where("id_usulan_akreditasi != ?", penilaian.IDUsulanAkreditasi).Where("id_identitas_jurnal = ?", penilaian.IDIdentitasJurnal).Where("sts_pengajuan_usulan = ?", "1").Count(&check_status)
	if check_status == 0 {
		penilaian.Status = "Usulan Baru"
	} else {
		penilaian.Status = "Re-akreditasi"
	}
	if data.Error != nil {
		return entity.DetailPenilaian{}, data.Error
	}

	return penilaian, nil
}
