package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/vcraescu/go-paginator/v2"
	"github.com/vcraescu/go-paginator/v2/adapter"
	"github.com/vcraescu/go-paginator/v2/view"
)

func ListSlider(search string, page string, row string, baseUrl string) (helper.ResponsePagination, error) {
	var slider []entity.Slider

	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("sliders")
	if search != "" {
		data.Where("lower(title) LIKE ?", "%"+strings.ToLower(search)+"%")
	}
	data.Order("torder ASC")
	data.Find(&slider)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(slider)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&slider); err != nil {
		panic(err)
	}

	for i, v := range slider {
		path := baseUrl + "/file/slider/"
		if v.Imagename != "" {
			slider[i].PathImagename = path + v.Imagename
		}
	}
	res := helper.BuildPaginationResponse(slider, view)
	res.PerPage = rows
	res.Total = total
	return res, nil
}

func InsertSlider(req entity.SliderDTO, c *fiber.Ctx) (entity.Slider, error) {
	var slider entity.Slider
	var torder int
	db := config.Con()
	defer config.CloseCon(db)

	db.Table("sliders").Order("torder desc").Limit(1).Pluck("torder", &torder)

	date, errors := time.Parse("2006-01-02", req.DateLast)
	if errors != nil {
		return entity.Slider{}, errors
	}

	slider.Title = req.Title
	slider.SubTitle = req.SubTitle
	slider.Url = req.Url
	slider.DateLast = date
	slider.Torder = int64(torder) + 1
	slider.DateCreated = time.Now()
	if req.Imagename != nil {
		filename, _ := helper.UploadFile("./files/slider", req.Imagename, c)
		slider.Imagename = filename
	}

	saveSlider := db.Table("sliders").Save(&slider)

	if saveSlider.Error != nil {
		return entity.Slider{}, saveSlider.Error
	}

	return slider, nil
}

func ShowSlider(id int, baseUrl string) (interface{}, error) {
	var slider entity.Slider
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("sliders")
	data.Where("id = ?", id)
	data.Take(&slider)
	if data.Error != nil {
		return nil, data.Error
	}

	path := baseUrl + "/file/slider/"
	if slider.Imagename != "" {
		slider.PathImagename = path + slider.Imagename
	}
	return slider, nil
}

func UpdateSlider(req entity.SliderDTO, old entity.Slider, id int, c *fiber.Ctx) (entity.Slider, error) {
	var slider entity.Slider
	db := config.Con()
	defer config.CloseCon(db)

	date, errors := time.Parse("2006-01-02", req.DateLast)
	if errors != nil {
		return entity.Slider{}, errors
	}

	slider.ID = int64(id)
	slider.Title = req.Title
	slider.SubTitle = req.SubTitle
	slider.Url = req.Url
	slider.DateLast = date
	slider.Torder = old.Torder
	slider.DateCreated = old.DateCreated
	if req.Imagename != nil {
		os.Remove("./files/slider/" + old.Imagename)
		filename, _ := helper.UploadFile("./files/slider", req.Imagename, c)
		slider.Imagename = filename
	} else {
		slider.Imagename = old.Imagename
	}

	saveSlider := db.Table("sliders").Save(&slider)

	if saveSlider.Error != nil {
		return entity.Slider{}, saveSlider.Error
	}

	return slider, nil
}
