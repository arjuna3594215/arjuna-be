package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
)

func ListMenu() ([]entity.Menu, error) {
	var Parent []entity.Menu

	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("menus").Where("type = ?", "Parent").Order("id asc").Find(&Parent)
	for i, v := range Parent {
		Parent[i].Permission = getPermissionByMenu(int(v.ID))
	}

	if data.Error != nil {
		return nil, data.Error
	}

	for i, v := range Parent {
		var child []entity.Menu
		db.Table("menus").Where("type = ?", "Child").Where("id_parent = ?", v.ID).Find(&child)
		if len(child) < 1 {
			Parent[i].Type = "Link"
		}
		for i, v := range child {
			child[i].AB = v.Name[0:1]
			child[i].Permission = getPermissionByMenu(int(v.ID))
		}
		Parent[i].Child = child
	}

	return Parent, nil
}

func getPermissionByMenu(id_menu int) string {
	var permission entity.Permission

	db := config.Con()
	defer config.CloseCon(db)

	db.Table("permissions").Where("id_menu = ?", id_menu).Take(&permission)
	return permission.Name
}
