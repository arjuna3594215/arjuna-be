package service

import (
	"arjuna-api/config"
	"arjuna-api/entity"
	"arjuna-api/helper"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/vcraescu/go-paginator/v2"
	"github.com/vcraescu/go-paginator/v2/adapter"
	"github.com/vcraescu/go-paginator/v2/view"
)

func ListUser(search string, kewenangan string, page string, row string, active string) (l_user helper.ResponsePagination, err error) {
	var user []entity.User
	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("pengguna as p")
	if kewenangan != "" {
		data.Distinct("p.id_pengguna, p.id_personal, p.username, psl.nama, k.kewenangan")
		data.Joins("inner join kewenangan_pengguna as kp on kp.id_pengguna = p.id_pengguna AND kp.id_kewenangan = ?", kewenangan)
		data.Joins("inner join kewenangan as k on k.id_kewenangan = kp.id_kewenangan")
		data.Where("kp.sts_aktif_kewenangan_pengguna = ?", "1")
	} else {
		data.Distinct("p.id_pengguna, p.id_personal, p.username, psl.nama")
	}
	data.Joins("inner join personal as psl on psl.id_personal = p.id_personal")
	data.Where("p.sts_aktif_pengguna = ?", active)
	data.Where("psl.sts_aktif_personal = ?", active)
	if search != "" {
		data.Where("lower(psl.nama) LIKE ?", "%"+strings.ToLower(search)+"%")
		data.Or("p.username = ?", search)
	}
	data.Order("p.id_pengguna asc")
	// data.Group("p.id_pengguna")
	// data.Limit(20)
	data.Find(&user)
	if err != nil {
		return helper.ResponsePagination{}, err
	}

	total := len(user)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&user); err != nil {
		panic(err)
	}
	res := helper.BuildPaginationResponse(user, view)
	res.PerPage = rows
	res.Total = total

	return res, nil
}

func InsertUser(req entity.UserDTO) (res interface{}, err error) {
	var personal entity.Personal
	var pengguna entity.Pengguna
	var dataMailRegistration entity.DataMailRegistration

	db := config.Con()
	defer config.CloseCon(db)

	// Save Personal
	personal.Nama = req.Nama
	personal.NamaInstitusi = req.NamaInstitusi
	personal.Email = req.Email
	personal.NOHandPhone = req.NOHandphone
	// personal.IDNegara
	personal.StsAktifPersonal = "1"
	savePersonal, errInserPersonal := InsertPersonal(personal)
	if errInserPersonal != nil {
		return nil, errInserPersonal
	}
	dataPersonal := savePersonal.(entity.Personal)
	// Save Pengguna
	pengguna.IDPersonal = dataPersonal.IDPersonal
	pengguna.Username = req.Username
	pengguna.Password = config.EncryptPassword(req.Password)
	pengguna.StsAktifPengguna = "1"
	savePengguna, errInsertPengguna := InsertPengguna(pengguna)
	if errInsertPengguna != nil {
		return nil, errInsertPengguna
	}

	dataMailRegistration.Name = dataPersonal.Nama
	dataMailRegistration.Email = dataPersonal.Email
	dataMailRegistration.Username = pengguna.Username
	dataMailRegistration.Date = time.Now().Year()
	go func() {
		err := helper.SendMailRegistration(dataMailRegistration)
		if err != nil {
			log.Println(err)
		}
	}()
	dataPengguna := savePengguna.(entity.Pengguna)
	// Insert Kode Aktivasi
	currentTime := time.Now()
	kode_activasi := dataPersonal.Nama + "|" + currentTime.Format("2006-01-0215:04:05")
	db.Table("aktivasi").Create(map[string]interface{}{"id_pengguna": dataPengguna.IDPengguna, "kd_aktivasi": config.EncryptPassword(kode_activasi)})
	// Insert Kewenangan
	db.Table("kewenangan_pengguna").Create(map[string]interface{}{"id_pengguna": dataPengguna.IDPengguna, "id_kewenangan": 3, "sts_aktif_kewenangan_pengguna": "1"})
	return dataPersonal, nil
}

func ShowUser(id int) (d_user interface{}, err error) {
	var user entity.User
	db := config.Con()
	defer config.CloseCon(db)

	data := db.Table("pengguna")
	data.Select("pengguna.*, personal.email, personal.nama, personal.nama_institusi, personal.no_hand_phone")
	data.Joins("inner join personal on personal.id_personal = pengguna.id_personal")
	data.Where("pengguna.id_pengguna = ?", id)
	data.Take(&user)
	if data.Error != nil {
		return nil, data.Error
	}
	return user, nil
}

func UpdateUser(req entity.UserUpdateDTO, id_pengguna int, id_personal int) (d_user interface{}, err error) {
	var personal entity.Personal
	var pengguna entity.Pengguna
	db := config.Con()
	defer config.CloseCon(db)

	// Update Personal
	personal.IDPersonal = int64(id_personal)
	personal.Nama = req.Nama
	personal.NamaInstitusi = req.NamaInstitusi
	personal.Email = req.Email
	personal.NOHandPhone = req.NOHandphone
	// personal.IDNegara
	if req.Password != "" {
		personal.StsAktifPersonal = "1"
	}
	savePersonal, errUpdatePersonal := UpdatePersonal(personal)
	if errUpdatePersonal != nil {
		return nil, errUpdatePersonal
	}

	if req.Password != "" {
		pengguna.IDPengguna = int64(id_pengguna)
		pengguna.Password = config.EncryptPassword(req.Password)
		pengguna.StsAktifPengguna = "1"
		_, errUpdatePengguna := UpdatePengguna(pengguna)
		if errUpdatePengguna != nil {
			return nil, errUpdatePengguna
		}
		// db.Table("aktivasi").Where("id_pengguna = ?", id_pengguna).Delete()
	}
	return savePersonal, nil
}

func UpdateKewenanganUser(req entity.KewenanganUserDTO, id int) (interface{}, error) {
	var count int64
	db := config.Con()
	defer config.CloseCon(db)

	db.Table("kewenangan_pengguna").Where("id_pengguna = ?", id).Where("id_kewenangan = ?", req.IDKewenangan).Count(&count)
	if count > 0 {
		update := db.Table("kewenangan_pengguna").Where("id_pengguna = ?", id).Where("id_kewenangan = ?", req.IDKewenangan).Update("sts_aktif_kewenangan_pengguna", req.StsAktifKewenanganPengguna)
		if update.Error != nil {
			return nil, update.Error
		}
	} else {
		save := db.Table("kewenangan_pengguna").Create(map[string]interface{}{"id_pengguna": id, "id_kewenangan": req.IDKewenangan, "sts_aktif_kewenangan_pengguna": "1"})
		if save.Error != nil {
			return nil, save.Error
		}
	}
	return true, nil
}

func ListUserAssessor(search string, kewenangan string, page string, row string, filter string, id_usulan_akreditasi string) (helper.ResponsePagination, error) {
	var user []entity.ListUserAssessor
	var id_personal []int64
	rows, _ := strconv.Atoi(row)
	pages, _ := strconv.Atoi(page)

	db := config.Con()
	defer config.CloseCon(db)

	if kewenangan == "5" {
		db.Table("penugasan_penilaian_mgmt").Select("id_personal").Where("id_usulan_akreditasi = ?", id_usulan_akreditasi).Find(&id_personal)

	} else if kewenangan == "7" {
		db.Table("penugasan_penilaian_evaluator").Select("id_personal").Where("id_usulan_akreditasi = ?", id_usulan_akreditasi).Find(&id_personal)
	}

	data := db.Table("kewenangan_pengguna AS t1")
	if filter == "1" {
		data.Distinct("t2.id_pengguna, t3.id_personal, t3.nama, t2.username, t3.nama_institusi, t3.email, t3.no_hand_phone, t4.kewenangan")
	} else {
		data.Select("t2.id_pengguna, t3.id_personal, t3.nama, t2.username, t3.nama_institusi, t3.email, t3.no_hand_phone, t4.kewenangan")
	}
	data.Joins("INNER JOIN pengguna AS t2 ON t2.id_pengguna = t1.id_pengguna")
	data.Joins("INNER JOIN personal AS t3 ON t3.id_personal = t2.id_personal")
	data.Joins("INNER JOIN kewenangan AS t4 ON t4.id_kewenangan = t1.id_kewenangan")
	if search != "" {
		if filter == "1" {
			data.Joins("LEFT JOIN bidang_ilmu_interest AS t5 ON t5.id_personal = t3.id_personal")
			data.Joins("LEFT JOIN bidang_ilmu AS t6 ON t6.id_bidang_ilmu = t5.id_bidang_ilmu ")
			data.Where("lower(t6.bidang_ilmu) LIKE ?", "%"+strings.ToLower(search)+"%")
		} else {
			data.Where("lower(t3.nama) LIKE ?", "%"+strings.ToLower(search)+"%")
			// data.Or("t2.username = ?", search)
		}
	}
	if kewenangan != "4" {
		if len(id_personal) > 0 {
			data.Where("t3.id_personal NOT IN ?", id_personal)
		}
	}
	data.Where("t1.sts_aktif_kewenangan_pengguna = ?", "1")
	data.Where("t1.id_kewenangan = ?", kewenangan)
	data.Where("t2.sts_aktif_pengguna = ?", "1")
	data.Where("t3.sts_aktif_personal = ?", "1")
	data.Order("t3.id_personal")
	data.Find(&user)
	if data.Error != nil {
		return helper.ResponsePagination{}, data.Error
	}

	total := len(user)
	p := paginator.New(adapter.NewGORMAdapter(data), rows)
	p.SetPage(pages)
	view := view.New(p)
	if err := p.Results(&user); err != nil {
		panic(err)
	}

	for i, v := range user {
		user[i].BidangIlmu = ListBidangIlmuByPersonal(int(v.IDPersonal))
	}

	res := helper.BuildPaginationResponse(user, view)
	res.PerPage = rows
	res.Total = total

	return res, nil
}
