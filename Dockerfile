FROM golang:alpine

ENV TZ="Asia/Jakarta"

RUN apk update && apk add --no-cache git

WORKDIR /app

COPY . .

#RUN go install github.com/swaggo/swag/cmd/swag@v1.8.6
#RUN swag init --parseDependency --parseInternal --parseDepth 1

RUN go mod tidy

RUN go build -o binary

ENTRYPOINT ["/app/binary"]

EXPOSE 8080
ENV PORT 8080
ENV HOST 0.0.0.0