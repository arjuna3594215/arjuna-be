package helper

import (
	"arjuna-api/entity"
	"bytes"
	"fmt"
	"html/template"
	"log"
	"net/smtp"
	"time"

	"github.com/jordan-wright/email"

	"gopkg.in/gomail.v2"
)

func SendForgotPasswordMail(req entity.DataMailForgotPass) error {
	Subject := "ARJUNA - Reset Password"
	templatePath := "view/email/forgot-pass.html"
	err := SendMailToUser(req.Email, Subject, templatePath, req)
	if err != nil {
		return nil
	}
	return nil
}

func SendMailRegistration(req entity.DataMailRegistration) error {
	Subject := "ARJUNA - Registrasi Akun"
	templatePath := "view/email/registration-mail.html"
	err := SendMailToUser(req.Email, Subject, templatePath, req)
	if err != nil {
		return nil
	}
	return nil
}

func SendKontakMail(req entity.Kontak) error {
	Subject := "ARJUNA - Kontak Message"
	templatePath := "view/email/kontak.html"
	err := SendMailToUser("arjuna@kemdikbud.go.id", Subject, templatePath, req)
	if err != nil {
		return nil
	}
	return nil
}
func SendPenugasanBaruMail(req entity.PenugasanBaruMail) error {
	subject := "Penugasan baru di Arjuna"
	templatePath := "view/email/penugasan_baru.html"
	err := SendMailToUser(req.EmailPIC, subject, templatePath, req)
	return err
}
func SendRejectUsulanMail(req entity.DataMailRejectUsulan) error {
	var Subject string
	if req.NamaJurnal != "" {
		Subject = "Usulan " + req.NamaJurnal + " ditolak"
	} else {
		Subject = "Usulan " + req.NamaAwalJurnal + " ditolak"
		req.NamaJurnal = req.NamaAwalJurnal
	}

	dt, _ := time.Parse("2006-01-02", req.TglUsulan)
	req.TglUsulan = dt.Format("2 January 2006")
	tahun := time.Now()
	req.Tahun = tahun.Format("2006")
	templatePath := "view/email/reject_usulan.html"
	err := SendMailToUser(req.EmailPic, Subject, templatePath, req)
	if err != nil {
		return nil
	}
	return nil
}

// with jordan-wright/email package
func SendMailToUser(to string, subject string, templatePath string, templateData interface{}) error {
	const CONFIG_SMTP_HOST = "smtp.gmail.com"
	const CONFIG_SMTP_PORT = 587
	const CONFIG_SENDER_NAME = "CS ARJUNA RISTEKDIKTI"
	//const CONFIG_SENDER_EMAIL = "anggamv11@gmail.com"
	//const CONFIG_EMAIL_PASSWORD = "hjvazjkfdsxwuswa"
	const CONFIG_SENDER_EMAIL = "cs.arjuna.ristekdikti@gmail.com"
	const CONFIG_EMAIL_PASSWORD = "cgficizyorngrbtq"

	t, _ := ParseTemplate(templatePath, templateData)

	mailer := email.NewEmail()
	mailer.From = fmt.Sprintf("%s <%s>", CONFIG_SENDER_NAME, to)
	mailer.Subject = subject
	mailer.HTML = []byte(t)
	mailer.To = []string{to}

	smtpAuth := smtp.PlainAuth("", CONFIG_SENDER_EMAIL, CONFIG_EMAIL_PASSWORD, CONFIG_SMTP_HOST)
	err := mailer.Send(CONFIG_SMTP_HOST+":"+fmt.Sprintf("%d", CONFIG_SMTP_PORT), smtpAuth)

	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func SendMail(to string, subject string, templatePath string, templateData interface{}) error {
	const CONFIG_SMTP_HOST = "sandbox.smtp.mailtrap.io"
	const CONFIG_SMTP_PORT = 2525
	const CONFIG_SENDER_NAME = "CS ARJUNA RISTEKDIKTI <emailanda@gmail.com>"
	const CONFIG_AUTH_EMAIL = "dbec2b4c0cbee4"
	const CONFIG_AUTH_PASSWORD = "3ade8823e2e048"

	// const CONFIG_SMTP_HOST = "smtp.gmail.com"
	// const CONFIG_SMTP_PORT = 587
	// const CONFIG_SENDER_NAME = "CS ARJUNA RISTEKDIKTI <cs.arjuna.ristekdikti@gmail.com>"
	// const CONFIG_AUTH_EMAIL = "cs.arjuna.ristekdikti@gmail.com"
	// const CONFIG_AUTH_PASSWORD = "arjunaristekdikti25.com"

	t, err := ParseTemplate(templatePath, templateData)
	if err != nil {
		log.Print(err)
		return err
	}

	mailer := gomail.NewMessage()
	mailer.SetHeader("From", CONFIG_SENDER_NAME)
	mailer.SetHeader("To", to)
	mailer.Embed("./public/arjuna_.png")
	//mailer.SetAddressHeader("Cc", "anggamv11@gmail.com", "Tra Lala La")
	mailer.SetHeader("Subject", subject)
	mailer.SetBody("text/html", t)
	// mailer.Attach("./sample.png")

	dialer := gomail.NewDialer(
		CONFIG_SMTP_HOST,
		CONFIG_SMTP_PORT,
		CONFIG_AUTH_EMAIL,
		CONFIG_AUTH_PASSWORD,
	)

	err = dialer.DialAndSend(mailer)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	return nil
}

func ParseTemplate(templateFileName string, data interface{}) (string, error) {
	t, err := template.ParseFiles(templateFileName)
	if err != nil {
		return "", err
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		fmt.Println(err)
		return "", err
	}
	return buf.String(), nil
}
