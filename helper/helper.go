package helper

import (
	"fmt"
	"math/rand"
	"mime/multipart"

	"github.com/gofiber/fiber/v2"
)

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func UploadFile(path string, file *multipart.FileHeader, c *fiber.Ctx) (string, error) {
	FilesName := RandStringRunes(5) + file.Filename
	// Save the files
	if err := c.SaveFile(file, fmt.Sprintf(path+"/%s", FilesName)); err != nil {
		return "", err
	}
	return FilesName, nil
}
