package helper

import "github.com/vcraescu/go-paginator/v2/view"

type ResponsePagination struct {
	CurrentPage int         `json:"current_page"`
	Data        interface{} `json:"data"`
	// NextPage    int         `json:"nextPage"`
	// PrevPage    int         `json:"prevPage"`
	LastPage int `json:"last_page"`
	PerPage  int `json:"per_page"`
	Total    int `json:"total"`
}

func GetCurrentPage(view view.Viewer) int {
	Current, err := view.Current()
	if err != nil {
		panic(err)
	}
	return Current
}

func GetLastPage(view view.Viewer) int {
	last, err := view.Last()
	if err != nil {
		panic(err)
	}
	return last
}

func BuildPaginationResponse(data interface{}, view view.Viewer) ResponsePagination {
	var res ResponsePagination
	res.CurrentPage = GetCurrentPage(view)
	res.Data = data
	res.LastPage = GetLastPage(view)
	return res
}
