package helper

import (
	"github.com/go-playground/validator/v10"
)

type ResponseValid struct {
	ValidationError interface{}
}

type Test struct{}

func BuildValidationResponse(err error) ResponseValid {
	var res ResponseValid
	output := map[string]interface{}{}
	for _, err := range err.(validator.ValidationErrors) {
		output[err.Field()] = []interface{}{BuildMessageValidation(err.Field(), err.Tag(), err.Param())}
	}
	res.ValidationError = output
	return res
}

func BuildSingleValidationResponse(field string, tag string, params string) ResponseValid {
	var res ResponseValid
	output := map[string]interface{}{}
	output[field] = []interface{}{BuildMessageValidation(field, tag, params)}
	res.ValidationError = output
	return res
}

func BuildMessageValidation(field string, tag string, param string) string {
	var message string
	if tag == "required" {
		//message = "The " + field + " field is required"
		message = "Data " + field + " tidak boleh kosong"

	} else if tag == "email" {
		//message = "The " + field + " field must be a valid email address"
		message = "Data " + field + " harus diisi dengan alamat Email yang sah"

	} else if tag == "min" {
		//message = "The " + field + " field must be minimum " + param + " char"
		message = "Data " + field + " harus diisi minimum " + param + " char"
	
	} else if tag == "max" {
		message = "The " + field + " field must be maximum " + param + " char"
		message = "Data " + field + " maksimal" + param + " char"

	} else if tag == "duplicate" {
		//message = "The " + field + " field must contain a unique value"
		message = "Data " + field + " sudah terdaftar di sistem Arjuna"

	} else if tag == "image" {
		//message = "The " + field + " field must be image type"
		message = "Data /file" + field + " harus bertipe image"

	} else if tag == "png" {
		//message = "The " + field + " field must be image " + tag + " type"
		message = "Data /file" + field + " harus bertipe " + tag + " image" 

	} else if tag == "oldsamepass" {
		message = "The " + field + " field must be same"
		message = "Data " + field + " harus sama"

	} else if tag == "newsamepass" {
		message = "The " + field + " field must be same with confirm password"
		message = "Data " + field + " harus sama dengan Konfirmas Password"

	} else if tag == "notfound" {
		//message = "The " + field + " not found"
		message = "Data " + field + " tidak ditemukan"

	} else {
		message = field + " " + tag
	}
	return message
}
