package helper

import (
	"log"
	"os"

	"github.com/gofiber/fiber/v2/middleware/logger"
)

func HandleLogger() interface{} {
	file, err := os.OpenFile("./fiber.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer file.Close()
	logger := logger.New(logger.Config{
		Format:     "[${ip}] : [${time}] ${status} - ${method} ${path}\n",
		TimeFormat: "Mon Jan 2 15:04:05 MST 2006",
		TimeZone:   "Asia/Jakarta",
		// Output: file,
	})
	return logger
}
