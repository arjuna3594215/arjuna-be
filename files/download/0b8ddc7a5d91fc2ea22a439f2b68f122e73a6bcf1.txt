-- FUNCTION: account.authenticate_user(character varying, character varying, character varying)

-- DROP FUNCTION account.authenticate_user(character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION account.authenticate_user(
	par_username character varying,
	par_password character varying,
	par_ipaddress character varying)
    RETURNS TABLE(id_personal uuid, id uuid, nama character varying, nomor character varying, kdlevel character, namalevel character varying, kdjabatan json, namajabatan json, id_prodi uuid, kd_prodi character, namaprodi character varying, id_fakultas uuid, kd_fakultas character, namafakultas character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

Declare
 _attemp int := (select attemp from log.honeypot where ip_address=par_ipaddress);
begin 
if _attemp<3 or _attemp is null then
	RAISE NOTICE 'HERE';
	if exists (select * from account.user where username=lower(par_username) and password=crypt(par_password,password)) then
		delete from log.honeypot where ip_address=par_ipaddress;									
		return query (
		SELECT 
			--t1.username,
			t1.id_personal,
			CASE WHEN t1.kdleveluser = '02' THEN t5.id_dosen else t4.id_mahasiswa END AS id,
			CASE WHEN t1.kdleveluser = '02' THEN ((CASE WHEN t5.gelar_depan IS NOT NULL THEN t5.gelar_depan||' ' ELSE '' END) || t2.nama || (CASE WHEN t5.gelar_belakang IS NOT NULL THEN ' '||t5.gelar_belakang ELSE '' END))::varchar ELSE t2.nama END AS nama,
			CASE WHEN t1.kdleveluser = '01' THEN t4.nim WHEN t1.kdleveluser = '02' THEN t5.nip WHEN t1.kdleveluser = '03' THEN t6.nip END AS nomor,
			t1.kdleveluser,
			t3.leveluser,
			json_agg(t7.kd_jabatan) kdjabatan,
			json_agg(t8.nama) namakdjabatan,
			--json_agg(t7.stsdata) statuskdjabatan,
			--json_agg(t7.tglakhirjabatan) tglkdjabatan,
			t9.id_prodi,
			t9.kd_prodi,
			t9.nama,
			t10.id_fakultas,
			t10.kd_fakultas,
			t10.nama
			FROM account."user"  t1
			JOIN person.personal t2 ON t2.id_personal = t1.id_personal
			JOIN account.leveluser t3 ON t3.kdleveluser = t1.kdleveluser
			LEFT JOIN person.mahasiswa t4 ON t4.id_personal = t1.id_personal
			LEFT JOIN person.dosen t5 ON t5.id_personal = t1.id_personal
			LEFT JOIN person.karyawan t6 ON t6.id_personal = t1.id_personal
			LEFT JOIN person.jabatantambahan t7 ON t7.id_personal = t1.id_personal AND t7.stsdata = '1' AND t7.tglakhirjabatan >= current_date
			LEFT JOIN support.jabatan t8 ON t8.kd_jabatan = t7.kd_jabatan
			LEFT JOIN akademik.prodi t9 ON t9.id_prodi = t4.id_prodi OR t9.id_prodi = t5.id_prodi
			LEFT JOIN akademik.fakultas t10 ON t10.id_fakultas = t9.id_fakultas
		WHERE username=lower(par_username) and password=crypt(par_password,password)
		GROUP BY 
-- 			t1.username,
			t1.id_personal,
			t5.id_dosen,
			t4.id_mahasiswa,
			t2.nama,
			t4.nim,
			t5.nip,
			t6.nip,
			t1.kdleveluser,
			t3.leveluser,
			t9.id_prodi,
			t9.kd_prodi,
			t9.nama,
			t10.id_fakultas,
			t10.kd_fakultas,
			t10.nama,
			
	t5.gelar_depan,
	t5.gelar_belakang
		);
	else
		if _attemp is null then
			insert into log.honeypot(ip_address, attemp) values (par_ipaddress,1);
		else
			update log.honeypot set attemp=_attemp+1 where ip_address=par_ipaddress;
		end if;
	end if;
else
		RAISE NOTICE 'THERE';
end if;
end;

$BODY$;

ALTER FUNCTION account.authenticate_user(character varying, character varying, character varying)
    OWNER TO admin;
