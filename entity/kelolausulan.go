package entity

import "time"

type UsulanAkreditasiBaru struct {
	IdUsulanAkreditasi     int64     `json:"id_usulan_akreditasi"`
	IdIdentitasJurnal      int64     `json:"id_identitas_jurnal"`
	NamaJurnal             string    `json:"nama_jurnal"`
	IdPic                  int64     `json:"id_pic"`
	IdEic                  int64     `json:"id_eic"`
	UserPenilai            string    `json:"user_penilai"`
	PasswdPenilai          string    `json:"passwd_penilai"`
	FileDokumenUsulan      string    `json:"file_dokumen_usulan"`
	TglPengajuanUsulan     time.Time `json:"tgl_pengajuan_usulan"`
	Eissn                  string    `json:"eissn"`
	Pissn                  string    `json:"pissn"`
	Publisher              string    `json:"publisher"`
	Society                string    `json:"society"`
	AlamatOai              string    `json:"alamat_oai"`
	DoiJurnal              string    `json:"doi_jurnal"`
	NamaAwalJurnal         string    `json:"nama_awal_jurnal"`
	TglPembuatan           string    `json:"tgl_pembuatan"`
	UrlJurnal              string    `json:"url_jurnal"`
	UrlContact             string    `json:"url_contact"`
	UrlEditor              string    `json:"url_editor"`
	Country                string    `json:"country"`
	City                   string    `json:"city"`
	AlamatJurnal           string    `json:"alamat_jurnal"`
	NoTeleponJurnal        string    `json:"no_telepon_jurnal"`
	AlamatSurelJurnal      string    `json:"alamat_surel_jurnal"`
	TglAkhirTerakreditasi  string    `json:"tgl_akhir_terakreditasi"`
	UrlStatistikPengunjung string    `json:"url_statistik_pengunjung"`
	NamaPic                string    `json:"nama_pic"`
	NamaInstitusi          string    `json:"nama_institusi"`
	EmailPic               string    `json:"email_pic"`
	NoHandPhonePic         string    `json:"no_hand_phone_pic"`
	NamaEic                string    `json:"nama_eic"`
	NoTeleponEic           string    `json:"no_telepon_eic"`
	AlamatSurelEic         string    `json:"alamat_surel_eic"`
	NilaiTotal             float64   `json:"nilai_total"`
	BidangIlmu             string    `json:"bidang_ilmu"`
	AlasanPenolakan        string    `json:"alasan_penolakan"`
	PemberiKomentar        string    `json:"pemberi_komentar"`
	StatusUsulanAkreditasi bool      `json:"status_usulan_akreditasi"`

	UrlGooglescholar string  `json:"url_googlescholar,omitempty"`
	IdResubmit       string  `json:"id_resubmit,omitempty"`
	AlasanResubmit   string  `json:"alasan_resubmit,omitempty"`
	TglBatasResubmit string  `json:"tgl_batas_resubmit,omitempty"`
	NilaiSa          float64 `json:"nilai_sa,omitempty"`
}

type DetailUsulanAkreditasiBaru struct {
	IDUsulanAkreditasi     int64     `json:"id_usulan_akreditasi"`
	IDIdentitasJurnal      int64     `json:"id_identitas_jurnal"`
	NamaJurnal             string    `json:"nama_jurnal"`
	Eissn                  string    `json:"eissn"`
	Pissn                  string    `json:"pissn"`
	Publisher              string    `json:"publisher"`
	Society                string    `json:"society"`
	NamaAwalJurnal         string    `json:"nama_awal_jurnal"`
	TglPembuatan           string    `json:"tgl_pembuatan"`
	UrlJurnal              string    `json:"url_jurnal"`
	UrlContact             string    `json:"url_contact"`
	UrlEditor              string    `json:"url_editor"`
	Country                string    `json:"country"`
	City                   string    `json:"city"`
	AlamatJurnal           string    `json:"alamat_jurnal"`
	NoTeleponJurnal        string    `json:"no_telepon_jurnal"`
	AlamatSurelJurnal      string    `json:"alamat_surel_jurnal"`
	TglAkhirTerakreditasi  string    `json:"tgl_akhir_terakreditasi"`
	IdPic                  int64     `json:"id_pic"`
	NamaPic                string    `json:"nama_pic"`
	NamaInstitusi          string    `json:"nama_institusi"`
	EmailPic               string    `json:"email_pic"`
	NoHandPhonePic         string    `json:"no_hand_phone_pic"`
	IdEic                  int64     `json:"id_eic"`
	NamaEic                string    `json:"nama_eic"`
	NoTeleponEic           string    `json:"no_telepon_eic"`
	AlamatSurelEic         string    `json:"alamat_surel_eic"`
	AlamatOai              string    `json:"alamat_oai"`
	DoiJurnal              string    `json:"doi_jurnal"`
	UrlStatistikPengunjung string    `json:"url_statistik_pengunjung"`
	UrlEtikapublikasi      string    `json:"url_etikapublikasi"`
	UserPenilai            string    `json:"user_penilai"`
	PasswdPenilai          string    `json:"passwd_penilai"`
	FileDokumenUsulan      string    `json:"file_dokumen_usulan"`
	TglPengajuanUsulan     time.Time `json:"tgl_pengajuan_usulan"`
	NilaiTotal             float64   `json:"nilai_total"`
	GradeAkreditasi        string    `json:"grade_akreditasi"`
	BidangIlmu             string    `json:"bidang_ilmu"`
	Issue                  []Issue   `json:"issue" gorm:"foreignKey:IDUsulanAkreditasi;references:IDUsulanAkreditasi"`
	IssueSebelumnya        []Issue   `json:"issue_sebelumnya" gorm:"foreignKey:IDUsulanAkreditasi;references:IDUsulanAkreditasi"`
}

type UsulanActionDTO struct {
	IDUsulanAkreditasi   int64  `json:"id_usulan_akreditasi"`
	StsHasilDeskEvaluasi string `json:"sts_hasil_desk_evaluasi"`
	AlasanPenolakan      string `json:"alasan_penolakan"`
}

type UsulanAkreditasiDitolak struct {
	IDUsulanAkreditasiDitolak int64     `json:"id_usulan_akreditasi_ditolak" gorm:"default:null"`
	IDUsulanAkreditasi        int64     `json:"id_usulan_akreditasi" gorm:"default:null"`
	IDPersonal                int64     `json:"id_personal" gorm:"default:null"`
	AlasanPenolakan           string    `json:"alasan_penolakan" gorm:"default:null"`
	TglCreated                time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated                time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
	KdStsAktif                string    `json:"kd_sts_aktif" gorm:"default:null"`
	IDPeriodeAkreditasi       int64     `json:"id_periode_akreditasi" gorm:"default:null"`
}

type DataMailRejectUsulan struct {
	NamaJurnal     string //
	NamaAwalJurnal string //
	Eissn          string //
	Comment        string
	TglUsulan      string //
	NamaPic        string //
	EmailPic       string //
	Tahun          string
}

//======================================================================================================================

type DistribusiPenilaian struct {
	IdUsulanAkreditasi          int64     `json:"id_usulan_akreditasi"`
	IdIdentitasJurnal           int64     `json:"id_identitas_jurnal"`
	NamaDeskevaluator           string    `json:"nama_deskevaluator,omitempty"` //kd
	NamaJurnal                  string    `json:"nama_jurnal"`
	Eissn                       string    `json:"eissn"`
	Pissn                       string    `json:"pissn"`
	Publisher                   string    `json:"publisher"`
	Society                     string    `json:"society"`
	NamaAwalJurnal              string    `json:"nama_awal_jurnal"`
	TglPembuatan                time.Time `json:"tgl_pembuatan"`
	UrlJurnal                   string    `json:"url_jurnal"`
	UrlContact                  string    `json:"url_contact"`
	UrlEditor                   string    `json:"url_editor"`
	Country                     string    `json:"country"`
	City                        string    `json:"city"`
	AlamatJurnal                string    `json:"alamat_jurnal"`
	NoTeleponJurnal             string    `json:"no_telepon_jurnal"`
	AlamatSurelJurnal           string    `json:"alamat_surel_jurnal"`
	TglAkhirTerakreditasi       time.Time `json:"tgl_akhir_terakreditasi"`
	IdPic                       int64     `json:"id_pic"`
	NamaPic                     string    `json:"nama_pic"`
	NamaInstitusi               string    `json:"nama_institusi"`
	EmailPic                    string    `json:"email_pic"`
	NoHandPhonePic              string    `json:"no_hand_phone_pic"`
	IdEic                       int64     `json:"id_eic"`
	NamaEic                     string    `json:"nama_eic"`
	NoTeleponEic                string    `json:"no_telepon_eic"`
	AlamatSurelEic              string    `json:"alamat_surel_eic"`
	AlamatOai                   string    `json:"alamat_oai"`
	DoiJurnal                   string    `json:"doi_jurnal"`
	UrlStatistikPengunjung      string    `json:"url_statistik_pengunjung"`
	UserPenilai                 string    `json:"user_penilai"`
	PasswdPenilai               string    `json:"passwd_penilai"`
	FileDokumenUsulan           string    `json:"file_dokumen_usulan"`
	TglPengajuanUsulan          time.Time `json:"tgl_pengajuan_usulan"`
	TglDeskEvaluasi             time.Time `json:"tgl_desk_evaluasi"`
	JmlAsessorDitugaskan        int64     `json:"jml_asessor_ditugaskan,omitempty"` //kd
	JmlArtikel                  int64     `json:"jml_artikel,omitempty"`
	JmlArtikelTdkTerpenuhi      int64     `json:"jml_artikel_tdk_terpenuhi,omitempty"`
	JmlAsessorArtikelDitugaskan int64     `json:"jml_asessor_artikel_ditugaskan,omitempty"`
	JmlAsessorArtikelMenerima   int64     `json:"jml_asessor_artikel_menerima,omitempty"`
	JmlAsessorArtikelMenolak    int64     `json:"jml_asessor_artikel_menolak,omitempty"`
	JmlMinAsessorArtikel        int64     `json:"jml_min_asessor_artikel,omitempty"`
	JmlAsessorMgmtDitugaskan    int64     `json:"jml_asessor_mgmt_ditugaskan,omitempty"`
	JmlAsessorMgmtMenerima      int64     `json:"jml_asessor_mgmt_menerima,omitempty"`
	JmlAsessorMgmtMenolak       int64     `json:"jml_asessor_mgmt_menolak,omitempty"`
	JmlMinAsessorMgmt           int64     `json:"jml_min_asessor_mgmt,omitempty"`
	NamaAsessor                 string    `json:"nama_asessor,omitempty"`
	JmlAsessorMenerima          int64     `json:"jml_asessor_menerima,omitempty"` //kd
	JmlAsessorMenolak           int64     `json:"jml_asessor_menolak,omitempty"`  //kd
	JmlMinAsessor               int64     `json:"jml_min_asessor,omitempty"`      //kd
	BidangIlmu                  string    `json:"bidang_ilmu"`
	StsAkreditasi               int64     `json:"sts_akreditasi"`
	JmlRecord                   int64     `json:"jml_record"`
}

type HasilPenilaianLebihDari70 struct {
	IdUsulanAkreditasi       int64   `json:"id_usulan_akreditasi"`
	TglUsulan                string  `json:"tgl_usulan"`
	NamaJurnal               string  `json:"nama_jurnal"`
	Eissn                    string  `json:"eissn"`
	Pissn                    string  `json:"pissn"`
	Publisher                string  `json:"publisher"`
	Society                  string  `json:"society"`
	AlamatOai                string  `json:"alamat_oai"`
	DoiJurnal                string  `json:"doi_jurnal"`
	NamaAwalJurnal           string  `json:"nama_awal_jurnal"`
	TglPembuatan             string  `json:"tgl_pembuatan"`
	UrlJurnal                string  `json:"url_jurnal"`
	UrlContact               string  `json:"url_contact"`
	UrlEditor                string  `json:"url_editor"`
	Country                  string  `json:"country"`
	City                     string  `json:"city"`
	Alamat                   string  `json:"alamat"`
	NoTelepon                string  `json:"no_telepon"`
	AlamatSurel              string  `json:"alamat_surel"`
	TglAkhirTerakreditasi    string  `json:"tgl_akhir_terakreditasi"`
	UrlStatistikPengunjung   string  `json:"url_statistik_pengunjung"`
	NamaPic                  string  `json:"nama_pic"`
	NamaInstitusi            string  `json:"nama_institusi"`
	NamaEic                  string  `json:"nama_eic"`
	AlamatSurelEic           string  `json:"alamat_surel_eic"`
	TotalNilaiMgmt           float64 `json:"total_nilai_mgmt"`
	DisinsentifNilaiMgmt     string  `json:"disinsentif_nilai_mgmt"`
	JmlItemNilaiMgmt         int64   `json:"jml_item_nilai_mgmt"`
	JmlItemNilaiMgmtSelesai  int64   `json:"jml_item_nilai_mgmt_selesai"`
	TotalNilaiIssue          float64 `json:"total_nilai_issue"`
	DisinsentifNilaiIssue    string  `json:"disinsentif_nilai_issue"`
	NilaiDetilIssue          string  `json:"nilai_detil_issue"`
	JmlItemNilaiIssue        int64   `json:"jml_item_nilai_issue"`
	JmlItemNilaiIssueSelesai int64   `json:"jml_item_nilai_issue_selesai"`
	GradeAkreditasi          string  `json:"grade_akreditasi"`
	AsesorMgmt               string  `json:"asesor_mgmt"`
	AsesorIssue              string  `json:"asesor_issue"`
	BidangIlmu               string  `json:"bidang_ilmu"`
	RekapIssue               string  `json:"rekap_issue"`
}

type HasilPenilaianKurangDari70 struct {
	IdUsulanAkreditasi           int64   `json:"id_usulan_akreditasi"`
	TglUsulan                    string  `json:"tgl_usulan"`
	NamaJurnal                   string  `json:"nama_jurnal"`
	Eissn                        string  `json:"eissn"`
	Pissn                        string  `json:"pissn"`
	Publisher                    string  `json:"publisher"`
	Society                      string  `json:"society"`
	AlamatOai                    string  `json:"alamat_oai"`
	DoiJurnal                    string  `json:"doi_jurnal"`
	NamaAwalJurnal               string  `json:"nama_awal_jurnal"`
	TglPembuatan                 string  `json:"tgl_pembuatan"`
	UrlJurnal                    string  `json:"url_jurnal"`
	UrlContact                   string  `json:"url_contact"`
	UrlEditor                    string  `json:"url_editor"`
	Country                      string  `json:"country"`
	City                         string  `json:"city"`
	Alamat                       string  `json:"alamat"`
	NoTelepon                    string  `json:"no_telepon"`
	AlamatSurel                  string  `json:"alamat_surel"`
	TglAkhirTerakreditasi        string  `json:"tgl_akhir_terakreditasi"`
	UrlStatistikPengunjung       string  `json:"url_statistik_pengunjung"`
	NamaPic                      string  `json:"nama_pic"`
	NamaInstitusi                string  `json:"nama_institusi"`
	NamaEic                      string  `json:"nama_eic"`
	AlamatSurelEic               string  `json:"alamat_surel_eic"`
	TotalNilaiEvaluator          float64 `json:"total_nilai_evaluator"`
	DisinsentifNilaiEvaluator    string  `json:"disinsentif_nilai_evaluator"`
	JmlItemNilaiEvaluator        int64   `json:"jml_item_nilai_evaluator"`
	JmlItemNilaiEvaluatorSelesai int64   `json:"jml_item_nilai_evaluator_selesai"`
	GradeAkreditasi              string  `json:"grade_akreditasi"`
	AsesorEvaluator              string  `json:"asesor_evaluator"`
	IDAsesorEvaluator            string  `json:"id_asesor_evaluator"`
	BidangIlmu                   string  `json:"bidang_ilmu"`
	RekapIssue                   string  `json:"rekap_issue"`
}

type HasilAkreditasi struct {
	IdUsulanAkreditasi     int64   `json:"id_usulan_akreditasi"`
	TglPenetapanAkreditasi string  `json:"tgl_penetapan_akreditasi"`
	NamaJurnal             string  `json:"nama_jurnal"`
	Eissn                  string  `json:"eissn"`
	Pissn                  string  `json:"pissn"`
	Publisher              string  `json:"publisher"`
	Society                string  `json:"society"`
	AlamatOai              string  `json:"alamat_oai"`
	DoiJurnal              string  `json:"doi_jurnal"`
	NamaAwalJurnal         string  `json:"nama_awal_jurnal"`
	TglPembuatan           string  `json:"tgl_pembuatan"`
	UrlJurnal              string  `json:"url_jurnal"`
	UrlContact             string  `json:"url_contact"`
	UrlEditor              string  `json:"url_editor"`
	Country                string  `json:"country"`
	City                   string  `json:"city"`
	Alamat                 string  `json:"alamat"`
	NoTelepon              string  `json:"no_telepon"`
	AlamatSurel            string  `json:"alamat_surel"`
	TglAkhirTerakreditasi  string  `json:"tgl_akhir_terakreditasi"`
	UrlStatistikPengunjung string  `json:"url_statistik_pengunjung"`
	NamaPic                string  `json:"nama_pic"`
	NamaInstitusi          string  `json:"nama_institusi"`
	NamaEic                string  `json:"nama_eic"`
	AlamatSurelEic         string  `json:"alamat_surel_eic"`
	NilaiTotal             float64 `json:"nilai_total"`
	GradeAkreditasi        string  `json:"grade_akreditasi"`
	IdSkAkreditasi         string  `json:"id_sk_akreditasi"`
	StsPublished           string  `json:"sts_published"`
	IDBidangIlmuJurnal     string  `json:"id_bidang_ilmu_jurnal"`
	BidangIlmuJurnal       string  `json:"bidang_ilmu_jurnal"`
}

type DetailHasilAkreditasi struct {
	IDSkAkreditasi string `json:"id_sk_akreditasi"`
	NO_SK          string `json:"no_sk"`
	JudulSk        string `json:"judul_sk"`
	TglSk          string `json:"tgl_sk"`
}

//======================================================================================================================

type ProgresPenilaian struct {
	No                            int64  `json:"no"`
	IDUsulanAkreditasi            int64  `json:"id_usulan_akreditasi"`
	TglUsulan                     string `json:"tgl_usulan"`
	UrlStatistikPengunjung        string `json:"url_statistik_pengunjung"`
	NamaJurnal                    string `json:"nama_jurnal"`
	Eissn                         string `json:"eissn"`
	Pissn                         string `json:"pissn"`
	Publisher                     string `json:"Publisher"`
	Society                       string `json:"society"`
	NamaAwalJurnal                string `json:"nama_awal_jurnal"`
	TglPembuatan                  string `json:"tgl_pembuatan"`
	UrlJurnal                     string `json:"url_jurnal"`
	UrlContact                    string `json:"url_contact"`
	UrlEditor                     string `json:"url_editor"`
	Country                       string `json:"country"`
	City                          string `json:"city"`
	Alamat                        string `json:"alamat"`
	NoTelepon                     string `json:"no_telepon"`
	AlamatSurel                   string `json:"alamat_surel"`
	TglAkhirTerakreditasi         string `json:"tgl_akhir_terakreditasi"`
	NamaPic                       string `json:"nama_pic"`
	NamaInstitusi                 string `json:"nama_institusi"`
	NamaEic                       string `json:"nama_eic"`
	AlamatSurelEic                string `json:"alamat_surel_eic"`
	AlamatOai                     string `json:"alamat_oai"`
	DoiJurnal                     string `json:"doi_jurnal"`
	TotalNilaiMgmt                string `json:"total_nilai_mgmt"`
	DisinsentifNilaiMgmt          string `json:"disinsentif_nilai_mgmt"`
	NilaiDetilMgmt                string `json:"nilai_detil_mgmt"`
	SemuaNilaiMgmt                string `json:"semua_nilai_mgmt"`
	JmlItemNilaiMgmt              string `json:"jml_item_nilai_mgmt"`
	JmlItemNilaiMgmtSelesai       string `json:"jml_item_nilai_mgmt_selesai"`
	TotalNilaiIssue               string `json:"total_nilai_issue"`
	DisinsentifNilaiIssue         string `json:"disinsentif_nilai_issue"`
	TotalNilai                    string `json:"total_nilai"`
	NilaiDetilIssue               string `json:"nilai_detil_issue"`
	SemuaNilaiIssue               string `json:"semua_nilai_issue"`
	JmlItemNilaiIssue             string `json:"jml_item_nilai_issue"`
	JmlItemNilaiIssueSelesai      string `json:"jml_item_nilai_issue_selesai"`
	AsesorMgmt                    string `json:"asesor_mgmt"`
	IDAsesorMgmt                  string `json:"id_asesor_mgmt"`
	TglPenugasanAsesorMgmt        string `json:"tgl_penugasan_asesor_mgmt"`
	TglTerimaPenugasanAsesorMgmt  string `json:"tgl_terima_penugasan_asesorMgmt"`
	AsesorIssue                   string `json:"asesor_issue"`
	IDAsesorIssue                 string `json:"id_asesor_issue"`
	TglPenugasanAsesorIssue       string `json:"tgl_penugasan_asesor_issue"`
	TglTerimaPenugasanAsesorIssue string `json:"tgl_terima_penugasan_asesor_issue"`
	BidangIlmu                    string `json:"bidang_ilmu"`
	UserPenilai                   string `json:"user_penilai"`
	PasswordPenilai               string `json:"password_penilai"`
	RekapIssue                    string `json:"eekap_issue"`
	RekapUrlIssue                 string `json:"rekap_url_issue"`
	StatusPenyesuaianNilaiIssue   string `json:"status_penyesuaian_nilai_issue"`
	StatusPenyesuaianNilaiMgmt    string `json:"status_Penyesuaian_nilai_mgmt"`
	FrekuensiTerbitan             string `json:"frekuensi_terbitan"`
	StatusSiapPermanen            string `json:"status_siap_permanen"`
	StsPenyesuaianIssue           string `json:"sts_penyesuaian_issue"`
	StsPenyesuaianMgmt            string `json:"sts_penyesuaian_mgmt"`
	StsAkreditasi                 int    `json:"sts_akreditasi"`
	NilaiDanPeringkatSebelumnya   string `json:"nilai_dan_peringkat_sebelumnya"`
	JmlRecord                     int64  `json:"jml_record"`
}

type DetailProgresPenilaianManagement struct {
	IDPenugasanPenilaian   int64     `json:"id_penugasan_penilaian"`
	IDUsulanAkreditasi     int64     `json:"id_usulan_akreditasi"`
	TglPenugasan           time.Time `json:"tgl_penugasan"`
	TglPenerimaan          time.Time `json:"tgl_penerimaan"`
	UrlStatistikPengunjung string    `json:"url_statistik_pengunjung"`
	NamaJurnal             string    `json:"nama_jurnal"`
	Eissn                  string    `json:"eissn"`
	Pissn                  string    `json:"pissn"`
	Publisher              string    `json:"publisher"`
	Society                string    `json:"society"`
	NamaAwalJurnal         string    `json:"nama_awal_jurnal"`
	TglPembuatan           string    `json:"tgl_pembuatan"`
	UrlJurnal              string    `json:"url_jurnal"`
	UrlContact             string    `json:"url_contact"`
	UrlEditor              string    `json:"url_editor"`
	Country                string    `json:"country"`
	City                   string    `json:"city"`
	Alamat                 string    `json:"alamat"`
	NoTelepon              string    `json:"no_telepon"`
	AlamatSurel            string    `json:"alamat_surel"`
	TglAkhirTerakreditasi  string    `json:"tgl_akhir_terakreditasi"`
	NamaPic                string    `json:"nama_pic"`
	NamaInstitusi          string    `json:"nama_institusi"`
	NamaEic                string    `json:"nama_eic"`
	AlamatSurelEic         string    `json:"alamat_surel_eic"`
	AlamatOai              string    `json:"alamat_oai"`
	DoiJurnal              string    `json:"doi_jurnal"`
	UserPenilai            string    `json:"user_penilai"`
	PasswdPenilai          string    `json:"passwd_penilai"`
	NamaPenilai            string    `json:"nama_penilai"`
	NilaiTotal             float64   `json:"nilai"`
	NilaiDisinsentif       float64   `json:"-"`
	Nilai                  float64   `json:"-"`
	SelisihPenilaian       string    `json:"selisih_penilaian"`
	// ISPenyelesaian         string    `json:"is_penyelesaian"`
	IDPenyesuaianNilai int64 `json:"id_penyesuaian_nilai"`
}

type DetailProgresPenilaianIssue struct {
	IDPenugasanPenilaianIssue int64     `json:"id_penugasan_penilaian_issue"`
	IDUsulanAkreditasi        int64     `json:"id_usulan_akreditasi"`
	TglPenugasan              time.Time `json:"tgl_penugasan"`
	TglPenerimaan             time.Time `json:"tgl_penerimaan"`
	UrlStatistikPengunjung    string    `json:"url_statistik_pengunjung"`
	NamaJurnal                string    `json:"nama_jurnal"`
	Eissn                     string    `json:"eissn"`
	Pissn                     string    `json:"pissn"`
	Publisher                 string    `json:"publisher"`
	Society                   string    `json:"society"`
	NamaAwalJurnal            string    `json:"nama_awal_jurnal"`
	TglPembuatan              string    `json:"tgl_pembuatan"`
	UrlJurnal                 string    `json:"url_jurnal"`
	UrlContact                string    `json:"url_contact"`
	UrlEditor                 string    `json:"url_editor"`
	Country                   string    `json:"country"`
	City                      string    `json:"city"`
	Alamat                    string    `json:"alamat"`
	NoTelepon                 string    `json:"no_telepon"`
	AlamatSurel               string    `json:"alamat_surel"`
	TglAkhirTerakreditasi     string    `json:"tgl_akhir_terakreditasi"`
	NamaPic                   string    `json:"nama_pic"`
	NamaInstitusi             string    `json:"nama_institusi"`
	NamaEic                   string    `json:"nama_eic"`
	AlamatSurelEic            string    `json:"alamat_surel_eic"`
	UrlIssue                  string    `json:"url_issue"`
	IDIssue                   string    `json:"id_issue"`
	JudulIssue                string    `json:"judul_issue"`
	VolumeIssue               string    `json:"volume_issue"`
	NomorIssue                string    `json:"nomor_issue"`
	TahunIssue                string    `json:"tahun_issue"`
	IDBidangIlmuIssue         string    `json:"id_bidang_ilmu_issue"`
	BidangIlmuIssue           string    `json:"bidang_ilmu_issue"`
	LevelTaksonomiIssue       string    `json:"level_taksonomi_issue"`
	AlamatOai                 string    `json:"alamat_oai"`
	DoiJurnal                 string    `json:"doi_jurnal"`
	UserPenilai               string    `json:"user_penilai"`
	PasswdPenilai             string    `json:"passwd_penilai"`
	NamaPenilai               string    `json:"nama_penilai"`
	NilaiTotal                float64   `json:"nilai"`
	NilaiDisinsentif          float64   `json:"-"`
	Nilai                     float64   `json:"-"`
	SelisihPenilaian          string    `json:"selisih_penilaian"`
	// ISPenyelesaian         string    `json:"is_penyelesaian"`
	IDPenyesuaianNilai int64 `json:"id_penyesuaian_nilai"`
}

type ProgresPenilaianSetPermanentDTO struct {
	IDPersonal  int64  `json:"id_personal" validate:"required"`
	IDPenugasan int64  `json:"id_penugasan" validate:"required"`
	Penilai     string `json:"penilai" validate:"required"`
}

type ProgresPenilaianPenyesuaianNilaiDTO struct {
	IDPenyesuaianNilai int64  `json:"id_penyesuaian_nilai"`
	IDPenugasan        int64  `json:"id_penugasan" validate:"required"`
	Penilai            string `json:"penilai" validate:"required"`
	Komentar           string `json:"komentar"`
}

type ProgresPenilaianPenyesuaianNilaiAsessorDTO struct {
	IDUsulanAkreditasi int64  `json:"id_usulan_akreditasi" validate:"required"`
	Penilai            string `json:"penilai" validate:"required"`
	Komentar           string `json:"komentar" validate:"required"`
}

type ProgresPenilaianPenyesuaianNilaiAsessor struct {
	//IDUsulanAkreditasi int64 `json:"id_usulan_akreditasi"`
	//Penilai            string `json:"penilai"`
	//Penilai  string `json:"penilai"`
	//Komentar string `json:"komentar"`
	//Lang                string `json:"lang"`
	Status     int    `json:"status"`
	Keterangan string `json:"keterangan"`
}

type PenyesuaianNilaiManagement struct {
	IDPenyesuaianNilaiMgmt        int64     `json:"id_penyesuaian_nilai_mgmt" gorm:"default:null"`
	IDPenugasanPenilaianManajemen int64     `json:"id_penugasan_penilaian_manajemen"`
	StatusPenyesuaian             bool      `json:"status_penyesuaian"`
	TglCreated                    time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated                    time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
	KomentarPenyesuaian           string    `json:"komentar_penyesuaian"`
}

type PenyesuaianNilaiIssue struct {
	IDPenyesuaianNilaiIssue   int64     `json:"id_penyesuaian_nilai_issue" gorm:"default:null"`
	IDPenugasanPenilaianIssue int64     `json:"id_penugasan_penilaian_issue"`
	StatusPenyesuaian         bool      `json:"status_penyesuaian"`
	TglCreated                time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated                time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
	KomentarPenyesuaian       string    `json:"komentar_penyesuaian"`
}

type DetailtListHasilPenilaianManajemen struct {
	NoUrutUnsur       int    `json:"no_urut_unsur"`
	UnsurPenilaian    string `json:"unsur_penilaian"`
	NoUrutSubUnsur    int    `json:"no_urut_sub_unsur"`
	SubUnsurPenilaian string `json:"sub_unsur_penilaian"`
	Nilai             string `json:"nilai"`
	Komentar          string `json:"komentar"`
}

type DetailtListHasilPenilaianSubstansi struct {
	IDIssue           int64  `json:"id_issue"`
	VolIssue          string `json:"vol_issue"`
	NoUrutUnsur       int    `json:"no_urut_unsur"`
	UnsurPenilaian    string `json:"unsur_penilaian"`
	NoUrutSubUnsur    int    `json:"no_urut_sub_unsur"`
	SubUnsurPenilaian string `json:"sub_unsur_penilaian"`
	Nilai             string `json:"nilai"`
	Komentar          string `json:"komentar"`
}

type KomentarAsessor struct {
	NoUrutUnsur    int     `json:"no_urut_unsur"`
	UnsurPenilaian string  `json:"unsur_penilaian"`
	Nilai          float64 `json:"nilai"`
	Komentar       string  `json:"komentar"`
}
