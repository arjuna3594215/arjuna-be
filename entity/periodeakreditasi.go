package entity

import "time"

type PeriodeAkreditasi struct {
	IDPeriodeAkreditasi       int64     `json:"id_periode_akreditasi" gorm:"default:null"`
	Tahun                     int64     `json:"tahun"`
	Periode                   string    `json:"periode"`
	StsAktifPeriodeAkreditasi string    `json:"sts_aktif_periode_akreditasi"`
	TglCreated                time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated                time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
}

type PeriodeAkreditasiDTO struct {
	Tahun   int64  `json:"tahun" validate:"required"`
	Periode string `json:"periode" validate:"required,max=4"`
}

type PeriodeAkreditasiStatusDTO struct {
	Status string `json:"status" form:"status" validate:"required"`
}
