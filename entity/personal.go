package entity

type Personal struct {
	IDPersonal         int64                `json:"id_personal" gorm:"default:null"`
	Nama               string               `json:"nama"`
	NamaInstitusi      string               `json:"nama_institusi"`
	Email              string               `json:"email"`
	NOHandPhone        string               `json:"no_hand_phone"`
	IDNegara           int64                `json:"id_negara" gorm:"default:null"`
	StsAktifPersonal   string               `json:"sts_aktif_personal"`
	Image              string               `json:"image" gorm:"default:null"`
	BidangIlmuInterest []BidangIlmuInterest `json:"bidang_ilmu_interest" gorm:"->;foreignKey:IDPersonal;references:IDPersonal"`
}
