package entity

type DasboardPengusulCount struct {
	TotalJurnalAktif         int64 `json:"total_jurnal_aktif"`
	TotalUsulanJurnal        int64 `json:"total_usulan_jurnal"`
	TotalJurnalTerakreditasi int64 `json:"total_jurnal_terakreditasi"`
}

type DashboardPenilaiCount struct {
	V_UsulanBelumDiterima      int64 `json:"usulan_belum_diterima"`
	V_UsulanBelumDinilai       int64 `json:"usulan_belum_dinilai"`
	V_UsulanOnProgress         int64 `json:"usulan_on_progress"`
	V_TotalPenugasan           int64 `json:"total_penugasan"`
	V_TotalUsulanDitolak       int64 `json:"total_usulan_ditolak"`
	V_TotalUsulanSelesai       int64 `json:"total_usulan_selesai"`
	V_TotalJurnalUsulanSelesai int64 `json:"total_jurnal_usulan_selesai"`
}

type DashboardDistributorCount struct {
	TotalUsulan           int64 `json:"total_usulan"`
	TotalTerakreditasi    int64 `json:"total_terakreditasi"`
	TotalTdkTerakreditasi int64 `json:"total_tdk_terakreditasi"`
}

type DashboardDistributorRekapAkreditasi struct {
	Jml_A70         int64  `json:"jml_a70"`
	Jml_B70         int64  `json:"jml_b70"`
	JmlTtl          int64  `json:"jml_ttl"`
	ID_SKAkreditasi int64  `json:"id_sk_akreditasi"`
	Tgl_SK          string `json:"tgl_sk"`
}
type DashboardDistributorDetail struct {
	Eissn      string `json:"eissn"`
	NamaJurnal string `json:"nama_jurnal"`
	UrlJurnal  string `json:"url_jurnal"`
	Penerbit   string `json:"penerbit"`
	JmlRecord  int64  `json:"jml_record"`
}

type DashboardDistributorPeta struct {
	TotalUsulanJurnalPerkota            int `json:"total_usulan_jurnal_perkota"`
	TotalJurnalTerakdreditasiPerkota    int `json:"total_jurnal_terakdreditasi_perkota"`
	TotalJurnalTdkTerakdreditasiPerkota int `json:"total_jurnal_tdk_terakdreditasi_perkota"`
}

type DashboardDistributorDetailSK struct {
	Eissn           string `json:"eissn"`
	NamaJurnal      string `json:"nama_jurnal"`
	UrlJurnal       string `json:"url_jurnal"`
	Penerbit        string `json:"penerbit"`
	GradeAkreditasi string `json:"grade_akreditasi"`
	JmlRecord       int64  `json:"jml_record"`
}

type DashboardDetailAsessor struct {
	Eissn       string `json:"eissn"`
	NamaJurnal  string `json:"nama_jurnal"`
	UrlJurnal   string `json:"url_jurnal"`
	Publisher   string `json:"publisher"`
	Issue       string `json:"issue"`
	UrlIssue    string `json:"url_issue"`
	AlasanTolak string `json:"alasan_tolak"`
	JmlRecord   int    `json:"jml_record"`
}

type DashboardDetailPengusul struct {
	Eissn           string `json:"eissn"`
	NamaJurnal      string `json:"nama_jurnal"`
	UrlJurnal       string `json:"url_jurnal"`
	Publisher       string `json:"publisher"`
	Progres         string `json:"progres"`
	TotalNilai      string `json:"total_nilai"`
	GradeAkreditasi string `json:"grade_akreditasi"`
	JmlRecord       int    `json:"jml_record"`
}
