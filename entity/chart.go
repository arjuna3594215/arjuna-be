package entity

type DataChart struct {
	Label string `json:"label"`
	Count int64  `json:"count"`
	Sum   int64  `json:"sum,omitempty"`
}

type BarChart struct {
	Title    string        `json:"title"`
	Xlabels  []string      `json:"xlabels"`
	Datasets []BarDatasets `json:"datasets"`
}

type BarDatasets struct {
	Data  []int  `json:"data"`
	Label string `json:"label"`
	ID    string `json:"id,omitempty"`
}

type PolarChart struct {
	Title    string         `json:"title"`
	Labels   []string       `json:"labels"`
	Datasets []PolarDataset `json:"datasets"`
}

type PolarDataset struct {
	Data []float64 `json:"data"`
	// Label string    `json:"label"`
	// ID    string    `json:"id,omitempty"`
}

type LineChart struct {
	Title    string         `json:"title"`
	Xlabels  []string       `json:"xlabels"`
	Datasets []LineDatasets `json:"datasets"`
}

type LineDatasets struct {
	Data  []int  `json:"data"`
	Label string `json:"label"`
	ID    string `json:"id,omitempty"`
}
