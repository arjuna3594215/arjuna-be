package entity

import "time"

type KinerjaPenilai struct {
	IDPersonal           int64  `json:"id_personal"`
	Nama                 string `json:"nama"`
	JmlDitawarkan        int64  `json:"jml_ditawarkan"`
	JmlBlmDiputuskan     int64  `json:"jml_blm_diputuskan"`
	JmlDitolak           int64  `json:"jml_ditolak"`
	JmlDiterima          int64  `json:"jml_diterima"`
	JmlBelumdilaksanakan int64  `json:"jml_belumdilaksanakan"`
	JmlDilaksanakan      int64  `json:"jml_dilaksanakan"`
	JmlDitetapkan        int64  `json:"jml_ditetapkan"`
}

type DetailListJurnalKinerjaPenilai struct {
	IDPenugasanPenilaian int64  `json:"id_penugasan_penilaian"`
	IDPersonal           int64  `json:"id_personal"`
	NamaJurnal           string `json:"nama_jurnal"`
}

type KinerjaPenilaiDTO struct {
	Penilai  string   `json:"penilai" query:"penilai"`
	Periode  []string `json:"periode" query:"periode"`
	Status   string   `json:"status" query:"status"`
	Personal string   `json:"personal" query:"personal"`
}

type DetailKinerjaPenilai struct {
	Nama                     string `json:"nama"`
	UsulanBelumDiterima      int64  `json:"usulan_belum_diterima"`
	UsulanBelumDinilai       int64  `json:"usulan_belum_dinilai"`
	UsulanOnProgres          int64  `json:"usulan_on_progres"`
	TotalPenugasan           int64  `json:"total_penugasan"`
	TotalUsulanDitolak       int64  `json:"total_usulan_ditolak"`
	TotalUsulanSelesai       int64  `json:"total_usulan_selesai"`
	TotalJurnalUsulanSelesai int64  `json:"total_jurnal_usulan_selesai"`
}

type ParentPeriodeKinerjaPenilai struct {
	Tahun string                       `json:"tahun"`
	Bulan []ChildPeriodeKinerjaPenilai `json:"bulan" gorm:"foreignKey:Tahun;references:Tahun"`
}

type ChildPeriodeKinerjaPenilai struct {
	Tahun       string `json:"tahun"`
	Bulan       string `json:"bulan"`
	Periode     string `json:"periode"`
	PeriodeText string `json:"periode_text"`
}

type ListPenugasanPenilaiEvaluator struct {
	IDPenugasanPenilaianEvaluator int64  `json:"id_penugasan_penilaian_evaluator"`
	IDPersonal                    int64  `json:"id_personal"`
	NamaPenilai                   string `json:"nama_penilai"`
	StsPenerimaanPenugasan        string `json:"sts_penerimaan_penugasan"`
	AlasanTolak                   string `json:"alasan_tolak"`
	TglPenugasan                  string `json:"tgl_penugasan"`
}

type ListPenugasanPenilaiIssue struct {
	IDPenugasanPenilaianIssue int64  `json:"id_penugasan_penilaian_issue"`
	IDPersonal                int64  `json:"id_personal"`
	NamaPenilai               string `json:"nama_penilai"`
	StsPenerimaanPenugasan    string `json:"sts_penerimaan_penugasan"`
	TglPenugasan              string `json:"tgl_penugasan"`
}

type ListPenugasanPenilaiManagement struct {
	IDPenugasanPenilaianManajemen int64  `json:"id_penugasan_penilaian_manajemen"`
	IDPersonal                    int64  `json:"id_personal"`
	NamaPenilai                   string `json:"nama_penilai"`
	StsPenerimaanPenugasan        string `json:"sts_penerimaan_penugasan"`
	TglPenugasan                  string `json:"tgl_penugasan"`
}

type PenugasanPenilaianManajemen struct {
	IDPenugasanPenilaianManajemen int64     `json:"id_penugasan_penilaian_manajemen" gorm:"default:null"`
	IDUsulanAkreditasi            int64     `json:"id_usulan_akreditasi" gorm:"default:null"`
	IDPersonal                    int64     `json:"id_personal" gorm:"default:null"`
	StsPenerimaanPenugasan        string    `json:"sts_penerimaan_penugasan" gorm:"default:null"`
	StsPelaksanaanTugas           string    `json:"sts_pelaksanaan_tugas" gorm:"default:null"`
	TglCreated                    time.Time `json:"tgl_created" gorm:"default:null"`
	TglUpdated                    time.Time `json:"tgl_updated" gorm:"default:null"`
	TglPenerimaanPenugasan        time.Time `json:"tgl_penerimaan_penugasan" gorm:"default:null"`
	TglPelaksanaanTugas           time.Time `json:"tgl_pelaksanaan_tugas" gorm:"default:null"`
	AlasanTolak                   string    `json:"alasan_tolak" gorm:"default:null"`
	ScreenshotTolak               string    `json:"screenshot_tolak" gorm:"default:null"`
}

type PenugasanPenilaianEvaluator struct {
	IDPenugasanPenilaianEvaluator int64     `json:"id_penugasan_penilaian_evaluator" gorm:"default:null"`
	IDUsulanAkreditasi            int64     `json:"id_usulan_akreditasi" gorm:"default:null"`
	IDPersonal                    int64     `json:"id_personal" gorm:"default:null"`
	StsPenerimaanPenugasan        string    `json:"sts_penerimaan_penugasan" gorm:"default:null"`
	StsPelaksanaanTugas           string    `json:"sts_pelaksanaan_tugas" gorm:"default:null"`
	TglCreated                    time.Time `json:"tgl_created" gorm:"default:null"`
	TglUpdated                    time.Time `json:"tgl_updated" gorm:"default:null"`
	TglPenerimaanPenugasan        time.Time `json:"tgl_penerimaan_penugasan" gorm:"default:null"`
	TglPelaksanaanTugas           time.Time `json:"tgl_pelaksanaan_tugas" gorm:"default:null"`
	AlasanTolak                   string    `json:"alasan_tolak" gorm:"default:null"`
	ScreenshotTolak               string    `json:"screenshot_tolak" gorm:"default:null"`
}

type PenugasanPenilaianIssue struct {
	IDPenugasanPenilaianIssue int64     `json:"id_penugasan_penilaian_issue" gorm:"default:null"`
	IDUsulanAkreditasi        int64     `json:"id_usulan_akreditasi" gorm:"default:null"`
	IDPersonal                int64     `json:"id_personal" gorm:"default:null"`
	IDIssue                   int64     `json:"id_issue" gorm:"default:null"`
	NamaPenilai               string    `json:"nama_penilai" gorm:"->"`
	StsPenerimaanPenugasan    string    `json:"sts_penerimaan_penugasan" gorm:"default:null"`
	StsPelaksanaanTugas       string    `json:"sts_pelaksanaan_tugas" gorm:"default:null"`
	TglCreated                time.Time `json:"tgl_created" gorm:"default:null"`
	TglUpdated                time.Time `json:"tgl_updated" gorm:"default:null"`
	TglPenerimaanPenugasan    time.Time `json:"tgl_penerimaan_penugasan" gorm:"default:null"`
	TglPelaksanaanTugas       time.Time `json:"tgl_pelaksanaan_tugas" gorm:"default:null"`
	KomentarPenolakan         string    `json:"komentar_penolakan" gorm:"default:null"`
	JumlahSampel              string    `json:"jumlah_sampel" gorm:"default:null"`
}

type SavePengusulPenilaiDTO struct {
	IDUsulanAkreditasi int64   `json:"id_usulan_akreditasi"`
	IDPersonal         int64   `json:"id_personal"`
	IDIssue            []int64 `json:"id_issue"`
	Assessor           string  `json:"assessor"`
}

type DeletePengusulPenilaiDTO struct {
	IDPenugasan        int64  `json:"id_penugasan"`
	IDPersonal         int64  `json:"id_personal"`
	IDUsulanAkreditasi int64  `json:"id_usulan_akreditasi"`
	Assessor           string `json:"assessor"`
}
