package entity

type Kota struct {
	IDProvinsi int64  `json:"id_provinsi"`
	Kota       string `json:"kota"`
	KodeKota   string `json:"kode_kota"`
}

type Country struct {
	Country     string `json:"country"`
	CodeCountry string `json:"code_country"`
}
