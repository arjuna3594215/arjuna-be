package entity

type FrontPageSlider struct {
	ID        int64  `json:"id"`
	Title     string `json:"title"`
	SubTitle  string `json:"sub_title"`
	Imagename string `json:"imagename"`
	Url       string `json:"url"`
	Torder    int64  `json:"torder"`
}

type FrontPageBerita struct {
	ID          int64  `json:"id"`
	Title       string `json:"title"`
	Snapshot    string `json:"snapshot"`
	Isi         string `json:"isi"`
	DateCreated string `json:"date_created"`
	Imagename   string `json:"imagename"`
	Torder      int64  `json:"torder"`
	MostViewed  int64  `json:"most_viewed"`
	Kategori    string `json:"kategori"`
	Lampiran1   string `json:"lampiran1"`
	Lampiran2   string `json:"lampiran2"`
	Lampiran3   string `json:"lampiran3"`
}

type FrontPageListJurnal struct {
	Eissn             string `json:"eissn"`
	IdIdentitasJurnal int64  `json:"id_identitas_jurnal"`
	IdJurnal          int64  `json:"id_jurnal"`
	NamaAwalJurnal    string `json:"nama_awal_jurnal"`
	NamaJurnal        string `json:"nama_jurnal"`
	Publisher         string `json:"publisher"`
	TglPembuatan      string `json:"tgl_pembuatan"`
	TglCreated        string `json:"tgl_created"`
	UrlJurnal         string `json:"url_jurnal"`
	UrlSinta          string `json:"url_sinta"`
	UrlContact        string `json:"url_contact"`
	UrlEditor         string `json:"url_editor"`
	Image             string `json:"image"`
}

type FrontPageJurnalProgres struct {
	IdUsulanAkreditasi               int64                          `json:"id_usulan_akreditasi"`
	IdIdentitasJurnal                int64                          `json:"id_identitas_jurnal"`
	NamaJurnal                       string                         `json:"nama_jurnal,omitempty"`
	Eissn                            string                         `json:"eissn"`
	Pissn                            string                         `json:"pissn"`
	TglCreatedPengajuanDlmProgress   string                         `json:"tgl_created_pengajuan_dlm_progress"`
	TglUpdatedPengajuanDlmProgress   string                         `json:"tgl_updated_pengajuan_dlm_progress"`
	NilaiTotalSa                     float64                        `json:"nilai_total_sa"`
	GradeAkreditasiSa                string                         `json:"grade_akreditasi_sa"`
	StsHasilDeskEvaluasi             string                         `json:"sts_hasil_desk_evaluasi"`
	HasilDeskEvaluasi                string                         `json:"hasil_desk_evaluasi"`
	TglPenetapanDeskevaluasi         string                         `json:"tgl_penetapan_deskevaluasi"`
	NilaiTotal                       float64                        `json:"nilai_total"`
	GradeAkreditasi                  string                         `json:"grade_akreditasi"`
	StsHasilAkreditasi               string                         `json:"sts_hasil_akreditasi"`
	TglPenetapanAkreditasi           string                         `json:"tgl_penetapan_akreditasi"`
	NoSk                             string                         `json:"no_sk"`
	JudulSk                          string                         `json:"judul_sk"`
	TglSk                            string                         `json:"tgl_sk"`
	TglPembuatanSk                   string                         `json:"tgl_pembuatan_sk"`
	TglUsulanAkreditasiTerakhir      string                         `json:"tgl_usulan_akreditasi_terakhir"`
	DataPenugasanPenerimaanManajemen []PenugasanPenerimaanManajemen `json:"penugasan_penerimaan_manajemen" gorm:"foreignKey:IdUsulanAkreditasi;references:IdUsulanAkreditasi"`
	DataPenugasanPenerimaanEvaluator []PenugasanPenerimaanEvaluator `json:"penugasan_penerimaan_evaluator" gorm:"foreignKey:IdUsulanAkreditasi;references:IdUsulanAkreditasi"`
}

type PenugasanPenerimaanManajemen struct {
	IdUsulanAkreditasi     int64  `json:"id_usulan_akreditasi" gorm:"foreignKey:IdUsulanAkreditasi;references:IdUsulanAkreditasi"`
	StsPenerimaanPenugasan string `json:"status_penerimaan_penugasan_penilaian"`
	StsPelaksanaanTugas    string `json:"status_pelaksanaan_tugas_penilaian"`
	TglCreated             string `json:"tgl_created_penilaian"`
	TglPenerimaanPenugasan string `json:"tgl_penerimaan_penugasan_penilaian"`
	TglPelaksanaanTugas    string `json:"tgl_pelaksanaan_tugas_penilaian"`
	Penilai                string `json:"penilai"`
}

type PenugasanPenerimaanEvaluator struct {
	IdUsulanAkreditasi     int64  `json:"id_usulan_akreditasi" gorm:"foreignKey:IdUsulanAkreditasi;references:IdUsulanAkreditasi"`
	StsPenerimaanPenugasan string `json:"status_penerimaan_penugasan_penilaian"`
	StsPelaksanaanTugas    string `json:"status_pelaksanaan_tugas_penilaian"`
	TglCreated             string `json:"tgl_created_penilaian"`
	TglPenerimaanPenugasan string `json:"tgl_penerimaan_penugasan_penilaian"`
	TglPelaksanaanTugas    string `json:"tgl_pelaksanaan_tugas_penilaian"`
	Penilai                string `json:"penilai"`
}

type FrontPageJurnalNameHistory struct {
	NamaJurnal string `json:"nama_jurnal"`
	TglCreated string `json:"tgl_created"`
	JmlRecord  string `json:"jml_record"`
}

type FrontPageRekapUsulan struct {
	TotalUsulanJurnal    int64
	UsulanBaru           int64
	ReAkreditasi         int64
	TotalUsulanJurnalL70 int64
	UsulanBaruL70        int64
	ReAkreditasiL70      int64
	TotalUsulanJurnaK70  int64
	UsulanBaruK70        int64
	ReAkreditasiK70      int64
}
