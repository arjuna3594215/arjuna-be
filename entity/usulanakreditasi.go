package entity

import "time"

type UsulanAkreditasi struct {
	IdUsulanAkreditasi         int64   `json:"id_usulan_akreditasi" gorm:"default:null"`
	IdIdentitasJurnal          int64   `json:"id_identitas_jurnal"`
	IdJurnal                   int64   `json:"id_jurnal" gorm:"->"`
	IdPic                      int64   `json:"id_pic"`
	UrlStatistikPengunjung     string  `json:"url_statistik_pengunjung" gorm:"default:null"`
	IdEic                      int64   `json:"id_eic"`
	UserPenilai                string  `json:"user_penilai"`
	PasswdPenilai              string  `json:"passwd_penilai"`
	StsPengajuanUsulan         string  `json:"sts_pengajuan_usulan" gorm:"<-:update"`
	FileDokumenUsulan          string  `json:"file_dokumen_usulan" gorm:"->"`
	StsAktifFormulir           string  `json:"sts_aktif_formulir" gorm:"->"`
	IdPeriodeAkreditasi        int64   `json:"id_periode_akreditasi" gorm:"default:null"`
	NamaJurnal                 string  `json:"nama_jurnal" gorm:"->"`
	GradeAkreditasi            string  `json:"grade_akreditasi" gorm:"->"`
	NilaiTotalSa               float64 `json:"nilai_total_sa" gorm:"->"`
	GradeAkreditasiSa          string  `json:"grade_akreditasi_sa" gorm:"->"`
	PeringkatGradeAkreditasiSa string  `json:"peringkat_grade_akreditasi_sa" gorm:"->"`
	StsAkreditasi              string  `json:"sts_akreditasi,omitempty" gorm:"->"`

	Alamat                string `json:"alamat" gorm:"->"`
	FrekuensiTerbitan     string `json:"frekuensi_terbitan" gorm:"->"`
	Progres               string `json:"progres" gorm:"->"`
	TahunTerbit           string `json:"tahun_terbit" gorm:"->"`
	TglAkhirTerakreditasi string `json:"tgl_akhir_terakreditasi" gorm:"->"`

	 //edit by agust 2023-04-02
	//TglUsulanAkreditasiTerakhir time.Time `json:"tgl_usulan_akreditasi_terakhir" gorm:"->"` 
	TglUsulanAkreditasiTerakhir string `json:"tgl_usulan_akreditasi_terakhir" gorm:"->"` 
	NilaiTotal                  float64   `json:"nilai_total" gorm:"->"`

	Eissn     string `json:"eissn" gorm:"->"`
	Pissn     string `json:"pissn" gorm:"->"`
	TglUsulan string `json:"tgl_usulan" gorm:"->"` // add by agust 2023-03-22 (tabah data tgl_usulan)
	StatusUsulanAkreditasi bool `json:"status_usulan_akreditasi" gorm:"->"` // add by agust 2023-03-22 (tabah data tgl_usulan)

}

type SaveUsulanAkreditasiDTO struct {
	IdJurnal          int64  `json:"id_jurnal" validate:"required"`
	IdIdentitasJurnal int64  `json:"id_identitas_jurnal" validate:"required"`
	UserPenilai       string `json:"user_penilai" validate:"required"`
	PasswdPenilai     string `json:"passwd_penilai" validate:"required"`
}

type UpdateUsulanAkreditasiDTO struct {
	UserPenilai   string `json:"user_penilai" validate:"required"`
	PasswdPenilai string `json:"passwd_penilai" validate:"required"`
}

type PenetapanAkreditasi struct {
	IdPenetapanAkreditasi      int64  `json:"Id_penetapan_akreditasi"`
	IdIdentitasJurnal          int64  `json:"Id_identitas_jurnal"`
	IdUsulanAkreditasi         int64  `json:"Id_usulan_akreditasi"`
	IdHasilAkreditasi          int64  `json:"Id_hasil_akreditasi"`
	AtsHasilAkreditasi         string `josn:"sts_hasil_akreditasi" gorm:"->"`
	IdAkAkreditasi             int64  `json:"Id_sk_akreditasi"`
	IdConfigurasi              int64  `json:"Id_configurasi"`
	urlSinta                   string `josn:"url_sinta"`
	issueTerakreditasi         string `josn:"issue_terakreditasi"`
	IdIssuePenetapanAkreditasi string `json:"Id_issue_penetapan_akreditasi"`
}

type ProgresUsulanAkreditasi struct {
	IdProgresUsulanAkreditasi int64     `json:"id_progres_usulan_akreditasi" gorm:"default:null"`
	IdUsulanAkreditasi        int64     `json:"id_usulan_akreditasi"`
	IdProgres                 int64     `json:"id_progres"`
	TglCreated                time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated                time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
}

type Resubmit struct {
	IdResubmit             int64     `json:"id_resubmit"`
	IdIdentitas_jurnal     int64     `json:"id_identitas_jurnal"`
	IdUsulanAkreditasi     int64     `json:"id_usulan_akreditasi"`
	TglPengajuanAkreditasi time.Time `json:"tgl_pengajuan_akreditasi"`
	AlasanResubmit         string    `json:"alasan_resubmit"`
	TglBatasResubmit       time.Time `json:"tgl_batas_resubmit"`
	IdPersonal             int64     `json:"id_personal"`
	TglCreated             time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated             time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
}

type PengajuanUsulanAkreditasi struct {
	IdPengajuanUsulanAkreditasi int64     `json:"id_pengajuan_usulan_akreditasi" gorm:"default:null"`
	IdUsulanAkreditasi          int64     `json:"id_usulan_akreditasi"`
	TglPengajuan                time.Time `json:"tgl_pengajuan"`
	IdResubmit                  int64     `json:"id_resubmit" gorm:"default:null"`
}

type PenetapanDeskEvaluasi struct {
	IdPenetapanDeskEvaluasi int64     `json:"id_penetapan_desk_evaluasi" gorm:"default:null"`
	IdIdentitasJurnal       int64     `json:"id_identitas_jurnal"`
	IdUsulanAkreditasi      int64     `json:"id_usulan_akreditasi"`
	StsHasilDeskEvaluasi    string    `json:"sts_hasil_desk_evaluasi"`
	IdPersonal              int64     `json:"id_personal"`
	TglCreated              time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated              time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
	IdPeriodeAkreditasi     int64     `json:"id_periode_akreditasi"`
}

type HasilPenilaianAkreditasi struct {
	IdIdentitasJurnal        int64   `json:"id_identitas_jurnal"`
	IdUsulanAkreditasi       int64   `json:"id_usulan_akreditasi"`
	IdPic                    int64   `json:"id_pic"`
	IdEic                    int64   `json:"id_eic"`
	NamaJurnal               string  `json:"nama_jurnal"`
	Eissn                    string  `json:"eissn"`
	Pissn                    string  `json:"pissn"`
	UrlStatistikPengunjung   string  `json:"url_statistik_pengunjung"`
	UrlJurnal                string  `json:"url_jurnal"`
	UrlContact               string  `json:"url_contact"`
	UrlEditor                string  `json:"url_editor"`
	AlamatOai                string  `json:"alamat_oai"`
	DoiJurnal                string  `json:"doi_jurnal"`
	TglDiusulkan             string  `json:"tgl_diusulkan"`
	NilaiTotalSa             float64 `json:"nilai_total_sa"`
	GradeAkreditasiSa        string  `json:"grade_akreditasi_sa"`
	NilaiTotalAkreditasi     float64 `json:"nilai_total_akreditasi"`
	GradeAkreditasi          string  `json:"grade_akreditasi"`
	TglPencatatanSk          string  `json:"tgl_pencatatan_sk"`
	StsHasilAkreditasi       string  `json:"sts_hasil_akreditasi"`
	HasilDeskEvaluasi        string  `json:"hasil_desk_evaluasi"`
	TglPenetapanDeskEvaluasi string  `json:"tgl_penetapan_desk_evaluasi"`
	KomentarPenolakan        string  `json:"komentar_penolakan"`
	GradeAkreditasiHasilAa   string  `json:"grade_akreditasi_hasil_sa"`
	StsHasilDeskEvaluasi     string  `json:"sts_hasil_desk_evaluasi"`
}

type UsulanItemPenolakan struct {
	IdUsulanItemPenolakan int64  `json:"id_usulan_item_penolakan"`
	ItemPenolakan         string `json:"item_penolakan"`
}

type KomentarHasilPenilaianAkreditasiDTO struct {
	IDPersonal         int64 `json:"id_personal"`
	IDUsulanAkreditasi int64 `json:"id_usulan_akreditasi"`
}

type KomentarHasilPenilaianAkreditasi struct {
	IdUsulanAkreditasi     int64             `json:"id_usulan_akreditasi"`
	IdIdentitasJurnal      int64             `json:"id_identitas_jurnal"`
	IdPic                  int64             `json:"id_pic"`
	NamaJurnal             string            `json:"nama_jurnal"`
	Eissn                  string            `json:"eissn"`
	Pissn                  string            `json:"pissn"`
	UrlJurnal              string            `json:"url_jurnal"`
	UrlContact             string            `json:"url_contact"`
	UrlEditor              string            `json:"url_editor"`
	UrlStatistikPengunjung string            `json:"url_statistik_pengunjung"`
	AlamatOai              string            `json:"alamat_oai"`
	DoiJurnal              string            `json:"doi_jurnal"`
	UserPenilai            string            `json:"user_penilai"`
	PasswordPenilai        string            `json:"password_penilai"`
	TglCreated             string            `json:"tgl_created"`
	TglUpdated             string            `json:"tgl_updated"`
	KomentarAsessor        []KomentarAsessor `json:"komentar_asessor" gorm:"-"`
}
