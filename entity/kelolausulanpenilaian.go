package entity

type PenugasanPenilaian struct {
	IDPenugasanPenilaian   int64  `json:"id_penugasan_penilaian"`
	IDUsulanAkreditasi     int64  `json:"id_usulan_akreditasi"`
	IDPersonal             int64  `json:"id_personal"`
	StsPenerimaanPenugasan string `json:"sts_penerimaan_penugasan"`
}

type UpdateStatusPenugasanPenilaianDTO struct {
	IDPersonal             int64  `json:"id_personal" validate:"required"`
	Penilai                string `json:"penilai" validate:"required"`
	StsPenerimaanPenugasan string `json:"sts_penerimaan_penugasaan" validate:"required"`
	KomentarPenolakan      string `json:"komentar_penolakan"`
}

type PenilaianSelesaiManagement struct {
	IDPenugasanPenilaianManajemen int64   `json:"id_penugasan_penilaian_manajemen"`
	IDUsulanAkreditasi            int64   `json:"id_usulan_akreditasi"`
	TglPenugasan                  string  `json:"tgl_penugasan"`
	TglPenerimaanPenugasan        string  `json:"tgl_penerimaan_penugasan"`
	TglPenetapanPenilaian         string  `json:"tgl_penetapan_penilaian"`
	UrlStatistikPengunjung        string  `json:"url_statistik_pengunjung"`
	NamaJurnal                    string  `json:"nama_jurnal"`
	Eissn                         string  `json:"eissn"`
	Pissn                         string  `json:"pissn"`
	Publisher                     string  `json:"publisher"`
	Society                       string  `json:"society"`
	NamaAwalJurnal                string  `json:"nama_awal_jurnal"`
	TglPembuatan                  string  `json:"tgl_pembuatan"`
	UrlJurnal                     string  `json:"url_jurnal"`
	UrlContact                    string  `json:"url_contact"`
	UrlEditor                     string  `json:"url_editor"`
	Country                       string  `json:"country"`
	City                          string  `json:"city"`
	Alamat                        string  `json:"alamat"`
	NoTelepon                     string  `json:"no_telepon"`
	AlamatSurel                   string  `json:"alamat_surel"`
	TglAkhirTerakreditasi         string  `json:"tgl_akhir_terakreditasi"`
	NamaPic                       string  `json:"nama_pic"`
	NamaInstitusi                 string  `json:"nama_institusi"`
	NamaEic                       string  `json:"nama_eic"`
	AlamatSurelEic                string  `json:"alamat_surel_eic"`
	AlamatOai                     string  `json:"alamat_oai"`
	DoiJurnal                     string  `json:"doi_jurnal"`
	NilaiDisinsentif              float64 `json:"nilai_disinsentif"`
	HasilPenilaianMgmt            float64 `json:"hasil_penilaian_mgmt"`
}

type PenilaianSelesaiEvaluator struct {
	IDPenugasanPenilaianEvaluator int64   `json:"id_penugasan_penilaian_evaluator"`
	IDUsulanAkreditasi            int64   `json:"id_usulan_akreditasi"`
	TglPenugasan                  string  `json:"tgl_penugasan"`
	TglPenerimaanPenugasan        string  `json:"tgl_penerimaan_penugasan"`
	TglPenetapanPenilaian         string  `json:"tgl_penetapan_penilaian"`
	UrlStatistikPengunjung        string  `json:"url_statistik_pengunjung"`
	NamaJurnal                    string  `json:"nama_jurnal"`
	Eissn                         string  `json:"eissn"`
	Pissn                         string  `json:"pissn"`
	Publisher                     string  `json:"publisher"`
	Society                       string  `json:"society"`
	NamaAwalJurnal                string  `json:"nama_awal_jurnal"`
	TglPembuatan                  string  `json:"tgl_pembuatan"`
	UrlJurnal                     string  `json:"url_jurnal"`
	UrlContact                    string  `json:"url_contact"`
	UrlEditor                     string  `json:"url_editor"`
	Country                       string  `json:"country"`
	City                          string  `json:"city"`
	Alamat                        string  `json:"alamat"`
	NoTelepon                     string  `json:"no_telepon"`
	AlamatSurel                   string  `json:"alamat_surel"`
	TglAkhirTerakreditasi         string  `json:"tgl_akhir_terakreditasi"`
	NamaPic                       string  `json:"nama_pic"`
	NamaInstitusi                 string  `json:"nama_institusi"`
	NamaEic                       string  `json:"nama_eic"`
	AlamatSurelEic                string  `json:"alamat_surel_eic"`
	AlamatOai                     string  `json:"alamat_oai"`
	DoiJurnal                     string  `json:"doi_jurnal"`
	NilaiDisinsentif              float64 `json:"nilai_disinsentif"`
	HasilPenilaianEvaluator       float64 `json:"hasil_penilaian_evaluator"`
}

type PenilaianSelesaiIssue struct {
	IDPenugasanPenilaianIssue int64   `json:"id_penugasan_penilaian_issue"`
	IDUsulanAkreditasi        int64   `json:"id_usulan_akreditasi"`
	TglPenugasan              string  `json:"tgl_penugasan"`
	TglPenerimaanPenugasan    string  `json:"tgl_penerimaan_penugasan"`
	TglPenetapanPenilaian     string  `json:"tgl_penetapan_penilaian"`
	UrlStatistikPengunjung    string  `json:"url_statistik_pengunjung"`
	NamaJurnal                string  `json:"nama_jurnal"`
	Eissn                     string  `json:"eissn"`
	Pissn                     string  `json:"pissn"`
	Publisher                 string  `json:"publisher"`
	Society                   string  `json:"society"`
	NamaAwalJurnal            string  `json:"nama_awal_jurnal"`
	TglPembuatan              string  `json:"tgl_pembuatan"`
	UrlJurnal                 string  `json:"url_jurnal"`
	UrlContact                string  `json:"url_contact"`
	UrlEditor                 string  `json:"url_editor"`
	Country                   string  `json:"country"`
	City                      string  `json:"city"`
	Alamat                    string  `json:"alamat"`
	NoTelepon                 string  `json:"no_telepon"`
	AlamatSurel               string  `json:"alamat_surel"`
	TglAkhirTerakreditasi     string  `json:"tgl_akhir_terakreditasi"`
	NamaPic                   string  `json:"nama_pic"`
	NamaInstitusi             string  `json:"nama_institusi"`
	NamaEic                   string  `json:"nama_eic"`
	AlamatSurelEic            string  `json:"alamat_surel_eic"`
	AlamatOai                 string  `json:"alamat_oai"`
	DoiJurnal                 string  `json:"doi_jurnal"`
	NilaiDisinsentif          float64 `json:"nilai_disinsentif"`
	HasilPenilaianIssue       float64 `json:"hasil_penilaian_issue"`
}

type KSPenugasanPenilaianManagement struct {
	IDPenugasanPenilaianManajemen int64   `json:"id_penugasan_penilaian_manajemen"`
	IDUsulanAkreditasi            int64   `json:"id_usulan_akreditasi"`
	TglPenugasan                  string  `json:"tgl_penugasan"`
	TglPenerimaan                 string  `json:"tgl_penerimaan"`
	UrlStatistikPengunjung        string  `json:"url_statistik_pengunjung"`
	NamaJurnal                    string  `json:"nama_jurnal"`
	Eissn                         string  `json:"eissn"`
	Pissn                         string  `json:"pissn"`
	Publisher                     string  `json:"publisher"`
	Society                       string  `json:"society"`
	NamaAwalJurnal                string  `json:"nama_awal_jurnal"`
	TglPembuatan                  string  `json:"tgl_pembuatan"`
	UrlJurnal                     string  `json:"url_jurnal"`
	UrlContact                    string  `json:"url_contact"`
	UrlEditor                     string  `json:"url_editor"`
	Country                       string  `json:"country"`
	City                          string  `json:"city"`
	Alamat                        string  `json:"alamat"`
	NoTelepon                     string  `json:"no_telepon"`
	AlamatSurel                   string  `json:"alamat_surel"`
	TglAkhirTerakreditasi         string  `json:"tgl_akhir_terakreditasi"`
	NamaPic                       string  `json:"nama_pic"`
	NamaInstitusi                 string  `json:"nama_institusi"`
	NamaEic                       string  `json:"nama_eic"`
	AlamatSurelEic                string  `json:"alamat_surel_eic"`
	AlamatOai                     string  `json:"alamat_oai"`
	DoiJurnal                     string  `json:"doi_jurnal"`
	UserPenilai                   string  `json:"user_penilai"`
	PasswordPenilai               string  `json:"password_penilai"`
	NilaiManagement               float64 `json:"nilai_management"`
	NilaiMgmt                     float64 `json:"-"`
	NilaiDisinsentif              float64 `json:"-"`
	SelisihPenilaian              string  `json:"selisih_penilaian"`
	PasanganAsessor               string  `json:"pasangan_asessor"`
	KomentarPenyesuaianNilai      string  `json:"komentar_penyesuaian_nilai"`
	Issue                         []Issue `json:"issue" gorm:"foreignKey:IDUsulanAkreditasi;references:IDUsulanAkreditasi"`
}

type KSPenugasanPenilaianEvaluator struct {
	IDPenugasanPenilaianEvaluator int64   `json:"id_penugasan_penilaian_evaluator"`
	IDUsulanAkreditasi            int64   `json:"id_usulan_akreditasi"`
	TglPenugasan                  string  `json:"tgl_penugasan"`
	TglPenerimaan                 string  `json:"tgl_penerimaan"`
	UrlStatistikPengunjung        string  `json:"url_statistik_pengunjung"`
	NamaJurnal                    string  `json:"nama_jurnal"`
	Eissn                         string  `json:"eissn"`
	Pissn                         string  `json:"pissn"`
	Publisher                     string  `json:"publisher"`
	Society                       string  `json:"society"`
	NamaAwalJurnal                string  `json:"nama_awal_jurnal"`
	TglPembuatan                  string  `json:"tgl_pembuatan"`
	UrlJurnal                     string  `json:"url_jurnal"`
	UrlContact                    string  `json:"url_contact"`
	UrlEditor                     string  `json:"url_editor"`
	Country                       string  `json:"country"`
	City                          string  `json:"city"`
	Alamat                        string  `json:"alamat"`
	NoTelepon                     string  `json:"no_telepon"`
	AlamatSurel                   string  `json:"alamat_surel"`
	TglAkhirTerakreditasi         string  `json:"tgl_akhir_terakreditasi"`
	NamaPic                       string  `json:"nama_pic"`
	NamaInstitusi                 string  `json:"nama_institusi"`
	NamaEic                       string  `json:"nama_eic"`
	AlamatSurelEic                string  `json:"alamat_surel_eic"`
	AlamatOai                     string  `json:"alamat_oai"`
	DoiJurnal                     string  `json:"doi_jurnal"`
	UserPenilai                   string  `json:"user_penilai"`
	PasswordPenilai               string  `json:"password_penilai"`
	NilaiEvaluator                float64 `json:"nilai_evaluator"`
	NilaiEvaluasi                 float64 `json:"-"`
	NilaiDisinsentif              float64 `json:"-"`
	// SelisihPenilaian              string  `json:"selisih_penilaian"`
	// PasanganAsessor               string  `json:"pasangan_assesor"`
	// KomentarPenyesuaianNilai      string  `json:"komentar_penyesuaian_nilai"`
	Issue []Issue `json:"issue" gorm:"foreignKey:IDUsulanAkreditasi;references:IDUsulanAkreditasi"`
}

type KSPenugasanPenilaianIssue struct {
	IDPenugasanPenilaianIssue int64              `json:"id_penugasan_penilaian_issue"`
	IDJurnal                  int64              `json:"id_jurnal"`
	IDIssue                   int64              `json:"id_issue"`
	IDUsulanAkreditasi        int64              `json:"id_usulan_akreditasi"`
	TglPenugasan              string             `json:"tgl_penugasan"`
	TglPenerimaan             string             `json:"tgl_penerimaan"`
	UrlStatistikPengunjung    string             `json:"url_statistik_pengunjung"`
	NamaJurnal                string             `json:"nama_jurnal"`
	Eissn                     string             `json:"eissn"`
	Pissn                     string             `json:"pissn"`
	Publisher                 string             `json:"publisher"`
	Society                   string             `json:"society"`
	NamaAwalJurnal            string             `json:"nama_awal_jurnal"`
	TglPembuatan              string             `json:"tgl_pembuatan"`
	UrlJurnal                 string             `json:"url_jurnal"`
	UrlContact                string             `json:"url_contact"`
	UrlEditor                 string             `json:"url_editor"`
	Country                   string             `json:"country"`
	City                      string             `json:"city"`
	Alamat                    string             `json:"alamat"`
	NoTelepon                 string             `json:"no_telepon"`
	AlamatSurel               string             `json:"alamat_surel"`
	TglAkhirTerakreditasi     string             `json:"tgl_akhir_terakreditasi"`
	NamaPic                   string             `json:"nama_pic"`
	NamaInstitusi             string             `json:"nama_institusi"`
	NamaEic                   string             `json:"nama_eic"`
	AlamatSurelEic            string             `json:"alamat_surel_eic"`
	UrlIssue                  string             `json:"url_issue"`
	JudulIssue                string             `json:"judul_issue"`
	VolumeIssue               string             `json:"volume_issue"`
	NomorIssue                string             `json:"nomor_issue"`
	TahunIssue                string             `json:"tahun_issue"`
	BidangIlmuJurnal          []BidangIlmuJurnal `json:"bidang_ilmu_jurnal" gorm:"foreignKey:IdJurnal;references:IDJurnal"`
	AlamatOai                 string             `json:"alamat_oai"`
	DoiJurnal                 string             `json:"doi_jurnal"`
	UserPenilai               string             `json:"user_penilai"`
	PasswordPenilai           string             `json:"password_penilai"`
	NilaiKonten               float64            `json:"-"`
	NilaiDisinsentif          float64            `json:"-"`
	SelisihPenilaian          string             `json:"selisih_penilaian"`
	PasanganAsessor           string             `json:"pasangan_asessor"`
	KomentarPenyesuaianNilai  string             `json:"komentar_penyesuaian_nilai"`
	NilaiIssue                float64            `json:"nilai_issue"`
}

type DetailPenilaian struct {
	IDIdentitasJurnal  int64   `json:"id_identitas_jurnal"`
	IDUsulanAkreditasi int64   `json:"id_usulan_akreditasi"`
	NamaJurnal         string  `json:"nama_jurnal"`
	UrlJurnal          string  `json:"url_jurnal"`
	Eissn              string  `json:"eissn"`
	Pissn              string  `json:"pissn"`
	UserPenilai        string  `json:"user_penilai"`
	PasswordPenilai    string  `json:"password_penilai"`
	Status             string  `json:"status"`
	NilaiTotal         float64 `json:"nilai"`
	Nilai              float64 `json:"-"`
	NilaiDisinsentif   float64 `json:"-"`
	Issue              []Issue `json:"issue" gorm:"foreignKey:IDUsulanAkreditasi;references:IDUsulanAkreditasi"`
}

type DetailPenilaianIssue struct {
	IDIssue            int64  `json:"id_issue"`
	IDJurnal           int64  `json:"id_jurnal"`
	IDUsulanAkreditasi int64  `json:"id_usulan_akreditasi"`
	IDIdentitasJurnal  int64  `json:"id_identitas_jurnal"`
	NamaJurnal         string `json:"nama_jurnal"`
	Publisher          string `json:"publisher"`
	Eissn              string `json:"eissn"`
	Pissn              string `json:"pissn"`
	UserPenilai        string `json:"user_penilai"`
	PasswordPenilai    string `json:"password_penilai"`
	Status             string `json:"status"`
	JudulIssue         string `json:"judul_issue"`
	VolumeIssue        string `json:"volume_issue"`
	NomorIssue         string `json:"nomor_issue"`
	TahunIssue         string `json:"tahun_issue"`
	UrlIssue           string `json:"url_issue"`

	//add agust 2023-03-20 tanbah total nilai issue
	IdPenugasanPenilaianIssue	int64  `json:"id_penugasan_penilaian_issue"`  
	NilaiIssue			float64 `json:"nilai_issue"`

	
}
