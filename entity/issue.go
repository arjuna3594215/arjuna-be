package entity

import (
	"time"
)

type Issue struct {
	IDIssue                 int64                     `json:"id_issue" gorm:"default:null"`
	IDJurnal                int64                     `json:"id_jurnal"`
	IDIdentitasJurnal       int64                     `json:"id_identitas_jurnal"`
	IDUsulanAkreditasi      int64                     `json:"id_usulan_akreditasi"`
	JudulIssue              string                    `json:"judul_issue" gorm:"default:null"`
	UrlIssue                string                    `json:"url_issue"`
	VolumeIssue             string                    `json:"volume_issue"`
	NomorIssue              string                    `json:"nomor_issue"`
	TahunIssue              string                    `json:"tahun_issue"`
	FrekuensiTerbitan       string                    `json:"frekuensi_terbitan" gorm:"<-:read"`
	TglCreated              time.Time                 `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated              time.Time                 `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
	DoiIssue                string                    `json:"doi_issue" gorm:"default:null"`
	PenugasanPenilaianIssue []PenugasanPenilaianIssue `json:"penugasan_penilaian_issue,omitempty" gorm:"foreignKey:IDIssue;references:IDIssue"`
}

type IssueDTO struct {
	// IdJurnal           int64  `json:"id_jurnal" validate:"required"`
	// IdIdentitasJurnal  int64  `json:"id_identitas_jurnal" validate:"required"`
	IdUsulanAkreditasi int64  `json:"id_usulan_akreditasi" validate:"required"`
	JudulIssue         string `json:"judul_issue"`
	UrlIssue           string `json:"url_issue" validate:"required,url"`
	VolumeIssue        string `json:"volume_issue" validate:"required"`
	NomorIssue         string `json:"nomor_issue" validate:"required"`
	TahunIssue         string `json:"tahun_issue" validate:"required,max=4,numeric"`
}

type IssuePenetapanAkreditasi struct {
	IDIssuePenetapanAkreditasi string    `json:"id_issue_penetapan_akreditasi"`
	VolumeAwal                 int64     `json:"volume_awal"`
	VolumeAkhir                int64     `json:"volume_akhir"`
	NomorAwal                  int64     `json:"nomor_awal"`
	NomorAkhir                 int64     `json:"nomor_akhir"`
	TahunAwal                  int64     `json:"tahun_awal"`
	TahunAkhir                 int64     `json:"tahun_akhir"`
	TglCreated                 time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated                 time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
	IDPenetapanAkreditasi      int64     `json:"id_penetapan_akreditasi"`
}
