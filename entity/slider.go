package entity

import (
	"mime/multipart"
	"time"
)

type Slider struct {
	ID            int64     `json:"id"`
	Title         string    `json:"title"`
	SubTitle      string    `json:"sub_title"`
	Imagename     string    `json:"imagename"`
	Url           string    `json:"url"`
	Torder        int64     `json:"torder"`
	DateLast      time.Time `json:"date_last"`
	DateCreated   time.Time `json:"date_created"`
	PathImagename string    `json:"path_imagename" gorm:"->"`
}

type SliderDTO struct {
	Title     string                `json:"title" form:"title" validate:"required"`
	SubTitle  string                `json:"sub_title" form:"sub_title" validate:"required"`
	Imagename *multipart.FileHeader `json:"imagename" form:"imagename"`
	Url       string                `json:"url" form:"url"`
	DateLast  string                `json:"date_last" form:"date_last"`
}
