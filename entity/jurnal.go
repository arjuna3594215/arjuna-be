package entity

import (
	"time"
)

type Jurnal struct {
	IdJurnal                    int64     `json:"id_jurnal" gorm:"default:null"`
	NamaAwalJurnal              string    `json:"nama_awal_jurnal" gorm:"<-:create"`
	TglPembuatan                time.Time `json:"tgl_pembuatan" gorm:"->"`
	UrlJurnal                   string    `json:"url_jurnal"`
	UrlContact                  string    `json:"url_contact"`
	Country                     string    `json:"country"`
	City                        string    `json:"city"`
	Alamat                      string    `json:"alamat"`
	NoTelepon                   string    `json:"no_telepon"`
	AlamatSurel                 string    `json:"alamat_surel"`
	TglAkhirTerakreditasi       time.Time `json:"tgl_akhir_terakreditasi" gorm:"<-:create;default:null"`
	//TglUsulanAkreditasiTerakhir time.Time `json:"tgl_usulan_akreditasi_terakhir" gorm:"default:null"`
	TglCreated                  time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated                  time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
	TglDuedate                  time.Time `json:"tgl_duedate" gorm:"<-:create;default:null"`
	UrlEditor                   string    `json:"url_editor"`
	UrlStatistikPengunjung      string    `json:"url_statistik_pengunjung"`
	UrlReviewer                 string    `json:"url_reviewer"`
	UrlPengindeks               string    `json:"url_pengindeks"`
	Tahun_1Terbit               string    `json:"tahun_1_terbit"`
	IDBidangFokus               int       `json:"id_bidang_fokus" gorm:"-"`
	NamaPT                      string    `json:"nama_pt" gorm:"-"`
}

type IdentitasJurnal struct {
	IdIdentitasJurnal       int64     `json:"id_identitas_jurnal" gorm:"default:null"`
	IdJurnal                int64     `json:"id_jurnal"`
	NamaJurnal              string    `json:"nama_jurnal"`
	Eissn                   string    `json:"eissn"`
	Pissn                   string    `json:"pissn" gorm:"default:null"`
	Publisher               string    `json:"publisher"`
	Society                 string    `json:"society"`
	StsAktifIdentitasJurnal string    `json:"sts_aktif_identitas_jurnal" gorm:"->"`
	TglCreated              time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated              time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
	AlamatOai               string    `json:"alamat_oai"`
	DoiJurnal               string    `json:"doi_jurnal"`
	UrlGooglescholar        string    `json:"url_googlescholar"`
	UrlEtikapublikasi       string    `json:"url_etikapublikasi"`
	FrekuensiTerbitan       int64     `json:"frekuensi_terbitan"`
	IDBidangFokus           int       `json:"id_bidang_fokus"`
	NamaPT                  string    `json:"nama_pt"`
}

type JurnalDTO struct {
	NamaAwalJurnal         string `json:"nama_awal_jurnal"`
	Tahun1Terbit           string `json:"tahun_1_terbit"`
	UrlJurnal              string `json:"url_jurnal"`
	UrlContact             string `json:"url_contact"`
	UrlEditor              string `json:"url_editor"`
	UrlReviewer            string `json:"url_reviewer"`
	CodeCountry            string `json:"code_country"`
	CodeCity               string `json:"code_city"`
	Alamat                 string `json:"alamat"`
	NoTelepon              string `json:"no_telepon"`
	AlamatSurel            string `json:"alamat_surel"`
	UrlStatistikPengunjung string `json:"url_statistik_pengunjung"`
	UrlPengindeks          string `json:"url_pengindeks"`
	NamaJurnal             string `json:"nama_jurnal"`
	Eissn                  string `json:"eissn" validate:"required"`
	Pissn                  string `json:"pissn"`
	Publisher              string `json:"publisher"`
	Society                string `json:"society"`
	UrlEtikapublikasi      string `json:"url_etikapublikasi"`
	UrlGooglescholar       string `json:"url_googlescholar"`
	AlamatOai              string `json:"alamat_oai"`
	DoiJurnal              string `json:"doi_jurnal"`
	FrekuensiTerbitan      string `json:"frekuensi_terbitan"`
	NamaEic                string `json:"nama_eic"`
	KodeBidangIlmu         string `json:"kode_bidang_ilmu"`
	IDBidangFokus          int    `json:"id_bidang_fokus"`
	NamaPT                 string `json:"nama_pt"`
	// tgl_pembuatan string `json:"	// tgl_pembuatan string `json:""`"`
}

type JurnalUpdateDTO struct {
	// IdIdentitasJurnal int64  `json:"id_identitas_jurnal"`
	// IdPengguna        int64  `json:"id_pengguna"`
	IdJurnal     int64  `json:"id_jurnal" validate:"required"`
	IdPic        int64  `json:"id_pic" validate:"required"`
	NamaJurnal   string `json:"nama_jurnal" validate:"required"`
	Eissn        string `json:"eissn" validate:"required"`
	//Pissn        string `json:"pissn" validate:"required"`  edit by agus 2023-04-09
	Pissn        string `json:"pissn"` 
	Publisher    string `json:"publisher" validate:"required"`
	//Society      string `json:"society" validate:"required"`
	Society      string `json:"society"`
	UrlJurnal    string `json:"url_jurnal" validate:"required"`
	Tahun1Terbit string `json:"tahun_1_terbit" validate:"required"`
	// TglPembuatan           string `json:"tgl_pembuatan"` edit by agus 2023-04-09
	UrlContact             string `json:"url_contact" validate:"required"`
	UrlStatistikPengunjung string `json:"url_statistik_pengunjung"`
	UrlPengindeks          string `json:"url_pengindeks"`
	UrlGooglescholar       string `json:"url_googlescholar"`
	UrlEditor              string `json:"url_editor" validate:"required"`
	UrlReviewer            string `json:"url_reviewer" validate:"required"`
	UrlEtikapublikasi      string `json:"url_etikapublikasi" validate:"required"`
	Country                string `json:"country" validate:"required"`
	City                   string `json:"city" validate:"required"`
	Alamat                 string `json:"alamat" validate:"required"`
	NoTelepon              string `json:"no_telepon" validate:"required"`
	AlamatSurel            string `json:"alamat_surel" validate:"required"`
	AlamatOai              string `json:"alamat_oai" validate:"required"`
	DoiJurnal              string `json:"doi_jurnal"`
	FrekuensiTerbitan      int64  `json:"frekuensi_terbitan" validate:"required"`
	NamaEic                string `json:"nama_eic"`
	//IDBidangFokus          int    `json:"id_bidang_fokus"` edit by agus 2023-04-09
	IDBidangFokus          int    `json:"id_bidang_fokus"validate:"required"`
	NamaPT                 string `json:"nama_pt"`
}

type DaftarTerbitanJurnal struct {
	IDPic                      int64   `json:"id_pic"`
	IdJurnal                   int64   `json:"id_jurnal"`
	IdIdentitasJurnal          int64   `json:"id_identitas_jurnal"`
	IDUsulanAkreditasi         int64   `json:"id_usulan_akreditasi"`
	NamaJurnal                 string  `json:"nama_jurnal"`
	TglPembuatan               string  `json:"tgl_pembuatan"`
	TglCreated                 string  `json:"tgl_created"`
	PicNama                    string  `json:"pic_nama"`
	PicEmail                   string  `json:"pic_email"`
	PicNoTelepon               string  `json:"pic_no_telepon"`
	Username                   string  `json:"username"`
	Alamat                     string  `json:"alamat"`
	AlamatSurel                string  `json:"alamat_surel"`
	NoTelepon                  string  `json:"no_telepon"`
	UrlJurnal                  string  `json:"url_jurnal"`
	StsAktifIdentitasJurnal    string  `json:"sts_aktif_identitas_jurnal"`
	NilaiTotal                 float64 `json:"nilai_total"`
	GradeAkreditasi            string  `json:"grade_akreditasi"`
	Image                      string  `json:"image"`
	TahunTerbit                string  `json:"tahun_terbit"`
	NilaiTotalSa               float64 `json:"nilai_total_sa"`
	GradeAkreditasiSa          string  `json:"grade_akreditasi_sa"`
	PeringkatGradeAkreditasiSa string  `json:"peringkat_grade_akreditasi_sa"`
	PeringkatGradeAkreditasi   string  `json:"peringkat_grade_akreditasi"` //Add by agus 2023-04-02
	FrekuensiTerbitan          int64   `json:"frekuensi_terbitan"`
	//TglUsulanAkreditasiTerakhir time.Time `json:"tgl_usulan_akreditasi_terakhir"` //edit by agus 2023-04-02
	TglUsulanAkreditasiTerakhir string `json:"tgl_usulan_akreditasi_terakhir"`
	Progres                     string `json:"progres"`
	IdProgres                   int    `json:"id_progres"`
	Eissn                       string `json:"eissn"`
	Pissn                       string `json:"pissn"`
	Publisher                   string `json:"publisher"`
	Society                     string `json:"society"`
	NamaAwalJurnal              string `json:"nama_awal_jurnal"`
	UrlContact                  string `json:"url_contact"`
	UrlEditor                   string `json:"url_editor"`
	UrlStatistikPengunjung      string `json:"url_statistik_pengunjung"`
	Country                     string `json:"country"`
	City                        string `json:"city"`
	//TglAkhirTerakreditasi     time.Time `json:"tgl_akhir_terakreditasi"`//edit by agus 2023-04-02
	TglAkhirTerakreditasi string  `json:"tgl_akhir_terakreditasi"`
	BidangIlmu            bool    `json:"bidang_ilmu"`
	Issue                 []Issue `json:"issue" gorm:"foreignKey:IDUsulanAkreditasi;references:IDUsulanAkreditasi"`
	// StsHasilAkreditasi     string    `json:"sts_hasil_akreditasi"`
}

type DetailJurnal struct {
	IdIdentitasJurnal           int64              `json:"id_identitas_jurnal"`
	IdJurnal                    int64              `json:"id_jurnal"`
	IdPic                       int64              `json:"id_pic"`
	NamaJurnal                  string             `json:"nama_jurnal"`
	Eissn                       string             `json:"eissn"`
	Pissn                       string             `json:"pissn"`
	Publisher                   string             `json:"publisher"`
	Society                     string             `json:"society"`
	NamaAwalJurnal              string             `json:"nama_awal_jurnal"`
	TahunTerbit                 string             `json:"tahun_terbit"`
	UrlJurnal                   string             `json:"url_jurnal"`
	UrlContact                  string             `json:"url_contact"`
	UrlEditor                   string             `json:"url_editor"`
	UrlStatistik_pengunjung     string             `json:"url_statistik_pengunjung"`
	UrlPengindeks               string             `json:"url_pengindeks"`
	UrlReviewer                 string             `json:"url_reviewer"`
	UrlEtikapublikasi           string             `json:"url_etikapublikasi"`
	UrlGooglescholar            string             `json:"url_googlescholar"`
	Country                     string             `json:"country"`
	City                        string             `json:"city"`
	Alamat                      string             `json:"alamat"`
	AlamatSurel                 string             `json:"alamat_surel"`
	NoTelepon                   string             `json:"no_telepon"`
	AlamatOai                   string             `json:"alamat_oai"`
	DoiJurnal                   string             `json:"doi_jurnal"`
	TglAkhirTerakreditasi       time.Time          `json:"tgl_akhir_terakreditasi"`
	TglUsulanAkreditasiTerakhir time.Time          `json:"tgl_usulan_akreditasi_terakhir"`
	FrekuensiTerbitan           int64              `json:"frekuensi_terbitan"`
	Image                       string             `json:"image"`
	EICJurnal                   EICJurnal          `json:"eicjurnal" gorm:"foreignKey:IdIdentitasJurnal;references:IdIdentitasJurnal"`
	BidangIlmuJurnal            []BidangIlmuJurnal `json:"bidang_ilmu_jurnal" gorm:"foreignKey:IdIdentitasJurnal;references:IdIdentitasJurnal"`
	IDBidangFokus               int                `json:"id_bidang_fokus"`
	NamaPT                      string             `json:"nama_pt"`
}

type EICJurnal struct {
	IdIdentitasJurnal int64     `json:"id_identitas_jurnal"`
	IdEic             int64     `json:"id_eic" gorm:"default:null"`
	NamaEic           string    `json:"nama_eic"`
	NoTelepon         string    `json:"no_telepon"`
	AlamatSurel       string    `json:"alamat_surel"`
	TglCreated        time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	TglUpdated        time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
}

type PICJurnal struct {
	IdPic             int64 `json:"id_pic" gorm:"default:null"`
	IdIdentitasJurnal int64 `json:"id_identitas_jurnal"`
	IdPersonal        int64 `json:"id_personal"`
}

type GetIdentitasPICDTO struct {
	Email string `json:"email" validate:"required,email"`
}

type UpdatePICDTO struct {
	IdPic      int64 `json:"id_pic" validate:"required"`
	IdPersonal int64 `json:"id_personal" validate:"required"`
}

type DaftarUsulanJurnal struct {
	IDIdentitasJurnal           int64     `json:"id_identitas_jurnal"`
	IdUsulanAkreditasi          int64     `json:"id_usulan_akreditasi"`
	IdPic1                      int64     `json:"id_pic1"`
	IdPic2                      int64     `json:"id_pic2"`
	IdPersonal                  int64     `json:"id_personal"`
	NamaJurnal                  string    `json:"nama_jurnal"`
	Publisher                   string    `json:"publisher"`
	UrlJurnal                   string    `json:"url_jurnal"`
	BidangIlmu                  string    `json:"bidang_ilmu"`
	TglUsulanAkreditasiTerakhir time.Time `json:"tgl_usulan_akreditasi_terakhir"`
	TglCreatedUsulan            time.Time `json:"tgl_created_usulan"`
	TglUpdatedUsulan            time.Time `json:"tgl_updated_usulan"`
	TglEvaluasidiri             time.Time `json:"tgl_evaluasidiri"`
	NilaiEvaluasi               float64   `json:"nilai_evaluasi"`
	IDPenetapanDeskEvaluasi     int64     `json:"id_penetapan_desk_evaluasi"`
	DeskCreate                  string    `json:"desk_create"`
	DeskUpdate                  string    `json:"desk_update"`
	StsHasilDeskEvaluasi        string    `json:"sts_hasil_desk_evaluasi"`
	PenugasanIssueCreated       string    `json:"penugasan_issue_created"`
	PenilaiIssue                []string  `json:"penilai_issue"`
	PenugasanIssueTerlaksana    string    `json:"penugasan_issue_terlaksana"`
	PenugasanMgmtCreated        string    `json:"penugasan_mgmt_created"`
	PenilaiMgmt                 []string  `json:"penilai_mgmt"`
	PenugasanMgmtTerlaksana     string    `json:"penugasan_mgmt_terlaksana"`
	Nilai                       float64   `json:"nilai"`
	PenetapanUpdated            string    `json:"penetapan_updated"`
	StsHasilAkreditasi          string    `json:"sts_hasil_akreditasi"`
	AkreditasiUpdated           string    `json:"akreditasi_updated"`
	NilaiAkreditasi             float64   `json:"nilai_akreditasi"`
	GradeAkreditasi             string    `json:"grade_akreditasi"`
}

type ListJurnalSk struct {
	IDPenetapanAkreditasi      int64   `json:"id_penetapan_akreditasi"`
	IDUsulanAkreditasi         int64   `json:"id_usulan_akreditasi"`
	IDIdentitasJurnal          int64   `json:"id_identitas_jurnal"`
	IDPic                      int64   `json:"id_pic"`
	IDEic                      int64   `json:"id_eic"`
	NamaJurnal                 string  `json:"nama_jurnal"`
	UrlJurnal                  string  `json:"url_jurnal"`
	Publisher                  string  `json:"publisher"`
	Alamat                     string  `json:"alamat"`
	City                       string  `json:"city"`
	Country                    string  `json:"country"`
	NoTelepon                  string  `json:"no_telepon"`
	AlamatSurel                string  `json:"alamat_surel"`
	Eissn                      string  `json:"eissn"`
	Pissn                      string  `json:"pissn"`
	UrlStatistikPengunjung     string  `json:"url_statistik_pengunjung"`
	UrlContact                 string  `json:"url_contact"`
	UrlEditor                  string  `json:"url_editor"`
	AlamatOai                  string  `json:"alamat_oai"`
	DoiJurnal                  string  `json:"doi_jurnal"`
	TglCreated                 string  `json:"tgl_created"`
	TglSk                      string  `json:"tgl_sk"`
	NilaiTotal                 float64 `json:"nilai_total"`
	GradeAkreditasi            string  `json:"grade_akreditasi"`
	UrlSinta                   string  `json:"url_sinta"`
	IDIssuePenetapanAkreditasi string  `json:"id_issue_penetapan_akreditasi"`
	IssueTerakreditasi         string  `json:"issue_terakreditasi"`
	VolumeAwal                 int64   `json:"volume_awal"`
	VolumeAkhir                int64   `json:"volume_akhir"`
	NomorAwal                  int64   `json:"nomor_awal"`
	NomorAkhir                 int64   `json:"nomor_akhir"`
	TahunAwal                  int64   `json:"tahun_awal"`
	TahunAkhir                 int64   `json:"tahun_akhir"`
}

type UpdateSKJurnalDTO struct {
	IDPenetapan int64  `json:"id_penetapan" validate:"required"`
	UrlSinta    string `json:"url_sinta" validate:"required,url"`
	VolumeAwal  int64  `json:"volume_awal" validate:"required"`
	VolumeAkhir int64  `json:"volume_akhir" validate:"required"`
	NomorAwal   int64  `json:"nomor_awal" validate:"required"`
	NomorAkhir  int64  `json:"nomor_akhir" validate:"required"`
	TahunAwal   int64  `json:"tahun_awal" validate:"required"`
	TahunAkhir  int64  `json:"tahun_akhir" validate:"required"`
}

type PrintSKJurnal struct {
	PQRCode             string `json:"p_qr_code" gorm:"column:p_qr_code; <-:read"`
	PBarcode            string `json:"p_barcode"`
	NOSk                string `json:"no_sk"`
	JudulSK             string `json:"judul_sk"`
	NamaJurnal          string `json:"nama_jurnal"`
	Eissn               string `json:"eissn"`
	Publisher           string `json:"publisher"`
	GradeAkreditasi     string `json:"grade_akreditasi"`
	IssueTerakreditasi  string `json:"issue_terakreditasi"`
	TanggalPenetapan    string `json:"tanggal_penetapan"`
	NamaPenandaTanganSK string `json:"nama_penanda_tangan_sk"`
	NipPenandaTanganSK  string `json:"nip_penanda_tangan_sk"`
	TglSK               string `json:"tgl_sk"`
	Periode             string `json:"periode"`
	Tahun               int    `json:"tahun"`
	TtdImg              string `json:"ttd_img"`
	StatusSK            int    `json:"status_sk"`
}

type JurnalPic struct {
	IdUsulanAkreditasi       int64   `json:"id_usulan_akreditasi"`
	IdIdentitasJurnal        int64   `json:"id_identitas_jurnal"`
	IdPic                    int64   `json:"id_pic"`
	IdEic                    int64   `json:"id_eic"`
	NamaJurnal               string  `json:"nama_jurnal"`
	Eissn                    string  `json:"eissn"`
	Pissn                    string  `json:"pissn"`
	UrlStatistikPengunjung   string  `json:"url_statistik_pengunjung"`
	UrlJurnal                string  `json:"url_jurnal"`
	UrlContact               string  `json:"url_contact"`
	Editor                   string  `json:"editor"`
	AlamatOai                string  `json:"alamat_oai"`
	DoiJurnal                string  `json:"doi_jurnal"`
	TglDiusulkan             string  `json:"tgl_diusulkan"`
	NilaiTotalSa             float64 `json:"nilai_total_sa"`
	GradeAkreditasiSa        string  `json:"grade_akreditasi_sa"`
	NilaiTotalAkreditasi     float64 `json:"nilai_total_akreditasi"`
	GradeAkreditasi          string  `json:"grade_akreditasi"`
	TglPencatatanSk          string  `json:"tgl_pencatatan_sk"`
	StsHasilAkreditasi       string  `json:"sts_hasil_akreditasi"`
	HasilDeskEvaluasi        string  `json:"hasil_desk_evaluasi"`
	TglPenetapanDeskevaluasi string  `json:"tgl_penetapan_deskevaluasi"`
	KomentarPenolakan        string  `json:"komentar_penolakan"`
	JmlRecord                int     `json:"jml_record"`
}
