package entity

import "time"

type Menu struct {
	ID         int64     `json:"id"`
	Name       string    `json:"title"`
	Type       string    `json:"type"`
	Path       string    `json:"path"`
	Icon       string    `json:"icon,omitempty" gorm:"default:null"`
	IDParent   int64     `json:"id_parent,omitempty" gorm:"default:null"`
	Permission string    `json:"permission,omitempty" gorm:"-"`
	AB         string    `json:"ab,omitempty" gorm:"-"`
	Child      []Menu    `json:"children,omitempty" gorm:"-"`
	CreatedBy  int64     `json:"-" gorm:"<-:create,default:null"`
	UpdatedBy  int64     `json:"-" gorm:"<-:update,default:null"`
	CreatedAt  time.Time `json:"-" gorm:"<-:create,default:current_timestamp"`
	UpdatedAt  time.Time `json:"-" gorm:"<-:update,default:current_timestamp ON update current_timestamp"`
}
