package entity

import (
	"mime/multipart"
	"time"
)

type Berita struct {
	ID            int64     `json:"id" gorm:"default:null"`
	DateCreated   time.Time `json:"date_created"`
	Imagename     string    `json:"imagename" gorm:"default:null"`
	Kategori      string    `json:"kategori"`
	Lampiran1     string    `json:"lampiran1" gorm:"default:null"`
	Lampiran2     string    `json:"lampiran2" gorm:"default:null"`
	Lampiran3     string    `json:"lampiran3" gorm:"default:null"`
	Status        string    `json:"status,omitempty" gorm:"->"`
	MostViewed    string    `json:"most_viewed,omitempty" gorm:"->"`
	Title         string    `json:"title" gorm:"->"`
	Isi           string    `json:"isi" gorm:"->"`
	Lang          string    `json:"lang" gorm:"->"`
	IDLang        int64     `json:"id_lang,omitempty" gorm:"->"`
	PathImagename string    `json:"path_imagename" gorm:"->"`
	PathLampiran1 string    `json:"path_lampiran1" gorm:"->"`
	PathLampiran2 string    `json:"path_lampiran2" gorm:"->"`
	PathLampiran3 string    `json:"path_lampiran3" gorm:"->"`
}

type BeritaLang struct {
	IDLang   int64  `json:"id_lang" gorm:"default:null"`
	ID       int64  `json:"id"`
	Title    string `json:"title"`
	Snapshot string `json:"snapshot"`
	Isi      string `json:"isi"`
	Lang     string `json:"lang"`
}

type BeritaDTO struct {
	Image     *multipart.FileHeader `json:"image" form:"image"`
	Lampiran1 *multipart.FileHeader `json:"lampiran1" form:"lampiran1"`
	Lampiran2 *multipart.FileHeader `json:"lampiran2" form:"lampiran2"`
	Lampiran3 *multipart.FileHeader `json:"lampiran3" form:"lampiran3"`
	Title     string                `json:"title" form:"title" validate:"required"`
	Isi       string                `json:"isi" form:"isi" validate:"required"`
	Lang      string                `json:"lang" form:"lang" validate:"required"`
}

type BeritaStatusDTO struct {
	Status string `json:"status" form:"status" validate:"required"`
}
