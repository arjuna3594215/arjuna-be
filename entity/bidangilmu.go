package entity

type BidangIlmu struct {
	IDBidangIlmu   string `json:"id_bidang_ilmu"`
	KodeBidangIlmu string `json:"kode_bidang_ilmu"`
	BidangIlmu     string `json:"bidang_ilmu"`
	BidangIlmuEn   string `json:"bidang_ilmu_en"`
}
type BidangSubIlmu struct {
	IDBidangIlmu   string `json:"id_bidang_ilmu"`
	KodeBidangIlmu string `json:"kode_bidang_ilmu"`
	BidangIlmu     string `json:"bidang_ilmu"`
	BidangIlmuEn   string `json:"bidang_ilmu_en"`
}

type BidangIlmuInterest struct {
	IDPersonal   int64  `json:"id_personal"`
	IDBidangIlmu string `json:"id_bidang_ilmu"`
	BidangIlmu   string `json:"bidang_ilmu" gorm:"->"`
}

type BidangIlmuJurnal struct {
	IdJurnal           int64  `json:"id_jurnal"`
	IdIdentitasJurnal  int64  `json:"id_identitas_jurnal" gorm:"->"`
	IdBidangIlmuJurnal string `json:"id_bidang_ilmu_jurnal" gorm:"default:null"`
	IdBidangIlmu       string `json:"id_bidang_ilmu"`
	KodeBidangIlmu     string `json:"kode_bidang_ilmu" gorm:"->"`
	BidangIlmu         string `json:"bidang_ilmu" gorm:"->"`
	LevelTaksonomi     string `json:"level_taksonomi" gorm:"->"`
	IdBidangIlmuParent string `json:"id_bidang_ilmu_parent" gorm:"->"`
}

type BidangIlmuJurnalDTO struct {
	IdJurnal     int64  `json:"id_jurnal" validate:"required"`
	IdBidangIlmu string `json:"id_bidang_ilmu" validate:"required"`
}
