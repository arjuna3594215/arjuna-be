package entity

import "time"

type Config struct {
	IDConfigurasi              int64     `json:"id_configurasi" gorm:"default:null"`
	Durasi                     int64     `json:"durasi"`
	MinimalNilaiA              float64   `json:"minimal_nilai_a"`
	MinimalNilaiB              float64   `json:"minimal_nilai_b"`
	MasaTungguMinimal          int64     `json:"masa_tunggu_minimal"`
	MasaPengingat              int64     `json:"masa_pengingat"`
	BatasPerbedaanNilaiMgmt    float64   `json:"batas_perbedaan_nilai_mgmt"`
	BatasPerbedaanNilaiArtikel float64   `json:"batas_perbedaan_nilai_artikel"`
	JmlAsessorMgmt             int64     `json:"jml_asessor_mgmt"`
	JmlAsessorArtikel          int64     `json:"jml_asessor_artikel"`
	TglPendaftaranMulai        time.Time `json:"tgl_pendaftaran_mulai"`
	TglPendaftaranAkhir        time.Time `json:"tgl_pendaftaran_akhir"`
	TglCreated                 time.Time `json:"tgl_created"`
	JmlAsessorIssue            int64     `json:"jml_asessor_issue" gorm:"default:null"`
	IntervalMostpopular        int64     `json:"interval_mostpopular"`
	Periode                    string    `json:"periode" gorm:"default:null"`
	StsAktifConfigurasi        string    `json:"sts_aktif_configurasi" gorm:"->"`
}

type ConfigDTO struct {
	Durasi                     int64   `json:"durasi" validate:"required"`
	MinimalNilaiA              float64 `json:"minimal_nilai_a" validate:"required"`
	MinimalNilaiB              float64 `json:"minimal_nilai_b" validate:"required"`
	MasaTungguMinimal          int64   `json:"masa_tunggu_minimal" validate:"required"`
	TglPendaftaranMulai        string  `json:"tgl_pendaftaran_mulai" validate:"required"`
	TglPendaftaranAkhir        string  `json:"tgl_pendaftaran_akhir" validate:"required"`
	MasaPengingat              int64   `json:"masa_pengingat" validate:"required"`
	BatasPerbedaanNilaiMgmt    float64 `json:"batas_perbedaan_nilai_mgmt" validate:"required"`
	BatasPerbedaanNilaiArtikel float64 `json:"batas_perbedaan_nilai_artikel" validate:"required"`
	JmlAsessorMgmt             int64   `json:"jml_asessor_mgmt" validate:"required"`
	JmlAsessorArtikel          int64   `json:"jml_asessor_artikel" validate:"required"`
	JmlAsessorIssue            int64   `json:"jml_asessor_issue"`
	// TglCreated                 time.Time `json:"tgl_created"`
	// IntervalMostpopular        int64     `json:"interval_mostpopular"`
	// Periode                    string    `json:"periode"`
}
