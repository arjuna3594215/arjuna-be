package entity

type Pengguna struct {
	IDPengguna       int64  `json:"id_pengguna" gorm:"default:null"`
	IDPersonal       int64  `json:"id_personal" gorm:"default:null"`
	Username         string `json:"username"`
	Password         string `json:"password"`
	StsAktifPengguna string `json:"sts_aktif_pengguna"`
}
