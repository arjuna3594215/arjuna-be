package entity

type User struct {
	IDPengguna    int64  `json:"id_pengguna"`
	IDPersonal    int64  `json:"id_personal"`
	Nama          string `json:"nama"`
	Username      string `json:"username"`
	Kewenangan    string `json:"kewenangan,omitempty"`
	NamaInstitusi string `json:"nama_institusi,omitempty"`
	Email         string `json:"email,omitempty"`
	NOHandPhone   string `json:"no_hand_phone,omitempty"`
}

type UserDTO struct {
	Nama          string `json:"nama" validate:"required"`
	NamaInstitusi string `json:"nama_institusi" validate:"required"`
	Email         string `json:"email" validate:"required,email"`
	NOHandphone   string `json:"no_handphone" validate:"required,min=11"`
	Username      string `json:"username" validate:"required,min=5"`
	Password      string `json:"password" validate:"required,min=8"`
}

type UserUpdateDTO struct {
	Nama          string `json:"nama" validate:"required"`
	NamaInstitusi string `json:"nama_institusi" validate:"required"`
	Email         string `json:"email" validate:"required,email"`
	NOHandphone   string `json:"no_handphone" validate:"required,min=11"`
	// Username      string `json:"username" validate:"required,min=5"`
	Password string `json:"password"`
}

type KewenanganUserDTO struct {
	IDKewenangan               int64  `json:"id_kewenangan" validate:"required"`
	StsAktifKewenanganPengguna string `json:"sts_aktif_kewenangan_pengguna" validate:"required"`
}

type ListUserAssessor struct {
	IDPengguna    int64                `json:"id_pengguna"`
	IDPersonal    int64                `json:"id_personal"`
	Nama          string               `json:"nama"`
	Username      string               `json:"username"`
	NamaInstitusi string               `json:"nama_institusi"`
	Email         string               `json:"email"`
	NOHandPhone   string               `json:"no_hand_phone"`
	Kewenangan    string               `json:"kewenangan"`
	BidangIlmu    []BidangIlmuInterest `json:"bidang_ilmu" gorm:"foreignKey:IDPersonal;references:IDPersonal"`
}
