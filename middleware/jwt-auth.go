package middleware

import (
	"arjuna-api/helper"
	"arjuna-api/service"

	"github.com/gofiber/fiber/v2"
)

func AuthorizeJWT() fiber.Handler {
	return func(c *fiber.Ctx) error {
		const BEARER_SCHEMA = "Bearer "
		Header := c.GetReqHeaders()
		authHeader := Header["Authorization"]
		if authHeader == "" {
			response := helper.BuildErrorResponse("Failed to process request", "No Token Found", nil)
			return c.Status(fiber.StatusUnauthorized).JSON(response)
		}
		token_string := authHeader[len(BEARER_SCHEMA):]
		token, err := service.ValidateToken(token_string)
		if token.Valid {
			// claims := token.Claims.(jwt.MapClaims)
			// log.Println("Claims[user_id]: ", claims["id_user"])
			// log.Println("Claims[issuer]: ", claims["iss"])
			return c.Next()
		} else {
			response := helper.BuildErrorResponse("Token is not valid", err.Error(), nil)
			return c.Status(fiber.StatusUnauthorized).JSON(response)
		}
	}
}
